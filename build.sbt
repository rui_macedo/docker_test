
import sbt._
import sbt.Keys._

scalaVersion := "2.11.1"

ApplicationBuild.settings

lazy val root = (project in file(".")).enablePlugins(PlayJava)

credentials += Credentials("Sonatype Nexus Repository Manager", "pixnexus.cloudapp.net", "developer", "tH3hasTezat8ATu")

resolvers += "Sonatype Nexus Repository Manager" at "http://pixnexus.cloudapp.net:8081/nexus/content/repositories/releases/"

resolvers += "google-sedis-fix" at "http://pk11-scratch.googlecode.com/svn/trunk"

Keys.fork in Test := false

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  filters,
  javaWs,
  "org.reflections" % "reflections" % "0.9.8" notTransitive (),
  "com.nimbusds" % "nimbus-jose-jwt" % "2.26",
  "com.pixeon" % "cloud-security-services" % "4.4.0",
  "com.pixeon" % "lib-cloud-utils" % "2.2.0",
  "com.microsoft.sqlserver" % "sqljdbc4" % "4.0",
  "com.edulify" %% "play-hikaricp" % "2.1.0",
  "com.google.inject" % "guice" % "4.0-beta4",
  "org.json"%"org.json"%"chargebee-1.0",
  "net.sf.flexjson" % "flexjson" % "3.2",
  "org.apache.commons" % "commons-lang3" % "3.1"  ,
  "com.amazonaws" % "aws-java-sdk-sqs" % "1.10.14",
  "com.amazonaws" % "aws-java-sdk-s3" % "1.10.14",
  "com.typesafe.akka" % "akka-testkit_2.11" % "2.3.4",
  "redis.clients" % "jedis" % "2.8.1",
  "com.pixeon" % "cloud-test-module" % "1.2.0",
  "org.logback-extensions" % "logback-ext-loggly" % "0.1.4",
  "com.pixeon" % "lib-pixeon-play" % "2.1.0",
  "com.netflix.hystrix" % "hystrix-core" % "1.5.3",
  "redis.clients" % "jedis" % "2.8.1",
  "com.typesafe.play.plugins" %% "play-plugins-mailer" % "2.3.1",
  "com.github.spullara.mustache.java" % "mustache-maven-plugin" % "0.9.3"
)


resolvers += Resolver.url("Edulify Repository", url("https://edulify.github.io/modules/releases/"))(Resolver.ivyStylePatterns)

jacoco.settings

Keys.fork in Test := false

testOptions += Tests.Argument(TestFrameworks.JUnit, "-v", "-q", "-a")

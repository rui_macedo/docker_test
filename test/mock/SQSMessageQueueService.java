package mock;

import services.external.api.QueueService;

import com.amazonaws.services.sqs.model.Message;


public interface SQSMessageQueueService extends QueueService<Message> {

}

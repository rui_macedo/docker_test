package services.patient;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import dto.PatientDTO;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import play.Logger;
import services.external.api.ServiceDispatcher;
import services.external.impl.ServiceResponse;
import test.common.BaseWithServer;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class GetPatientsTest extends BaseWithServer {

    private ObjectMapper objectMapper;
    private ServiceDispatcher dispatcher;
    private Long instId;
    private String mockJsonPatients;

    @Before
    public void createScenario() {
        instId = 1L;
        this.dispatcher = Mockito.mock(ServiceDispatcher.class);
        this.objectMapper = new ObjectMapper();
        this.objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        try {
            mockJsonPatients = FileUtils.readFileToString(new File("test/test/resources/list_patientsDTO.json"));
        } catch (IOException e) {
            Logger.error("test/test/resources/list_patientsDTO.json not found");
        }
    }


    @Test
    public void testSuccess() throws IOException {
        Mockito.when(this.dispatcher.doGetWithQueryParam(Mockito.any(), Mockito.any())).thenReturn(Mockito.mock(ServiceResponse.class));
        Mockito.when(this.dispatcher.doGetWithQueryParam(Mockito.any(), Mockito.any()).getContent()).thenReturn(this.mockJsonPatients);

        Optional<List<PatientDTO>> patients = Optional.of(objectMapper.readValue(this.mockJsonPatients, new TypeReference<List<PatientDTO>>() {
        }));

        GetPatients getPatients = new GetPatients(this.dispatcher, this.instId, Sets.newHashSet());
        Optional<List<PatientDTO>> optional =  getPatients.execute();

        assertEquals(patients.get().size(),optional.get().size());
        assertEquals(patients.get().get(0).name, optional.get().get(0).name);
        assertTrue(getPatients.isSuccessfulExecution());
        assertFalse(getPatients.isResponseFromFallback());

    }

    @Test
    public void testFailure() {
        Mockito.when(this.dispatcher.doGetWithQueryParam(Mockito.any(), Mockito.any()))
                .thenReturn(Mockito.mock(ServiceResponse.class));
        Mockito.when(this.dispatcher.doGetWithQueryParam(Mockito.any(), Mockito.any()).getContent())
                .thenThrow(new RuntimeException("Caused runtimeException in GetPatientsTest"));

        GetPatients getPatients = new GetPatients(this.dispatcher, this.instId, Sets.newHashSet());
        Optional<List<PatientDTO>> optional =  getPatients.execute();

        assertEquals(Optional.empty(), optional);
        assertTrue(getPatients.isFailedExecution());
        assertTrue(getPatients.isResponseFromFallback());
    }


}
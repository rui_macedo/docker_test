package test.models;

import static org.fest.assertions.Assertions.assertThat;
import models.Address;
import models.Institution;

import org.junit.Before;
import org.junit.Test;

import play.libs.F.Option;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import models.Unity;
import test.common.BaseWithServer;

public class UnityTest extends BaseWithServer {

  public static Institution institution;

  @Before
  public void createInstitution() {
    institution = new Institution("Test", "Test", "Test", "Test");
    institution.save();
  }

  @Test
  public void createUnity() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Unidade", address, 111, "unityCode");
    unity.save();

    Unity existent = Unity.findById(unity.id);
    assertThat(existent).isNotNull();
    assertThat(existent.name).isEqualTo("Unidade");
  }

  @Test
  public void deleteUnity() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Unidade", address, 111, "unityCode");
    unity.save();

    Unity existent = Unity.findById(unity.id);
    assertThat(existent).isNotNull();

    existent.delete();
    existent = Unity.findById(unity.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updateUnity() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Unidade", address, 111, "unityCode");
    unity.save();

    Unity existent = Unity.findById(unity.id);
    assertThat(existent).isNotNull();

    existent.name = "UnidadeAtualizada";
    existent.addressNumber = 222;
    existent.update();

    existent = Unity.findById(unity.id);
    assertThat(existent).isNotNull();
    assertThat(existent.name).isEqualTo("UnidadeAtualizada");
    assertThat(existent.addressNumber).isEqualTo(222);
  }

  @Test
  public void successAtQueryBind() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Unidade", address, 111, "unityCode");
    unity.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("unity", ids);
    Option<Unity> unityOpt = unity.bind("", data);
    assertThat(unityOpt.get()).isEqualTo(unity);
  }

  @Test
  public void failAtQueryBind() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Unidade", address, 111, "unityCode");
    unity.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("unity", ids);
    Option<Unity> unityOpt = unity.bind("", data);
    assertThat(unityOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("unity", ids2);
    Option<Unity> unityOpt2 = unity.bind("", data2);
    assertThat(unityOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Unidade", address, 111, "unityCode");
    unity.save();

    Unity uni = unity.bind("", "1");
    assertThat(uni).isEqualTo(unity);
  }

  @Test
  public void failAtPathBind() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Unidade", address, 111, "unityCode");
    unity.save();

    Unity uni = unity.bind("", "opa");
    assertThat(uni).isNull();
  }


  @Test
  public void testFinder() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Unidade", address, 111, "unityCode1");
    unity.save();


    Address address2 = new Address();
    address2.save();
    Unity unity2 = new Unity(institution, "Unidade2", address2, 113, "unityCode2");
    unity2.save();

    Unity uni = Unity.findById(unity.id);
    assertThat(uni).isNotNull();

    Unity uniNull = Unity.findById(null);
    assertThat(uniNull).isNull();

    Unity uni213 = Unity.findById(213);
    assertThat(uni213).isNull();

    List<Unity> all = Unity.findAllByTenant(institution.id);
    assertThat(all).hasSize(2).contains(unity, unity2);

    unity.delete();
    all = Unity.findAllByTenant(institution.id);
    assertThat(all).hasSize(1);
  }
}

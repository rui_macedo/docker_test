package test.models;

import models.Institution;

import models.scheduling.Contact;
import org.junit.Test;

import test.common.BaseWithServer;

import static org.fest.assertions.Assertions.assertThat;

public class ContactTest extends BaseWithServer {

	@Test
	public void createContactTest() {
	    Institution ints = new Institution("TNT", "NUName", "NUName", "9209423423");
	    ints.save();
	    Contact contact = new Contact(ints.id, "Joana", "irmã da paciente", "1111-1111");
	    contact.save();

		Contact existingContact = Contact.findById(contact.id);
		assertThat(existingContact).isNotNull();
		assertThat(existingContact.name).isEqualTo(contact.name);
		assertThat(existingContact.notes).isEqualTo(contact.notes);
		assertThat(existingContact.institutionId).isEqualTo(contact.institutionId);
	}
	
	@Test
	public void deleteContactTest() {
	    Institution ints = new Institution("TNT", "NUName", "NUName", "9209423423");
	    ints.save();
	    Contact contact = new Contact(ints.id, "Joana", "irmã da paciente", "1111-1111");
	    contact.save();

		Contact existingContact = Contact.findById(contact.id);
		assertThat(existingContact).isNotNull();
		assertThat(existingContact.name).isEqualTo(contact.name);
		assertThat(existingContact.phone).isEqualTo(contact.phone);
		assertThat(existingContact.institutionId).isEqualTo(contact.institutionId);
		
		contact.delete();
		
		existingContact = Contact.findById(contact.id);
		assertThat(existingContact).isNull();
		
	}
	
	@Test
	public void updateContactTest() {
	    Institution ints = new Institution("TNT", "NUName", "NUName", "9209423423");
	    ints.save();
	    Contact contact = new Contact(ints.id, "Joana", "irmã da paciente", "1111-1111");
	    contact.save();
		Contact existingContact = Contact.findById(contact.id);
		assertThat(existingContact).isNotNull();
		assertThat(existingContact.name).isEqualTo(contact.name);
		assertThat(existingContact.institutionId).isEqualTo(contact.institutionId);
		
		existingContact.name = "Not Joana anymore";
		existingContact.notes = "Not contact syster";
		existingContact.phone = "2222-2222";
		existingContact.update();		
	
		assertThat(existingContact.name).isNotEqualTo("Joana");
		assertThat(existingContact.name).isEqualTo("Not Joana anymore");
		assertThat(existingContact.notes).isNotEqualTo("irmã da paciente");
		assertThat(existingContact.notes).isEqualTo("Not contact syster");
		assertThat(existingContact.phone).isNotEqualTo("1111-1111");
		assertThat(existingContact.phone).isEqualTo("2222-2222");
		
	}

}

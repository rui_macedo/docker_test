package test.models;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;

import java.security.GeneralSecurityException;

import models.Institution;
import models.Patient;
import models.ScheduledOrder;

import org.junit.Test;

import test.common.BaseWithServer;

public class ScheduledOrderTest extends BaseWithServer {

  @Test
  public void successConstructorTest() {

    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    Patient patient = new Patient(institution, "Patient", "ref01");
    patient.save();

    ScheduledOrder order;
    try {
      order = new ScheduledOrder(patient, institution);
      order.save();
      assertThat(order).isNotNull();
      assertThat(order.getId()).isNotNull();
    } catch (GeneralSecurityException e) {
      fail();
    }
  }

  @Test
  public void errorConstructorTest() {

    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();
    Institution institution2 = new Institution("THTT", "dedasaa", "dedasaea", "03934933943");
    institution2.save();

    Patient patient = new Patient(institution2, "Patient", "ref01");
    patient.save();

    ScheduledOrder order;
    try {
      order = new ScheduledOrder(patient, institution);
      fail("Cant create objects cross tenant!");
    } catch (Exception e) {
      assertThat(e).isInstanceOf(GeneralSecurityException.class);
    }
  }
}

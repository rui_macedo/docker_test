package test.models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Country;

import org.junit.Test;

import play.libs.F.Option;
import test.common.BaseWithServer;

public class CountryTest extends BaseWithServer {

  // util to test in another class
  public static Country createFakeCountry() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    return c;
  }

  @Test
  public void successAtCreateCountry() {
    Country c = new Country(1l, "Country", "CT");
    c.save();

    // reload address
    c = Country.findById(c.id);
    assertThat(c).isNotNull();
    assertThat(c.getId()).isNotNull();
    assertThat(c.originalId).isEqualTo(1l);
    assertThat(c.name).isEqualTo("Country");
  }

  @Test
  public void successAtQueryBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("country", ids);
    Option<Country> cOpt = c.bind("", data);
    assertThat(cOpt.get()).isEqualTo(c);
  }

  @Test
  public void failAtQueryBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("country", ids);
    Option<Country> cOpt = c.bind("", data);
    assertThat(cOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("country", ids2);
    Option<Country> cOpt2 = c.bind("", data2);
    assertThat(cOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();

    Country strict = c.bind("", "1");
    assertThat(strict).isEqualTo(c);
  }

  @Test
  public void failAtPathBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();

    Country strict = c.bind("", "d");
    assertThat(strict).isNull();
    strict = c.bind("", "34");
    assertThat(strict).isNull();
  }

  @Test
  public void testFinders() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    Country c2 = new Country(1l, "Country 2", "CT");
    c2.save();

    Country bdCountry = Country.findById(c.id);
    assertThat(bdCountry).isNotNull();

    bdCountry = Country.findById(null);
    assertThat(bdCountry).isNull();

    bdCountry = Country.findById(23);
    assertThat(bdCountry).isNull();

    List<Country> stricts = Country.findAll(Country.class);
    assertThat(stricts).isNotEmpty().hasSize(2).contains(c, c2);
  }
}

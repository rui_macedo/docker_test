package test.models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import java.util.HashMap;
import java.util.Map;

import models.Institution;
import models.Section;
import models.Unity;
import models.Address;

import org.junit.Before;
import org.junit.Test;

import play.libs.F.Option;

import test.common.BaseWithServer;

public class SectionTest extends BaseWithServer {
  public static Institution institution;

  @Before
  public void createInstitution() {
    institution = new Institution("Test", "Test", "Test", "Test");
    institution.save();
  }

  @Test
  public void createSection() {

    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Teste", address, 333, "unityCode");
    unity.save();
    Section section = new Section(unity, "Secao");
    section.save();

    Section existent = Section.findById(section.id);
    assertThat(existent).isNotNull();
    assertThat(existent.institution).isEqualTo(institution);
    assertThat(existent.name).isEqualTo("Secao");
  }

  @Test
  public void deleteSection() {

    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Teste", address, 333, "unityCode");
    unity.save();
    Section section = new Section(unity, "Secao");
    section.save();

    Section existent = Section.findById(section.id);
    assertThat(existent).isNotNull();
    existent.delete();

    existent = Section.findById(section.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updateSection() {

    Address address = new Address();
    address.save();
    Address address2 = new Address();
    address2.save();

    Unity unity = new Unity(institution, "Teste", address, 333, "unityCode1");
    unity.save();
    Unity unity2 = new Unity(institution, "Unidade", address2, 111, "unityCode2");
    unity2.save();

    Section section = new Section(unity, "Secao");
    section.save();

    Section existent = Section.findById(section.id);
    existent.name = "Secaozao";
    existent.unity = unity2;
    existent.update();

    existent = Section.findById(section.id);
    assertThat(existent).isNotNull();
    assertThat(existent.id).isEqualTo(section.id);
    assertThat(existent.name).isEqualTo("Secaozao");
    assertThat(existent.unity).isEqualTo(unity2);
  }

  @Test
  public void successAtQueryBind() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Teste", address, 333, "unityCode");
    unity.save();
    Section section = new Section(unity, "Secao");
    section.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("section", ids);
    Option<Section> sectionOpt = section.bind("", data);
    assertThat(sectionOpt.get()).isEqualTo(section);
  }

  @Test
  public void failAtQueryBind() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Teste", address, 333, "unityCode");
    unity.save();
    Section section = new Section(unity, "Secao");
    section.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("section", ids);
    Option<Section> sectionOpt = section.bind("", data);
    assertThat(sectionOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("section", ids2);
    Option<Section> sectionOpt2 = section.bind("", data2);
    assertThat(sectionOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Teste", address, 333, "unityCode");
    unity.save();
    Section section = new Section(unity, "Secao");
    section.save();

    Section sec = section.bind("", "1");
    assertThat(sec).isEqualTo(section);
  }

  @Test
  public void failAtPathBind() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Teste", address, 333, "unityCode");
    unity.save();
    Section section = new Section(unity, "Secao");
    section.save();

    Section sec = section.bind("", "p");
    assertThat(sec).isNull();
  }

  @Test
  public void testFinder() {
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Teste", address, 333, "unityCode1");
    unity.save();
    Section section = new Section(unity, "Secao");
    section.save();

    Section section2 = new Section(unity, "Secao2");
    section2.save();

    Unity unity2 = new Unity(institution, "Teste2", address, 333, "unityCode2");
    unity2.save();

    Section section3 = new Section(unity2, "Secao3");
    section3.save();

    Section sec = Section.findById(section.id);
    assertThat(sec).isNotNull();

    Section secNull = Section.findById(null);
    assertThat(secNull).isNull();

    Section sec100 = Section.findById(100);
    assertThat(sec100).isNull();
    
    List<Section> all = Section.findAllByTenant(institution.id);
    assertThat(all).hasSize(3).contains(section,section2,section3);
    
    List<Section> allUnity = Section.findAllByUnity(unity);
    assertThat(allUnity).hasSize(2).contains(section,section2);
  }
}

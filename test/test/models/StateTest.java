package test.models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Country;
import models.State;

import org.junit.Test;

import play.libs.F.Option;
import test.common.BaseWithServer;

public class StateTest extends BaseWithServer {


  // util to test in another class
  public static State createFakeState() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    return st;
  }


  @Test
  public void successAtCreateState() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(23l, "State", c);
    st.save();

    // reload address
    st = State.findById(st.id);
    assertThat(st).isNotNull();
    assertThat(st.getId()).isNotNull();
    assertThat(st.originalId).isEqualTo(23l);
    assertThat(st.name).isEqualTo("State");
    assertThat(st.country).isEqualTo(c);
  }

  @Test
  public void successAtQueryBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("state", ids);
    Option<State> stOpt = st.bind("", data);
    assertThat(stOpt.get()).isEqualTo(st);
  }

  @Test
  public void failAtQueryBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("state", ids);
    Option<State> stOpt = st.bind("", data);
    assertThat(stOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("state", ids2);
    Option<State> stOpt2 = st.bind("", data2);
    assertThat(stOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();

    State strict = st.bind("", "1");
    assertThat(strict).isEqualTo(st);
  }

  @Test
  public void failAtPathBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();

    State strict = st.bind("", "d");
    assertThat(strict).isNull();
    strict = st.bind("", "34");
    assertThat(strict).isNull();
  }

  @Test
  public void testFinders() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();

    State st2 = new State(2l, "State2", c);
    st2.save();



    State bdDist = State.findById(st.id);
    assertThat(bdDist).isNotNull();

    bdDist = State.findById(null);
    assertThat(bdDist).isNull();

    bdDist = State.findById(23);
    assertThat(bdDist).isNull();

    List<State> stricts = State.findAllByCountry(c);
    assertThat(stricts).isNotEmpty().hasSize(2).contains(st, st2);

    List<State> strictsNull = State.findAllByCountry(null);
    assertThat(strictsNull).isEmpty();
  }
}

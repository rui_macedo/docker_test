package test.models.searchalg.finders;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;

import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

import models.Equipment;
import models.EquipmentResource;
import models.ExamProcedure;
import models.ExamProcedureTUSS;
import models.Institution;
import models.Patient;
import models.Resource;
import models.Room;
import models.ScheduledOrder;
import models.ScheduledProcedure;
import models.SchedulingBook;
import models.SchedulingBookPeriod;
import models.Section;
import models.searchalg.DynamicSlotGroup;
import models.searchalg.finders.ScheduledSlotsByResourceFinderStrategy;

import org.joda.time.DateTime;
import org.junit.Test;

import play.libs.F.Promise;
import test.common.BaseWithServer;
import common.constants.CalendarDay;
import common.constants.DayShift;
import forms.FormSchedulingProcedure;

public class ScheduledSlotsByResourceFinderStrategyTest extends BaseWithServer {
  @Test
  public void successAtRetrieveList(){
    
    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure ep = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    ep.save();

    SchedulingBook book = new SchedulingBook(institution, "Book 1", 30);
    book.save();

    book.addSchedulingBookPeriod(32400000, 43200000, CalendarDay.MON);

    Section section = new Section(institution, "Section");
    section.save();
    Room room = new Room(institution, "Sala 1");
    room.section = section;
    room.save();
    Equipment equip = new Equipment(institution, "E", "p", "e", section, room);
    equip.save();

    Resource resource = new EquipmentResource(equip, institution);
    resource.save();

    resource.addResourceWeekAgenda(CalendarDay.MON, 32400000, 43200000);

    SchedulingBookPeriod period =
        SchedulingBookPeriod.findPeriodByBook(book, 32400000, 43200000, CalendarDay.MON);
    //period.addResource(resource);
    
    ScheduledOrder order;
    Patient patient = new Patient(institution, "Patient", "ref01");
    patient.save();
    DateTime date = new DateTime(2015, 02, 02, 12, 22);
    try {
      order = new ScheduledOrder(patient, institution);
      order.save();
      FormSchedulingProcedure form = new FormSchedulingProcedure();
      form.book = book;
      form.examProcedure = ep;
      form.referenceDate = date.getMillis();
      form.resources = Collections.nCopies(1, resource);

      // 7:20 - 7:40h Scheduled
      form.startAt = 26400000;
      form.endAt = 27600000;
      ScheduledProcedure.createAndSaveAScheduling(form, order);

      // 8:20 - 8:40h Scheduled
      form.startAt = 30000000;
      form.endAt = 31200000;
      ScheduledProcedure.createAndSaveAScheduling(form, order);

      // 9h - 9:20h Scheduled
      form.startAt = 32400000;
      form.endAt = 33600000;

      ScheduledProcedure.createAndSaveAScheduling(form, order);
      
    } catch (GeneralSecurityException e) {
      fail();
    }
    ScheduledSlotsByResourceFinderStrategy finder = new ScheduledSlotsByResourceFinderStrategy(resource);
    List<DayShift> periods = Collections.emptyList();
    List<Promise<DynamicSlotGroup>> listOfPromisses = finder.getPromiseListOfDynamicSlotGroup(date, periods);
    assertThat(listOfPromisses).hasSize(1);
    Promise<DynamicSlotGroup> promise = listOfPromisses.get(0);
    DynamicSlotGroup d = promise.get(2000l);
    assertThat(d.getExams()).hasSize(3);
  }
}

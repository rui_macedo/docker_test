package test.models.searchalg.dayiterator;

import static org.fest.assertions.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import models.searchalg.dayiterator.DayIteratorStrategy;
import models.searchalg.dayiterator.PlusSpecificWeekDays;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.junit.Test;

import common.constants.CalendarDay;
import test.common.BaseWithServer;

public class PlusSpecificWeekDaysTest extends BaseWithServer{
  
  @Test
  public void successPlusSpecificWeekDays(){
    DateTime now = new DateTime(2015,2,4,9,0);//WED
    List<CalendarDay> calendarDays = new ArrayList<CalendarDay>();
    calendarDays.add(CalendarDay.MON);
    calendarDays.add(CalendarDay.FRI);
    DayIteratorStrategy it = new PlusSpecificWeekDays(calendarDays, now, 15);
    //verifies if datereference is next friday
    assertThat(Days.daysBetween(it.getCurrentIteratonDay(), new DateTime(2015,02,06,2,2)).getDays()).isEqualTo(0);
    
    DateTime firstMond = it.next();
    assertThat(Days.daysBetween(firstMond, new DateTime(2015,02,9,2,2)).getDays()).isEqualTo(0);
    
    DateTime secondFri = it.next();
    assertThat(Days.daysBetween(secondFri, new DateTime(2015,02,13,2,2)).getDays()).isEqualTo(0);
    assertThat(it.hasNext()).isTrue();

    DateTime secondMon = it.next();
    assertThat(Days.daysBetween(secondMon, new DateTime(2015,02,16,2,2)).getDays()).isEqualTo(0);
    assertThat(it.hasNext()).isTrue();
    
    DateTime thirdFri = it.next();
    assertThat(Days.daysBetween(thirdFri, new DateTime(2015,02,20,2,2)).getDays()).isEqualTo(0);
    assertThat(it.hasNext()).isFalse();
  }
  
  @Test
  public void successAtPlusUntilEndDate(){
    DateTime now = new DateTime(2015,2,4,9,0);//WED
    List<CalendarDay> calendarDays = new ArrayList<CalendarDay>();
    calendarDays.add(CalendarDay.WED);
    calendarDays.add(CalendarDay.FRI);

    DayIteratorStrategy it = new PlusSpecificWeekDays(calendarDays, now, 7);
    //verifies if datereference is today
    assertThat(Days.daysBetween(it.getCurrentIteratonDay(), now).getDays()).isEqualTo(0);

    DateTime firstFri = it.next();
    assertThat(Days.daysBetween(firstFri, new DateTime(2015,02,6,2,2)).getDays()).isEqualTo(0);
    assertThat(it.hasNext()).isTrue();
    
    DateTime secondMon = it.next();
    assertThat(Days.daysBetween(secondMon, new DateTime(2015,02,11,2,2)).getDays()).isEqualTo(0);
    assertThat(it.hasNext()).isFalse();
    
  }
  
}

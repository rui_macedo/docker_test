package test.models;

import static org.fest.assertions.Assertions.assertThat;
import models.Institution;
import models.Patient;
import test.common.BaseWithServer;

import org.junit.Test;

public class PatientTest extends BaseWithServer {

  @Test
  public void createPatient() {
    Institution ints = new Institution("TNT", "NUName", "NUName", "9209423423");
    ints.save();
    Patient patient = new Patient(ints, "Patient", "ref01");
    patient.name = "Jaime";
    patient.save();

    Patient existentPatient = Patient.finder.byId(patient.id);
    assertThat(existentPatient).isNotNull();
    assertThat(existentPatient.name).isEqualTo("Jaime");
  }

  @Test
  public void deletePatient() {
    Institution ints = new Institution("TNT", "NUName", "NUName", "9209423423");
    ints.save();
    Patient patient = new Patient(ints, "Patient", "ref01");
    patient.name = "Jaime";
    patient.save();

    Patient existentPatient = Patient.finder.byId(patient.id);
    assertThat(existentPatient).isNotNull();

    existentPatient.delete();
    existentPatient = Patient.finder.byId(patient.id);

    assertThat(existentPatient).isNull();
  }

  @Test
  public void updatePatient() {
    Institution ints = new Institution("TNT", "NUName", "NUName", "9209423423");
    ints.save();
    Patient patient = new Patient(ints, "Patient", "ref01");
    patient.name = "Jaime";
    patient.save();

    Patient existentPatient = Patient.finder.byId(patient.id);
    assertThat(existentPatient).isNotNull();
    
    existentPatient.name = "Jaimao";
    existentPatient.sex = "M";
    existentPatient.update();
    
    existentPatient = Patient.finder.byId(patient.id);
    assertThat(existentPatient).isNotNull();
    assertThat(existentPatient.name).isEqualTo("Jaimao");
    assertThat(existentPatient.sex).isEqualTo("M");
  }
}

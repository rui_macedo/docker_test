package test.models;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import models.Equipment;
import models.EquipmentResource;
import models.ExamProcedure;
import models.ExamProcedureTUSS;
import models.Institution;
import models.Patient;
import models.Resource;
import models.Room;
import models.ScheduledOrder;
import models.ScheduledProcedure;
import models.SchedulingBook;
import models.SchedulingBookPeriod;
import services.searchers.SchedulingSearchProvider;
import models.Section;
import models.Unity;
import models.searchalg.DynamicSlot;
import models.searchalg.DynamicSlotGroup;
import models.searchalg.ScheduleDailyCard;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Ignore;
import org.junit.Test;

import test.common.BaseWithServer;
import common.constants.CalendarDay;
import common.constants.DayShift;
import forms.FormSchedulingProcedure;

public class SchedulingSearchProviderTest extends BaseWithServer {

  @Test
  public void sucessAtGetSlotsBySchedulingBookPeriod() {
    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure ep = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    ep.save();

    SchedulingBook book = new SchedulingBook(institution, "Book 1", 30);
    book.save();

    book.addSchedulingBookPeriod(32400000, 43200000, CalendarDay.MON);

    Section section = new Section(institution, "Section");
    section.save();
    Room room = new Room(institution, "Sala 1");
    room.section = section;
    room.save();
    Equipment equip = new Equipment(institution, "E", "p", "e", section, room);
    equip.save();

    Resource resource = new EquipmentResource(equip, institution);
    resource.save();

    resource.addResourceWeekAgenda(CalendarDay.MON, 32400000, 43200000);

    SchedulingBookPeriod period = SchedulingBookPeriod.findPeriodByBook(book, 32400000, 43200000, CalendarDay.MON);

    //period.addResource(resource);

    DateTime dt = new DateTime(2015, 1, 5, 0, 0);// Monday
    List<DynamicSlot> dynamicSlots = DynamicSlot.convertList(period.getAvaibleIntervalSlotsInADay(new Interval(dt.withTimeAtStartOfDay(), dt), Collections.EMPTY_LIST), period, ep);

    assertThat(dynamicSlots).hasSize(6);
  }

  @Ignore
  @Test
  public void sucessAtGetSlotsBySchedulingBookPeriodWithOverlapedInterval() {
    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure ep = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    ep.save();

    SchedulingBook book = new SchedulingBook(institution, "Book 1", 30);
    book.save();

    book.addSchedulingBookPeriod(32400000, 43200000, CalendarDay.MON); // 9h - 12h

    Section section = new Section(institution, "Section");
    section.save();
    Room room = new Room(institution, "Sala 1");
    room.section = section;
    room.save();
    Equipment equipment = new Equipment(institution, "E", "p", "e", section, room);
    equipment.save();

    Resource resource = new EquipmentResource(equipment, institution);
    resource.save();
    resource.addResourceWeekAgenda(CalendarDay.MON, 29340000, 37980000);// 8:09 - 10:33

    SchedulingBookPeriod period = SchedulingBookPeriod.findPeriodByBook(book, 32400000, 43200000, CalendarDay.MON);

    //period.addResource(resource);

    DateTime dt = new DateTime(2015, 1, 5, 0, 0);// Monday
    List<DynamicSlot> dynamicSlots = DynamicSlot.convertList(period.getAvaibleIntervalSlotsInADay(new Interval(dt.withTimeAtStartOfDay(), dt), Collections.EMPTY_LIST), period, ep);
    assertThat(dynamicSlots).hasSize(3);
  }

  @Test
  public void successAtSearchGoodSetsOfSlotByExamProcedures() {
    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    Unity unity = new Unity(institution, "Unity", AddressTest.createFakeAddress(), 234, "unityCode");
    unity.save();

    /*
     * ExamCategory examCategory1 = new ExamCategory(institution, "cat"); examCategory1.save();
     * 
     * ExamCategory examCategory2 = new ExamCategory(institution, "cat2"); examCategory2.save();
     */

    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure exam1Category1 = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    exam1Category1.save();

    ExamProcedure exam2Category1 = new ExamProcedure(institution, "Proc 2", "0304", examProcedureTUSS);
    exam1Category1.save();

    ExamProcedure exam1Category2 = new ExamProcedure(institution, "Proc 22", "0305", examProcedureTUSS);
    exam1Category2.save();


    SchedulingBook bookCategory1 = new SchedulingBook(institution, "Book Category 1", 30);
    bookCategory1.unity = unity;
    bookCategory1.save();

    SchedulingBook bookCategory2 = new SchedulingBook(institution, "Book Category 2", 20);
    bookCategory2.unity = unity;
    bookCategory2.save();

    // configuring book1
    bookCategory1.addSchedulingBookPeriod(28800000, 43200000, CalendarDay.MON); // 8h - 12h

    Section section = new Section(institution, "Section");
    section.save();
    Room room = new Room(institution, "Sala 1");
    room.section = section;
    room.save();
    Equipment equip = new Equipment(institution, "E", "p", "e", section, room);
    equip.save();

    Resource resource = new EquipmentResource(equip, institution);
    resource.save();
    resource.addResourceWeekAgenda(CalendarDay.MON, 28800000, 43200000);// 8:00 - 12h

    SchedulingBookPeriod period = SchedulingBookPeriod.findPeriodByBook(bookCategory1, 28800000, 43200000, CalendarDay.MON);

    //period.addResource(resource);

    // configuring book2
    bookCategory2.addSchedulingBookPeriod(22800000, 44400000, CalendarDay.MON); // 6:20h - 12:20h

    Equipment equip2 = new Equipment(institution, "E", "p", "e", section, room);
    equip2.save();

    Resource resource2 = new EquipmentResource(equip2, institution);
    resource2.save();
    resource2.addResourceWeekAgenda(CalendarDay.MON, 22800000, 44400000);// 6:20 - 12:20

    SchedulingBookPeriod period2 = SchedulingBookPeriod.findPeriodByBook(bookCategory2, 22800000, 44400000, CalendarDay.MON);

    //period2.addResource(resource2);

    // Search parameters
    List<ExamProcedure> examProceduresToSearch = new ArrayList<ExamProcedure>();
    examProceduresToSearch.add(exam1Category1);
    examProceduresToSearch.add(exam1Category2);
    examProceduresToSearch.add(exam2Category1);
    DateTime dateTimeTosearch = new DateTime(2015, 1, 19, 00, 00);// Monday

    List<DayShift> periods = Collections.emptyList();
    List<CalendarDay> calendarDays = Collections.emptyList();

    List<ScheduleDailyCard> dailyCards =
        SchedulingSearchProvider.searchGoodSetsOfSlotByExamProcedures(examProceduresToSearch, dateTimeTosearch, dateTimeTosearch.plusDays(15), periods, calendarDays, org.fest.util.Collections.list(unity), institution, 3, 7200000l /*
                                                                                                                                                                                                                                       * 2
                                                                                                                                                                                                                                       * h
                                                                                                                                                                                                                                       */);

    assertThat(dailyCards).isNotNull().isNotEmpty().hasSize(1);
    ScheduleDailyCard card = dailyCards.get(0);
    assertThat(card.getDayOfCalendar()).isEqualTo(CalendarDay.MON);

    List<ExamProcedure> cardExamProcedures = card.getExamProcedures();
    assertThat(cardExamProcedures).isNotNull().isNotEmpty().hasSize(examProceduresToSearch.size()).contains(examProceduresToSearch.toArray());

    List<DynamicSlotGroup> slotGroups = card.getSlotsOfDay();
    assertThat(slotGroups).isNotNull().isNotEmpty();

    for (DynamicSlotGroup slotGroup : slotGroups) {
      List<DynamicSlot> slots = slotGroup.getExams();
      assertThat(slots).isNotEmpty().hasSize(examProceduresToSearch.size());
    }

  }

  @Ignore
  @Test
  public void successAtSearchGoodSetsOfSlotByExamProceduresWithSchedulings() {
    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    Unity unity = new Unity(institution, "Unity", AddressTest.createFakeAddress(), 234, "unityCode");
    unity.save();

    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure exam1Category1 = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    exam1Category1.save();

    ExamProcedure exam2Category1 = new ExamProcedure(institution, "Proc 2", "0304", examProcedureTUSS);
    exam1Category1.save();

    ExamProcedure exam1Category2 = new ExamProcedure(institution, "Proc 22", "0305", examProcedureTUSS);
    exam1Category2.save();


    SchedulingBook bookCategory1 = new SchedulingBook(institution, "Book Category 1", 30);
    bookCategory1.unity = unity;
    bookCategory1.save();

    SchedulingBook bookCategory2 = new SchedulingBook(institution, "Book Category 2", 20);
    bookCategory2.unity = unity;
    bookCategory2.save();

    // configuring book1
    bookCategory1.addSchedulingBookPeriod(28800000, 32400000, CalendarDay.MON); // 8h - 9h

    Section section = new Section(institution, "Section");
    section.save();
    Room room = new Room(institution, "Sala 1");
    room.section = section;
    room.save();
    Equipment equip = new Equipment(institution, "E", "p", "e", section, room);
    equip.save();

    Resource resource = new EquipmentResource(equip, institution);
    resource.save();
    resource.addResourceWeekAgenda(CalendarDay.MON, 28800000, 32400000);// 8:00 - 9h

    SchedulingBookPeriod period = SchedulingBookPeriod.findPeriodByBook(bookCategory1, 28800000, 32400000, CalendarDay.MON);

    //period.addResource(resource);

    // configuring book2
    bookCategory2.addSchedulingBookPeriod(26400000, 34800000, CalendarDay.MON); // 7:20h - 9:40h

    Equipment equip2 = new Equipment(institution, "E", "p", "e", section, room);
    equip2.save();

    Resource resource2 = new EquipmentResource(equip2, institution);
    resource2.save();
    resource2.addResourceWeekAgenda(CalendarDay.MON, 26400000, 34800000); // 7:20h - 9:40h

    SchedulingBookPeriod period2 = SchedulingBookPeriod.findPeriodByBook(bookCategory2, 26400000, 34800000, CalendarDay.MON);

    //period2.addResource(resource2);


    DateTime dateTimeTosearch = new DateTime(2015, 1, 19, 00, 00);// Monday

    // scheduling a few intervals to increase the difficulty
    Patient patient = new Patient(institution, "Patient", "ref01");
    patient.save();
    ScheduledOrder order;
    try {
      order = new ScheduledOrder(patient, institution);
      order.save();
      FormSchedulingProcedure form = new FormSchedulingProcedure();
      form.book = bookCategory2;
      form.examProcedure = exam1Category2;
      form.referenceDate = dateTimeTosearch.withTimeAtStartOfDay().getMillis();
      form.resources = Collections.nCopies(1, resource2);

      // 7:20 - 7:40h Scheduled
      form.startAt = 26400000;
      form.endAt = 27600000;
      ScheduledProcedure.createAndSaveAScheduling(form, order);

      // 8:20 - 8:40h Scheduled
      form.startAt = 30000000;
      form.endAt = 31200000;
      ScheduledProcedure.createAndSaveAScheduling(form, order);

      // 9h - 9:20h Scheduled
      form.startAt = 32400000;
      form.endAt = 33600000;

      ScheduledProcedure.createAndSaveAScheduling(form, order);
    } catch (GeneralSecurityException e) {
      fail();
    }


    // Search parameters
    List<ExamProcedure> examProceduresToSearch = new ArrayList<ExamProcedure>();
    examProceduresToSearch.add(exam1Category1);
    examProceduresToSearch.add(exam1Category2);
    examProceduresToSearch.add(exam2Category1);

    // Search

    List<DayShift> periods = Collections.emptyList();
    List<CalendarDay> calendarDays = Collections.emptyList();
    List<ScheduleDailyCard> dailyCards =
        SchedulingSearchProvider.searchGoodSetsOfSlotByExamProcedures(examProceduresToSearch, dateTimeTosearch, dateTimeTosearch.plusDays(15), periods, calendarDays, org.fest.util.Collections.list(unity), institution, 2, 7200000l);


    assertThat(dailyCards).isNotNull().isNotEmpty().hasSize(1);

    ScheduleDailyCard card = dailyCards.get(0);

    assertThat(card.getDayOfCalendar()).isEqualTo(CalendarDay.MON);

    List<ExamProcedure> cardExamProcedures = card.getExamProcedures();

    assertThat(cardExamProcedures).isNotNull().isNotEmpty().hasSize(examProceduresToSearch.size()).contains(examProceduresToSearch.toArray());

    List<DynamicSlotGroup> slotGroups = card.getSlotsOfDay();
    assertThat(slotGroups).isNotNull().isNotEmpty().hasSize(2);


    // Intervals to compare (valid ones)
    Interval i7h40mTo8h = new Interval(27600000l, 28800000);// 7:40 - 8h
    Interval i8hTo8h30min = new Interval(28800000, 30600000);// 8h - 8:30
    Interval i8h30minTo9h = new Interval(30600000, 32400000);// 8:30 - 9h
    Interval i8h40minTo9h = new Interval(31200000, 32400000);// 8:40 - 9:00
    Interval i9h20minTo9h40min = new Interval(33600000, 34800000);// 9:20 - 9:40

    for (DynamicSlotGroup slotGroup : slotGroups) {
      List<DynamicSlot> slots = slotGroup.getExams();
      assertThat(slots).isNotEmpty().hasSize(examProceduresToSearch.size());
      for (DynamicSlot slot : slots) {
        Interval slotInterval = slot.getInterval();
        if (!slotInterval.equals(i7h40mTo8h) && !slotInterval.equals(i8hTo8h30min) && !slotInterval.equals(i8h40minTo9h) && !slotInterval.equals(i8h30minTo9h) && !slotInterval.equals(i9h20minTo9h40min)) {
          fail(" Slots should be in this periods. Slot tested: " + slotInterval);
        }
      }
    }
  }


  @Test
  public void successAtSearchScheduledSlotInWeekByResource() {
    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure ep = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    ep.save();

    SchedulingBook book = new SchedulingBook(institution, "Book 1", 30);
    book.save();

    book.addSchedulingBookPeriod(32400000, 43200000, CalendarDay.MON);

    Section section = new Section(institution, "Section");
    section.save();
    Room room = new Room(institution, "Sala 1");
    room.section = section;
    room.save();
    Equipment equip = new Equipment(institution, "E", "p", "e", section, room);
    equip.save();

    Resource resource = new EquipmentResource(equip, institution);
    resource.save();

    resource.addResourceWeekAgenda(CalendarDay.MON, 32400000, 43200000);

    SchedulingBookPeriod period = SchedulingBookPeriod.findPeriodByBook(book, 32400000, 43200000, CalendarDay.MON);
    //period.addResource(resource);

    ScheduledOrder order;
    Patient patient = new Patient(institution, "Patient", "ref01");
    patient.save();
    DateTime date = new DateTime(2015, 02, 02, 12, 22);
    try {
      order = new ScheduledOrder(patient, institution);
      order.save();
      FormSchedulingProcedure form = new FormSchedulingProcedure();
      form.book = book;
      form.examProcedure = ep;
      form.referenceDate = date.getMillis();
      form.resources = Collections.nCopies(1, resource);

      /*
       * Schedulings in fev 02 = 2 schedulings
       */
      // 7:20 - 7:40h Scheduled
      form.startAt = 26400000;
      form.endAt = 27600000;
      ScheduledProcedure.createAndSaveAScheduling(form, order);

      // 8:20 - 8:40h Scheduled
      form.startAt = 30000000;
      form.endAt = 31200000;
      ScheduledProcedure.createAndSaveAScheduling(form, order);
      /*
       * Schedulings in fev 03 = 1 schedulings
       */
      form.referenceDate = new DateTime(2015, 02, 03, 12, 22).getMillis();
      // 9h - 9:20h Scheduled
      form.startAt = 32400000;
      form.endAt = 33600000;
      ScheduledProcedure.createAndSaveAScheduling(form, order);
      /*
       * Schedulings in fev 04 = 1 schedulings
       */
      form.referenceDate = new DateTime(2015, 02, 04, 12, 22).getMillis();
      // 9h - 9:20h Scheduled
      form.startAt = 32400000;
      form.endAt = 33600000;
      ScheduledProcedure.createAndSaveAScheduling(form, order);
      /*
       * Schedulings in fev 05 = 1 schedulings
       */
      form.referenceDate = new DateTime(2015, 02, 05, 12, 22).getMillis();
      // 9h - 9:20h Scheduled
      form.startAt = 32400000;
      form.endAt = 33600000;
      ScheduledProcedure.createAndSaveAScheduling(form, order);
      /*
       * Schedulings in fev 06 = 2 schedulings
       */
      form.referenceDate = new DateTime(2015, 02, 06, 12, 22).getMillis();
      // 9h - 9:20h Scheduled
      form.startAt = 32400000;
      form.endAt = 33600000;

      ScheduledProcedure.createAndSaveAScheduling(form, order);
      form.referenceDate = new DateTime(2015, 02, 06, 12, 22).getMillis();
      // 7:20 - 7:40h Scheduled
      form.startAt = 26400000;
      form.endAt = 27600000;
      ScheduledProcedure.createAndSaveAScheduling(form, order);

    } catch (GeneralSecurityException e) {
      fail();
    }
    List<ScheduleDailyCard> searchScheduledSlotInWeekByResource = SchedulingSearchProvider.searchScheduledSlotInWeekByResource(resource, date);
    assertThat(searchScheduledSlotInWeekByResource).hasSize(7);

    // sunday
    ScheduleDailyCard sunday = searchScheduledSlotInWeekByResource.get(0);
    assertThat(sunday.getSize()).isEqualTo(0);
    // monday
    ScheduleDailyCard monday = searchScheduledSlotInWeekByResource.get(1);
    assertThat(monday.getSize()).isEqualTo(2);
    // tuesday
    ScheduleDailyCard tuesday = searchScheduledSlotInWeekByResource.get(2);
    assertThat(tuesday.getSize()).isEqualTo(1);
    // wednesday
    ScheduleDailyCard wednesday = searchScheduledSlotInWeekByResource.get(3);
    assertThat(wednesday.getSize()).isEqualTo(1);
    // thursday
    ScheduleDailyCard thursday = searchScheduledSlotInWeekByResource.get(4);
    assertThat(thursday.getSize()).isEqualTo(1);
    // friday
    ScheduleDailyCard friday = searchScheduledSlotInWeekByResource.get(5);
    assertThat(friday.getSize()).isEqualTo(2);
    // saturday
    ScheduleDailyCard saturday = searchScheduledSlotInWeekByResource.get(6);
    assertThat(saturday.getSize()).isEqualTo(0);
  }

}

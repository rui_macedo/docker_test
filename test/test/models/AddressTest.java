package test.models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Address;
import models.City;
import models.Country;
import models.District;
import models.State;

import org.junit.Test;

import play.libs.F.Option;
import test.common.BaseWithServer;

public class AddressTest extends BaseWithServer {
  
  //helper to test
  
  public static Address createFakeAddress(){
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();
    Address add = new Address(4l, "Address my address", "Avenida", "00300300", dist);
    add.save();
    return add;
  }
  
  

  @Test
  public void successAtCreateAddress() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();
    Address add = new Address(4l, "Address my address", "Avenida", "00300300", dist);
    add.save();

    // reload address
    add = Address.findById(add.id);
    assertThat(add).isNotNull();
    assertThat(add.getId()).isNotNull();
    assertThat(add.originalId).isEqualTo(4l);
    assertThat(add.name).isEqualTo("Address my address");
    assertThat(add.type).isEqualTo("Avenida");
    assertThat(add.postalCode).isEqualTo("00300300");
    assertThat(add.district).isEqualTo(dist);
  }

  @Test
  public void successAtQueryBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();
    Address add = new Address(4l, "Address my address", "Avenida", "00300300", dist);
    add.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("address", ids);
    Option<Address> addressOpt = add.bind("", data);
    assertThat(addressOpt.get()).isEqualTo(add);
  }

  @Test
  public void failAtQueryBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();
    Address add = new Address(4l, "Address my address", "Avenida", "00300300", dist);
    add.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("address", ids);
    Option<Address> addressOpt = add.bind("", data);
    assertThat(addressOpt.isEmpty()).isTrue();
    
    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("address", ids2);
    Option<Address> addressOpt2 = add.bind("", data2);
    assertThat(addressOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();
    Address add = new Address(4l, "Address my address", "Avenida", "00300300", dist);
    add.save();

    Address address = add.bind("", "1");
    assertThat(address).isEqualTo(add);
  }

  @Test
  public void failAtPathBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();
    Address add = new Address(4l, "Address my address", "Avenida", "00300300", dist);
    add.save();

    Address address = add.bind("", "d");
    assertThat(address).isNull();
    address = add.bind("", "34");
    assertThat(address).isNull();
  }
  
  @Test
  public void testFinders(){
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();
    Address add = new Address(4l, "Address my address", "Avenida", "00300300", dist);
    add.save();
    Address add2 = new Address(4l, "Address my address", "Avenida", "00300300", dist);
    add2.save();
    
    Address bdAdd = Address.findById(add.id);
    assertThat(bdAdd).isNotNull();
    
    Address bdAddNull = Address.findById(null);
    assertThat(bdAddNull).isNull();
    
    Address bdAdd23 = Address.findById(23l);
    assertThat(bdAdd23).isNull();
    
    List<Address> address = Address.findByPostalCode("00300300");
    assertThat(address).isNotEmpty().hasSize(2).contains(add, add2);
    
    List<Address> addressNull = Address.findByPostalCode("00333333");
    assertThat(addressNull).isEmpty();
    
    List<Address> distAddress = Address.findAllByDistrict(dist);
    assertThat(distAddress).isNotEmpty().hasSize(2).contains(add, add2);
    
    List<Address> distAddressNull = Address.findAllByDistrict(null);
    assertThat(distAddressNull).isEmpty();
  }
}

package test.models;

import static org.fest.assertions.Assertions.assertThat;
import models.ExamProcedureTUSS;
import models.Institution;

import org.junit.Before;
import org.junit.Test;

import play.libs.F.Option;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import models.ExamProcedure;
import test.common.BaseWithServer;

public class ExamProcedureTest extends BaseWithServer {

  public static Institution institution;
  public static ExamProcedureTUSS examProcedureTUSS;

  
  @Before
  public void createInstitution() {
    institution = new Institution("Test", "Test", "Test", "Test");
    institution.save();
    
    examProcedureTUSS = new ExamProcedureTUSS("TUSS", "123");
    examProcedureTUSS.save();
    
  }

  @Test
  public void createExamProcedure() {

    ExamProcedure examprocedure = new ExamProcedure(institution,"Exame1","111",examProcedureTUSS);
    examprocedure.preparation = "- Vir bem alimentado, descansado e sem sono;\n- Trazer uma toalha de rosto;\n- Chegar 15 antes do horário marcado.";
    examprocedure.save();

    ExamProcedure existent = ExamProcedure.findById(examprocedure.id);
    assertThat(existent).isNotNull();
  }

  @Test
  public void deleteExamProcedure() {
    ExamProcedure examprocedure = new ExamProcedure(institution,"Exame1","111",examProcedureTUSS);
    examprocedure.save();

    ExamProcedure existent = ExamProcedure.findById(examprocedure.id);
    assertThat(existent).isNotNull();

    existent.delete();
    assertThat(existent.deleted).isTrue();
  }

  @Test
  public void updateExamProcedure() {
    ExamProcedure examprocedure = new ExamProcedure(institution,"Exame1","111",examProcedureTUSS);
    examprocedure.preparation = "Sem informações.";
    examprocedure.save();

    ExamProcedure existent = ExamProcedure.findById(examprocedure.id);
    assertThat(existent).isNotNull();
    existent.name = "nameUpdated";
    existent.preparation = "- Vir bem alimentado, descansado e sem sono;\n- Trazer uma toalha de rosto;\n- Chegar 15 antes do horário marcado.";
    existent.update();

    existent = ExamProcedure.findById(examprocedure.id);
    assertThat(existent).isNotNull();
    assertThat(existent.name).isEqualTo("nameUpdated");
    assertThat(existent.preparation).isEqualTo("- Vir bem alimentado, descansado e sem sono;\n- Trazer uma toalha de rosto;\n- Chegar 15 antes do horário marcado.");
  }

  @Test
  public void successAtQueryBind() {
    ExamProcedure examprocedure = new ExamProcedure(institution,"Exame1","111",examProcedureTUSS);
    examprocedure.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("examprocedure", ids);
    Option<ExamProcedure> examprocedureOpt = examprocedure.bind("", data);
    assertThat(examprocedureOpt.get()).isEqualTo(examprocedure);
  }

  @Test
  public void failAtQueryBind() {
	  
    ExamProcedure examprocedure = new ExamProcedure(institution,"Exame1","111",examProcedureTUSS);
    examprocedure.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("examprocedure", ids);
    Option<ExamProcedure> examprocedureOpt = examprocedure.bind("", data);
    assertThat(examprocedureOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("examprocedure", ids2);
    Option<ExamProcedure> examprocedureOpt2 = examprocedure.bind("", data2);
    assertThat(examprocedureOpt2.isEmpty()).isTrue();
    
  }

  @Test
  public void successAtPathBind() {
    ExamProcedure examprocedure = new ExamProcedure(institution,"Exame1","111",examProcedureTUSS);
    examprocedure.save();

    ExamProcedure testingExamProcedure = examprocedure.bind("", "1");
    assertThat(testingExamProcedure).isEqualTo(examprocedure);
  }

  @Test
  public void failAtPathBind() {
    ExamProcedure examprocedure = new ExamProcedure(institution,"Exame1","111",examProcedureTUSS);
    examprocedure.save();

    ExamProcedure testingExamProcedure = examprocedure.bind("", "opa");
    assertThat(testingExamProcedure).isNull();
  }


  @Test
  public void testFinder() {
    ExamProcedure examprocedure = new ExamProcedure(institution,"Exame1","111",examProcedureTUSS);
    examprocedure.save();

    ExamProcedure examprocedure2 = new ExamProcedure(institution,"Exame2","222",examProcedureTUSS);
    examprocedure2.save();


    ExamProcedure testingExamProcedure = ExamProcedure.findById(examprocedure.id);
    assertThat(testingExamProcedure).isNotNull();

    ExamProcedure testingNull = ExamProcedure.findById(null);
    assertThat(testingNull).isNull();

    ExamProcedure testing213 = ExamProcedure.findById(213L);
    assertThat(testing213).isNull();

    List<ExamProcedure> all = ExamProcedure.findAllByInstitution(institution.id, null, null,null);
    assertThat(all).hasSize(2).contains(examprocedure, examprocedure2);
  }
}

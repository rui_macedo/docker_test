package test.models;

import static org.fest.assertions.Assertions.assertThat;

import models.Institution;

import org.junit.Test;

import test.common.BaseWithServer;

public class InstitutionTest extends BaseWithServer {

  @Test
  public void createInstitution() {
    Institution institution = new Institution("Clinica", "Test", "Clinis", "265658");
    institution.save();

    Institution existent = Institution.finder.byId(institution.id);
    assertThat(existent).isNotNull();
    assertThat(existent.tenantName).isEqualTo("Clinica");
    assertThat(existent.businessName).isEqualTo("Test");
    assertThat(existent.businessSocialName).isEqualTo("Clinis");
    assertThat(existent.cnpj).isEqualTo("265658");
  }

  @Test
  public void deleteInstitution() {
    Institution institution = new Institution("Clinica", "Test", "Clinis", "265658");
    institution.save();

    Institution existent = Institution.finder.byId(institution.id);
    assertThat(existent).isNotNull();

    existent.delete();
    existent = Institution.finder.byId(institution.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updateInstitution() {
    Institution institution = new Institution("Clinica", "Test", "Clinis", "265658");
    institution.save();

    Institution existent = Institution.finder.byId(institution.id);
    assertThat(existent).isNotNull();

    existent.tenantName = "Novo";
    existent.businessName = "Novo";
    existent.businessSocialName = "Novo";
    existent.cnpj = "2222";
    existent.update();

    existent = Institution.finder.byId(institution.id);
    assertThat(existent).isNotNull();
    assertThat(existent.tenantName).isEqualTo("Novo");
    assertThat(existent.businessName).isEqualTo("Novo");
    assertThat(existent.businessSocialName).isEqualTo("Novo");
    assertThat(existent.cnpj).isEqualTo("2222");
  }

  @Test
  public void testFinders() {
    Institution institution = new Institution("Clinica", "Test", "Clinis", "265658");
    institution.save();
    
    Institution existent = Institution.byId(institution.id);
    assertThat(institution).isEqualTo(existent);
    
    existent = Institution.byTenantName("Clinica");
    assertThat(existent).isEqualTo(institution);
  }
}

package test.models;


import static org.fest.assertions.Assertions.assertThat;
import models.Institution;

import org.junit.Before;
import org.junit.Test;

import play.libs.F.Option;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import models.InformativeTextType;
import test.common.BaseWithServer;

public class InformativeTextTypeTest extends BaseWithServer {

  public static Institution institution;

  @Before
  public void createInstitution() {
    institution = new Institution("Test", "Test", "Test", "Test");
    institution.save();
  }

  @Test
  public void createInformativeTextType() {

    InformativeTextType informativetexttype = new InformativeTextType(institution,"Teste");
    informativetexttype.save();

    InformativeTextType existent = InformativeTextType.findById(informativetexttype.id);
    assertThat(existent.name).isEqualTo("Teste");
    assertThat(existent).isNotNull();
  }

  @Test
  public void deleteInformativeTextType() {
    InformativeTextType informativetexttype = new InformativeTextType(institution,"Teste");
    informativetexttype.save();

    InformativeTextType existent = InformativeTextType.findById(informativetexttype.id);
    assertThat(existent).isNotNull();

    existent.delete();
    existent = InformativeTextType.findById(informativetexttype.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updateInformativeTextType() {
    InformativeTextType informativetexttype = new InformativeTextType(institution,"Teste");
    informativetexttype.save();

    InformativeTextType existent = InformativeTextType.findById(informativetexttype.id);
    assertThat(existent).isNotNull();
    existent.name = "nameUpdated";

    existent.update();

    existent = InformativeTextType.findById(informativetexttype.id);
    assertThat(existent).isNotNull();
    assertThat(existent.name).isEqualTo("nameUpdated");
  }

  @Test
  public void successAtQueryBind() {
    InformativeTextType informativetexttype = new InformativeTextType(institution,"Teste");
    informativetexttype.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("informativetexttype", ids);
    Option<InformativeTextType> informativetexttypeOpt = informativetexttype.bind("", data);
    assertThat(informativetexttypeOpt.get()).isEqualTo(informativetexttype);
  }

  @Test
  public void failAtQueryBind() {
    InformativeTextType informativetexttype = new InformativeTextType(institution,"Teste");
    informativetexttype.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("informativetexttype", ids);
    Option<InformativeTextType> informativetexttypeOpt = informativetexttype.bind("", data);
    assertThat(informativetexttypeOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("informativetexttype", ids2);
    Option<InformativeTextType> informativetexttypeOpt2 = informativetexttype.bind("", data2);
    assertThat(informativetexttypeOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    InformativeTextType informativetexttype = new InformativeTextType(institution,"Teste");
    informativetexttype.save();

    InformativeTextType testingInformativeTextType = informativetexttype.bind("", "1");
    assertThat(testingInformativeTextType).isEqualTo(informativetexttype);
  }

  @Test
  public void failAtPathBind() {
    InformativeTextType informativetexttype = new InformativeTextType(institution,"Teste");
    informativetexttype.save();

    InformativeTextType testingInformativeTextType = informativetexttype.bind("", "opa");
    assertThat(testingInformativeTextType).isNull();
  }


  @Test
  public void testFinder() {
    InformativeTextType informativetexttype = new InformativeTextType(institution,"Teste");
    informativetexttype.save();

    InformativeTextType informativetexttype2 = new InformativeTextType(institution,"Teste");
    informativetexttype2.save();


    InformativeTextType testingInformativeTextType = InformativeTextType.findById(informativetexttype.id);
    assertThat(testingInformativeTextType).isNotNull();

    InformativeTextType testingNull = InformativeTextType.findById(null);
    assertThat(testingNull).isNull();

    InformativeTextType testing213 = InformativeTextType.findById(213L);
    assertThat(testing213).isNull();

    List<InformativeTextType> all = InformativeTextType.findAllByInstitution(institution.id,null);
    assertThat(all).hasSize(2).contains(informativetexttype, informativetexttype2);
  }
}

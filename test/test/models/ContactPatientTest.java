package test.models;

import static org.fest.assertions.Assertions.assertThat;

import models.scheduling.Contact;
import org.junit.Test;

import models.scheduling.ContactPatient;
import models.Institution;
import models.Patient;
import test.common.BaseWithServer;

public class ContactPatientTest extends BaseWithServer {

  @Test
  public void createContactPatient() {
    Institution ints = new Institution("TNT", "NUName", "NUName", "9209423423");
    ints.save();
    Contact contact = new Contact(ints.id, "Joana", "irmã da paciente", "1111-1111");
    contact.save();
    
    Patient patient = new Patient(ints, "Patient", "ref01");
    patient.save();

    ContactPatient cp = new ContactPatient(ints.id,contact);
    cp.contact = contact;
    cp.patientId = patient.id;
    cp.save();

    cp = ContactPatient.finder.byId(cp.id);
    assertThat(cp).isNotNull();
  }
  
  @Test
  public void deleteContactPatient() {
    Institution ints = new Institution("TNT", "NUName", "NUName", "9209423423");
    ints.save();    
    Contact contact = new Contact(ints.id, "Joana", "irmã da paciente", "1111-1111");
    contact.save();
    Patient patient = new Patient(ints, "Patient", "ref01");
    patient.save();

    ContactPatient cp = new ContactPatient(ints.id,contact);
    cp.contact = contact;
    cp.patientId = patient.id;
    cp.save();

    ContactPatient existentcp = ContactPatient.finder.byId(cp.id);
    assertThat(existentcp).isNotNull();
    
    cp.delete();
    existentcp = ContactPatient.finder.byId(cp.id);
    assertThat(existentcp).isNull();
  }
  
  @Test 
  public void updateContactPatient() {
    Institution ints = new Institution("TNT", "NUName", "NUName", "9209423423");
    ints.save();
    Contact contact = new Contact(ints.id, "Joana", "irmã da paciente", "1111-1111");
    contact.save();
    Patient patient = new Patient(ints, "Patient", "ref01");
    patient.save();

    ContactPatient cp = new ContactPatient(ints.id,contact);
    cp.contact = contact;
    cp.patientId = patient.id;
    cp.save();
    
    contact = new Contact(ints.id, "Joao", "irmão do paciente", "1111-1112");
    contact.save();
    
    cp.contact = contact;
    cp.update();
    
    ContactPatient existentcp = ContactPatient.finder.byId(cp.id);
    
    assertThat(existentcp.contact.name).isEqualTo("Joao");
  }
}

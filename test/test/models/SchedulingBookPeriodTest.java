package test.models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import models.ExamProcedure;
import models.ExamProcedureTUSS;
import models.Institution;
import models.SchedulingBook;
import models.SchedulingBookPeriod;

import org.fest.util.Collections;
import org.junit.Test;

import test.common.BaseWithServer;

import common.constants.CalendarDay;
import common.constants.DayShift;

public class SchedulingBookPeriodTest extends BaseWithServer {

  @Test
  public void perfectfindPeriodsByCalendarDay() {

    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure ep = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    ep.save();

    SchedulingBook book = new SchedulingBook(institution, "Book 1", 30);
    book.save();

    book.addSchedulingBookPeriod(6 * 60 * 60 * 1000, 12 * 60 * 60 * 1000, CalendarDay.MON);// 6h -
                                                                                           // 12h

    book.addSchedulingBookPeriod(12 * 60 * 60 * 1000, 18 * 60 * 60 * 1000, CalendarDay.MON);// 12h -
                                                                                            // 18h
    book.addSchedulingBookPeriod(18 * 60 * 60 * 1000, (24 * 60 * 60 * 1000) - 1, CalendarDay.MON);// 12h
                                                                                                  // 23:59:59.999h

    SchedulingBookPeriod morningPeriod =
        SchedulingBookPeriod.findPeriodByBook(book, 6 * 60 * 60 * 1000, 12 * 60 * 60 * 1000,
            CalendarDay.MON);
    SchedulingBookPeriod afternoonPeriod =
        SchedulingBookPeriod.findPeriodByBook(book, 12 * 60 * 60 * 1000, 18 * 60 * 60 * 1000,
            CalendarDay.MON);
    SchedulingBookPeriod nightPeriod =
        SchedulingBookPeriod.findPeriodByBook(book, 18 * 60 * 60 * 1000, (24 * 60 * 60 * 1000) - 1,
            CalendarDay.MON);

    List<SchedulingBookPeriod> periodsInDb =
        SchedulingBookPeriod.findSpecificOverlapedPeriodsByCalendarDay(book, CalendarDay.MON,
            Collections.list(DayShift.MOR, DayShift.AFT, DayShift.NIG));

    assertThat(periodsInDb).isNotEmpty().hasSize(3)
        .contains(morningPeriod, afternoonPeriod, nightPeriod);

    periodsInDb =
        SchedulingBookPeriod.findSpecificOverlapedPeriodsByCalendarDay(book, CalendarDay.MON,
            Collections.list(DayShift.MOR));
    assertThat(periodsInDb).isNotEmpty().hasSize(1).contains(morningPeriod);

    periodsInDb =
        SchedulingBookPeriod.findSpecificOverlapedPeriodsByCalendarDay(book, CalendarDay.MON,
            Collections.list(DayShift.MOR, DayShift.NIG));
    assertThat(periodsInDb).isNotEmpty().hasSize(2).contains(morningPeriod, nightPeriod);

    periodsInDb =
        SchedulingBookPeriod.findSpecificOverlapedPeriodsByCalendarDay(book, CalendarDay.MON,
            java.util.Collections.EMPTY_LIST);
    assertThat(periodsInDb).isNotEmpty().hasSize(3)
        .contains(morningPeriod, afternoonPeriod, nightPeriod);
  }

  @Test
  public void randomPeriodsfindPeriodsByCalendarDay() {

    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure ep = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    ep.save();

    SchedulingBook book = new SchedulingBook(institution, "Book 1", 30);
    book.save();

    book.addSchedulingBookPeriod(8 * 60 * 60 * 1000, 12 * 60 * 60 * 1000, CalendarDay.MON);// 8h -
                                                                                           // 12h

    book.addSchedulingBookPeriod(14 * 60 * 60 * 1000, 17 * 60 * 60 * 1000, CalendarDay.MON);// 14h -
                                                                                            // 17h
    book.addSchedulingBookPeriod(18 * 60 * 60 * 1000, 22 * 60 * 60 * 1000, CalendarDay.MON);// 18h
                                                                                            // 22h

    SchedulingBookPeriod morningPeriod =
        SchedulingBookPeriod.findPeriodByBook(book, 8 * 60 * 60 * 1000, 12 * 60 * 60 * 1000,
            CalendarDay.MON);
    SchedulingBookPeriod afternoonPeriod =
        SchedulingBookPeriod.findPeriodByBook(book, 14 * 60 * 60 * 1000, 17 * 60 * 60 * 1000,
            CalendarDay.MON);
    SchedulingBookPeriod nightPeriod =
        SchedulingBookPeriod.findPeriodByBook(book, 18 * 60 * 60 * 1000, 22 * 60 * 60 * 1000,
            CalendarDay.MON);

    List<SchedulingBookPeriod> periodsInDb =
        SchedulingBookPeriod.findSpecificOverlapedPeriodsByCalendarDay(book, CalendarDay.MON,
            Collections.list(DayShift.MOR, DayShift.AFT, DayShift.NIG));

    assertThat(periodsInDb).isNotEmpty().hasSize(3)
        .contains(morningPeriod, afternoonPeriod, nightPeriod);

    periodsInDb =
        SchedulingBookPeriod.findSpecificOverlapedPeriodsByCalendarDay(book, CalendarDay.MON,
            Collections.list(DayShift.MOR));
    assertThat(periodsInDb).isNotEmpty().hasSize(1).contains(morningPeriod);

    periodsInDb =
        SchedulingBookPeriod.findSpecificOverlapedPeriodsByCalendarDay(book, CalendarDay.MON,
            Collections.list(DayShift.MOR, DayShift.NIG));
    assertThat(periodsInDb).isNotEmpty().hasSize(2).contains(morningPeriod, nightPeriod);

    periodsInDb =
        SchedulingBookPeriod.findSpecificOverlapedPeriodsByCalendarDay(book, CalendarDay.MON,
            java.util.Collections.EMPTY_LIST);
    assertThat(periodsInDb).isNotEmpty().hasSize(3)
        .contains(morningPeriod, afternoonPeriod, nightPeriod);
  }
}

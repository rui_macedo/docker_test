package test.models;

import static org.fest.assertions.Assertions.assertThat;
import models.Country;
import models.MedicalCouncil;
import models.State;

import org.junit.Before;
import org.junit.Test;

import play.libs.F.Option;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import models.Physician;
import test.common.BaseWithServer;

public class PhysicianTest extends BaseWithServer {

  public static MedicalCouncil medicalCouncil;
  public static State state;
  public static Country country;

  @Before
  public void createInstitution() {
    medicalCouncil = new MedicalCouncil("Conselho", "CC");
    medicalCouncil.save();
 
    country = new Country(1L,"Brasil","BR");
    country.save();
    
    state = new State(1L, "Estado", country);
    state.save();
  }

  @Test
  public void createPhysician() {

    Physician physician = new Physician("Teste",medicalCouncil,"12345",state);
    physician.save();

    Physician existent = Physician.findById(physician.id);
    assertThat(existent).isNotNull();
  }

  @Test
  public void deletePhysician() {
    Physician physician = new Physician("Teste",medicalCouncil,"12345",state);
    physician.save();

    Physician existent = Physician.findById(physician.id);
    assertThat(existent).isNotNull();

    existent.delete();
    existent = Physician.findById(physician.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updatePhysician() {
    Physician physician = new Physician("Teste",medicalCouncil,"12345",state);
    physician.save();

    Physician existent = Physician.findById(physician.id);
    assertThat(existent).isNotNull();
    //existent.name = "nameUpdated";

    existent.update();

    existent = Physician.findById(physician.id);
    assertThat(existent).isNotNull();
    //assertThat(existent.name).isEqualTo("nameUpdated");
  }

  @Test
  public void successAtQueryBind() {
    Physician physician = new Physician("Teste",medicalCouncil,"12345",state);
    physician.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("id", ids);
    Option<Physician> physicianOpt = physician.bind("", data);
    assertThat(physicianOpt.get()).isEqualTo(physician);
  }

  @Test
  public void failAtQueryBind() {
    Physician physician = new Physician("Teste",medicalCouncil,"12345",state);
    physician.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("id", ids);
    Option<Physician> physicianOpt = physician.bind("", data);
    assertThat(physicianOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("id", ids2);
    Option<Physician> physicianOpt2 = physician.bind("", data2);
    assertThat(physicianOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    Physician physician = new Physician("Teste",medicalCouncil,"12345",state);
    physician.save();

    Physician testingPhysician = physician.bind("", "1");
    assertThat(testingPhysician).isEqualTo(physician);
  }

  @Test
  public void failAtPathBind() {
    Physician physician = new Physician("Teste",medicalCouncil,"12345",state);
    physician.save();

    Physician testingPhysician = physician.bind("", "opa");
    assertThat(testingPhysician).isNull();
  }


  @Test
  public void testFinder() {
    Physician physician = new Physician("Teste",medicalCouncil,"12345",state);
    physician.save();

    Physician physician2 = new Physician("Teste",medicalCouncil,"45678",state);   
    physician2.save();


    Physician testingPhysician = Physician.findById(physician.id);
    assertThat(testingPhysician).isNotNull();

    Physician testingNull = Physician.findById(null);
    assertThat(testingNull).isNull();

    Physician testing213 = Physician.findById(213L);
    assertThat(testing213).isNull();

    List<Physician> all = Physician.findAllByName("");
    assertThat(all).hasSize(2).contains(physician, physician2);
  }
}

package test.models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.City;
import models.Country;
import models.District;
import models.State;

import org.junit.Test;

import play.libs.F.Option;
import test.common.BaseWithServer;

public class DistrictTest extends BaseWithServer {

  // util to test in another class
  public static District createFakeDistrict() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();
    return dist;
  }

  @Test
  public void successAtCreateDistrict() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();

    // reload address
    dist = District.findById(dist.id);
    assertThat(dist).isNotNull();
    assertThat(dist.getId()).isNotNull();
    assertThat(dist.originalId).isEqualTo(3l);
    assertThat(dist.name).isEqualTo("District");
    assertThat(dist.city).isEqualTo(city);
  }

  @Test
  public void successAtQueryBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("district", ids);
    Option<District> distOpt = dist.bind("", data);
    assertThat(distOpt.get()).isEqualTo(dist);
  }

  @Test
  public void failAtQueryBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("district", ids);
    Option<District> distOpt = dist.bind("", data);
    assertThat(distOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("district", ids2);
    Option<District> distOpt2 = dist.bind("", data2);
    assertThat(distOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();

    District district = dist.bind("", "1");
    assertThat(district).isEqualTo(dist);
  }

  @Test
  public void failAtPathBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();

    District district = dist.bind("", "d");
    assertThat(district).isNull();
    district = dist.bind("", "34");
    assertThat(district).isNull();
  }

  @Test
  public void testFinders() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    District dist = new District(3l, "District", city);
    dist.save();

    District dist2 = new District(4l, "District2", city);
    dist2.save();

    District bdDist = District.findById(dist.id);
    assertThat(bdDist).isNotNull();

    bdDist = District.findById(null);
    assertThat(bdDist).isNull();

    bdDist = District.findById(23l);
    assertThat(bdDist).isNull();

    List<District> districts = District.findAllByCity(city);
    assertThat(districts).isNotEmpty().hasSize(2).contains(dist, dist2);

    List<District> districtsNull = District.findAllByCity(null);
    assertThat(districtsNull).isEmpty();
  }
}

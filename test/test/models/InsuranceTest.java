package test.models;

import static org.fest.assertions.Assertions.assertThat;
import models.Institution;
import models.InsuranceHealthCare;

import org.junit.Before;
import org.junit.Test;

import play.libs.F.Option;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import models.Insurance;
import test.common.BaseWithServer;
import test.utils.ModelFactory;

public class InsuranceTest extends BaseWithServer {

  public static Institution institution;
  public static InsuranceHealthCare insuranceHealthCare;
  
  @Before
  public void createInstitution() {
    institution = new Institution("Test", "Test", "Test", "Test");
    institution.save();
    
    insuranceHealthCare = new InsuranceHealthCare("123", "12333", "UNIMED", "UNIMED SA");
    insuranceHealthCare.save();
  }

  @Test
  public void createInsurance() {
    Insurance insurance = ModelFactory.createInsurance(institution);
    Insurance existent = Insurance.findById(insurance.id);
    assertThat(existent).isNotNull();
  }

  @Test
  public void deleteInsuranceCarrier() {
    Insurance insurance = ModelFactory.createInsurance(institution);
    Insurance existent = Insurance.findById(insurance.id);
    assertThat(existent).isNotNull();
    existent.delete();
    existent = Insurance.findById(insurance.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updateInsuranceCarrier() {
    Insurance insurance = ModelFactory.createInsurance(institution);
    Insurance existent = Insurance.findById(insurance.id);
    assertThat(existent).isNotNull();
    existent.update();
    existent = Insurance.findById(insurance.id);
    assertThat(existent).isNotNull();
  }

  @Test
  public void successAtQueryBind() {
    Insurance insurance = ModelFactory.createInsurance(institution);
    insurance.save();
    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("insurance", ids);
    Option<Insurance> insurancecarrierOpt = insurance.bind("", data);
    assertThat(insurancecarrierOpt.get()).isEqualTo(insurance);
  }

  @Test
  public void failAtQueryBind() {

   Insurance insurance = ModelFactory.createInsurance(institution);

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("insurance", ids);
    Option<Insurance> insurancecarrierOpt = insurance.bind("", data);
    assertThat(insurancecarrierOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("insurance", ids2);
    Option<Insurance> insurancecarrierOpt2 = insurance.bind("", data2);
    assertThat(insurancecarrierOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    Insurance insurance = ModelFactory.createInsurance(institution);
    Insurance testingInsurance = insurance.bind("", "1");
    assertThat(testingInsurance).isEqualTo(insurance);
  }

  @Test
  public void failAtPathBind() {
    Insurance insurance = ModelFactory.createInsurance(institution);
    Insurance testingInsurance = insurance.bind("", "opa");
    assertThat(testingInsurance).isNull();
  }


  @Test
  public void testFinder() {

      Insurance insurance = ModelFactory.createInsurance(institution);
      Insurance insurance2 = ModelFactory.createInsurance(institution);
      Insurance testingInsurance = Insurance.findById(insurance.id);

      assertThat(testingInsurance).isNotNull();

      Insurance testingNull = Insurance.findById(null);
      assertThat(testingNull).isNull();

      Insurance testing213 = Insurance.findById(213L);
      assertThat(testing213).isNull();

      List<Insurance> all = Insurance.findAllByTenant(institution.id, null);
      assertThat(all).hasSize(2).contains(insurance, insurance2);

  }
}

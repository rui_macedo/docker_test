package test.models;

import static org.fest.assertions.Assertions.assertThat;
import models.Country;
import models.Employee;
import models.Institution;
import models.MedicalCouncil;
import models.Physician;
import models.State;

import org.junit.Before;
import org.junit.Test;

import play.libs.F.Option;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import models.PhysicianEmployee;
import test.common.BaseWithServer;

public class PhysicianEmployeeTest extends BaseWithServer {

  public static Institution institution;
  public static Physician physician;
  public static Employee employee;
  public static MedicalCouncil medicalCouncil;
  public static State state;
  public static Country country;
  
  @Before
  public void createInstitution() {
    institution = new Institution("Test", "Test", "Test", "Test");
    institution.save();
    
    medicalCouncil = new MedicalCouncil("Conselho", "CC");
    medicalCouncil.save();
 
    country = new Country(1L,"Brasil","BR");
    country.save();
    
    state = new State(1L, "Estado", country);
    state.save();
    
    physician = new Physician("Medico", medicalCouncil, "123", state);
    
    employee = new Employee(institution, "Empregado");
    
  }

  @Test
  public void createPhysicianEmployee() {

    PhysicianEmployee physicianemployee = new PhysicianEmployee(institution,physician,employee);
    physicianemployee.save();

    PhysicianEmployee existent = PhysicianEmployee.findById(physicianemployee.id);
    assertThat(existent).isNotNull();
  }

  @Test
  public void deletePhysicianEmployee() {
    PhysicianEmployee physicianemployee = new PhysicianEmployee(institution,physician,employee);
    physicianemployee.save();

    PhysicianEmployee existent = PhysicianEmployee.findById(physicianemployee.id);
    assertThat(existent).isNotNull();

    existent.delete();
    existent = PhysicianEmployee.findById(physicianemployee.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updatePhysicianEmployee() {
    PhysicianEmployee physicianemployee = new PhysicianEmployee(institution,physician,employee);
    physicianemployee.save();

    PhysicianEmployee existent = PhysicianEmployee.findById(physicianemployee.id);
    assertThat(existent).isNotNull();
    existent.name = "nameUpdated";
    existent.update();

    existent = PhysicianEmployee.findById(physicianemployee.id);
    assertThat(existent).isNotNull();
    assertThat(existent.name).isEqualTo("nameUpdated");
  }

  @Test
  public void successAtQueryBind() {
    PhysicianEmployee physicianemployee = new PhysicianEmployee(institution,physician,employee);
    physicianemployee.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("id", ids);
    Option<PhysicianEmployee> physicianemployeeOpt = physicianemployee.bind("", data);
    assertThat(physicianemployeeOpt.get()).isEqualTo(physicianemployee);
  }

  @Test
  public void failAtQueryBind() {
    PhysicianEmployee physicianemployee = new PhysicianEmployee(institution,physician,employee);
    physicianemployee.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("id", ids);
    Option<PhysicianEmployee> physicianemployeeOpt = physicianemployee.bind("", data);
    assertThat(physicianemployeeOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("id", ids2);
    Option<PhysicianEmployee> physicianemployeeOpt2 = physicianemployee.bind("", data2);
    assertThat(physicianemployeeOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    PhysicianEmployee physicianemployee = new PhysicianEmployee(institution,physician,employee);
    physicianemployee.save();

    PhysicianEmployee testingPhysicianEmployee = physicianemployee.bind("", "1");
    assertThat(testingPhysicianEmployee).isEqualTo(physicianemployee);
  }

  @Test
  public void failAtPathBind() {
    PhysicianEmployee physicianemployee = new PhysicianEmployee(institution,physician,employee);
    physicianemployee.save();

    PhysicianEmployee testingPhysicianEmployee = physicianemployee.bind("", "opa");
    assertThat(testingPhysicianEmployee).isNull();
  }


  @Test
  public void testFinder() {
    PhysicianEmployee physicianemployee = new PhysicianEmployee(institution,physician,employee);
    physicianemployee.save();

    PhysicianEmployee physicianemployee2 = new PhysicianEmployee(institution,physician,employee);
    physicianemployee2.save();


    PhysicianEmployee testingPhysicianEmployee = PhysicianEmployee.findById(physicianemployee.id);
    assertThat(testingPhysicianEmployee).isNotNull();

    PhysicianEmployee testingNull = PhysicianEmployee.findById(null);
    assertThat(testingNull).isNull();

    PhysicianEmployee testing213 = PhysicianEmployee.findById(213L);
    assertThat(testing213).isNull();

    List<PhysicianEmployee> all = PhysicianEmployee.findAllByInstitution(institution.id);
    assertThat(all).hasSize(2).contains(physicianemployee, physicianemployee2);
  }
}

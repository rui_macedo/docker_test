package test.models;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

import play.libs.F.Option;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import models.MedicalCouncil;
import test.common.BaseWithServer;

public class MedicalCouncilTest extends BaseWithServer {

  @Test
  public void createMedicalCouncil() {

    MedicalCouncil medicalcouncil = new MedicalCouncil("Conselho","CC");
    medicalcouncil.save();

    MedicalCouncil existent = MedicalCouncil.findById(medicalcouncil.id);
    assertThat(existent).isNotNull();
  }

  @Test
  public void deleteMedicalCouncil() {
    MedicalCouncil medicalcouncil = new MedicalCouncil("Conselho","CC");
    medicalcouncil.save();

    MedicalCouncil existent = MedicalCouncil.findById(medicalcouncil.id);
    assertThat(existent).isNotNull();

    existent.delete();
    existent = MedicalCouncil.findById(medicalcouncil.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updateMedicalCouncil() {
    MedicalCouncil medicalcouncil = new MedicalCouncil("Conselho","CC");
    medicalcouncil.save();

    MedicalCouncil existent = MedicalCouncil.findById(medicalcouncil.id);
    assertThat(existent).isNotNull();
    existent.name = "nameUpdated";

    existent.update();

    existent = MedicalCouncil.findById(medicalcouncil.id);
    assertThat(existent).isNotNull();
    assertThat(existent.name).isEqualTo("nameUpdated");
  }

  @Test
  public void successAtQueryBind() {
    MedicalCouncil medicalcouncil = new MedicalCouncil("Conselho","CC");
    medicalcouncil.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("medicalcouncil", ids);
    Option<MedicalCouncil> medicalcouncilOpt = medicalcouncil.bind("", data);
    assertThat(medicalcouncilOpt.get()).isEqualTo(medicalcouncil);
  }

  @Test
  public void failAtQueryBind() {
    MedicalCouncil medicalcouncil = new MedicalCouncil("Conselho","CC");
    medicalcouncil.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("medicalcouncil", ids);
    Option<MedicalCouncil> medicalcouncilOpt = medicalcouncil.bind("", data);
    assertThat(medicalcouncilOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("medicalcouncil", ids2);
    Option<MedicalCouncil> medicalcouncilOpt2 = medicalcouncil.bind("", data2);
    assertThat(medicalcouncilOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    MedicalCouncil medicalcouncil = new MedicalCouncil("Conselho","CC");
    medicalcouncil.save();

    MedicalCouncil testingMedicalCouncil = medicalcouncil.bind("", "1");
    assertThat(testingMedicalCouncil).isEqualTo(medicalcouncil);
  }

  @Test
  public void failAtPathBind() {
    MedicalCouncil medicalcouncil = new MedicalCouncil("Conselho","CC");
    medicalcouncil.save();

    MedicalCouncil testingMedicalCouncil = medicalcouncil.bind("", "opa");
    assertThat(testingMedicalCouncil).isNull();
  }


  @Test
  public void testFinder() {
    MedicalCouncil medicalcouncil = new MedicalCouncil("Conselho","CC");
    medicalcouncil.save();

    MedicalCouncil medicalcouncil2 = new MedicalCouncil("Conselho2","CC2");
    medicalcouncil2.save();


    MedicalCouncil testingMedicalCouncil = MedicalCouncil.findById(medicalcouncil.id);
    assertThat(testingMedicalCouncil).isNotNull();

    MedicalCouncil testingNull = MedicalCouncil.findById(null);
    assertThat(testingNull).isNull();

    MedicalCouncil testing213 = MedicalCouncil.findById(213L);
    assertThat(testing213).isNull();

    List<MedicalCouncil> all = MedicalCouncil.findAll(null,null);
    assertThat(all).hasSize(2).contains(medicalcouncil, medicalcouncil2);
  }
}

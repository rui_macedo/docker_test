package test.models;


import static org.fest.assertions.Assertions.assertThat;
import models.InformativeTextType;
import models.Institution;

import org.junit.Before;
import org.junit.Test;

import play.libs.F.Option;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import models.InformativeText;
import test.common.BaseWithServer;

public class InformativeTextTest extends BaseWithServer {

  public static Institution institution;
  public static InformativeTextType informativeTextType;
  
  @Before
  public void createInstitution() {
    institution = new Institution("Test", "Test", "Test", "Test");
    institution.save();

    informativeTextType = new InformativeTextType(institution,"Tipo1");
	informativeTextType.save();
  }

  @Test
  public void createInformativeText() {
    InformativeText informativetext = new InformativeText(institution,"Teste",informativeTextType);
    informativetext.save();

    InformativeText existent = InformativeText.findById(informativetext.id);
    assertThat(existent).isNotNull();
  }

  @Test
  public void deleteInformativeText() {
	InformativeText informativetext = new InformativeText(institution,"Teste",informativeTextType);
  
    informativetext.save();

    InformativeText existent = InformativeText.findById(informativetext.id);
    assertThat(existent).isNotNull();

    existent.delete();
    existent = InformativeText.findById(informativetext.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updateInformativeText() {
	InformativeText informativetext = new InformativeText(institution,"Teste",informativeTextType);

	informativetext.save();

    InformativeText existent = InformativeText.findById(informativetext.id);
    assertThat(existent).isNotNull();
    //existent.name = "nameUpdated";

    existent.update();

    existent = InformativeText.findById(informativetext.id);
    assertThat(existent).isNotNull();
    //assertThat(existent.name).isEqualTo("nameUpdated");
  }

  @Test
  public void successAtQueryBind() {
    InformativeText informativetext = new InformativeText(institution,"Teste",informativeTextType);

    informativetext.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("informativetext", ids);
    Option<InformativeText> informativetextOpt = informativetext.bind("", data);
    assertThat(informativetextOpt.get()).isEqualTo(informativetext);
  }

  @Test
  public void failAtQueryBind() {
    InformativeText informativetext = new InformativeText(institution,"Teste",informativeTextType);
    
    informativetext.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("informativetext", ids);
    Option<InformativeText> informativetextOpt = informativetext.bind("", data);
    assertThat(informativetextOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("informativetext", ids2);
    Option<InformativeText> informativetextOpt2 = informativetext.bind("", data2);
    assertThat(informativetextOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    InformativeText informativetext = new InformativeText(institution,"Teste",informativeTextType);

    informativetext.save();

    InformativeText testingInformativeText = informativetext.bind("", "1");
    assertThat(testingInformativeText).isEqualTo(informativetext);
  }

  @Test
  public void failAtPathBind() {
    InformativeText informativetext = new InformativeText(institution,"Teste",informativeTextType);

    informativetext.save();

    InformativeText testingInformativeText = informativetext.bind("", "opa");
    assertThat(testingInformativeText).isNull();
  }


  @Test
  public void testFinder() {
    InformativeText informativetext = new InformativeText(institution,"Teste",informativeTextType);

    informativetext.save();

    InformativeText informativetext2 = new InformativeText(institution,"Teste1",informativeTextType);
    informativetext2.save();


    InformativeText testingInformativeText = InformativeText.findById(informativetext.id);
    assertThat(testingInformativeText).isNotNull();

    InformativeText testingNull = InformativeText.findById(null);
    assertThat(testingNull).isNull();

    InformativeText testing213 = InformativeText.findById(213L);
    assertThat(testing213).isNull();

    List<InformativeText> all = InformativeText.findAllByInstitution(institution.id);
    assertThat(all).hasSize(2).contains(informativetext, informativetext2);
  }
}

package test.models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Equipment;
import models.Institution;
import models.Room;
import models.Section;

import org.junit.Test;

import play.libs.F.Option;
import test.common.BaseWithServer;

public class EquipmentTest extends BaseWithServer {

  @Test
  public void successCreateEquipment() {
    Institution inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
    Section section = new Section(inst, "Section");
    section.save();
    Room room = new Room(inst, "Sala 1");
    room.section = section;
    room.save();
    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();
    assertThat(equipment.getId()).isNotNull();
    assertThat(Equipment.findById(equipment.id)).isNotNull();
  }

  @Test
  public void successAtQueryBind() {
    Institution inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
    Section section = new Section(inst, "Section");
    section.save();
    Room room = new Room(inst, "Sala 1");
    room.section = section;
    room.save();
    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("equipment", ids);
    Option<Equipment> instGrpOp = equipment.bind("", data);
    assertThat(instGrpOp.get()).isEqualTo(equipment);
  }

  @Test
  public void failAtQueryBind() {
    Institution inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
    Section section = new Section(inst, "Section");
    section.save();
    Room room = new Room(inst, "Sala 1");
    room.section = section;
    room.save();
    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("equipment", ids);
    Option<Equipment> instGrpOp = equipment.bind("", data);
    assertThat(instGrpOp.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    Institution inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
    Section section = new Section(inst, "Section");
    section.save();
    Room room = new Room(inst, "Sala 1");
    room.section = section;
    room.save();
    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();
    Equipment strict = equipment.bind("", "1");
    assertThat(strict).isEqualTo(equipment);
  }

  @Test
  public void failAtPathBind() {
    Institution inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
    Section section = new Section(inst, "Section");
    section.save();
    Room room = new Room(inst, "Sala 1");
    room.section = section;
    room.save();
    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();

    Equipment equip = equipment.bind("", "d");
    assertThat(equip).isNull();
  }

  @Test
  public void testFinders() {
    
    Institution inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
    Section section = new Section(inst, "Section");
    section.save();
    Room room = new Room(inst, "Sala 1");
    room.section = section;
    room.save();
    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();

    Equipment equip = Equipment.findById(equipment.id);
    assertThat(equip).isNotNull();

    equip = Equipment.findById(null);
    assertThat(equip).isNull();

    equip = Equipment.findById(23l);
    assertThat(equip).isNull();

    List<Equipment> equipments = Equipment.findAll(inst.id);
    assertThat(equipments).isNotEmpty();
  }

}

package test.models;

import static org.fest.assertions.Assertions.assertThat;
import models.Institution;

import org.junit.Before;
import org.junit.Test;

import play.libs.F.Option;

import java.util.List;
import java.util.HashMap;
import java.util.Map;

import models.ExamModality;
import test.common.BaseWithServer;

public class ExamModalityTest extends BaseWithServer {

  public static Institution institution;

  @Before
  public void createInstitution() {
    institution = new Institution("Test", "Test", "Test", "Test");
    institution.save();
  }

  @Test
  public void createExamModality() {

    ExamModality exammodality = new ExamModality(institution);
    exammodality.save();

    ExamModality existent = ExamModality.findById(exammodality.id);
    assertThat(existent).isNotNull();
  }

  @Test
  public void deleteExamModality() {
    ExamModality exammodality = new ExamModality(institution);
    exammodality.save();

    ExamModality existent = ExamModality.findById(exammodality.id);
    assertThat(existent).isNotNull();

    existent.delete();
    existent = ExamModality.findById(exammodality.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updateExamModality() {
    ExamModality exammodality = new ExamModality(institution);
    exammodality.save();

    ExamModality existent = ExamModality.findById(exammodality.id);
    assertThat(existent).isNotNull();
    //existent.name = "nameUpdated";

    existent.update();

    existent = ExamModality.findById(exammodality.id);
    assertThat(existent).isNotNull();
    //assertThat(existent.name).isEqualTo("nameUpdated");
  }

  @Test
  public void successAtQueryBind() {
    ExamModality exammodality = new ExamModality(institution);
    exammodality.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {exammodality.getId().toString()};
    data.put("exammodality", ids);
    Option<ExamModality> exammodalityOpt = exammodality.bind("", data);
    assertThat(exammodalityOpt.get()).isEqualTo(exammodality);
  }

  @Test
  public void failAtQueryBind() {
    ExamModality exammodality = new ExamModality(institution);
    exammodality.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("exammodality", ids);
    Option<ExamModality> exammodalityOpt = exammodality.bind("", data);
    assertThat(exammodalityOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("exammodality", ids2);
    Option<ExamModality> exammodalityOpt2 = exammodality.bind("", data2);
    assertThat(exammodalityOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    ExamModality exammodality = new ExamModality(institution);
    exammodality.save();

    ExamModality testingExamModality = exammodality.bind("", exammodality.getId().toString());
    assertThat(testingExamModality).isEqualTo(exammodality);
  }

  @Test
  public void failAtPathBind() {
    ExamModality exammodality = new ExamModality(institution);
    exammodality.save();

    ExamModality testingExamModality = exammodality.bind("", "opa");
    assertThat(testingExamModality).isNull();
  }


  @Test
  public void testFinder() {
    ExamModality exammodality = new ExamModality(institution);
    exammodality.save();

    ExamModality exammodality2 = new ExamModality(institution);
    exammodality2.save();


    ExamModality testingExamModality = ExamModality.findById(exammodality.id);
    assertThat(testingExamModality).isNotNull();

    ExamModality testingNull = ExamModality.findById(null);
    assertThat(testingNull).isNull();

    ExamModality testing213 = ExamModality.findById(213L);
    assertThat(testing213).isNull();

    List<ExamModality> all = ExamModality.findAllByInstitution(institution.id);
    assertThat(all).hasSize(2).contains(exammodality, exammodality2);
  }
}
 

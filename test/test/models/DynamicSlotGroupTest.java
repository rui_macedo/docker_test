package test.models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import models.Equipment;
import models.EquipmentResource;
import models.ExamProcedure;
import models.ExamProcedureTUSS;
import models.Institution;
import models.Resource;
import models.Room;
import models.SchedulingBook;
import models.SchedulingBookPeriod;
import models.Section;
import models.searchalg.DynamicSlot;
import models.searchalg.DynamicSlotGroup;

import org.joda.time.Interval;
import org.junit.Test;

import test.common.BaseWithServer;
import common.constants.CalendarDay;
import common.utils.DateUtils;

public class DynamicSlotGroupTest extends BaseWithServer {

  @Test
  public void successAtgetMostClosestSlot() {

    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure ep = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    ep.save();

    SchedulingBook book = new SchedulingBook(institution, "Book 1", 30);
    book.save();

    book.addSchedulingBookPeriod(32400000, 43200000, CalendarDay.MON);

    Section section = new Section(institution, "Section");
    section.save();
    Room room = new Room(institution, "Sala 1");
    room.section = section;
    room.save();
    Equipment equip = new Equipment(institution, "E", "p", "e", section, room);
    equip.save();

    
    Resource resource = new EquipmentResource(equip, institution);
    resource.save();

    resource.addResourceWeekAgenda(CalendarDay.MON, 32400000, 43200000);

    SchedulingBookPeriod period =
        SchedulingBookPeriod.findPeriodByBook(book, 32400000, 43200000, CalendarDay.MON);

    //period.addResource(resource);
    
    Interval slotsInterval = new Interval(21600000l, 75600000l);//6h - 21h
    List<Interval> intervals =  DateUtils.splitInterval(slotsInterval, 1800000);//30min
    List<DynamicSlot> dSlots = new ArrayList<DynamicSlot>();
    for(Interval interval : intervals){
      dSlots.add(new DynamicSlot(period, interval, ep));
    }
    DynamicSlotGroup dynamicSlotGroup = new DynamicSlotGroup(dSlots);
    
    Interval slot1 = new Interval(33300000,  35400000); //9:15 - 9:50
    DynamicSlot dSlot1 = new DynamicSlot(period, slot1, ep);
    
    DynamicSlot closestSlot1 = dynamicSlotGroup.getMostClosestSlot(dSlot1);
    assertThat(closestSlot1.startAt).isEqualTo(36000000);//10h
    assertThat(closestSlot1.endAt).isEqualTo(37800000);//10:30
    
    
    Interval slot2 = new Interval(23400000,  27300000); //6:30h - 7:35
    DynamicSlot dSlot2 = new DynamicSlot(period, slot2, ep);
    
    DynamicSlot closestSlot2 = dynamicSlotGroup.getMostClosestSlot(dSlot2);
    assertThat(closestSlot2.startAt).isEqualTo(21600000);//6h 
    assertThat(closestSlot2.endAt).isEqualTo(23400000);//6:30
  }

  @Test
  public void successAtgetMostClosestSlotNull() {

    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();


    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure ep = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    ep.save();

    SchedulingBook book = new SchedulingBook(institution, "Book 1", 30);
    book.save();

    book.addSchedulingBookPeriod(32400000, 43200000, CalendarDay.MON);

    Section section = new Section(institution, "Section");
    section.save();
    Room room = new Room(institution, "Sala 1");
    room.section = section;
    room.save();
    Equipment equip = new Equipment(institution, "E", "p", "e", section, room);
    equip.save();

    Resource resource = new EquipmentResource(equip, institution);
    resource.save();

    resource.addResourceWeekAgenda(CalendarDay.MON, 32400000, 43200000);

    SchedulingBookPeriod period =
        SchedulingBookPeriod.findPeriodByBook(book, 32400000, 43200000, CalendarDay.MON);

    //period.addResource(resource);
    
    Interval slotsInterval = new Interval(21600000l, 75600000l);//6h - 21h
    List<Interval> intervals =  DateUtils.splitInterval(slotsInterval, 1800000);//30min
    List<DynamicSlot> dSlots = new ArrayList<DynamicSlot>();
    for(Interval interval : intervals){
      dSlots.add(new DynamicSlot(period, interval, ep));
    }
    DynamicSlotGroup dynamicSlotGroup = new DynamicSlotGroup(dSlots);
    
    DynamicSlot closestSlot1 = dynamicSlotGroup.getMostClosestSlot(null);
    assertThat(closestSlot1).isNull();
  }
  
  @Test
  public void successAtgetClosestIFIgnored() {

    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    ExamProcedureTUSS examProcedureTUSS = new ExamProcedureTUSS("NA", "232");
    examProcedureTUSS.save();

    ExamProcedure ep = new ExamProcedure(institution, "Proc", "0303", examProcedureTUSS);
    ep.save();

    SchedulingBook book = new SchedulingBook(institution, "Book 1", 30);
    book.save();

    book.addSchedulingBookPeriod(32400000, 43200000, CalendarDay.MON);

    Section section = new Section(institution, "Section");
    section.save();
    Room room = new Room(institution, "Sala 1");
    room.section = section;
    room.save();
    Equipment equip = new Equipment(institution, "E", "p", "e", section, room);
    equip.save();

    Resource resource = new EquipmentResource(equip, institution);
    resource.save();

    resource.addResourceWeekAgenda(CalendarDay.MON, 32400000, 43200000);

    SchedulingBookPeriod period =
        SchedulingBookPeriod.findPeriodByBook(book, 32400000, 43200000, CalendarDay.MON);

    //period.addResource(resource);
    
    Interval slotsInterval = new Interval(21600000l, 75600000l);//6h - 21h
    List<Interval> intervals =  DateUtils.splitInterval(slotsInterval, 1800000);//30min
    List<DynamicSlot> dSlots = new ArrayList<DynamicSlot>();
    for(Interval interval : intervals){
      dSlots.add(new DynamicSlot(period, interval, ep));
    }
    DynamicSlotGroup dynamicSlotGroup = new DynamicSlotGroup(dSlots);

    Interval slot1 = new Interval(33300000,  35400000); //9:15 - 9:50
    DynamicSlot dSlot1 = new DynamicSlot(period, slot1, ep);
    
    DynamicSlot closestSlot1 = dynamicSlotGroup.getMostClosestSlot(dSlot1);
    assertThat(closestSlot1.startAt).isEqualTo(36000000);//10h
    assertThat(closestSlot1.endAt).isEqualTo(37800000);//10:30
    

    DynamicSlot closestSlot2 = dynamicSlotGroup.getMostClosestSlot(dSlot1, closestSlot1);
    assertThat(closestSlot2.startAt).isEqualTo(30600000);//8:30
    assertThat(closestSlot2.endAt).isEqualTo(32400000);//9:00
    
    DynamicSlot closestSlot3 = dynamicSlotGroup.getMostClosestSlot(dSlot1, closestSlot1, closestSlot2);
    assertThat(closestSlot3.startAt).isEqualTo(37800000);//10:30
    assertThat(closestSlot3.endAt).isEqualTo(39600000);//11:30
  }
}

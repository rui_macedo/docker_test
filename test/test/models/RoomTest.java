package test.models;

import static org.fest.assertions.Assertions.assertThat;
import models.Institution;
import models.Room;

import org.junit.Test;

import test.common.BaseWithServer;

public class RoomTest extends BaseWithServer {

  @Test
  public void createRoomTest() {
    Institution inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
    Room room = new Room(inst, "Sala 1");
    room.save();

    Room existingRoom = Room.findById(room.id);
    assertThat(existingRoom).isNotNull();
    assertThat(existingRoom.name).isEqualTo(room.name);
    assertThat(existingRoom.institution).isEqualTo(room.institution);
  }

  @Test
  public void deleteRoomTest() {
    Institution inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
    Room room = new Room(inst, "Sala 1");
    room.save();

    Room existingRoom = Room.findById(room.id);
    assertThat(existingRoom).isNotNull();
    assertThat(existingRoom.name).isEqualTo(room.name);
    assertThat(existingRoom.institution).isEqualTo(room.institution);

    room.delete();

    existingRoom = Room.findById(room.id);
    assertThat(existingRoom).isNull();

  }

  @Test
  public void updateRoomTest() {
    Institution inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
    Room room = new Room(inst, "Sala 1");
    room.save();

    Room existingRoom = Room.findById(room.id);
    assertThat(existingRoom).isNotNull();
    assertThat(existingRoom.name).isEqualTo(room.name);
    assertThat(existingRoom.institution).isEqualTo(room.institution);

    existingRoom.name = "Nova Sala 1";
    existingRoom.update();

    assertThat(existingRoom.name).isNotEqualTo("Sala 1");
    assertThat(existingRoom.name).isEqualTo("Nova Sala 1");

  }

}

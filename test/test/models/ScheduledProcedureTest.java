package test.models;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import models.Equipment;
import models.EquipmentResource;
import models.ExamProcedure;
import models.ExamProcedureTUSS;
import models.Institution;
import models.Patient;
import models.Resource;
import models.Room;
import models.ScheduledOrder;
import models.ScheduledProcedure;
import models.ScheduledProcedureResource;
import models.ScheduledProcedureSchedulingBook;
import models.SchedulingBook;
import models.Section;

import org.junit.Test;

import test.common.BaseWithServer;
import forms.FormSchedulingProcedure;

public class ScheduledProcedureTest extends BaseWithServer {

  @Test
  public void successConstructorTest() {

    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    Patient patient = new Patient(institution, "Patient", "ref01");
    patient.save();

    ScheduledOrder order;
    try {
      order = new ScheduledOrder(patient, institution);
      order.save();
      ExamProcedureTUSS pt = new ExamProcedureTUSS("Tuss", "39393");
      pt.save();
      ExamProcedure ep = new ExamProcedure(institution, "Proc", "393", pt);
      ep.save();
      ScheduledProcedure proc = new ScheduledProcedure(order, ep);
      proc.save();
    } catch (GeneralSecurityException e) {
      fail();
    }
  }

  @Test
  public void errorConstructorTest() {


    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();
    Institution institution2 = new Institution("THTT", "dedasaa", "dedasaea", "03934933943");
    institution2.save();

    Patient patient = new Patient(institution, "Patient", "ref01");
    patient.save();

    ScheduledOrder order;
    try {
      order = new ScheduledOrder(patient, institution2);
      order.save();
      ExamProcedureTUSS pt = new ExamProcedureTUSS("Tuss", "39393");
      pt.save();
      ExamProcedure ep = new ExamProcedure(institution, "Proc", "393", pt);
      ep.save();
      ScheduledProcedure proc = new ScheduledProcedure(order, ep);
      fail("Cant create objects cross tenant!");
    } catch (Exception e) {
      assertThat(e).isInstanceOf(GeneralSecurityException.class);
    }
  }

  @Test
  public void createAndSaveASchedulingTest() {

    FormSchedulingProcedure form = new FormSchedulingProcedure();

    Institution institution = new Institution("THT", "dedea", "dedea", "0393493943");
    institution.save();

    ExamProcedureTUSS pt = new ExamProcedureTUSS("Tuss", "39393");
    pt.save();

    SchedulingBook book = new SchedulingBook(institution, "dddd", 30);
    book.save();

    ExamProcedure ep = new ExamProcedure(institution, "Proc", "393", pt);
    ep.save();

    Patient patient = new Patient(institution, "Patient", "ref01");
    patient.save();

    ScheduledOrder order;
    try {
      order = new ScheduledOrder(patient, institution);
      order.save();

      Section section = new Section(institution, "Section");
      section.save();
      Room room = new Room(institution, "Sala 1");
      room.section = section;
      room.save();
      Equipment equipment = new Equipment(institution, "E", "p", "e", section, room);
      equipment.save();

      Resource res = new EquipmentResource(equipment, institution);
      res.save();

      List<Resource> resL = new ArrayList<Resource>();
      resL.add(res);
      form.book = book;
      form.startAt = 9 * 60 * 60 * 1000;
      form.endAt = 570 * 60 * 1000;
      form.examProcedure = ep;
      // form.referenceDate = DateTime.now().withTimeAtStartOfDay().toDate();
      form.resources = resL;
      ScheduledProcedure sproc = ScheduledProcedure.createAndSaveAScheduling(form, order);
      assertThat(sproc).isNotNull();
      assertThat(sproc.getId()).isNotNull();
      assertThat(ScheduledProcedureSchedulingBook.findByScheduledProcedure(sproc)).isNotNull();
      assertThat(ScheduledProcedureResource.findScheduledProcedureResourceByResource(res))
          .isNotNull();
      assertThat(
          ScheduledProcedureResource.findScheduledProcedureResourceByScheduledProcedure(sproc))
          .isNotNull();
    } catch (GeneralSecurityException e) {
      e.printStackTrace();
    }
  }

}

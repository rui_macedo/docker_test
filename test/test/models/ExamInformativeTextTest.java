package test.models;

import static org.fest.assertions.Assertions.assertThat;
import models.ExamProcedure;
import models.ExamProcedureTUSS;
import models.InformativeText;
import models.InformativeTextType;
import models.Institution;
import models.Insurance;
import models.InsurancePlan;
import models.InsuranceHealthCare;
import models.InsuranceHealthCarePlan;

import org.junit.Before;
import org.junit.Test;

import play.libs.F.Option;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import models.ExamInformativeText;
import test.common.BaseWithServer;

public class ExamInformativeTextTest extends BaseWithServer {

  public static Institution institution;
  
  public static ExamProcedureTUSS examProcedureTUSS;

  public static InsuranceHealthCare insuranceHealthCare;

  public static InsuranceHealthCarePlan insuranceHealthCarePlan;
  
  public static Insurance insurance;
   
  public static InsurancePlan insurancePlan;

  public static ExamProcedure examProcedure;

  public static InformativeTextType informativeTextType;
  
  public static InformativeText informativeText;

  @Before
  public void createInstitution() {
    institution = new Institution("Test", "Test", "Test", "Test");
    institution.save();
    
    insuranceHealthCare = new InsuranceHealthCare("123", "12333", "UNIMED", "UNIMED SA");
    insuranceHealthCare.save();
    
    insuranceHealthCarePlan = new InsuranceHealthCarePlan(insuranceHealthCare, "123", "PLANO MASTER");
    insuranceHealthCarePlan.save();
    
    insurance = new Insurance(institution, "Name", "Description");
    insurance.save();
    
    insurancePlan = new InsurancePlan(institution, "Name", insurance);
    insurancePlan.save();

    examProcedureTUSS = new ExamProcedureTUSS("TUSS", "123");
    examProcedureTUSS.save();

    examProcedure = new ExamProcedure(institution,"Exame1","111",examProcedureTUSS);
    examProcedure.save();

    informativeTextType = new InformativeTextType(institution,"Tipo 1");
    informativeTextType.save();
    
    informativeText = new InformativeText(institution,"Texto",informativeTextType);
    informativeText.save();
  }

  @Test
  public void createExamInformativeText() {
	  
    ExamInformativeText examinformativetext = new ExamInformativeText(institution,informativeText, insurance, insurancePlan, examProcedure);
    examinformativetext.save();

    ExamInformativeText existent = ExamInformativeText.findById(examinformativetext.id);
    assertThat(existent).isNotNull();
  }

  @Test
  public void deleteExamInformativeText() {

    ExamInformativeText examinformativetext = new ExamInformativeText(institution,informativeText, insurance, insurancePlan, examProcedure);
    examinformativetext.save();

    ExamInformativeText existent = ExamInformativeText.findById(examinformativetext.id);
    assertThat(existent).isNotNull();

    existent.delete();
    existent = ExamInformativeText.findById(examinformativetext.id);
    assertThat(existent).isNull();
  }

  @Test
  public void updateExamInformativeText() {
    ExamInformativeText examinformativetext = new ExamInformativeText(institution,informativeText, insurance, insurancePlan, examProcedure);
    examinformativetext.save();

    ExamInformativeText existent = ExamInformativeText.findById(examinformativetext.id);
    assertThat(existent).isNotNull();
    //existent.name = "nameUpdated";

    existent.update();

    existent = ExamInformativeText.findById(examinformativetext.id);
    assertThat(existent).isNotNull();
    //assertThat(existent.name).isEqualTo("nameUpdated");
  }

  @Test
  public void successAtQueryBind() {
    ExamInformativeText examinformativetext = new ExamInformativeText(institution,informativeText, insurance, insurancePlan, examProcedure);
    examinformativetext.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("examinformativetext", ids);
    Option<ExamInformativeText> examinformativetextOpt = examinformativetext.bind("", data);
    assertThat(examinformativetextOpt.get()).isEqualTo(examinformativetext);
  }

  @Test
  public void failAtQueryBind() {
    ExamInformativeText examinformativetext = new ExamInformativeText(institution,informativeText, insurance, insurancePlan, examProcedure);
    examinformativetext.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("examinformativetext", ids);
    Option<ExamInformativeText> examinformativetextOpt = examinformativetext.bind("", data);
    assertThat(examinformativetextOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("examinformativetext", ids2);
    Option<ExamInformativeText> examinformativetextOpt2 = examinformativetext.bind("", data2);
    assertThat(examinformativetextOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    ExamInformativeText examinformativetext = new ExamInformativeText(institution,informativeText, insurance, insurancePlan, examProcedure);
    examinformativetext.save();

    ExamInformativeText testingExamInformativeText = examinformativetext.bind("", "1");
    assertThat(testingExamInformativeText).isEqualTo(examinformativetext);
  }

  @Test
  public void failAtPathBind() {
    ExamInformativeText examinformativetext = new ExamInformativeText(institution,informativeText, insurance, insurancePlan, examProcedure);
    examinformativetext.save();

    ExamInformativeText testingExamInformativeText = examinformativetext.bind("", "opa");
    assertThat(testingExamInformativeText).isNull();
  }


  @Test
  public void testFinder() {
    ExamInformativeText examinformativetext = new ExamInformativeText(institution,informativeText, insurance, insurancePlan, examProcedure);
    examinformativetext.save();

    ExamInformativeText examinformativetext2 = new ExamInformativeText(institution,informativeText, insurance, insurancePlan, examProcedure);
    examinformativetext2.save();

    ExamInformativeText testingExamInformativeText = ExamInformativeText.findById(examinformativetext.id);
    assertThat(testingExamInformativeText).isNotNull();

    ExamInformativeText testingNull = ExamInformativeText.findById(null);
    assertThat(testingNull).isNull();

    ExamInformativeText testing213 = ExamInformativeText.findById(213L);
    assertThat(testing213).isNull();
    
     ArrayList<Long> examProcedures = new ArrayList<Long>();
    examProcedures.add(examProcedure.id);
    
    List<ExamInformativeText> all = ExamInformativeText.findAllByInstitution(institution.id, insurance.id, insurancePlan.id, examProcedures);
    /*Remember it has no double texts*/
    assertThat(all).hasSize(1);
  }
}

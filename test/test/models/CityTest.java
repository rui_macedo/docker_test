package test.models;

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.City;
import models.Country;
import models.State;

import org.junit.Test;

import play.libs.F.Option;
import test.common.BaseWithServer;

public class CityTest extends BaseWithServer {

  // util to test in another class
  public static City createFakeCity() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();
    return city;
  }

  @Test
  public void successAtCreateCity() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(23l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();

    // reload address
    city = City.findById(city.id);
    assertThat(city).isNotNull();
    assertThat(city.getId()).isNotNull();
    assertThat(city.originalId).isEqualTo(2l);
    assertThat(city.name).isEqualTo("City");
    assertThat(city.state).isEqualTo(st);
  }

  @Test
  public void successAtQueryBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"1"};
    data.put("city", ids);
    Option<City> cityOpt = city.bind("", data);
    assertThat(cityOpt.get()).isEqualTo(city);
  }

  @Test
  public void failAtQueryBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();

    Map<String, String[]> data = new HashMap<String, String[]>();
    String[] ids = {"d"};
    data.put("city", ids);
    Option<City> cityOpt = city.bind("", data);
    assertThat(cityOpt.isEmpty()).isTrue();

    Map<String, String[]> data2 = new HashMap<String, String[]>();
    String[] ids2 = {"10"};
    data2.put("city", ids2);
    Option<City> cityOpt2 = city.bind("", data2);
    assertThat(cityOpt2.isEmpty()).isTrue();
  }

  @Test
  public void successAtPathBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();

    City cityrict = city.bind("", "1");
    assertThat(cityrict).isEqualTo(city);
  }

  @Test
  public void failAtPathBind() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();

    City cityrict = city.bind("", "d");
    assertThat(cityrict).isNull();
    cityrict = city.bind("", "34");
    assertThat(cityrict).isNull();
  }

  @Test
  public void testFinders() {
    Country c = new Country(1l, "Country", "CT");
    c.save();
    State st = new State(2l, "State", c);
    st.save();
    City city = new City(2l, "City", st);
    city.save();

    City city2 = new City(2l, "City3", st);
    city2.save();


    City bdDist = City.findById(city.id);
    assertThat(bdDist).isNotNull();

    bdDist = City.findById(null);
    assertThat(bdDist).isNull();

    bdDist = City.findById(23l);
    assertThat(bdDist).isNull();

    List<City> cityricts = City.findAllByState(st);
    assertThat(cityricts).isNotEmpty().hasSize(2).contains(city, city2);

    List<City> cityrictsNull = City.findAllByState(null);
    assertThat(cityrictsNull).isEmpty();
  }
}

package test.models;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import models.MedicalCouncil;
import models.Physician;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import test.common.BaseWithServer;

import com.avaje.ebean.Ebean;

@RunWith(Parameterized.class)
public class PhysicianFindByCRMAndUFTest extends BaseWithServer {

  private static final String DEFAULT_UF = "UF";
  private String persistedCRM;
  private String findByCRM;

  @Parameters(name="actual: crm = {0} - find by: crm = {1}")
  public static Collection<Object[]> data() {
      return Arrays.asList(new Object[][] {     
               { "12-3", "12-3" },
               { "0123", "123" },
               { "012.-3", "12-3" },
               { "012-3", "12-3" },
               { "01.2-3.", "01.2-3." },
               { "01*2@-3!", "01*2@-3" },
               { "00012-3", "12-3" },
               { "12-3", "012-3" },
               { "12-3", "00012-3" },
               { "012-3", "00012-3" },
               { "00012-3", "012-3" },
               { "00012-3", "00012-3" },
         });
  }
  
  @Before
  public void cleanItUp() {
    Ebean.delete(Physician.all());
  }
  
  public PhysicianFindByCRMAndUFTest(String persistedCRM, String findByCRM) {
    this.persistedCRM = persistedCRM;
    this.findByCRM = findByCRM;
  }
  
  @Test
  public void test() {
    MedicalCouncil mc = new MedicalCouncil("name", "name");
    mc.save();
    Physician physician = new Physician("Name", mc,persistedCRM, null);
    physician.uf = DEFAULT_UF;
    physician.save();
    
    
    assertEquals(physician, Physician.findByMedicalCouncilRegistrationAndUF(findByCRM, DEFAULT_UF));
  }
  
}
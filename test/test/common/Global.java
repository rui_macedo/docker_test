package test.common;

import actors.QueueReader;
import com.amazonaws.services.sqs.model.Message;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import com.pixeon.cloudsecure.remote.authorization.CloudCredentialsProvider;
import com.pixeon.cloudsecure.remote.authorization.MockAuthProvider;
import common.annotations.BaseSimpleModelFormatterImpl;
import common.annotations.BaseTenantModelFormatterImpl;
import common.utils.SqlUtils;
import controllers.*;
import mock.SQSMessageQueueService;
import models.BaseSimpleModel;
import models.BaseTenantModel;
import org.mockito.Mockito;
import play.Application;
import play.GlobalSettings;
import play.Play;
import play.data.format.Formatters;
import play.libs.ws.WSClient;
import services.InsuranceServiceImpl;
import services.PatientServiceImpl;
import services.SchedulingServiceImpl;
import services.api.ContactService;
import services.api.InsurancePlanService;
import services.api.InsuranceService;
import services.api.SchedulingFilterService;
import services.api.SchedulingService;
import services.api.SlotService;
import services.SlotServiceImpl;
import services.configuration.DefaultReservationConfiguration;
import services.configuration.SchedulingReservationConfiguration;
import services.external.api.EmployeeData;
import services.external.api.ExamProcedureService;
import services.external.api.QueueService;
import services.external.impl.ExamProcedureServiceImpl;
import services.external.impl.SQSQueueService;
import services.patient.PatientService;

import static com.google.common.base.Preconditions.checkNotNull;

import controllers.Slots;
import services.searchers.SchedulingFilterServiceImpl;


/**
 * 
 * {@link http://www.playframework.com/documentation/2.2.x/JavaGlobal}
 * 
 * @author abner.rolim
 * @author ariel.pires
 * 
 */
public class Global extends GlobalSettings {

  private final Long currentEmployee = 1L;
  private Injector injector;
  private WSClient wsClientMock;
  private SQSMessageQueueService queueServiceMock;

  /*
   * (non-Javadoc)
   * 
   * @see play.GlobalSettings#onStart(play.Application) used to create the admin user on developer
   * mode
   */
  @Override
  public void onStart(Application app) {

    Formatters.register(BaseSimpleModel.class, new BaseSimpleModelFormatterImpl());
    Formatters.register(BaseTenantModel.class, new BaseTenantModelFormatterImpl());
    SqlUtils.createDDLToMemoryDB();

    injector = Guice.createInjector(new AbstractModule() {

      @Override
      protected void configure() {
        bind(new TypeLiteral<QueueService<Message>>() {}).toInstance(getQueueServiceMock());
        bind(WSClient.class).toInstance(getWSClientMock());
        bind(EmployeeData.class).toInstance(getSecureCall());
        bind(CloudCredentialsProvider.class).to(MockAuthProvider.class);
        bind(SlotService.class).to(SlotServiceImpl.class);
        bind(ContactService.class).toInstance(Mockito.mock(ContactService.class));
        bind(InsuranceService.class).toInstance(Mockito.mock(InsuranceService.class));
        bind(InsurancePlanService.class).toInstance(Mockito.mock(InsurancePlanService.class));
        bind(SchedulingService.class).toInstance(Mockito.mock(SchedulingService.class));
        bind(services.api.PatientService.class).to(PatientServiceImpl.class);
        bind(services.patient.PatientService.class).toInstance(getPatientService());
        bind(ExamProcedureService.class).toInstance(Mockito.mock(ExamProcedureService.class));
        bind(SchedulingFilterService.class).to(SchedulingFilterServiceImpl.class);
        bind(SchedulingReservationConfiguration.class).toInstance(getReservationConfig());
        requestStaticInjection(Insurances.class, Slots.class, SQSQueueService.class,
                QueueReader.class, Contacts.class, InsuranceServiceImpl.class, InsurancePlans.class,
                ContactPatients.class, PrePatients.class, Schedulings.class, Reservations.class,
                SchedulingServiceImpl.class);
      }
    });

    super.onStart(app);
  }

  public QueueService<Message> getQueueServiceMock() {
    if (this.queueServiceMock == null) {
      this.queueServiceMock = Mockito.mock(SQSMessageQueueService.class);
    }
    return this.queueServiceMock;
  }

  protected services.patient.PatientService getPatientService(){
    return new Mockito().mock(services.patient.PatientService.class);
  }

  public WSClient getWSClientMock() {
    if (this.wsClientMock == null) {
      this.wsClientMock = Mockito.mock(WSClient.class);
    }
    return this.wsClientMock;
  }

  public Injector getInjector() {
    checkNotNull(this.injector, "Injector is null");
    return this.injector;
  }

  @Override
  public <T> T getControllerInstance(Class<T> aClass) throws Exception {
    return injector.getInstance(aClass);
  }

  protected EmployeeData getSecureCall() {
    return Mockito.mock(EmployeeData.class);
  }

  protected SchedulingReservationConfiguration getReservationConfig() {
    return new DefaultReservationConfiguration(
            Play.application().configuration()
                    .getBoolean("scheduling.reservation.auto.email.enable", false)
    );

  }

}

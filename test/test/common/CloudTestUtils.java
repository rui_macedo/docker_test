package test.common;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import play.api.mvc.Call;
import play.filters.csrf.CSRF;
import play.filters.csrf.CSRFFilter;
import play.mvc.HandlerRef;
import play.mvc.Result;
import play.test.FakeApplication;
import play.test.FakeRequest;
import play.test.Helpers;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebeaninternal.server.ddl.DdlGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.pixeon.cloudsecure.token.TestTokenUtils;
import com.pixeon.cloudsecure.utils.CloudSecureUtils;

import common.constants.IHttpHeaderParamKeys;
import common.utils.DateUtils;


/**
 * Base class for tests. Provide helpers Manages the life cycle (starts and stop) of a fake app with
 * in-memory database. All modifications on models will be applied automatically by creating a new
 * ddl file. Can execute a sql file to previously populate the database. (located by default at
 * "test/ddl/HSQL_INSERT.sql") All those features will be executed for every test class that extends
 * this class
 * 
 * @author abner.rolim
 */
public class CloudTestUtils extends Helpers {

  public static DdlGenerator ddl;
  public static EbeanServer server;
  public static FakeApplication app;

  public static String authParams;

  public static int FAKE_BROWSER_PORT = 3333;
  public static String FAKE_BROWSER_BASE_URL = "http://localhost:" + FAKE_BROWSER_PORT;


  /**
   * call a fakeRequest to CSRF protected controllers methods that came from a form post to call
   * secured methods, you have to call {@link CloudTestUtils#makeAppLogin(String, String)} first
   * into your test
   * 
   * @param method the controller method to be called. You can resolve using
   *        controllers.routes.ref.[controllerClassName].[method]
   * @param formData The form with filled with data to send to method.
   * @return the result of callAction {@link Helpers#callAction(HandlerRef, play.test.FakeRequest)}
   */
  public static Result callFormActionWithCSRF(HandlerRef method, Map<String, String> formData,
      Call call) {
    return callActionWithCSRF(method, fakeRequest(call).withFormUrlEncodedBody(formData));
  }
  
  /**
   * call a fakeRequest to CSRF protected controllers methods that came from a form post to call
   * secured methods, you have to call {@link CloudTestUtils#makeAppLogin(String, String)} first
   * into your test
   * 
   * @param method the controller method to be called. You can resolve using
   *        controllers.routes.ref.[controllerClassName].[method]
   * @param formData The json with filled with data to send to method.
   * @return the result of callAction {@link Helpers#callAction(HandlerRef, play.test.FakeRequest)}
   */
  public static Result callJsonFormActionWithCSRF(HandlerRef method, JsonNode formData,
      Call call) {
    return callActionWithCSRF(method, fakeRequest(call).withJsonBody(formData));
  }

  /**
   * call a fakeRequest to CSRF protected controllers methods without any data (simple GET) to call
   * secured methods, you have to call {@link CloudTestUtils#makeAppLogin(String, String)} first if
   * you intent access a protected method
   * 
   * @param method the controller method to be called. You can resolve using
   *        controllers.routes.ref.[controllerClassName].[method]
   * @return the result of callAction {@link Helpers#callAction(HandlerRef, play.test.FakeRequest)}
   */
  public static Result callSimpleActionWithCSRF(HandlerRef method, Call call) {
    return callActionWithCSRF(method, fakeRequest(call));
  }

  /**
   * call a custom fakeRequest and add Authorization Header
   * <p>
   * it's used to call protected controllers methods to call secured methods, you have to call
   * {@link CloudTestUtils#makeAppLogin(String, String)} first if you intent access a protected
   * method
   * 
   * @param method the controller method to be called. You can resolve using
   *        controllers.routes.ref.[controllerClassName].[method]
   * @param customFakeRequest customized {@link FakeRequest}
   * @return the result of callAction {@link Helpers#callAction(HandlerRef, play.test.FakeRequest)}
   */
  public static Result callActionWithCSRF(HandlerRef method, FakeRequest customFakeRequest) {
    /*
     * Apply cookies first to FakeRequest and after, the session. If you invert this order, you will
     * generate a error on tests (CSRF Token is missing) Probably a Cookies overrides the session
     */
    if (authParams != null) {
      customFakeRequest.withHeader(IHttpHeaderParamKeys.AUTHORIZATION,
          getCurrentAuthorizationHeaderValue());
    }
    FakeRequest fkReq =
        customFakeRequest.withSession(CSRF.TokenName(), CSRFFilter.apply$default$5()
            .generateToken());
    return callAction(method, fkReq);
  }


  public static String getCurrentAuthorizationHeaderValue() {
    if (authParams != null) {
      return CloudSecureUtils.getAuthorizationHeaderValue(authParams);
    }
    return null;
  }

  public static void fakeAuthByCloudAuthentication(Long userId) {
    fakeAuthByCloudAuthenticationWithVerifyOnTenant(userId, 1l);
  }

  public static void fakeAuthByCloudAuthenticationWithVerifyOnTenant(Long userId, Long tenantId) {
    fakeAuthByCloudSecure(userId, tenantId);
  }
  
  public static void logout(){
    authParams = null;
  }

  public static void fakeAuthByCloudSecure(Long userId, Long tenantId) {
    List<String> aud = Collections.emptyList();
    List<String> tenants = new ArrayList<String>();
    tenants.add(tenantId.toString());
    DateTime expirationTime = DateUtils.nowDateTimePlusMinutes(10);
    DateTime notBeforeTime = DateUtils.nowDateTimeMinusMinutes(10);
    authParams = TestTokenUtils.createSignedAccessTokenHeader(1l, userId,tenants , DateTime.now(), expirationTime, notBeforeTime, 1l);
  }

 
}

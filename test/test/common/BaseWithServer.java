package test.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.SocketUtils;

import play.test.FakeApplication;
import play.test.Helpers;
import play.test.WithServer;

public class BaseWithServer extends WithServer {
  
  int currentPort;
  
  @Override
  protected FakeApplication provideFakeApplication() {
    final Map<String, Object> initialConfig = new HashMap<>();
    // puts database config into server config. you can overwrite all default configs of
    // application.conf
    initialConfig.putAll(Helpers.inMemoryDatabase());
    // set this for true if you want see all the sql statements
    initialConfig.put("ebean.default", "models.*");
    initialConfig.put("db.default.logStatements", false);
    initialConfig.put("logger.com.jolbox", "ERROR");
    initialConfig.put("evolutionplugin", "disabled");
    //need to put user and pass to hikariDB
    initialConfig.put("cloudsecure.security.scopes.verifier.disableontest", "true");
    initialConfig.put("cloudsecure.security.insecure.methods.verifier.disableontest", "true");
    //need to put user and pass to hikariDB
    initialConfig.put("db.default.user", "");
    initialConfig.put("db.default.password", "");
    initialConfig.put("swagger.api.basepath", "http://scheduling.with.no.swagger/");
    initialConfig.put("cloudsecure.security.authentication.signature","De234rwrtw5w45tgefr4");
    initialConfig.put("cloudsecure.security.services.signature", "De234rwrtw5w45tgefr4");
    initialConfig.put("slot.booked.expiration.time", "10");
    initialConfig.put("getpatients.api.endpoint", "/v1/patient-data/institution/{institution}/ws/patients/");
    initialConfig.put("smtp.mock", "true");
    List<String> withoutPlugins = Arrays.asList("play.modules.swagger.SwaggerPlugin");
    initialConfig.putAll(provideInitialConfigurations());

    List<String> additionalPlugins = Arrays.asList("com.pixeon.cloudsecure.token.WSSecureCallPlugin");

    FakeApplication app = Helpers.fakeApplication(initialConfig, additionalPlugins, withoutPlugins, new test.common.Global());
    
    return app;
  }

  @Override
  protected int providePort() {
    currentPort = SocketUtils.findAvailableTcpPort();
    return currentPort;
  }
  
  public int getCurrentPort(){
    return currentPort;
  }
  
  public Map<String, Object> provideInitialConfigurations() {
    return new HashMap<String, Object>(); 
  }

}

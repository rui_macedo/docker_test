package test.utils;

import common.constants.SlotStatus;

import dto.SchedulingDTO;
import models.*;

import models.Insurance;
import models.InsurancePlan;
import models.scheduling.Contact;
import models.scheduling.PhoneTypeEnum;
import models.scheduling.Scheduling;

import models.scheduling.SchedulingFilter;
import models.scheduling.SchedulingReservation;
import org.apache.commons.lang3.math.NumberUtils;

import common.constants.CalendarDay;
import common.constants.SchedulingBookSlotStatus;
import common.utils.DateUtils;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class ModelFactory {

  public static final Long DEFAULT_TENANT_ID = 1L;
  public static final Long OTHER_TENANT_ID = 2L;


  public static ExamModality createExamModality(Institution institution) {
    ExamModality examModality = new ExamModality(institution);
    examModality.save();
    return examModality;
  }

  public static ExamProcedure createExamProcedure(Institution institution, ExamModality examModality) {
    ExamProcedure examProcedure = new ExamProcedure(institution, "test", "code-uno");
    examProcedure.examModality = examModality;
    examProcedure.save();
    return examProcedure;
  }

  public static Institution createInstitution(String tenantName) {
    Institution institution = new Institution(tenantName, "INSTTEST", "INSTTEST", "TEST");
    institution.save();
    return institution;
  }

  public static Insurance createInsurance(Institution institution) {
    Insurance insurance = new Insurance(institution, "Name", "Description");
    insurance.save();
    return insurance;
  }

  public static Restriction createAndSaveRestriction(Institution institution, String name) {
    Restriction rest = new Restriction();
    rest.name = name;
    rest.institution = institution;
    rest.save();
    return rest;
  }

  public static SchedulingBook createAndSaveSchedulingBook(Institution institution, Unity unity) {
    SchedulingBook book = new SchedulingBook();
    book = new SchedulingBook();
    book.name = "Name";
    book.slotSizeMin = 20;
    book.institution = institution;
    book.unity = unity;
    book.isDeleted = false;
    book.statusSlots = SchedulingBookSlotStatus.WAITING;
    book.schedulingBookRestriction = new ArrayList<SchedulingBookRestriction>();
    book.schedulingBookRestriction.add(new SchedulingBookRestriction(createAndSaveRestriction(institution, "Name Restriction")));
    book.schedulingBookWeekAgendas = new HashSet<SchedulingBookWeekAgenda>();
    book.save();
    SchedulingBookWeekAgenda week = new SchedulingBookWeekAgenda(institution, book, CalendarDay.FRI);
    week.save();
    week.schedulingBookPeriods = new HashSet<SchedulingBookPeriod>();
    SchedulingBookPeriod period = new SchedulingBookPeriod(institution, 36600000, 64800000, week);
    period.save();
    week.schedulingBookPeriods.add(period);
    week.save();
    book.schedulingBookWeekAgendas.add(week);
    book.save();
    return book;
  }

  public static Address createAddress() {
    Address address = new Address();
    address.name = "Foxwood street, 10";
    address.save();
    return address;
  }

  public static Unity createUnity(Institution tenant, String name, String code) {
    Unity unity = new Unity(tenant, name, ModelFactory.createAddress(), 111, code);
    unity.save();
    return unity;
  }

  public static Unity createUnity(Institution tenant) {
    Unity unity = new Unity(tenant, "unity", ModelFactory.createAddress(), 111, "unityCode");
    unity.save();
    return unity;
  }
  
  public static Unity createUnity(Institution tenant, String unityCode) {
    Unity unity = new Unity(tenant, "unity", ModelFactory.createAddress(), 111, unityCode);
    unity.save();
    return unity;
  }
  
  public static Unity unityCreateRequestBuilder(Institution tenant, Address address) {
    Unity unity = new Unity(tenant, "unity", address, NumberUtils.INTEGER_ONE, "unityCode");
    unity.latitude = "000";
    unity.longitude = "000";
    unity.businessSocialName = "social";
    unity.cnpj = "11111";
    unity.telephone = "1111-1111";
    unity.openingHours = "8";
    return unity;
  }
  
  public static Unity unityCreateRequestBuilder(Institution tenant, Address address, String code) {
    Unity unity = new Unity(tenant, "unity", address, NumberUtils.INTEGER_ONE, code);
    unity.latitude = "000";
    unity.longitude = "000";
    unity.businessSocialName = "social";
    unity.cnpj = "11111";
    unity.telephone = "1111-1111";
    unity.openingHours = "8";
    return unity;
  }

  public static Unity unityCreateRequestBuilder(Institution tenant,String name) {
    Unity unity = new Unity(tenant, name, ModelFactory.createAddress(), NumberUtils.INTEGER_ONE, "unityCode");
    unity.latitude = "000";
    unity.longitude = "000";
    unity.businessSocialName = "social";
    unity.cnpj = "11111";
    unity.telephone = "1111-1111";
    unity.openingHours = "8";
    return unity;
  }

  public static Map<String, String> unityCreateBadRequestBuilder(Institution tenant, Address address) {
    Map<String, String> badRequest = new HashMap<>();
    badRequest.put("error", "unity");
    badRequest.put("institution", tenant.id.toString());
    badRequest.put("address", address.id.toString());
    badRequest.put("addressNumber", "1");
    badRequest.put("latitude", "000");
    badRequest.put("longitude", "000");
    badRequest.put("telephone", "1111-1111");
    badRequest.put("openingHours", "8");
    badRequest.put("unityCode", "unityCode");
    badRequest.put("cnpj", "11111");
    badRequest.put("businessSocialName", "social");
    return badRequest;
  }

  public static Map<String, String> examCategoryCreateBadRequestBuilder(Institution tenant) {

    Map<String, String> formBadRequest = new HashMap<>();
    formBadRequest.put("error", "ExamCategory");
    formBadRequest.put("institution", tenant.id.toString());
    return formBadRequest;

  }


  public static Map<String, String> examCategoryUpdateRequestBuilder(Institution tenant) {
    Map<String, String> formRequest = new HashMap<>();
    formRequest.put("name", "UPDATED");
    formRequest.put("institution", tenant.id.toString());
    return formRequest;
  }

  public static Map<String, String> examCategoryUpdateBadRequestBuilder(Institution tenant) {
    Map<String, String> badRequest = new HashMap<>();
    badRequest.put("name", "UPDATED");
    badRequest.put("id", "bla");
    badRequest.put("institution", tenant.id.toString());
    badRequest.put("error", "unity");
    return badRequest;
  }

  public static Slot createAndSaveSlot(SchedulingBook book) {

    Slot slot = new Slot();
    slot.book = book;
    slot.institution = book.institution;
    slot.isDeleted = false;
    slot.unity = book.unity;
    slot.name = book.name;
    slot.startAt = new DateTime();
    slot.endAt = new DateTime().plusMinutes(20);
    slot.status = SlotStatus.AVAILABLE;
    slot.save();
    return slot;

  }

  public static ExamProcedure createExamProcedure(Institution institution, ExamModality examModality, String name, String code) {
    ExamProcedure examProcedure = new ExamProcedure(institution, name, code);
    examProcedure.examModality = examModality;
    examProcedure.save();
    return examProcedure;
  }
  
  public static InsurancePlan createInsurancePlan(Institution institution, Insurance insurance, ProcedureTable prota){
    InsurancePlan insurancePlan = new InsurancePlan(institution, insurance);
    insurancePlan.name = "insuPlan";
    insurancePlan.procedureTable = prota;
    insurancePlan.save();
    return insurancePlan;
  }

  public static ProcedureTable createProcedureTable(Institution institution) {
    ProcedureTable proTab = new ProcedureTable(institution, "ProTabl");
    proTab.save();
    return proTab;
  }

  public static SchedulingDTO createSchedulingDTO(Long tenant){
    SchedulingDTO dto = new SchedulingDTO();
    dto.institutionId = tenant;
    dto.id = NumberUtils.LONG_ONE;
    dto.employeeId = NumberUtils.LONG_ONE;
    dto.creationDate = DateUtils.getCurrentDateTimeGMTZero();
    dto.updatedDate  = DateUtils.getCurrentDateTimeGMTZero();
    return dto;
  }

  public static Scheduling createAndSaveScheduling(Long institutionId, Long employeeId){
    Scheduling scheduling = new Scheduling();
    scheduling.employeeId = employeeId;
    scheduling.institutionId = institutionId;
    scheduling.notes = "You are awesome";
    scheduling.save();
    return scheduling;
  }


  public static Scheduling createAndSaveScheduling(Institution institution, Long employeeId){
    Scheduling scheduling = new Scheduling();
    scheduling.employeeId = employeeId;
    scheduling.contact = createAndSaveContact(institution.id, "You are pretty");
    scheduling.patientId = createAndSavePatient(institution, "You are a bitch", "100000").id;
    scheduling.institutionId = institution.id;
    scheduling.notes = "You are awesome";
    scheduling.save();
    return scheduling;
  }

  public static Contact createAndSaveContact(Long institution, String name){
    Contact contact = new Contact();
    contact.institutionId = institution;
    contact.name = name;
    contact.phone = "555555";
    contact.phoneType = PhoneTypeEnum.HOME;
    contact.save();
    return contact;

  }

  public static SchedulingFilter createAndSaveSchedulingFilter(Scheduling scheduling, SchedulingFilter.Type type, String value) {
    SchedulingFilter filter = new SchedulingFilter();
    filter.filterType = type;
    filter.filterValue = value;
    filter.institutionId = scheduling.institutionId;
    filter.scheduling = scheduling;
    return filter;
  }
  public static Patient createAndSavePatient(Institution tenant, String name, String internalId){

    Patient patient = new Patient(tenant, name, internalId);
    patient.active = true;
    patient.sex = "M";
    patient.save();
    return patient;

  }

}

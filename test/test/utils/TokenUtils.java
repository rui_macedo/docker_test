package test.utils;

import com.pixeon.cloudsecure.token.TestTokenUtils;

import java.util.List;
import java.util.Map;

public class TokenUtils {

  public static String createSignedAccessTokenHeader(Map<String, List<String>> tenantServicesScopes, Long institutionID) {
    return createSignedAccessTokenHeader(tenantServicesScopes, 1l, 20l, institutionID);
  }
  
  public static String createSignedAccessTokenHeader(Map<String, List<String>> tenantServicesScopes, Long issuerId, Long institutionID) {
    return createSignedAccessTokenHeader(tenantServicesScopes, issuerId, 20l, institutionID);
  }
  
  public static String createSignedAccessTokenHeader(Map<String, List<String>> tenantServicesScopes, Long issuerId, Long subjectId, Long institutionID) {
    return TestTokenUtils.createSignedAccessTokenHeader(subjectId, institutionID);
  }


}

package test.utils;

import com.pixeon.cloudsecure.constants.IHttpHeaderParamKeys;

import java.util.function.Consumer;

import play.libs.Json;
import play.mvc.HandlerRef;
import play.mvc.Result;
import play.test.FakeRequest;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static play.test.Helpers.UNAUTHORIZED;
import static play.test.Helpers.callAction;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;
import static play.test.Helpers.status;
import static test.utils.AuthorizationUtils.createAuthScope;
import static test.utils.TokenUtils.createSignedAccessTokenHeader;

public class AssertionUtils {

    public static void assertNonAuthorizedRequestReturnsNotAuthorized(String requestType, String url) {
      FakeRequest fakeRequest = fakeRequest(requestType, url);
      Result result = route(fakeRequest);
      assertEquals(UNAUTHORIZED, status(result));
    }

    public static void assertNonAuthorizedRequestReturnsNotAuthorized(String requestType, String url, Object body) {
        FakeRequest fakeRequest = fakeRequest(requestType, url);
        fakeRequest.withJsonBody(Json.toJson(body));
        Result result = route(fakeRequest);
        assertEquals(UNAUTHORIZED, status(result));
    }
    
    private static FakeRequest createFakeRequestWithSignedToken(String requestType, String requestUrl, String signedToken) {
       return fakeRequest(requestType, requestUrl).withHeader(IHttpHeaderParamKeys.AUTHORIZATION, signedToken);
    }
    
    public static Result performAuthorizedRequest(Long institution, String scope, String requestType, String requestUrl, HandlerRef actionUrl) {
      FakeRequest fkRequest = createFakeRequestWithSignedToken(requestType, requestUrl, createSignedAccessTokenHeader(createAuthScope(scope), institution));
      return callAction(actionUrl, fkRequest);
    }
    
    public static Result performAuthorizedRequest(Long institution, String scope, String requestType, String requestUrl, Object body, HandlerRef actionUrl) {
      FakeRequest fkRequest = createFakeRequestWithSignedToken(requestType, requestUrl, createSignedAccessTokenHeader(createAuthScope(scope), institution));
      fkRequest.withJsonBody(Json.toJson(body));
      return callAction(actionUrl, fkRequest);
    }
    
    public static void assertAuthorizedRequestReturnsExpectedValue(Long institution, String scope, String requestType, String requestUrl, HandlerRef actionUrl, Integer expectedCode) {
      assertAuthorizedRequestWithLambda(institution, scope, requestType, requestUrl, actionUrl, result -> assertThat(status(result)).isEqualTo(expectedCode));
    }
    
    public static void assertAuthorizedRequestReturnsExpectedValue(Long institution, String scope, String requestType, String requestUrl, Object body, HandlerRef actionUrl, Integer expectedCode) {
      assertAuthorizedRequestWithLambda(institution, scope, requestType, requestUrl, body, actionUrl, result -> assertThat(status(result)).isEqualTo(expectedCode));
    }
    
    public static void assertAuthorizedRequestWithLambda(Long institution, String scope, String requestType, String requestUrl, Object body, HandlerRef actionUrl, Consumer<Result> assertLambda) {
      assertLambda.accept(performAuthorizedRequest(institution, scope, requestType, requestUrl, body, actionUrl));
    }
    
    public static void assertAuthorizedRequestWithLambda(Long institution, String scope, String requestType, String requestUrl, HandlerRef actionUrl, Consumer<Result> assertLambda) {
      assertLambda.accept(performAuthorizedRequest(institution, scope, requestType, requestUrl, actionUrl));
    }
}

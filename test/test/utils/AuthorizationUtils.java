package test.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthorizationUtils {

  public static Map<String, List<String>> createAuthScope(String scopeName) {
    return createAuthScope(scopeName, ModelFactory.DEFAULT_TENANT_ID);
  }

  public static Map<String, List<String>> createAuthScope(String scopeName, long tenant) {
    Map<String, List<String>> tenantServicesScopes = new HashMap<String, List<String>>();

    List<String> scopes = new ArrayList<String>();
    scopes.add(scopeName);
    tenantServicesScopes.put(String.valueOf(tenant), scopes);
    return tenantServicesScopes;
  }
}

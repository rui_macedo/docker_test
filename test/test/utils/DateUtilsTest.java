package test.utils;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Test;

import test.common.BaseWithServer;

import common.utils.DateUtils;

public class DateUtilsTest extends BaseWithServer {
  public static int slot13min = 13 * 60 * 1000;
  public static int slot30min = 30 * 60 * 1000;

  @Test
  public void suceessAtsplitIntervalTest() {
    Interval interval1 = new Interval(32400000l, 51600000l);// 9h - 14:20

    List<Interval> intervals = DateUtils.splitInterval(interval1, slot13min);
    assertThat(intervals).hasSize(24);
    for (Interval interval : intervals) {
      assertThat(interval.toDurationMillis()).isEqualTo(slot13min);
    }
  }

  @Test
  public void suceessAtconvertIntervalToCurrentChronologyTest() {
    Interval interval1 = DateUtils.convertIntervalToCurrentChronology(32400000l, 51600000l);// 9h -
                                                                                            // 14:20
    assertThat(interval1.getEnd().toLocalDate()).isEqualTo(DateTime.now().toLocalDate());
  }

  @Test
  public void errorAtoverlapIntervalsWithSlotRoundWithNotOvelapIntervalsTest() {
    Interval interval1 = new Interval(32400000l, 51600000l);// 9h - 14:20
    Interval interval2 = new Interval(51600000l, 58500000);// 14:20h - 16:15
    Interval needToBeNull =
        DateUtils.overlapIntervalsWithSlotRound(interval1, interval2, slot30min);
    assertThat(needToBeNull).isNull();
  }

  @Test
  public void successAtoverlapIntervalsWithSlotRoundWithEqualsIntervalsTest() {
    Interval interval1 = new Interval(32400000l, 51600000l);// 9h - 14:20
    Interval interval2 = new Interval(32400000l, 51600000l);// 9h - 14:20
    Interval igualInterval =
        DateUtils.overlapIntervalsWithSlotRound(interval1, interval2, slot30min);
    assertThat(igualInterval).isEqualTo(interval1);
    assertThat(igualInterval).isEqualTo(interval2);
  }

  @Test
  public void successAtoverlapIntervalsWithSlotRoundWithOverlapedIntervalsTest() {
    Interval referenceInterval = new Interval(32400000l, 62400000l);// 9h - 17:20
    Interval interval1 = new Interval(41520000l, 58620000l);// 11:32h - 16:17
    Interval result1 =
        DateUtils.overlapIntervalsWithSlotRound(referenceInterval, interval1, slot30min); // 16
                                                                                          // slots
                                                                                          // of
                                                                                          // 30min
    assertThat(result1.getStartMillis()).isEqualTo(12 * 60 * 60 * 1000); // 12h
    assertThat(result1.getEndMillis()).isEqualTo(16 * 60 * 60 * 1000); // 16h

    Interval interval2 = new Interval(29700000l, 58620000l);// 8:15h - 16:17
    Interval result2 =
        DateUtils.overlapIntervalsWithSlotRound(referenceInterval, interval2, slot30min); // 16
                                                                                          // slots
                                                                                          // of
                                                                                          // 30min
    assertThat(result2.getStartMillis()).isEqualTo(referenceInterval.getStartMillis()); // 12h
    assertThat(result2.getEndMillis()).isEqualTo(16 * 60 * 60 * 1000); // 16h

    Interval interval3 = new Interval(37800000l, 65820000l);// 10:30h - 18:17
    Interval result3 =
        DateUtils.overlapIntervalsWithSlotRound(referenceInterval, interval3, slot30min); // 16
                                                                                          // slots
                                                                                          // of
                                                                                          // 30min
    assertThat(result3.getStartMillis()).isEqualTo(37800000l); // 10:30h
    assertThat(result3.getEndMillis()).isEqualTo(referenceInterval.getEndMillis()); // 16h

  }

  @Test
  public void successAtsubtractIntervalWithSlotRoundWithNotOverlapedIntervalTest() {
    Interval referenceInterval = new Interval(32400000l, 51600000l);// 9h - 14:20
    Interval interval1 = new Interval(51600000l, 58500000);// 14:20h - 16:15
    Interval result1 =
        DateUtils.subtractIntervalWithSlotRound(referenceInterval, interval1, slot30min);
    assertThat(result1).isEqualTo(referenceInterval);
  }

  public void successAtsubtractIntervalWithSlotRoundWithBiggerIntervalToSubtractTest() {
    Interval referenceInterval = new Interval(32400000l, 51600000l);// 9h - 14:20
    Interval interval1 = new Interval(28800000l, 54000000l);// 8h - 15h
    Interval result1 =
        DateUtils.subtractIntervalWithSlotRound(referenceInterval, interval1, slot30min);
    assertThat(result1).isNull();
  }

  @Test
  public void errorAtsubtractIntervalWithSlotRoundWithBiggerIntervalReference() {
    Interval referenceInterval = new Interval(28800000l, 54000000l);// 8h - 15h
    Interval interval1 = new Interval(32400000l, 51600000l);// 9h - 14:20
    try {
      Interval result1 =
          DateUtils.subtractIntervalWithSlotRound(referenceInterval, interval1, slot30min);
      fail("Should not be possible split a interval");
    } catch (Exception e) {
      assertThat(e).isInstanceOf(IllegalArgumentException.class);
    }
  }

  @Test
  public void successAtsubtractIntervalWithSlotRound() {
    Interval referenceInterval = new Interval(28800000l, 54000000l);// 8h - 15h
    Interval interval1 = new Interval(25200000l, 47580000l);// 7h - 13:13
    Interval result1 =
        DateUtils.subtractIntervalWithSlotRound(referenceInterval, interval1, slot30min);
    assertThat(result1.getStartMillis()).isEqualTo(48600000);
    assertThat(result1.getEndMillis()).isEqualTo(referenceInterval.getEndMillis());

  }

  @Test
  public void successAtGetPreviousWeekDay() {
    DateTime dt = new DateTime(2015, 02, 02, 10, 10);// monday
    DateTime prvMonday = DateUtils.getPrevioustDay(dt, 1);
    assertThat(prvMonday.dayOfWeek().get()).isEqualTo(1);
    assertThat(prvMonday.getYear()).isEqualTo(2015);
    assertThat(prvMonday.getMonthOfYear()).isEqualTo(1);
    assertThat(prvMonday.getDayOfMonth()).isEqualTo(26);

    DateTime prvWed = DateUtils.getPrevioustDay(dt, 3);// wednesday
    assertThat(prvWed.dayOfWeek().get()).isEqualTo(3);
    assertThat(prvWed.getYear()).isEqualTo(2015);
    assertThat(prvWed.getMonthOfYear()).isEqualTo(1);
    assertThat(prvWed.getDayOfMonth()).isEqualTo(28);

  }
}

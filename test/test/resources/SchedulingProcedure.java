package test.resources;

import java.util.List;

import javax.validation.constraints.NotNull;

public class SchedulingProcedure {
  @NotNull
  public Long book;
  @NotNull
  public Integer startAt;
  @NotNull
  public Integer endAt;
  @NotNull
  public Long examProcedure;
  @NotNull
  public Long referenceDate;
  @NotNull
  public List<Long> resources;
}

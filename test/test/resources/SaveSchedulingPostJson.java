package test.resources;

import java.util.List;

import javax.validation.constraints.NotNull;

import play.data.validation.Constraints.Required;

public class SaveSchedulingPostJson {
  
  @NotNull
  public Long patient;
  @Required
  public List<SchedulingProcedure> schedulingProcedures;

}

package test.controllers;

import static cloud.tests.utils.AssertionUtils.performAuthorizedRequest;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static play.mvc.Http.Status.ACCEPTED;
import static play.test.Helpers.status;

import com.google.common.collect.ImmutableMap;
import common.constants.SlotStatus;

import controllers.Schedulings;
import dto.SlotReservationDTO;
import models.*;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;

import play.libs.Json;
import play.mvc.Result;
import play.test.Helpers;
import test.common.BaseWithServer;
import test.common.CloudTestUtils;
import test.utils.AssertionUtils;
import test.utils.ModelFactory;
import forms.FormMessageSlot;

public class SlotsTest extends BaseWithServer {

  private static String SCOPE_KEY = "agd_save_slots";
  private static Long DEFAULT_EMPLOYEE_ID = 1L;
  private static DateTimeFormatter ISO8601_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z");
  private SchedulingBook book;
  private Institution institution;
  private Unity unity;
  private Slot slot;

  @Before
  public void initialize() {
    institution = new Institution("name", "businessName", "businessSocialName", "cnpj");
    institution.save();
    unity = ModelFactory.createUnity(institution);
    book = ModelFactory.createAndSaveSchedulingBook(institution, unity);
    slot = ModelFactory.createAndSaveSlot(book);
  }

  @Test
  public void validSendMessage() {
    FormMessageSlot slot = new FormMessageSlot(this.book.id, 20, ISO8601_FORMATTER.parseDateTime("2015-10-16T08:00:00Z"),
            ISO8601_FORMATTER.parseDateTime("2015-10-17T18:00:00Z"));
    CloudTestUtils.fakeAuthByCloudSecure(1l, this.institution.id);
    Result result = CloudTestUtils.callJsonFormActionWithCSRF(controllers.routes.ref.Slots.generateSlots(this.institution.getId()),
            Json.toJson(slot), controllers.routes.Slots.generateSlots(this.institution.getId()));
    assertThat(status(result)).isEqualTo(ACCEPTED);
  }

  @Test
  public void makeSlotReservationSuccess(){
    when(Schedulings.employeeService.getCurrentEmployeeId())
              .thenReturn(DEFAULT_EMPLOYEE_ID);
    Result result = slotReservation();
    assertThat(Helpers.status(result)).isEqualTo(Helpers.OK);
    slot.refresh();
    assertThat(slot.status).isEqualTo(SlotStatus.UNAVAILABLE);
    assertThat(slot.reservation).isEqualTo(SlotReservation.findBySlot(slot));
    SlotReservationDTO reservation = Json.fromJson(Json.parse(Helpers.contentAsString(result)), SlotReservationDTO.class);
    assertThat(slot.reservation.token).isEqualTo(reservation.token);
    assertThat(slot.reservation.employeeId).isEqualTo(DEFAULT_EMPLOYEE_ID);
  }



 @Test
  public void makeSlotReservationOnNonExistentSlotReturnNotFound(){
    Long nonExistentSlog = 10L;
    Result result = performAuthorizedRequest(institution.id, SCOPE_KEY, Helpers.PUT,
            controllers.routes.Slots.lock(institution.id, nonExistentSlog).url(),
            controllers.routes.ref.Slots.lock(institution.id, nonExistentSlog));
    assertThat(Helpers.status(result)).isEqualTo(Helpers.NOT_FOUND);
  }

  @Test
  public void makeSlotReservationOnUnavailableSlotReturnBadRequest(){
    slot.unavailable();
    Result result = performAuthorizedRequest(institution.id, SCOPE_KEY,  Helpers.PUT,
            controllers.routes.Slots.lock(institution.id, slot.id).url(),
            controllers.routes.ref.Slots.lock(institution.id, slot.id));
    assertThat(Helpers.status(result)).isEqualTo(Helpers.BAD_REQUEST);
  }
  @Test
  public void makeSlotReservationFromAnotherTenantReturnsForbidden(){
    Institution otherTenant = ModelFactory.createInstitution("otherFakeTenant");
    Result result = performAuthorizedRequest(otherTenant.id, SCOPE_KEY, Helpers.PUT,
            controllers.routes.Slots.lock(otherTenant.id, slot.id).url(),
            controllers.routes.ref.Slots.lock(otherTenant.id, slot.id));
    assertThat(Helpers.status(result)).isEqualTo(Helpers.FORBIDDEN);
  }

  @Test
  public void undoSlotReservationWithSuccess(){
    slotReservation();
    slot.refresh();
    Result result = performAuthorizedRequest(institution.id, SCOPE_KEY,  Helpers.PUT,
            controllers.routes.Slots.unlock(institution.id, slot.id).url(),
            ImmutableMap.<String, String>builder().put("token", slot.reservation.token).build(),
            controllers.routes.ref.Slots.unlock(institution.id, slot.id));
    assertThat(Helpers.status(result)).isEqualTo(Helpers.OK);
  }

  @Test
  public void undoSlotReservationFromAnotherTenantReturnForbidden(){
    Institution otherTenant = ModelFactory.createInstitution("otherFakeTenant");
    Result result = performAuthorizedRequest(otherTenant.id, SCOPE_KEY,  Helpers.PUT,
            controllers.routes.Slots.unlock(otherTenant.id, slot.id).url(), Json.parse(Helpers.contentAsString(slotReservation())),
            controllers.routes.ref.Slots.unlock(otherTenant.id, slot.id));
    assertThat(Helpers.status(result)).isEqualTo(Helpers.FORBIDDEN);
  }

  @Test
  public void undoSlotReservationOnNonExistentSlotReturnNotFound(){
    Long nonExistentSlot = 10L;
    Result result = performAuthorizedRequest(institution.id, SCOPE_KEY,  Helpers.PUT,
            controllers.routes.Slots.unlock(institution.id, nonExistentSlot).url(), Json.parse(Helpers.contentAsString(slotReservation())),
            controllers.routes.ref.Slots.unlock(institution.id, nonExistentSlot));
    assertThat(Helpers.status(result)).isEqualTo(Helpers.NOT_FOUND);
  }

  @Test
  public void undoSlotReservationOnAvailableSlotReturnBadRequest(){
    Result result = performAuthorizedRequest(institution.id, SCOPE_KEY,  Helpers.PUT,
            controllers.routes.Slots.unlock(institution.id, slot.id).url(), Json.parse(Helpers.contentAsString(slotReservation())),
            controllers.routes.ref.Slots.unlock(institution.id, slot.id));
    assertThat(Helpers.status(result)).isEqualTo(Helpers.BAD_REQUEST);
  }


  @Test
  public void undoSlotReservationWithInvalidTokenReturnUnauthorized(){
    slotReservation();
    SlotReservationDTO invalidToken = new SlotReservationDTO();
    invalidToken.token = "wow";
      Result result = performAuthorizedRequest(institution.id, SCOPE_KEY, Helpers.PUT,
              controllers.routes.Slots.unlock(institution.id, slot.id).url(), Json.toJson(invalidToken),
              controllers.routes.ref.Slots.unlock(institution.id, slot.id));
    assertThat(Helpers.status(result)).isEqualTo(Helpers.UNAUTHORIZED);
  }

  @Test
  public void undoSlotReservationWithEmptyBodyRequestReturnBadRequest(){
    slotReservation();
    Result result = performAuthorizedRequest(institution.id, SCOPE_KEY,  Helpers.PUT,
            controllers.routes.Slots.unlock(institution.id, slot.id).url(),
            controllers.routes.ref.Slots.unlock(institution.id, slot.id));
    assertThat(Helpers.status(result)).isEqualTo(Helpers.BAD_REQUEST);

  }

  private Result slotReservation(){
    return performAuthorizedRequest(institution.id, SCOPE_KEY,  Helpers.PUT,
            controllers.routes.Slots.lock(institution.id, slot.id).url(),
            controllers.routes.ref.Slots.lock(institution.id, slot.id));
  }

}

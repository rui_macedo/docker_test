package test.controllers;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static play.mvc.Http.Status.BAD_REQUEST;
import static play.mvc.Http.Status.CREATED;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.status;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Institution;
import models.SchedulingBook;
import models.Unity;

import org.junit.Before;
import org.junit.Test;

import play.libs.Json;
import play.mvc.Result;
import play.test.Helpers;
import test.common.BaseWithServer;
import test.common.CloudTestUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import dto.SchedulingBookDTO;
import dto.SchedulingBookDetailsDTO;

public class SchedulingbooksTest extends BaseWithServer {

  public Institution inst;

  @Before
  public void settings() {
    inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
  }

  @Test
  public void validSaveSchedulingbooks() {
    Unity unity = new Unity(inst, "name", null, 1, "unityCode");
    unity.save();
    SchedulingBookDetailsDTO dto = new SchedulingBookDetailsDTO();
    dto.name = "name";
    dto.slotSizeMin = -30;
    dto.unity = unity.wrap();
    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);
    Result result = CloudTestUtils.callJsonFormActionWithCSRF(controllers.routes.ref.SchedulingBooks.save(inst.id), Json.toJson(dto),
            controllers.routes.SchedulingBooks.save(inst.id));
    String bodyContent = Helpers.contentAsString(result);
    assertThat(status(result)).isEqualTo(CREATED);
    try {
      SchedulingBookDetailsDTO book = Json.fromJson(Json.parse(bodyContent), SchedulingBookDetailsDTO.class);
      assertThat(SchedulingBook.findById(book.id)).isNotNull();
    } catch (Exception e) {
      fail(e.toString());
    }
  }

  @Test
  public void invalidSaveSchedulingbooksFormError() {
    Map<String, String> formParamsCloudService = new HashMap<String, String>();
    Unity unity = new Unity(inst, "name", null, 1, "unityCode");
    unity.save();
    formParamsCloudService.put("slotSizeMin", "30");
    formParamsCloudService.put("unity", unity.id.toString());
    formParamsCloudService.put("institution", inst.id.toString());
    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);

    Result result = CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.SchedulingBooks.save(inst.id), formParamsCloudService,
            controllers.routes.SchedulingBooks.save(inst.id));
    assertThat(status(result)).isEqualTo(BAD_REQUEST);
  }

  @Test
  public void validSchedulingBookUpdate() {
    Unity unity = new Unity(inst, "name", null, 1, "unityCode");
    unity.save();
    SchedulingBookDetailsDTO dto = new SchedulingBookDetailsDTO();
    dto.name = "name";
    dto.slotSizeMin = -30;
    dto.unity = unity.wrap();
    SchedulingBook book = new SchedulingBook();
    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);
    book.unwrap(dto, inst.id);
    book.save();
    book.name = "Name22";
    Result result = CloudTestUtils.callJsonFormActionWithCSRF(controllers.routes.ref.SchedulingBooks.update(book, inst.id),
            Json.toJson(book.wrap()), controllers.routes.SchedulingBooks.update(book, inst.id));
    assertThat(status(result)).isEqualTo(OK);
    String bodyContent = Helpers.contentAsString(result);
    try {
      dto = Json.fromJson(Json.parse(bodyContent), SchedulingBookDetailsDTO.class);
      assertThat(dto.name).isEqualTo("Name22");
    } catch (Exception e) {
      fail(e.toString());
    }
  }

  @Test
  public void invalidSchedulingBookUpdateFormError() {
    SchedulingBook schedulingBook = new SchedulingBook(inst, "name", 10);
    schedulingBook.save();
    Unity unity = new Unity(inst, "name", null, 1, "unityCode");
    unity.save();
    assertThat(SchedulingBook.findById(schedulingBook.id)).isNotNull();
    Map<String, String> formParamsCloudService = new HashMap<String, String>();
    formParamsCloudService.put("slotSizeMin", "30");
    formParamsCloudService.put("unity", unity.id.toString());
    formParamsCloudService.put("institution", inst.id.toString());
    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);
    Result result = CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.SchedulingBooks.update(schedulingBook, inst.id),
            formParamsCloudService, controllers.routes.SchedulingBooks.update(schedulingBook, inst.id));
    assertThat(status(result)).isEqualTo(BAD_REQUEST);
  }


  @Test
  public void validSchedulingBookDelete() {
    SchedulingBook schedulingBook = new SchedulingBook(inst, "name", 10);
    schedulingBook.save();
    Unity unity = new Unity(inst, "name", null, 1, "unityCode");
    unity.save();
    assertThat(SchedulingBook.findById(schedulingBook.id)).isNotNull();
    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);

    Result result = CloudTestUtils.callSimpleActionWithCSRF(controllers.routes.ref.SchedulingBooks.delete(schedulingBook, inst.id),
            controllers.routes.SchedulingBooks.delete(schedulingBook, inst.id));

    assertThat(SchedulingBook.findById(schedulingBook.id)).isNull();
  }


  @Test
  public void successAtFindAll() {
    SchedulingBook schedulingBook = new SchedulingBook(inst, "name", 10);
    schedulingBook.save();
    SchedulingBook schedulingBook2 = new SchedulingBook(inst, "name", 10);
    schedulingBook2.save();
    SchedulingBook schedulingBook3 = new SchedulingBook(inst, "name", 10);
    schedulingBook3.save();
    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);

    Result result = CloudTestUtils.callSimpleActionWithCSRF(controllers.routes.ref.SchedulingBooks.findAll(inst.id, Collections.EMPTY_LIST),
            controllers.routes.SchedulingBooks.findAll(inst.id, Collections.EMPTY_LIST));
    assertThat(status(result)).isEqualTo(OK);

    String bodyContent = Helpers.contentAsString(result);
    try {
      List<SchedulingBookDTO> groups = new ObjectMapper().readValue(bodyContent, new TypeReference<List<SchedulingBookDTO>>() {});
      assertThat(groups).contains(schedulingBook.wrap(), schedulingBook2.wrap(), schedulingBook3.wrap());
    } catch (Exception e) {
      fail(e.toString());
    }
  }

  @Test
  public void successAtFindById() {
    SchedulingBook schedulingBook = new SchedulingBook(inst, "name", 10);
    schedulingBook.save();
    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);
    Result result = CloudTestUtils.callSimpleActionWithCSRF(controllers.routes.ref.SchedulingBooks.byId(schedulingBook, inst.id, Collections.EMPTY_LIST),
            controllers.routes.SchedulingBooks.byId(schedulingBook, inst.id, Collections.EMPTY_LIST));
    assertThat(status(result)).isEqualTo(OK);
    String bodyContent = Helpers.contentAsString(result);
    try {
      assertThat(Json.fromJson(Json.parse(bodyContent), SchedulingBookDetailsDTO.class).name).isEqualTo("name");
    } catch (Exception e) {
      fail(e.toString());
    }
  }
}

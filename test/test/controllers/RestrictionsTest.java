package test.controllers;


import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static play.mvc.Http.Status.CREATED;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.status;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import models.Institution;
import models.Restriction;

import org.junit.Test;

import play.libs.Json;
import play.mvc.Result;
import play.test.Helpers;
import test.common.BaseWithServer;
import test.common.CloudTestUtils;

import dto.RestrictionDTO;

public class RestrictionsTest extends BaseWithServer {

  @Test
  public void listRestrictionsTest() {
    Institution institution = new Institution("TESTE", "TESTE", "TESTE", "00000");
    institution.save();
    Restriction restriction = new Restriction();
    restriction.name = "Name";
    restriction.institution = institution;
    restriction.isDeleted = false;
    restriction.save();

    Restriction restriction2 = new Restriction();
    restriction2.name = "Name2";
    restriction2.institution = institution;
    restriction.isDeleted = false;
    restriction2.save();

    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);
    Result result = CloudTestUtils.callSimpleActionWithCSRF(controllers.routes.ref.Restrictions.all(institution.id, Collections.EMPTY_LIST), controllers.routes.Restrictions.all(institution.id, Collections.EMPTY_LIST));
    assertThat(status(result)).isEqualTo(OK);
    String jsonContent = Helpers.contentAsString(result);
    List list = Json.fromJson(Json.parse(jsonContent), List.class);
    assertThat(list).isNotEmpty();

  }

  @Test
  public void listRestrictionsByIdTest() {
    Institution institution = new Institution("TESTE", "TESTE", "TESTE", "00000");
    institution.save();
    Restriction restriction = new Restriction();
    restriction.name = "Name";
    restriction.institution = institution;
    restriction.isDeleted = false;
    restriction.save();
    
    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);
    Result result = CloudTestUtils.callSimpleActionWithCSRF(controllers.routes.ref.Restrictions.byId(1l, restriction, Collections.EMPTY_LIST), controllers.routes.Restrictions.byId(1l, restriction, Collections.EMPTY_LIST));

    assertThat(status(result)).isEqualTo(OK);

  }

  @Test
  public void saveRoomTest() throws IOException {
    Institution institution = new Institution("TESTE", "TESTE", "TESTE", "00000");
    institution.save();
    RestrictionDTO dto = new RestrictionDTO();
    dto.name = "Name";
    
    CloudTestUtils.fakeAuthByCloudSecure(1l, institution.id);
    
    
    Result result = CloudTestUtils.callJsonFormActionWithCSRF(controllers.routes.ref.Restrictions.save(institution.id), Json.toJson(dto), controllers.routes.Restrictions.save(institution.id));
    String bodyContent = Helpers.contentAsString(result);
    assertThat(status(result)).isEqualTo(CREATED);
    try {
      RestrictionDTO restriction = Json.fromJson(Json.parse(bodyContent), RestrictionDTO.class);
      assertThat(Restriction.findById(restriction.id)).isNotNull();
    } catch (Exception e) {
      fail(e.toString());
    }

  }
}

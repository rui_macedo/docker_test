package test.controllers;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static play.mvc.Http.Status.BAD_REQUEST;
import static play.mvc.Http.Status.CREATED;
import static play.mvc.Http.Status.OK;
import static play.mvc.Http.Status.UNAUTHORIZED;
import static play.test.Helpers.status;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Equipment;
import models.Institution;
import models.Room;
import models.Section;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import play.libs.Json;
import play.mvc.Result;
import play.test.Helpers;
import test.common.BaseWithServer;
import test.common.CloudTestUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EquipmentsTest extends BaseWithServer {

  public Institution inst;

  @Before
  public void settings() {
    inst = new Institution("Inst", "Clin", "Soc", "88888888896552");
    inst.save();
  }

  @Test
  public void validSaveEquipment() {
    Room room = new Room(inst, "Sala 1");
    room.save();
    Section section = new Section(inst, "Section");
    section.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "Equip");
    formParamsCloudService.put("producer", "Producer");
    formParamsCloudService.put("aeTitle", "AETITLE");
    formParamsCloudService.put("institution", inst.id.toString());
    formParamsCloudService.put("room", room.id.toString());
    formParamsCloudService.put("section", section.id.toString());

    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);

    Result result =
        CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.Equipments.save(inst.id),
            formParamsCloudService, controllers.routes.Equipments.save(inst.id));
    String bodyContent = Helpers.contentAsString(result);
    assertThat(status(result)).isEqualTo(CREATED);
    try {
      Equipment equipment = Json.fromJson(Json.parse(bodyContent), Equipment.class);
      assertThat(Equipment.findById(equipment.id)).isNotNull();
    } catch (Exception e) {
      fail(e.toString());
    }
  }

  @Test
  public void invalidSaveEquipmentFormError() {
    Room room = new Room(inst, "Sala 1");
    room.save();
    Section section = new Section(inst, "Section");
    section.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("error", "Equip");
    formParamsCloudService.put("producer", "Producer");
    formParamsCloudService.put("aeTitle", "AETITLE");
    formParamsCloudService.put("institution", inst.id.toString());
    formParamsCloudService.put("room", room.id.toString());
    formParamsCloudService.put("section", section.id.toString());

    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);

    Result result =
        CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.Equipments.save(inst.id),
            formParamsCloudService, controllers.routes.Equipments.save(inst.id));

    assertThat(status(result)).isEqualTo(BAD_REQUEST);
  }

  @Test
  public void invalidSaveEquipmentUnauthorized() {

    CloudTestUtils.logout();

    Room room = new Room(inst, "Sala 1");
    room.save();
    Section section = new Section(inst, "Section");
    section.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "Equip");
    formParamsCloudService.put("producer", "Producer");
    formParamsCloudService.put("aeTitle", "AETITLE");
    formParamsCloudService.put("institution", inst.id.toString());
    formParamsCloudService.put("room", room.id.toString());
    formParamsCloudService.put("section", section.id.toString());


    Result result =
        CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.Equipments.save(inst.id),
            formParamsCloudService, controllers.routes.Equipments.save(inst.id));
    String bodyContent1 = Helpers.contentAsString(result);
    assertThat(status(result)).isEqualTo(UNAUTHORIZED);
  }

  @Test
  public void validEquipmentUpdate() {
    Room room = new Room(inst, "Sala 1");
    Section section = new Section(inst, "Section");
    section.save();
    room.section = section;
    room.save();



    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();

    assertThat(Equipment.findById(equipment.id)).isNotNull();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "EquipamentoOK");
    formParamsCloudService.put("producer", "Producer");
    formParamsCloudService.put("aeTitle", "AETITLE");
    formParamsCloudService.put("institution", inst.id.toString());
    formParamsCloudService.put("room", room.id.toString());
    formParamsCloudService.put("section", section.id.toString());

    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);

    Result result =
        CloudTestUtils.callFormActionWithCSRF(
            controllers.routes.ref.Equipments.update(equipment, inst.id), formParamsCloudService,
            controllers.routes.Equipments.update(equipment, inst.id));
    assertThat(status(result)).isEqualTo(OK);
    String bodyContent = Helpers.contentAsString(result);
    try {
      equipment = Json.fromJson(Json.parse(bodyContent), Equipment.class);
      assertThat(equipment.name).isEqualTo("EquipamentoOK");
    } catch (Exception e) {
      fail(e.toString());
    }
  }

  @Test
  public void invalidEquipmentUpdateFormError() {
    Room room = new Room(inst, "Sala 1");
    Section section = new Section(inst, "Section");
    section.save();
    room.section = section;
    room.save();
    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();

    assertThat(Equipment.findById(equipment.id)).isNotNull();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("error", "EquipamentoOK");
    formParamsCloudService.put("producer", "Producer");
    formParamsCloudService.put("aeTitle", "AETITLE");
    formParamsCloudService.put("institution", inst.id.toString());
    formParamsCloudService.put("room", room.id.toString());
    formParamsCloudService.put("section", section.id.toString());

    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);

    Result result =
        CloudTestUtils.callFormActionWithCSRF(
            controllers.routes.ref.Equipments.update(equipment, inst.id), formParamsCloudService,
            controllers.routes.Equipments.update(equipment, inst.id));
    assertThat(status(result)).isEqualTo(BAD_REQUEST);
  }

  @Ignore
  @Test
  public void invalidEquipmentUpdateUnauthorized() {
    Room room = new Room(inst, "Sala 1");
    Section section = new Section(inst, "Section");
    section.save();
    room.section = section;
    room.save();

    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();

    assertThat(Equipment.findById(equipment.id)).isNotNull();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "EquipamentoOK");
    formParamsCloudService.put("producer", "Producer");
    formParamsCloudService.put("aeTitle", "AETITLE");
    formParamsCloudService.put("institution", inst.id.toString());
    formParamsCloudService.put("room", room.id.toString());
    formParamsCloudService.put("section", section.id.toString());

    CloudTestUtils.logout();

    Result result =
        CloudTestUtils.callFormActionWithCSRF(
            controllers.routes.ref.Equipments.update(equipment, inst.id), formParamsCloudService,
            controllers.routes.Equipments.update(equipment, inst.id));
    assertThat(status(result)).isEqualTo(UNAUTHORIZED);
  }

  @Test
  public void validEquipmentDelete() {
    Room room = new Room(inst, "Sala 1");
    room.save();
    Section section = new Section(inst, "Section");
    section.save();

    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();

    assertThat(Equipment.findById(equipment.id)).isNotNull();

    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);

    Result result =
        CloudTestUtils.callSimpleActionWithCSRF(
            controllers.routes.ref.Equipments.delete(equipment, inst.id),
            controllers.routes.Equipments.delete(equipment, inst.id));
    System.out.println(Helpers.contentAsString(result));
    assertThat(Equipment.findById(equipment.id)).isNull();
  }

  @Test
  public void invalidEquipmentDeleteUnauthorized() {

    Room room = new Room(inst, "Sala 1");
    room.save();
    Section section = new Section(inst, "Section");
    section.save();

    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();

    assertThat(Equipment.findById(equipment.id)).isNotNull();

    CloudTestUtils.logout();

    Result result =
        CloudTestUtils.callSimpleActionWithCSRF(
            controllers.routes.ref.Equipments.delete(equipment, inst.id),
            controllers.routes.Equipments.delete(equipment, inst.id));

    assertThat(status(result)).isEqualTo(UNAUTHORIZED);
  }

  @Test
  public void successAtFindAll() {
    Room room = new Room(inst, "Sala 1");
    room.save();
    Section section = new Section(inst, "Section");
    section.save();
    Equipment equipment = new Equipment(inst, "E", "p", "e", section, room);
    equipment.save();

    Equipment equipment2 = new Equipment(inst, "E", "p", "e", section, room);
    equipment2.save();

    Equipment equipment3 = new Equipment(inst, "E", "p", "e", section, room);
    equipment3.save();

    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);

    Result result =
        CloudTestUtils.callSimpleActionWithCSRF(
            controllers.routes.ref.Equipments.findAll(inst.id, Collections.EMPTY_LIST),
            controllers.routes.Equipments.findAll(inst.id, Collections.EMPTY_LIST));
    assertThat(status(result)).isEqualTo(OK);
    String bodyContent = Helpers.contentAsString(result);
    try {
      List<Equipment> groups =
          new ObjectMapper().readValue(bodyContent, new TypeReference<List<Equipment>>() {});
      assertThat(groups).contains(equipment3, equipment2, equipment3);
    } catch (Exception e) {
      fail(e.toString());
    }
  }

  @Test
  public void successAtFindById() {
    Room room = new Room(inst, "Sala 1");
    room.save();
    Section section = new Section(inst, "Section");
    section.save();

    Equipment equipment =
        new Equipment(inst, "EquipamentoUm", "Producer", "AETitle", section, room);
    equipment.save();

    CloudTestUtils.fakeAuthByCloudSecure(1l, inst.id);

    Result result =
        CloudTestUtils.callSimpleActionWithCSRF(
            controllers.routes.ref.Equipments.byId(equipment, inst.id, Collections.EMPTY_LIST),
            controllers.routes.Equipments.byId(equipment, inst.id, Collections.EMPTY_LIST));
    assertThat(status(result)).isEqualTo(OK);
    String bodyContent = Helpers.contentAsString(result);
    try {
      equipment = Json.fromJson(Json.parse(bodyContent), Equipment.class);
      assertThat(equipment.name).isEqualTo("EquipamentoUm");
    } catch (Exception e) {
      fail(e.toString());
    }
  }
}

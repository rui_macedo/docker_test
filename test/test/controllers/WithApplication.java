package test.controllers;

import java.util.Map;

import test.common.Global;
import cloud.tests.basetest.BaseTestWithApplication;

public class WithApplication extends BaseTestWithApplication {

  private Global global;

  @Override
  public Map<String, Object> provideInitialConfigurations() {
    Map<String, Object> configs = super.provideInitialConfigurations();
    // replace logger level
    // You need to set logger to DEBUG if you wanna log sqls
    configs.put("db.default.logStatements", true);
    configs.put("logger.com.jolbox", "DEBUG");
    // need to put user and pass to hikariDB
    configs.put("db.default.user", "");
    configs.put("db.default.password", "");


    configs.put("cloudsecure.security.authentication.signature", "De234rwrtw5w45tgefr4");
    configs.put("cloudsecure.security.services.signature", "45j4509-s0idfpo40");

    configs.put("aws.sqs.import.name", "queue.name.mock");

    return configs;
  }
  
  @Override
  public Global provideGlobal() {
    if (this.global == null) {
      this.global = new Global();
    }
    return this.global;
  }

}

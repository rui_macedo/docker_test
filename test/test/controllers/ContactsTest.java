package test.controllers;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.status;

import test.common.BaseWithServer;

import com.pixeon.cloudsecure.token.WSSecureCallPlugin;

public class ContactsTest extends BaseWithServer {

	  public WSSecureCallPlugin wsCaller;
	  
	  //FIXME: arrumar isso, o teste não está passando
	  
	  
/*
	  @Before
	  public void startPlugin() {
	    wsCaller = app.getWrappedApplication().plugin(WSSecureCallPlugin.class).getOrElse(null);
	    
	  }
	
	@Test
	public void listContactsTest() {

		Institution institution = new Institution("TESTE", "TESTE", "TESTE",
				"00000");
		institution.save();	
		
		Contact contact =  new Contact("Contato", "obs contato","(41)1111-1111");
		contact.institution = institution;
		contact.save();
		Contact contact2 =  new Contact("Contato 2", "obs contato 2","(41)2222-2222");
		contact2.institution = institution;
		contact2.save();
		CloudTestUtils.authByCloudSecure(1l, 1l, "list_contacts");
		Result result = CloudTestUtils.callSimpleActionWithCSRF(
				routes.ref.Contacts.all(institution.id,null, Collections.EMPTY_LIST),
				routes.Contacts.all(institution.id,null, Collections.EMPTY_LIST));

		assertThat(status(result)).isEqualTo(OK);
		String jsonContent = Helpers.contentAsString(result);
		List contacts = Json.fromJson(Json.parse(jsonContent), List.class);
		assertThat(contacts).isNotEmpty();

	}

	@Test
	public void listContactByIdTest() {
		Contact contact = new Contact("Contato", "obs contato","(41)1111-1111");
		contact.save();
		Contact contact2 = new Contact("Contato 2", "obs contato 2","(41)2222-2222");
		contact2.save();
		CloudTestUtils.authByCloudSecure(1l, 1l, "list_contacts");	
		Result result = CloudTestUtils.callSimpleActionWithCSRF(
				routes.ref.Contacts.byId(1l, 1l, Collections.EMPTY_LIST),
				routes.Contacts.byId(1l, 1l, Collections.EMPTY_LIST));

		assertThat(status(result)).isEqualTo(OK);

	}

	@Test
	public void saveContactTest() throws IOException {
		Institution institution = new Institution("TESTE", "TESTE", "TESTE",
				"00000");
		institution.save();

		// SAVE REQUESTED ORDER
		String string_json = "";

		string_json = FileUtils.readFileToString(new File(
				"test/test/resources/save_contact.json"));

		ObjectMapper mapper_save = new ObjectMapper();

		JsonNode json_save = mapper_save.readValue(string_json, JsonNode.class);
		CloudTestUtils.authByCloudSecure(1l, 1l, "save_contact");
		Result resultReq = CloudTestUtils.callJsonFormActionWithCSRF(controllers.routes.ref.Contacts.save(1l), json_save,controllers.routes.Contacts.save(1l));				
		
		assertThat(status(resultReq)).isEqualTo(OK);

		Contact contact = Contact.findById(1l);
		assertThat(contact.name).isEqualTo("Joana");
		assertThat(contact.observations).isEqualTo("irma do paciente");
		assertThat(contact.phone).isEqualTo("1111-1111");

	}
	
	@Test
	public void updateContactTest() throws IOException {
		Institution institution = new Institution("TESTE", "TESTE", "TESTE",
				"00000");
		institution.save();

		Contact contact = new Contact("Contato", "obs contato","(41)1111-1111");
		contact.institution = institution;
		contact.save();

		contact = Contact.findById(1l);
		assertThat(contact.name).isEqualTo("Contato");		
		assertThat(contact.observations).isEqualTo("obs contato");
		assertThat(contact.phone).isEqualTo("(41)1111-1111");

		String string_json = FileUtils.readFileToString(new File(
				"test/test/resources/update_contact.json"));
		
		ObjectMapper mapper_save = new ObjectMapper();

		JsonNode json_update = mapper_save.readValue(string_json, JsonNode.class);
	
		CloudTestUtils.authByCloudSecure(1l, 1l, "update_contact");
		Result resultReqUpdate = CloudTestUtils.callJsonFormActionWithCSRF(	controllers.routes.ref.Contacts.update(1l,1l), json_update,controllers.routes.Contacts.update(1l,1l));
		
		assertThat(status(resultReqUpdate)).isEqualTo(OK);
		Contact updated_contact = Contact.findById(1l);
		assertThat(updated_contact.name).isEqualTo("Joana 2");
		assertThat(updated_contact.observations).isEqualTo("irma do paciente 2");
		assertThat(updated_contact.phone).isEqualTo("2222-2222");	
	}
	
	@Test
	public void deleteContactTest() throws IOException {
		Institution institution = new Institution("TESTE", "TESTE", "TESTE",
				"00000");
		institution.save();

		Contact contact = new Contact("Contato", "obs contato","(41)1111-1111");
		contact.institution = institution;
		contact.save();

		contact = Contact.findById(1l);
		assertThat(contact.name).isEqualTo("Contato");
		
		
		CloudTestUtils.authByCloudSecure(1l, 1l, "delete_contact");
		Result result_delete = CloudTestUtils.callSimpleActionWithCSRF(
				routes.ref.Contacts.delete(1l, 1l),
				routes.Contacts.delete(1l, 1l));
		assertThat(status(result_delete)).isEqualTo(OK);
		contact = null;
		contact = Contact.findById(1l);
		assertThat(contact).isEqualTo(null);
		
	}*/
}

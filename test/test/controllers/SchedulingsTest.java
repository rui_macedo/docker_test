package test.controllers;

import static cloud.tests.utils.AssertionUtils.performAuthorizedRequest;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import cloud.tests.utils.JsonUtils;
import controllers.Schedulings;
import controllers.routes;
import dto.ErrorDTO;
import dto.ResponseError;
import dto.SchedulingDTO;
import models.Equipment;
import models.EquipmentResource;
import models.ExamProcedure;
import models.ExamProcedureTUSS;
import models.Institution;
import models.Patient;
import models.Resource;
import models.Room;
import models.SchedulingBook;
import models.Section;

import org.apache.commons.io.FileCleaner;
import org.apache.commons.lang.math.NumberUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import models.scheduling.Scheduling;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import test.common.BaseWithServer;
import test.common.CloudTestUtils;
import test.resources.SaveSchedulingPostJson;
import test.resources.SchedulingProcedure;
import test.utils.ModelFactory;

import com.fasterxml.jackson.databind.JsonNode;

public class SchedulingsTest extends BaseWithServer {

  private Institution institution;
  private Scheduling scheduling;
  private final Long EMPLOYEE_ID = 1L;
  private final Long otherTenantId = 2L;
  private final Long otherSchedulingId = 2L;
  private final Long otherEmployeeId = 2L;

  @Before
  public void initialize(){
     institution = ModelFactory.createInstitution("King of lords");
     scheduling = ModelFactory.createAndSaveScheduling(institution, EMPLOYEE_ID);
  }

  @Test
  public void startANewSchedulingProcessSucessfully(){

    when(Schedulings.service.startScheduling(anyLong(), anyLong()))
            .thenReturn(ModelFactory.createSchedulingDTO(institution.id));

    Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.POST,
            controllers.routes.Schedulings.start(institution.id).url(),
            controllers.routes.ref.Schedulings.start(institution.id));

    SchedulingDTO response = JsonUtils.fromJsonResult(result, SchedulingDTO.class);

    assertEquals(Http.Status.CREATED, Helpers.status(result));
    assertEquals(NumberUtils.LONG_ONE, response.id);
    assertEquals(institution.id, response.institutionId);
    assertNotNull(response.employeeId);
    assertNotNull(response.creationDate);
    assertNotNull(response.updatedDate);
    assertEquals(response.creationDate, response.updatedDate);

  }

  @Test
  public void whenASchedulingExistsReturnsAValidDTO(){

    when(Schedulings.service.findSchedulingByIdOptional(1L))
            .thenReturn(Optional.of(scheduling.wrap()));

    Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.GET,
            controllers.routes.Schedulings.getScheduling(institution.id, scheduling.id).url(),
            controllers.routes.ref.Schedulings.getScheduling(institution.id, scheduling.id));

    SchedulingDTO responseFromServer = JsonUtils.fromJsonResult(result, SchedulingDTO.class);
    SchedulingDTO responseShouldBe = scheduling.wrap();

    assertEquals(Http.Status.OK, Helpers.status(result));
    assertEquals(responseShouldBe.id, responseFromServer.id);
    assertEquals(responseShouldBe.institutionId, responseFromServer.institutionId);
    assertEquals(responseShouldBe.employeeId, responseFromServer.employeeId);
    assertEquals(responseShouldBe.notes, responseFromServer.notes);
    assertEquals(responseShouldBe.status, responseFromServer.status);

  }


  @Test
  public void whenASchedulingNotExistsReturnsNotFound(){

    when(Schedulings.service.findSchedulingByIdOptional(1L))
            .thenReturn(Optional.empty());

    Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.GET,
            controllers.routes.Schedulings.getScheduling(institution.id, scheduling.id).url(),
            controllers.routes.ref.Schedulings.getScheduling(institution.id, scheduling.id));

    assertEquals(Http.Status.NOT_FOUND, Helpers.status(result));
    ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);
    assertEquals(ResponseError.Codes.NOT_FOUND.name(), response.code);
    assertEquals("Not found a Scheduling with the given id", String.valueOf(response.errors));

  }

  @Test
  public void whenTryingToRetrieveASchedulingFromAnotherTenantReturnsForbidden(){

    when(Schedulings.service.findSchedulingByIdOptional(1L))
            .thenReturn(Optional.of(scheduling.wrap()));

    Result result = performAuthorizedRequest(otherTenantId, "agd_search_slots", Helpers.GET,
            controllers.routes.Schedulings.getScheduling(otherTenantId, scheduling.id).url(),
            controllers.routes.ref.Schedulings.getScheduling(otherTenantId, scheduling.id));

    ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);

    assertEquals(Http.Status.FORBIDDEN, Helpers.status(result));
    assertEquals(ResponseError.Codes.CROSS_TENANT.name(), response.code);
    assertEquals("The scheduling process with the given id belongs to another tenant",
            String.valueOf(response.errors));

  }

  @Test
  public void whenRequestHasNoPayloadReturnsBadRequestWithBodyContentProblemCode(){
    Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.POST,
            controllers.routes.Schedulings.updateScheduling(institution.id, scheduling.id).url(),
            controllers.routes.ref.Schedulings.updateScheduling(institution.id, scheduling.id));
    assertEquals(Http.Status.BAD_REQUEST, Helpers.status(result));
    ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);
    assertEquals(ResponseError.Codes.BODY_CONTENT_PROBLEM.name(), response.code);
    assertEquals("Request payload is expected", String.valueOf(response.errors));
  }

  @Test
  public void whenTryingToUpdateASchedulingWhichNotExistsReturnsNotFound(){

    Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.POST,
            controllers.routes.Schedulings.updateScheduling(institution.id, otherSchedulingId).url(),
            Json.toJson(scheduling.wrap()),
            controllers.routes.ref.Schedulings.updateScheduling(institution.id, otherSchedulingId));

    ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);

    assertEquals(Http.Status.NOT_FOUND, Helpers.status(result));
    assertEquals(ResponseError.Codes.NOT_FOUND.name(), response.code);
    assertEquals("Not found a scheduling with the given id", String.valueOf(response.errors));
  }


  @Test
  public void whenTryingToUpdateASchedulingWhichBelongsToAnotherTenantReturnForbidden(){

    Result result = performAuthorizedRequest(otherTenantId, "agd_search_slots", Helpers.POST,
            controllers.routes.Schedulings.updateScheduling(otherTenantId, scheduling.id).url(),
            Json.toJson(scheduling.wrap()),
            controllers.routes.ref.Schedulings.updateScheduling(otherTenantId, scheduling.id));

    ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);

    assertEquals(Http.Status.FORBIDDEN, Helpers.status(result));
    assertEquals(ResponseError.Codes.CROSS_TENANT.name(), response.code);
    assertEquals("The scheduling process with the given id belongs to another tenant",
            String.valueOf(response.errors));
  }

  @Test
  public void whenTryingToUpdateASchedulingWhichBelongsToAnotherEmployeeReturnForbidden(){

    doNothing().when(Schedulings.service).updateSchedulingOnlyInProgress(any(), any());

    when(Schedulings.employeeService.getCurrentEmployeeId())
            .thenReturn(otherEmployeeId);

    Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.POST,
            controllers.routes.Schedulings.updateScheduling(institution.id, scheduling.id).url(),
            Json.toJson(scheduling.wrap()),
            controllers.routes.ref.Schedulings.updateScheduling(institution.id, scheduling.id));

    ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);

    assertEquals(Http.Status.FORBIDDEN, Helpers.status(result));
    assertEquals(ResponseError.Codes.NOT_OWNER.name(), response.code);
    assertEquals("You are not the owner of this booking", String.valueOf(response.errors));
  }

  @Test
  public void whenRequestIsOkayUpdateASchedulingSucessfully(){

    doNothing().when(Schedulings.service).updateSchedulingOnlyInProgress(any(), any());

    when(Schedulings.employeeService.getCurrentEmployeeId())
            .thenReturn(scheduling.employeeId);

    Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.POST,
            controllers.routes.Schedulings.updateScheduling(institution.id, scheduling.id).url(),
            Json.toJson(scheduling.wrap()),
            controllers.routes.ref.Schedulings.updateScheduling(institution.id, scheduling.id));

    assertEquals(Http.Status.OK, Helpers.status(result));

  }

  @Test
  public void whenRequestIsOkayButSomethingWentWrongReturnsServiceUnavailable(){

    doThrow(new RuntimeException()).when(Schedulings.service).updateSchedulingOnlyInProgress(any(), any());

    when(Schedulings.employeeService.getCurrentEmployeeId())
            .thenReturn(scheduling.employeeId);

    Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.POST,
            controllers.routes.Schedulings.updateScheduling(institution.id, scheduling.id).url(),
            Json.toJson(scheduling.wrap()),
            controllers.routes.ref.Schedulings.updateScheduling(institution.id, scheduling.id));

    assertEquals(Http.Status.SERVICE_UNAVAILABLE, Helpers.status(result));
    ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);
    assertEquals(ResponseError.Codes.SERVICE_UNAVAILABLE.name(), response.code);
    assertEquals("We are not able to process the request. Try again later",
            String.valueOf(response.errors));
  }

  @Test
  public void whenTryingToFinishASchedulingWhichNotExistsReturnsNotFound(){

      Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.PUT,
              controllers.routes.Schedulings.finishScheduling(institution.id, otherSchedulingId).url(),
              Json.toJson(scheduling.wrap()),
              controllers.routes.ref.Schedulings.updateScheduling(institution.id, otherSchedulingId));

      ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);

      assertEquals(Http.Status.NOT_FOUND, Helpers.status(result));
      assertEquals(ResponseError.Codes.NOT_FOUND.name(), response.code);
      assertEquals("Not found a scheduling with the given id", String.valueOf(response.errors));

  }

  @Test
  public void whenTryingToFinishASchedulingWhichBelongsToAnotherTenantReturnsForbidden(){

      Result result = performAuthorizedRequest(otherTenantId, "agd_search_slots", Helpers.PUT,
              controllers.routes.Schedulings.finishScheduling(otherTenantId, scheduling.id).url(),
              controllers.routes.ref.Schedulings.finishScheduling(otherTenantId, scheduling.id));

      ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);

      assertEquals(Http.Status.FORBIDDEN, Helpers.status(result));
      assertEquals(ResponseError.Codes.CROSS_TENANT.name(), response.code);
      assertEquals("Scheduling  with the given id belongs to another tenant",
              String.valueOf(response.errors));
  }

  @Test
  public void whenTryingToFinishASchedulingWhichBelongsToAnotherEmployeeReturnsForbidden(){

      when(Schedulings.employeeService.getCurrentEmployeeId())
              .thenReturn(otherEmployeeId);

      doNothing().when(Schedulings.service).finishScheduling(any());


      Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.PUT,
              controllers.routes.Schedulings.finishScheduling(institution.id, scheduling.id).url(),
              controllers.routes.ref.Schedulings.finishScheduling(institution.id, scheduling.id));


      ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);

      assertEquals(Http.Status.FORBIDDEN, Helpers.status(result));
      assertEquals(ResponseError.Codes.NOT_OWNER.name(), response.code);
      assertEquals("You are not the employee responsable for this process", String.valueOf(response.errors));

  }

  @Test
  public void whenTryingToFinishASchedulingAndSomethingWentWrongReturnsServiceUnavailable(){

      doThrow(new RuntimeException()).when(Schedulings.service).finishScheduling(any());

      when(Schedulings.employeeService.getCurrentEmployeeId())
              .thenReturn(scheduling.employeeId);

      Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.PUT,
              controllers.routes.Schedulings.finishScheduling(institution.id, scheduling.id).url(),
              controllers.routes.ref.Schedulings.finishScheduling(institution.id, scheduling.id));

      assertEquals(Http.Status.SERVICE_UNAVAILABLE, Helpers.status(result));
      ErrorDTO response = JsonUtils.fromJsonResult(result, ErrorDTO.class);
      assertEquals(ResponseError.Codes.SERVICE_UNAVAILABLE.name(), response.code);
      assertEquals("We are not able to process the request. Try again later",
              String.valueOf(response.errors));
  }

  @Test
  public void whenTryingToFinishASchedulingAndEverythingIsOkayReturnOk(){

      when(Schedulings.employeeService.getCurrentEmployeeId())
              .thenReturn(scheduling.employeeId);

      doNothing().when(Schedulings.service).finishScheduling(any());


      Result result = performAuthorizedRequest(institution.id, "agd_search_slots", Helpers.PUT,
              controllers.routes.Schedulings.finishScheduling(institution.id, scheduling.id).url(),
              controllers.routes.ref.Schedulings.finishScheduling(institution.id, scheduling.id));

      assertEquals(Http.Status.OK, Helpers.status(result));

  }

}

package test.controllers;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static play.mvc.Http.Status.*;
import static play.test.Helpers.status;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import models.Address;
import models.Country;
import models.Section;
import models.Institution;
import models.Unity;
import test.common.BaseWithServer;
import test.common.CloudTestUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pixeon.cloudsecure.token.WSSecureCallPlugin;

import org.junit.Test;
import org.junit.Before;

import play.mvc.Result;
import play.test.Helpers;

public class SectionsTest extends BaseWithServer {

  @Test
  public void validSaveSection() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "Section1");
    formParamsCloudService.put("institution", institution.id.toString());
    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

    Result result =
        CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.Sections.save(institution.id),
            formParamsCloudService, controllers.routes.Sections.save(institution.id));

    assertThat(status(result)).isEqualTo(OK);

    Section section = Section.findById(1);
    assertThat(section.name).isEqualTo("Section1");
  }

  @Test
  public void validSaveSectionUnity() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Unidade", address, 2666, "unityCode");
    unity.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "Section1");
    formParamsCloudService.put("unity", unity.id.toString());
    formParamsCloudService.put("institution", institution.id.toString());

    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

    Result result =
        CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.Sections.save(institution.id),
            formParamsCloudService, controllers.routes.Sections.save(institution.id));

    assertThat(status(result)).isEqualTo(OK);
    Section section = Section.findById(1);
    assertThat(section.name).isEqualTo("Section1");
  }

  @Test
  public void invalidSaveSectionByFormError() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("erro", "Section1");
    formParamsCloudService.put("institution", institution.id.toString());
    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

    Result result =
        CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.Sections.save(institution.id),
            formParamsCloudService, controllers.routes.Sections.save(institution.id));

    assertThat(status(result)).isEqualTo(BAD_REQUEST);
  }

  @Test
  public void invalidSaveSectionUnauthorized() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Institution institution2 = new Institution("Test2", "Teste2", "Teste2", "Teste2");
    institution2.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "Section1");
    formParamsCloudService.put("institution", institution2.id.toString());
    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

    Result result =
        CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.Sections.save(1l),
            formParamsCloudService, controllers.routes.Sections.save(1l));

    assertThat(status(result)).isEqualTo(UNAUTHORIZED);
  }

  @Test
  public void validUpdateSection() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Section section = new Section(institution, "SectionOld");
    section.save();

    Section existentSection = Section.findById(section.id);
    assertThat(existentSection).isNotNull();
    assertThat(section).isEqualTo(existentSection);

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "SectionNew");
    formParamsCloudService.put("institution", institution.id.toString());

    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);
    Result result =
        CloudTestUtils.callFormActionWithCSRF(
            controllers.routes.ref.Sections.update(1l, existentSection), formParamsCloudService,
            controllers.routes.Sections.update(1l, existentSection));

    assertThat(status(result)).isEqualTo(OK);

    existentSection = Section.findById(section.id);
    assertThat(existentSection.name).isEqualTo("SectionNew");
  }

  @Test
  public void invalidUpdateSectionByFormError() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Section section = new Section(institution, "Sec");
    section.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("erro", "Section1");
    formParamsCloudService.put("institution", institution.id.toString());
    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

    Result result =
        CloudTestUtils.callFormActionWithCSRF(
            controllers.routes.ref.Sections.update(institution.id, section),
            formParamsCloudService, controllers.routes.Sections.update(institution.id, section));

    assertThat(status(result)).isEqualTo(BAD_REQUEST);
  }


  @Test
  public void invalidUpdateSectionNull() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "SectionNew");
    formParamsCloudService.put("institution", institution.id.toString());

    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);
    try {
      CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.Sections.update(1l, null),
          formParamsCloudService, controllers.routes.Sections.update(1l, null));
      fail();
    } catch (NullPointerException e) {
      assertThat(e);
    }
  }


  @Test
  public void invalidUpdateSectionUnauthorized() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Institution institution2 = new Institution("Test2", "Teste2", "Teste2", "Teste2");
    institution2.save();

    Section section = new Section(institution2, "Sec");
    section.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "Section1");
    formParamsCloudService.put("institution", institution2.id.toString());
    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

    Result result =
        CloudTestUtils.callFormActionWithCSRF(controllers.routes.ref.Sections.update(1l, section),
            formParamsCloudService, controllers.routes.Sections.update(1l, section));

    assertThat(status(result)).isEqualTo(UNAUTHORIZED);
  }

  @Test
  public void validDeleteSection() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Section section = new Section(institution, "name");
    section.save();

    Section existentSection = Section.findById(section.id);
    assertThat(existentSection).isNotNull();

    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

    Result result =
        CloudTestUtils.callSimpleActionWithCSRF(
            controllers.routes.ref.Sections.delete(1l, section),
            controllers.routes.Sections.delete(1l, section));
    assertThat(status(result)).isEqualTo(OK);
    assertThat(Section.findById(section.id)).isNull();
  }

  @Test
  public void invalidDeleteSectionNull() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Map<String, String> formParamsCloudService = new HashMap<String, String>();

    formParamsCloudService.put("name", "SectionNew");
    formParamsCloudService.put("institution", institution.id.toString());

    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

    try {
      CloudTestUtils.callSimpleActionWithCSRF(controllers.routes.ref.Sections.delete(1l, null),
          controllers.routes.Sections.delete(1l, null));
      fail();
    } catch (NullPointerException e) {
      assertThat(e);
    }
  }

  @Test
  public void invalidDeleteSectionUnauthorized() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Institution institution2 = new Institution("Test2", "Teste2", "Teste2", "Teste2");
    institution2.save();

    Section section = new Section(institution2, "name");
    section.save();

    Section existentSection = Section.findById(section.id);
    assertThat(existentSection).isNotNull();

    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

    Result result =
        CloudTestUtils.callSimpleActionWithCSRF(
            controllers.routes.ref.Sections.delete(1l, section),
            controllers.routes.Sections.delete(1l, section));
    assertThat(status(result)).isEqualTo(UNAUTHORIZED);
  }

  @Test
  public void listAllSectionsByTenant() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Section section1 = new Section(institution, "sec1");
    section1.save();

    Section section2 = new Section(institution, "sec2");
    section2.save();

    Section section3 = new Section(institution, "sec3");
    section3.save();

    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);
    Result result =
        CloudTestUtils.callSimpleActionWithCSRF(
            controllers.routes.ref.Sections.all(institution.id, Collections.EMPTY_LIST),
            controllers.routes.Sections.all(institution.id, Collections.EMPTY_LIST));
    assertThat(status(result)).isEqualTo(OK);
    String bodyContent1 = Helpers.contentAsString(result);

    try {
      List<Country> list =
          new ObjectMapper().readValue(bodyContent1, new TypeReference<List<Section>>() {});
      assertThat(list).isNotNull().isNotEmpty().hasSize(3).contains(section1, section2, section3);
    } catch (IOException e) {
      fail("You should be able to parse the content of result");
    }
  }

  @Test
  public void listAllSectionsByUnity() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();
    Address address = new Address();
    address.save();
    Unity unity = new Unity(institution, "Unidade", address, 2666, "unityCode");
    unity.save();

    Section section1 = new Section(unity, "sec1");
    section1.save();

    Section section2 = new Section(unity, "sec2");
    section2.save();

    Section section3 = new Section(unity, "sec3");
    section3.save();

    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);
    Result result =
        CloudTestUtils.callSimpleActionWithCSRF(controllers.routes.ref.Sections.allByUnity(
            institution.id, unity, Collections.EMPTY_LIST), controllers.routes.Sections.allByUnity(
            institution.id, unity, Collections.EMPTY_LIST));
    assertThat(status(result)).isEqualTo(OK);
    String bodyContent1 = Helpers.contentAsString(result);

    try {
      List<Country> list =
          new ObjectMapper().readValue(bodyContent1, new TypeReference<List<Section>>() {});
      assertThat(list).isNotNull().isNotEmpty().hasSize(3).contains(section1, section2, section3);
    } catch (IOException e) {
      fail("You should be able to parse the content of result");
    }
  }

  @Test
  public void findById() {
    Institution institution = new Institution("Test", "Teste", "Teste", "Teste");
    institution.save();

    Section section1 = new Section(institution, "sec1");
    section1.save();

    CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

    Result result =
        CloudTestUtils.callSimpleActionWithCSRF(
            controllers.routes.ref.Sections.byId(institution.id, section1, Collections.EMPTY_LIST),
            controllers.routes.Sections.byId(institution.id, section1, Collections.EMPTY_LIST));

    assertThat(status(result)).isEqualTo(OK);
    String bodyContent1 = Helpers.contentAsString(result);

    try {
      Section section = new ObjectMapper().readValue(bodyContent1, new TypeReference<Section>() {});
      assertThat(section).isEqualTo(section1);
    } catch (IOException e) {
      fail("You should be able to parse the content of result");
    }
  }
}

package test.controllers;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;
import static org.junit.Assert.assertEquals;
import static play.mvc.Http.Status.BAD_REQUEST;
import static play.mvc.Http.Status.CREATED;
import static play.mvc.Http.Status.OK;
import static play.mvc.Http.Status.UNAUTHORIZED;
import static play.test.Helpers.status;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Collections;
import java.util.List;

import models.Address;
import models.Institution;
import models.Unity;

import org.junit.Before;
import org.junit.Test;

import play.test.Helpers;
import play.mvc.Result;
import test.common.BaseWithServer;
import test.common.CloudTestUtils;
import test.utils.ModelFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import static test.utils.AssertionUtils.performAuthorizedRequest;

public class UnitiesTest extends BaseWithServer {

    public static Institution tenant;

    @Before
    public void initializing(){
        tenant = ModelFactory.createInstitution("BLA");
    }

    @Test
    public void validSaveUnity() {
        Unity unityCreateRequest = ModelFactory.unityCreateRequestBuilder(tenant, ModelFactory.createAddress());
        Result result =  performAuthorizedRequest(ModelFactory.DEFAULT_TENANT_ID, "agd_unity_sv", Helpers.POST,
                controllers.routes.Unities.save(ModelFactory.DEFAULT_TENANT_ID).url(), unityCreateRequest, controllers.routes.ref.Unities.save(ModelFactory.DEFAULT_TENANT_ID));
        assertEquals(CREATED, status(result));
        Unity unity = Unity.findById(1);
        assertThat(unity).isNotNull();
        assertThat(unity.name).isEqualTo("unity");
        assertThat(unity.creationDate).isNotNull();
    }

    @Test
    public void invalidSaveUnityFormError() {
        Map<String, String> unityCreateBadRequest = ModelFactory.unityCreateBadRequestBuilder(tenant, ModelFactory.createAddress());
        Result result =  performAuthorizedRequest(ModelFactory.DEFAULT_TENANT_ID, "agd_unity_sv", Helpers.POST,
                controllers.routes.Unities.save(ModelFactory.DEFAULT_TENANT_ID).url(), unityCreateBadRequest, controllers.routes.ref.Unities.save(ModelFactory.DEFAULT_TENANT_ID));
        assertThat(status(result)).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void invalidSaveUnauthorized() {
        Unity unityCreateRequest = ModelFactory.unityCreateRequestBuilder(tenant, ModelFactory.createAddress());
        Result result =  performAuthorizedRequest(ModelFactory.OTHER_TENANT_ID, "agd_unity_sv", Helpers.POST,
                controllers.routes.Unities.save(ModelFactory.OTHER_TENANT_ID).url(), unityCreateRequest, controllers.routes.ref.Unities.save(ModelFactory.OTHER_TENANT_ID));

        assertThat(status(result)).isEqualTo(UNAUTHORIZED);
    }

    @Test
    public void validUpdateUnity() {

        Unity unity = ModelFactory.createUnity(tenant);
        Address address = ModelFactory.createAddress();

        Map<String, String> formParamsCloudService = new HashMap<>();

        formParamsCloudService.put("name", "unitynew");
        formParamsCloudService.put("institution", tenant.id.toString());
        formParamsCloudService.put("address", address.id.toString());
        formParamsCloudService.put("addressNumber", "100");
        formParamsCloudService.put("latitude", "000");
        formParamsCloudService.put("longitude", "000");
        formParamsCloudService.put("telephone", "1111-1111");
        formParamsCloudService.put("openingHours", "8");
        formParamsCloudService.put("unityCode", "unityCode");
        formParamsCloudService.put("cnpj", "11111");
        formParamsCloudService.put("businessSocialName", "social");

        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        Result result =
                CloudTestUtils.callFormActionWithCSRF(
                        controllers.routes.ref.Unities.update(tenant.id, unity),
                        formParamsCloudService,
                        controllers.routes.Unities.update(tenant.id, unity));

        assertThat(status(result)).isEqualTo(OK);

        Unity existentUnity = Unity.findById(unity.id);
        assertThat(existentUnity).isNotNull();
        assertThat(existentUnity.name).isEqualTo("unitynew");
        assertThat(existentUnity.addressNumber).isEqualTo(100);
        assertThat(existentUnity.updateDate).isNotNull();
    }

    @Test
    public void invalidUpdateFormError() {
        Unity unity = ModelFactory.createUnity(tenant);
        Map<String, String> badRequest = ModelFactory.unityCreateBadRequestBuilder(tenant, unity.address);
        Result result =  performAuthorizedRequest(ModelFactory.DEFAULT_TENANT_ID, "agd_unity_sv", Helpers.PUT,
                controllers.routes.Unities.update(ModelFactory.DEFAULT_TENANT_ID, unity).url(), badRequest, controllers.routes.ref.Unities.update(ModelFactory.DEFAULT_TENANT_ID, unity));
        assertThat(status(result)).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void invalidUpdateNull() {
        Institution institution = new Institution("INSTITUTION", "INSTTEST", "INSTTEST", "TEST");
        institution.save();

        Address address = new Address();
        address.save();

        Unity unity = new Unity(institution, "UnityOld", address, 111, "unityCode");
        unity.save();

        Unity existentUnity = Unity.findById(unity.id);
        assertThat(existentUnity).isNotNull();
        assertThat(existentUnity).isEqualTo(unity);

        Map<String, String> formParamsCloudService = new HashMap<String, String>();

        formParamsCloudService.put("name", "unitynew");
        formParamsCloudService.put("institution", institution.id.toString());
        formParamsCloudService.put("address", address.id.toString());
        formParamsCloudService.put("addressNumber", "100");
        formParamsCloudService.put("latitude", "000");
        formParamsCloudService.put("longitude", "000");
        formParamsCloudService.put("telephone", "1111-1111");
        formParamsCloudService.put("openingHours", "8");
        formParamsCloudService.put("unityCode", "unityCode");
        formParamsCloudService.put("cnpj", "11111");
        formParamsCloudService.put("businessSocialName", "social");

        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        try {
            CloudTestUtils.callFormActionWithCSRF(
                    controllers.routes.ref.Unities.update(institution.id, null), formParamsCloudService,
                    controllers.routes.Unities.update(institution.id, null));
            fail();
        } catch (NullPointerException e) {
            assertThat(e);
        }
    }

    @Test
    public void invalidUpdateUnauthorized() {

        Unity unity = ModelFactory.unityCreateRequestBuilder(tenant, ModelFactory.createAddress());
        unity.institution = ModelFactory.createInstitution("BlaBla");

        Result result =  performAuthorizedRequest(ModelFactory.DEFAULT_TENANT_ID, "agd_unity_sv", Helpers.PUT,
                controllers.routes.Unities.update(ModelFactory.DEFAULT_TENANT_ID, unity).url(), unity, controllers.routes.ref.Unities.update(ModelFactory.DEFAULT_TENANT_ID, unity));
        assertThat(status(result)).isEqualTo(UNAUTHORIZED);

    }

    @Test
    public void validDeleteUnity() {
        Unity unity = ModelFactory.createUnity(tenant);

        Result result =
                CloudTestUtils.callSimpleActionWithCSRF(controllers.routes.ref.Unities.delete(tenant.id, unity),
                        controllers.routes.Unities.delete(tenant.id, unity));
        assertThat(status(result)).isEqualTo(OK);

        result =
                CloudTestUtils.callSimpleActionWithCSRF(
                        controllers.routes.ref.Unities.all(tenant.id, Collections.EMPTY_LIST),
                        controllers.routes.Unities.all(tenant.id, Collections.EMPTY_LIST));

        assertThat(status(result)).isEqualTo(OK);
        String bodyContent1 = Helpers.contentAsString(result);

        try {
            List<Unity> list =
                    new ObjectMapper().readValue(bodyContent1, new TypeReference<List<Unity>>() {});
            assertThat(list).excludes(unity);
        } catch (IOException e) {
            fail("You should be able to parse the content of result");
        }

    }

    @Test
    public void invalidDeleteUnityNull() {
        Institution institution = new Institution("INSTITUTION", "INSTTEST", "INSTTEST", "TEST");
        institution.save();

        Address address = new Address();
        address.save();

        Unity unity = new Unity(institution, "unity", address, 111, "unityCode");
        unity.save();

        Unity existentUnity = Unity.findById(unity.id);
        assertThat(existentUnity).isNotNull();
        assertThat(existentUnity).isEqualTo(unity);

        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        try {
            CloudTestUtils.callSimpleActionWithCSRF(controllers.routes.ref.Unities.delete(1l, null),
                    controllers.routes.Unities.delete(1l, null));
            fail();
        } catch (NullPointerException e) {
            assertThat(e);
        }
    }

    @Test
    public void invalidDeleteUnityUnauthorized() {
        Institution institution = new Institution("INSTITUTION", "INSTTEST", "INSTTEST", "TEST");
        institution.save();

        Institution institution2 = new Institution("INSTITUTION2", "INSTTEST2", "INSTTEST2", "TEST2");
        institution2.save();

        Address address = new Address();
        address.save();

        Unity unity = new Unity(institution2, "unity", address, 111, "unityCode");
        unity.save();

        Unity existentUnity = Unity.findById(unity.id);
        assertThat(existentUnity).isNotNull();
        assertThat(existentUnity).isEqualTo(unity);

        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        Result result =
                CloudTestUtils.callSimpleActionWithCSRF(controllers.routes.ref.Unities.delete(1l, unity),
                        controllers.routes.Unities.delete(1l, unity));

        assertThat(status(result)).isEqualTo(UNAUTHORIZED);
    }

    @Test
    public void listAllByTenant() {
        Unity unity =  ModelFactory.createUnity(tenant, "1");
        Unity unity2 = ModelFactory.createUnity(tenant, "2");
        Unity unity3 = ModelFactory.createUnity(tenant, "3");

        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        Result result =
                CloudTestUtils.callSimpleActionWithCSRF(
                        controllers.routes.ref.Unities.all(tenant.id, Collections.EMPTY_LIST),
                        controllers.routes.Unities.all(tenant.id, Collections.EMPTY_LIST));

        System.out.println(Helpers.contentAsString(result));
        assertThat(status(result)).isEqualTo(OK);
        String bodyContent1 = Helpers.contentAsString(result);

        try {
            List<Unity> list =
                    new ObjectMapper().readValue(bodyContent1, new TypeReference<List<Unity>>() {});
            assertThat(list).isNotNull().isNotEmpty().hasSize(3).contains(unity, unity2, unity3);
        } catch (IOException e) {
            fail("You should be able to parse the content of result");
        }
    }

    @Test
    public void findById() {
        Unity unity = ModelFactory.createUnity(tenant);
        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        Result result =
                CloudTestUtils.callSimpleActionWithCSRF(
                        controllers.routes.ref.Unities.byId(tenant.id, unity, Collections.EMPTY_LIST),
                        controllers.routes.Unities.byId(tenant.id, unity, Collections.EMPTY_LIST));

        assertThat(status(result)).isEqualTo(OK);
        String bodyContent1 = Helpers.contentAsString(result);

        try {
            Unity existent = new ObjectMapper().readValue(bodyContent1, new TypeReference<Unity>() {});
            assertThat(existent).isEqualTo(unity);
        } catch (IOException e) {
            fail("You should be able to parse the content of result");
        }
    }

    @Test
    public void unityCodeShouldBeUniqueAtSaveError(){
        String notUniqueCode = "notUniqueCode";
        Unity unity = ModelFactory.createUnity(tenant, notUniqueCode);
        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        Unity unityCreateRequest = ModelFactory.unityCreateRequestBuilder(tenant, ModelFactory.createAddress(), notUniqueCode);
        Result result =  performAuthorizedRequest(ModelFactory.DEFAULT_TENANT_ID, "agd_unity_sv", Helpers.POST,
                controllers.routes.Unities.save(ModelFactory.DEFAULT_TENANT_ID).url(), unityCreateRequest, controllers.routes.ref.Unities.save(ModelFactory.DEFAULT_TENANT_ID));
        assertEquals(BAD_REQUEST, status(result));
    }

    @Test
    public void unityCodeShouldBeUniqueAtUpdateOK() {
        String notUniqueCode = "notUniqueCode";
        Unity unity = ModelFactory.createUnity(tenant, notUniqueCode);
        Address address = ModelFactory.createAddress();

        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        Map<String, String> formParamsCloudService = new HashMap<>();

        formParamsCloudService.put("name", "unitynew");
        formParamsCloudService.put("institution", tenant.id.toString());
        formParamsCloudService.put("address", address.id.toString());
        formParamsCloudService.put("addressNumber", "100");
        formParamsCloudService.put("latitude", "000");
        formParamsCloudService.put("longitude", "000");
        formParamsCloudService.put("telephone", "1111-1111");
        formParamsCloudService.put("openingHours", "8");
        formParamsCloudService.put("unityCode", notUniqueCode);
        formParamsCloudService.put("cnpj", "11111");
        formParamsCloudService.put("businessSocialName", "social");

        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        Result result =
                CloudTestUtils.callFormActionWithCSRF(
                        controllers.routes.ref.Unities.update(tenant.id, unity),
                        formParamsCloudService,
                        controllers.routes.Unities.update(tenant.id, unity));

        assertThat(status(result)).isEqualTo(OK);
    }

    @Test
    public void unityCodeShouldBeUniqueAtUpdateError() {
        String uniqueCode = "uniqueCode";
        String notUniqueCode = "notuniqueCode2";
        Unity unity = ModelFactory.createUnity(tenant, uniqueCode);
        Unity unity2 = ModelFactory.createUnity(tenant, notUniqueCode);
        Address address = ModelFactory.createAddress();

        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        Map<String, String> formParamsCloudService = new HashMap<>();

        formParamsCloudService.put("name", "unitynew");
        formParamsCloudService.put("institution", tenant.id.toString());
        formParamsCloudService.put("address", address.id.toString());
        formParamsCloudService.put("addressNumber", "100");
        formParamsCloudService.put("latitude", "000");
        formParamsCloudService.put("longitude", "000");
        formParamsCloudService.put("telephone", "1111-1111");
        formParamsCloudService.put("openingHours", "8");
        formParamsCloudService.put("unityCode", notUniqueCode);
        formParamsCloudService.put("cnpj", "11111");
        formParamsCloudService.put("businessSocialName", "social");

        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        Result result =
                CloudTestUtils.callFormActionWithCSRF(
                        controllers.routes.ref.Unities.update(tenant.id, unity),
                        formParamsCloudService,
                        controllers.routes.Unities.update(tenant.id, unity));

        assertThat(status(result)).isEqualTo(BAD_REQUEST);
    }

    @Test
    public void createUnityNameShouldBeUniqueInTenant() {
        String sameName = "repeat";

        Unity unity = ModelFactory.createUnity(tenant, sameName, "uno");

        Unity unityCreateRequest = ModelFactory.unityCreateRequestBuilder(tenant, sameName);
        Result result =  performAuthorizedRequest(tenant.id, "agd_unity_sv", Helpers.POST,
                controllers.routes.Unities.save(tenant.id).url(), unityCreateRequest, controllers.routes.ref.Unities.save(tenant.id));
        assertEquals(BAD_REQUEST, status(result));
        assertThat(Helpers.contentAsString(result)).contains("O nome da unidade já existe");
    }

    @Test
    public void updateUnityNameShouldBeUniqueInTenant() {
        String sameName = "sameName";
        ModelFactory.createUnity(tenant, sameName, "uno");

        Unity unity = ModelFactory.createUnity(tenant,"anyName", "dos");
        Address address = ModelFactory.createAddress();

        Map<String, String> formParamsCloudService = new HashMap<>();

        formParamsCloudService.put("name", sameName);
        formParamsCloudService.put("institution", tenant.id.toString());
        formParamsCloudService.put("address", address.id.toString());
        formParamsCloudService.put("addressNumber", "100");
        formParamsCloudService.put("latitude", "000");
        formParamsCloudService.put("longitude", "000");
        formParamsCloudService.put("telephone", "1111-1111");
        formParamsCloudService.put("openingHours", "8");
        formParamsCloudService.put("unityCode", "dos");
        formParamsCloudService.put("cnpj", "11111");
        formParamsCloudService.put("businessSocialName", "social");

        CloudTestUtils.fakeAuthByCloudSecure(1l, 1l);

        Result result =
                CloudTestUtils.callFormActionWithCSRF(
                        controllers.routes.ref.Unities.update(tenant.id, unity),
                        formParamsCloudService,
                        controllers.routes.Unities.update(tenant.id, unity));
        assertThat(status(result)).isEqualTo(BAD_REQUEST);
        assertThat(Helpers.contentAsString(result)).contains("O nome da unidade já existe");
    }
}

package test.services;

import static org.fest.assertions.Assertions.assertThat;
import models.Institution;
import models.SchedulingBook;
import models.Slot;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;

import services.SlotServiceImpl;
import services.api.SlotService;
import test.common.BaseWithServer;
import test.utils.ModelFactory;

import common.constants.SchedulingBookSlotStatus;

import forms.FormMessageSlot;

public class SchedulingSlotServiceTest extends BaseWithServer {

  private SchedulingBook book;
  private FormMessageSlot formMessageSlot;
  DateTimeFormatter ISO8601_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z");
  private SlotService service;

  @Before
  public void initialize() {
    service = new SlotServiceImpl();
    Institution institution = new Institution("name", "businessName", "businessSocialName", "cnpj");
    institution.save();
    this.book = ModelFactory.createAndSaveSchedulingBook(institution, ModelFactory.createUnity(institution));
    DateTime start = ISO8601_FORMATTER.parseDateTime("2015-10-16T08:00:00Z");
    DateTime end = ISO8601_FORMATTER.parseDateTime("2015-10-17T18:00:00Z");
    this.formMessageSlot = new FormMessageSlot(this.book.id, 20, start,end );
  }

  @Test
  public void validCreatedSlots() {
    service.generate(this.formMessageSlot);
    SchedulingBook findBook = SchedulingBook.findById(this.book.id);
    assertThat(findBook.statusSlots).isEqualTo(SchedulingBookSlotStatus.SUCCESS);
    assertThat(Slot.findAllBySchedulingBookAndDate(findBook, this.formMessageSlot.startAt, this.formMessageSlot.endAt).size()).isEqualTo(23);
  }
  
  @Test
  public void duplicatePeriodForSlots() {
    service.generate(this.formMessageSlot);
    SchedulingBook findBook = SchedulingBook.findById(this.book.id);
    assertThat(findBook.statusSlots).isEqualTo(SchedulingBookSlotStatus.SUCCESS);
    assertThat(Slot.findAllBySchedulingBookAndDate(findBook, new DateTime("2015-10-16T08:00:00Z"), new DateTime("2015-10-17T18:00:00Z")).size()).isEqualTo(23);
    
    FormMessageSlot formMessageSlot = new FormMessageSlot(this.book.id, 30, ISO8601_FORMATTER.parseDateTime("2015-10-16T14:00:00Z"), ISO8601_FORMATTER.parseDateTime("2015-10-17T18:00:00Z"));
    service.generate(formMessageSlot);
    SchedulingBook newfindBook = SchedulingBook.findById(this.book.id);
    assertThat(newfindBook.statusSlots).isEqualTo(SchedulingBookSlotStatus.SUCCESS);
    //Continua com 23 Slots.
    assertThat(Slot.findAllBySchedulingBookAndDate(newfindBook, this.formMessageSlot.startAt, this.formMessageSlot.endAt).size()).isEqualTo(23);
  }

  @Test
  public void invalidPeriod() {
    FormMessageSlot formMessageSlot = new FormMessageSlot(this.book.id, 30, ISO8601_FORMATTER.parseDateTime("2015-10-17T14:00:00Z"), ISO8601_FORMATTER.parseDateTime("2015-10-16T18:00:00Z"));
    service.generate(formMessageSlot);
    SchedulingBook findBook = SchedulingBook.findById(this.book.id);
    assertThat(Slot.findAllBySchedulingBookAndDate(findBook, this.formMessageSlot.startAt, this.formMessageSlot.endAt).size()).isEqualTo(0);
  }
  
}

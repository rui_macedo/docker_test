package test.services;

import models.Institution;
import models.scheduling.Scheduling;
import models.scheduling.SchedulingFilter;
import org.junit.Before;
import services.api.SchedulingFilterService;
import services.searchers.SchedulingFilterServiceImpl;
import test.common.BaseWithServer;
import test.utils.ModelFactory;

import static org.junit.Assert.assertEquals;

public class SchedulingFilterServiceTest extends BaseWithServer {


    private SchedulingFilterService service;
    private Institution tenant;
    private Long employeeId = 1L;

    @Before
    public void initialize() {
        service = new SchedulingFilterServiceImpl();
        tenant = ModelFactory.createInstitution("name");
    }

    public void testFindSchedulingFiltersBySchedulingId() {
        Scheduling scheduling = ModelFactory.createAndSaveScheduling(tenant.getId(), employeeId);
        ModelFactory.createAndSaveSchedulingFilter(scheduling, SchedulingFilter.Type.EXAM_PROCEDURE, "5");
        ModelFactory.createAndSaveSchedulingFilter(scheduling, SchedulingFilter.Type.INSURANCE, "2");
        ModelFactory.createAndSaveSchedulingFilter(scheduling, SchedulingFilter.Type.INSURANCE_PLAN, "1");
        ModelFactory.createAndSaveSchedulingFilter(ModelFactory.createAndSaveScheduling(tenant.getId(), employeeId), SchedulingFilter.Type.EXAM_PROCEDURE, "5");

        assertEquals(service.findSchedulingFiltersBySchedulingId(scheduling.getId()).size(), 3);
    }

    public void testDeleteBySchedulingId() {
        Scheduling scheduling = ModelFactory.createAndSaveScheduling(tenant.getId(), employeeId);
        ModelFactory.createAndSaveSchedulingFilter(scheduling, SchedulingFilter.Type.EXAM_PROCEDURE, "5");
        ModelFactory.createAndSaveSchedulingFilter(scheduling, SchedulingFilter.Type.INSURANCE, "2");
        ModelFactory.createAndSaveSchedulingFilter(scheduling, SchedulingFilter.Type.INSURANCE_PLAN, "1");
        ModelFactory.createAndSaveSchedulingFilter(ModelFactory.createAndSaveScheduling(tenant.getId(), employeeId), SchedulingFilter.Type.EXAM_PROCEDURE, "5");
        service.deleteBySchedulingId(scheduling.getId());
        assertEquals(service.findSchedulingFiltersBySchedulingId(scheduling.getId()).size(), 0);
    }
}
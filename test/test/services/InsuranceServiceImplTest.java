package test.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import models.Institution;
import models.Insurance;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import play.libs.Json;
import queries.InsuranceQuery;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;
import resource.InsuranceResource;
import services.InsuranceServiceImpl;
import test.common.BaseWithServer;
import test.utils.ModelFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class InsuranceServiceImplTest extends BaseWithServer {

    private JedisPool jedisPoolMock;

    private Jedis jedisMock;

    private Institution institution;

    private InsuranceServiceImpl cache;

    @Before
    public void createScenario() {
        institution = ModelFactory.createInstitution("InstitutionName");
        jedisPoolMock = Mockito.mock(JedisPool.class);
        jedisMock = Mockito.mock(Jedis.class);
        when(jedisPoolMock.getResource()).thenReturn(jedisMock);
        cache = new InsuranceServiceImpl(jedisPoolMock, true);
    }

    @Test
    public void cleanInsuranceKeysForInstitution() {
        Set<String> insuranceKeys = Sets.newHashSet(
                getCacheKey(institution.getId(), 1111L),
                getCacheKey(institution.getId(), 2222L),
                getCacheKey(institution.getId(), 3333L)
        );

        Transaction multiDelete = Mockito.mock(Transaction.class);
        when(jedisMock.keys(getCacheKey(institution.getId()))).thenReturn(insuranceKeys);
        when(jedisMock.multi()).thenReturn(multiDelete);

        cache.cleanCache(institution.getId());

        for (String key : insuranceKeys) {
            verify(multiDelete).del(key);
        }
        verify(multiDelete).exec();
    }

    @Test
    public void listFromCacheEmptyList() {
        assertEquals(Collections.emptyList(), cache.list(institution.getId(), new InsuranceQuery(null, "name", institution.getId())));
    }

    @Test
    public void listFromCache() throws IOException {

        String string_json = FileUtils.readFileToString(new File("test/test/resources/insurance.json"));
        List<Insurance> listInsurance = new ObjectMapper().readValue(string_json, new TypeReference<List<Insurance>>() {});

        List<String> listString  = listInsurance
                .parallelStream()
                .map(l -> String.valueOf(Json.toJson(l)))
                .collect(Collectors.toList());

        when(jedisMock.keys(getCacheKey(institution.getId())))
                .thenReturn(Sets.newHashSet(getCacheKey(institution.getId(), 1111L)));

        when(jedisMock.mget(jedisMock.keys(
                            getCacheKey(institution.getId()))
                                .stream()
                                .map(e -> e)
                                .toArray(String[]::new))
        ).thenReturn(listString);


        List<InsuranceResource> result = cache.list(institution.getId(), new InsuranceQuery(null, "name", institution.getId()));

        assertEquals(result.size(), 4);
        assertEquals(result.get(0).name, "GEAP");
    }


    private String getCacheKey(Long institutionId) {
        return "scheduling.insurance." + institutionId + ".*";
    }

    private String getCacheKey(Long institutionId, Long insuranceId) {
        return "scheduling.insurance." + institutionId + "." + insuranceId;
    }

}

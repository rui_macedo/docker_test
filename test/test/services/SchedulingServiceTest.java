package test.services;

import com.google.common.collect.Lists;

import com.wordnik.swagger.model.Model;

import common.constants.SlotStatus;
import dto.SchedulingDTO;
import forms.FormScheduling;
import forms.FormSchedulingOrder;
import forms.FormSchedulingReservation;
import models.*;
import models.scheduling.Scheduling;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import services.SchedulingServiceImpl;
import services.api.SchedulingService;
import test.common.BaseWithServer;
import test.utils.ModelFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class SchedulingServiceTest extends BaseWithServer {


    private SchedulingService service;
    private Institution tenant;
    private Scheduling scheduling;
    private Long otherSchedulingId = 2L;
    private Long employeeId = 1L;
    private FormScheduling form;

    @Before
    public void initialize() {
        service = new SchedulingServiceImpl();
        tenant = ModelFactory.createInstitution("Fab");
        scheduling = ModelFactory.createAndSaveScheduling(tenant, 1L);
        form = new FormScheduling();
        form.contactId = 10L;
        form.patientId = 1L;
        form.notes = "Welcome to a new kind of tension.\n" +
                "All across the alienation.";
    }

    @Test
    public void whenSchedulingExistsReturnsAValidDTO() {
        SchedulingDTO dto = service.findSchedulingById(scheduling.id);
        assertEquals(scheduling.wrap().id, dto.id);
    }

    @Test
    public void whenSchedulingNotExistsReturnsNull() {
        SchedulingDTO dto = service.findSchedulingById(otherSchedulingId);
        assertNull(dto);
    }

    @Test
    public void whenSchedulingExistsReturnsAValidOptionalDTO() {
        Optional<SchedulingDTO> dto = service.findSchedulingByIdOptional(scheduling.id);
        assertEquals(Optional.of(scheduling.wrap()).get().id, dto.get().id);
    }

    @Test
    public void whenSchedulingNotExistsReturnsEmptyOptional() {
        Optional<SchedulingDTO> dto = service.findSchedulingByIdOptional(otherSchedulingId);
        assertEquals(Optional.empty(), dto);
    }

    @Test
    public void startANewSchedulingSuccessfully() {
        SchedulingDTO dto = service.startScheduling(tenant.id, employeeId);
        assertEquals(tenant.id, dto.institutionId);
        assertEquals(employeeId, dto.employeeId);
        assertEquals(otherSchedulingId, dto.id);
        assertEquals(Scheduling.Status.IN_PROGRESS, dto.status);
        assertNull(dto.contact);
        assertNull(dto.patientId);
        assertNull(dto.patientName);
        assertNull(dto.notes);
        assertNotNull(dto.creationDate);
        assertNotNull(dto.updatedDate);
    }

    @Test
    public void whenTryingToUpdateAValuableSchedulingJustUpdateStatusToFinished() {
        scheduling.contact = ModelFactory.createAndSaveContact(tenant.id, "Bla Bla bla");
        scheduling.patientId = 1L;
        scheduling.update();
        service.finishScheduling(scheduling);
        scheduling.refresh();
        assertEquals(Scheduling.Status.FINISHED, scheduling.status);
    }


    @Test
    public void whenTryingToUpdateAnUnValuableSchedulingDeleteRegister() {
        Scheduling newScheduling = ModelFactory.createAndSaveScheduling(tenant.id, employeeId);
        service.finishScheduling(newScheduling);
        assertNull(Scheduling.findById(2L));
    }

    @Test
    public void whenTryingToUpdateASchedulingWithNullMakeSureNoExceptionIsThrown() {
        FormScheduling form = new FormScheduling();
        form.contactId = 10L;
        form.patientId = 1L;
        form.notes = "Wake me up when september ends";
        service.updateSchedulingOnlyInProgress(null, form);
    }

    @Test
    public void whenTryingToUpdateASchedulingThatIsNoMoreInProgressDotNothing() {
        scheduling.status = Scheduling.Status.FINISHED;
        scheduling.update();

        service.updateSchedulingOnlyInProgress(scheduling, form);

        assertEquals(2L, scheduling.version);

    }

    @Test
    public void whenTryingToUpdateASchedulingWithPatientIdInsteadOfPatientName() {
        service.updateSchedulingOnlyInProgress(scheduling, form);
        scheduling.refresh();
        assertEquals(StringUtils.EMPTY, scheduling.patientName);
        assertEquals(form.patientId, scheduling.patientId);
        assertEquals(form.notes, scheduling.notes);
        assertTrue(scheduling.updated.isAfter(scheduling.created));
    }

    @Test
    public void whenTryingToUpdateASchedulingThatHasPatientIdToPatientName() {
        scheduling.patientId = 10L;
        scheduling.update();

        form.patientId = null;
        form.patientName = "Katy Perry";

        scheduling.refresh();

        service.updateSchedulingOnlyInProgress(scheduling, form);

        scheduling.refresh();

        assertNull(scheduling.patientId);
        assertEquals(form.patientName, scheduling.patientName);
        assertEquals(form.notes, scheduling.notes);
        assertTrue(scheduling.updated.isAfter(scheduling.created));
    }

    @Test
    public void testSchedulingReservationSuccess() {

        SchedulingBook book = ModelFactory.createAndSaveSchedulingBook(tenant, ModelFactory.createUnity(tenant));
        Slot slot1 = ModelFactory.createAndSaveSlot(book);
        Slot slot2 = ModelFactory.createAndSaveSlot(book);
        Slot slot3 = ModelFactory.createAndSaveSlot(book);
        List<Long> listSlotId = Lists.newArrayList(slot1.getId(), slot2.getId(), slot3.getId());

        Insurance insurance = ModelFactory.createInsurance(tenant);
        InsurancePlan insurancePlan = ModelFactory.createInsurancePlan(tenant, insurance, ModelFactory.createProcedureTable(tenant));

        ExamProcedure examProcedure = ModelFactory.createExamProcedure(tenant, ModelFactory.createExamModality(tenant));
        Scheduling scheduling = ModelFactory.createAndSaveScheduling(tenant, 1L);

        FormSchedulingReservation form = new FormSchedulingReservation(Arrays.asList(
                new FormSchedulingOrder(insurance.id,insurancePlan.id,examProcedure.id,listSlotId
        )));

        service.makeBooking(scheduling, form);

        slot1.refresh();
        slot2.refresh();
        slot3.refresh();
        scheduling.refresh();

        assertEquals(slot1.status, SlotStatus.BOOKED);
        assertEquals(slot2.status, SlotStatus.BOOKED);
        assertEquals(slot3.status, SlotStatus.BOOKED);
        assertEquals(scheduling.status, Scheduling.Status.BOOKED);
    }


}

FROM dordoka/play-framework
MAINTAINER Ruizera <rui.macedo@pixeon.com>
ENV WHO_ENV_AM_I dev 
ENV WHO_PROJECT_AM_I scheduling
ENV MY_PIXEON_PROJECT flow

RUN sudo mkdir /system

RUN sudo apt-get -y update
RUN sudo apt-get -y install mysql-server
RUN sudo apt-get -y install libmysqlclient-dev
RUN sudo apt-get -y install python-pip python-dev build-essential 
RUN sudo pip install --upgrade pip 
RUN sudo pip install MySQL-python
RUN sudo pip install boto

ADD ./app.zip /system
ADD ./discovering.zip /mnt
ADD ./star.crt /mnt
RUN cd /system && sudo unzip -o app.zip && sudo rm -f app.zip && dir=$(ls /system/) && sudo mv /system/${dir} /system/cloud-app && sudo rm -f /system/cloud-app/bin/*.bat && \
        fileExec=$(ls /system/cloud-app/bin/) && sudo mv /system/cloud-app/bin/${fileExec} /system/cloud-app/bin/playStart && \
        cd /mnt && sudo mv star.crt /system && sudo unzip -o discovering.zip && cd docker-discovery && sudo python2.7 main.py
ADD ./runmyapp.sh /system
EXPOSE 9000
WORKDIR /system
CMD activator test

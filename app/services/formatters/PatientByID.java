package services.formatters;

import com.google.common.collect.ImmutableMap;
import services.external.EndPointConfiguration;
import utils.PathUtils;

public class PatientByID implements PathFormatter {

    private final String path;

    public PatientByID(Long institutionId) {

        path =
                PathUtils.getFormatted(EndPointConfiguration.GET_PATIENTS,
                        ImmutableMap.<String, String>builder()
                                .put("institution", String.valueOf(institutionId)).build());
    }

    @Override
    public String getFormatWSPath() {
        return path;
    }

    @Override
    public String getUnformattedWSPath() {
        return EndPointConfiguration.GET_PATIENTS;
    }

}

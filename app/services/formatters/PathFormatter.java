package services.formatters;

public interface PathFormatter {

    public String getFormatWSPath();

    public String getUnformattedWSPath();

}

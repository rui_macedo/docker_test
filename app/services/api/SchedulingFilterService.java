package services.api;

import models.scheduling.SchedulingFilter;

import java.util.List;

public interface SchedulingFilterService {

    public List<SchedulingFilter> findSchedulingFiltersBySchedulingId(Long schedulingId);

    public void deleteBySchedulingId(Long schedulingId);

}


package services.api;

import java.util.Optional;

import dto.SchedulingDTO;
import forms.FormScheduling;
import forms.FormSchedulingReservation;
import forms.voucher.FormVoucher;
import models.EmailMessage;
import models.scheduling.Scheduling;
import resource.SchedulingFullReservationResource;

public interface SchedulingService {

    public SchedulingDTO findSchedulingById(Long id);

    public Optional<SchedulingDTO> findSchedulingByIdOptional(Long id);

    public SchedulingDTO startScheduling(Long institutionId, Long employeeId);

    public void updateSchedulingOnlyInProgress(Scheduling scheduling, FormScheduling form);

    public void finishScheduling(Scheduling scheduling);

    public void makeBooking(Scheduling scheduling, FormSchedulingReservation card);

    public SchedulingFullReservationResource mapSchedulingToFullReservationResource(Scheduling scheduling);

    public void sendVoucher(Long schedulingId, String email, String subject);

    public void sendVoucher(EmailMessage email);

    public void sendVoucher(Long schedulingId);

    public void sendVoucher(Scheduling scheduling);
}


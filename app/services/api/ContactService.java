package services.api;

import dto.ContactDTO;
import dto.ContactPatientDTO;
import forms.FormContactPatient;
import models.scheduling.Contact;
import models.scheduling.ContactPatient;
import queries.ContactAllQuery;

import java.util.List;
import java.util.Optional;

public interface ContactService {

    public ContactDTO findById(Long id);

    public Optional<ContactDTO> findByIdOptional(Long id);

    public ContactPatientDTO findContactPatientById(Long id);

    public Optional<ContactPatientDTO> findContactPatientByIdOptional(Long id);

    public List<ContactDTO> list(ContactAllQuery query);

    public ContactDTO create(Long institutionId, ContactDTO request);

    public void update(Contact contact, ContactDTO request);

    public List<ContactPatientDTO> getPatientByContact(Long contactId);

    public ContactPatientDTO addPatient(Long institutionId, FormContactPatient contactPatient);

    public void updateContactPatient(ContactPatient cp,FormContactPatient contactPatient);

}

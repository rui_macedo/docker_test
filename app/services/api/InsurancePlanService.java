package services.api;

import queries.InsurancePlanQuery;
import resource.InsurancePlanResource;

import java.util.List;
import java.util.Optional;

public interface InsurancePlanService {

    public List<InsurancePlanResource> findAllEnabledByInsurance(InsurancePlanQuery query);

    public InsurancePlanResource findById(Long id);

    public Optional<InsurancePlanResource> findByIdOptional(Long id);

    public void cleanCache(InsurancePlanQuery query);

    public InsurancePlanResource findDefaultByInsurance(Long id);

    public Optional<InsurancePlanResource> findDefaultByInsuranceOptional(Long id);

}

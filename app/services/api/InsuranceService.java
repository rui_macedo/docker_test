package services.api;

import queries.InsuranceQuery;
import resource.InsurancePlanResource;
import resource.InsuranceResource;

import java.util.List;
import java.util.Optional;

public interface InsuranceService {

    public List<InsuranceResource> list(Long instituionId, InsuranceQuery query);

    public InsuranceResource findById(Long id);

    public Optional<InsuranceResource> findByIdOptional(Long id);

    public List<InsurancePlanResource> findInsurancePlans(Long insuranceId);

    public void cleanCache(Long idInstitution);
    public InsurancePlanResource getDefaultPlanForPrivateInsurance(Long institution);

    public Optional<InsurancePlanResource> getDefaultPlanForPrivateInsuranceOptional(Long institution);

    public Optional<InsuranceResource> getPrivateInsuranceOptional(Long institution);

    public InsuranceResource getPrivateInsurance(Long institution);

}

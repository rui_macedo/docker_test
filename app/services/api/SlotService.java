package services.api;

import java.util.List;

import forms.FormMessageSlot;
import queries.slots.SlotAvailableQuery;
import services.searchers.model.Card;

public interface SlotService {

  public void generate(FormMessageSlot messageSlot);

  public List<Card> getAvailableSlotsByShorterStayingTime(SlotAvailableQuery query);

  public void slotsBooking(List<Long> ids);

}

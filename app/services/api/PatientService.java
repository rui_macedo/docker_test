package services.api;

import dto.PrePatientDTO;
import forms.FormPrePatient;

import java.util.List;
import java.util.Optional;

@Deprecated
public interface PatientService {

    public PrePatientDTO savePreRegister(Long institutionId, FormPrePatient form);

    public List<PrePatientDTO> getAllPreRegisters(Long institutionId);

    public Optional<PrePatientDTO> findPreRegisterByIdOptional(Long id);

    public PrePatientDTO findPreRegisterById(Long id);

}

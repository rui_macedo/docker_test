package services.searchers.model;

import dto.InsuranceDTO;
import dto.InsurancePlanDTO;

public class AgendaWithWastingTime {

    public Long examProcedureId;
    public Book schedulingBook;
    public Integer wastingTime;
    public Integer numberNecessarySlots;
    public Integer slotSize;
    public InsuranceDTO insurance;
    public InsurancePlanDTO insurancePlan;


    public AgendaWithWastingTime(Long examProcedureId, Book schedulingBook, Integer wastingTime,
                                 Integer numberNecessarySlots, Integer slotSize, InsuranceDTO insurance,
                                 InsurancePlanDTO insurancePlan) {
        this.examProcedureId = examProcedureId;
        this.schedulingBook = schedulingBook;
        this.wastingTime = wastingTime;
        this.numberNecessarySlots = numberNecessarySlots;
        this.slotSize = slotSize;
        this.insurance = insurance;
        this.insurancePlan = insurancePlan;
    }

}

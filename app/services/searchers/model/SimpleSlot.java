package services.searchers.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.deserializers.TimestampDateDeserializer;

import common.annotations.serializers.TimestampDateSerializer;

import org.joda.time.DateTime;

import java.io.Serializable;

public class SimpleSlot implements Serializable {

    public Long id;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime start;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime end;

}

package services.searchers.model;

import dto.InsuranceDTO;
import dto.InsurancePlanDTO;

public class SimpleInsurance {

    public InsuranceDTO insurance;
    public InsurancePlanDTO insurancePlan;

    public SimpleInsurance(InsuranceDTO insurance, InsurancePlanDTO insurancePlan) {
        this.insurance = insurance;
        this.insurancePlan = insurancePlan;
    }
}

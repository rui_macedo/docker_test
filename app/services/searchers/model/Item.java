package services.searchers.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.deserializers.TimestampDateDeserializer;

import common.annotations.serializers.TimestampDateSerializer;

import dto.InsuranceDTO;
import dto.InsurancePlanDTO;
import dto.PhysicianDTO;
import dto.RestrictionDTO;
import dto.UnityDTO;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Item implements Serializable {

    public SimpleExamProcedure examProcedure;

    @JsonIgnore
    public UnityDTO unity;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime start;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime end;

    public Duration duration;

    public PhysicianDTO physician;

    public InsuranceDTO insurance;

    public InsurancePlanDTO insurancePlan;

    public List<RestrictionDTO> restrictions;

    public List<SimpleSlot> slots;

    public Item() {}

    public Item(SimpleExamProcedure examProcedure, DateTime start) {
        this.examProcedure = examProcedure;
        this.start = start;
        this.slots = new ArrayList<>();
        this.restrictions = new ArrayList<>();
    }

    public Item(SimpleExamProcedure examProcedure) {
        this.examProcedure = examProcedure;
        this.slots = new ArrayList<>();
        this.restrictions = new ArrayList<>();
    }

    public Item(UnityDTO unity, SimpleExamProcedure examProcedure, DateTime start, PhysicianDTO physician,
                List<RestrictionDTO> restrictions, InsuranceDTO insurance, InsurancePlanDTO insurancePlan ) {
        this.unity = unity;
        this.examProcedure = examProcedure;
        this.start = start;
        this.physician = physician;
        this.slots = new ArrayList<>();
        this.restrictions = restrictions;
        this.insurance = insurance;
        this.insurancePlan = insurancePlan;
    }
}

package services.searchers.model;

import org.apache.commons.lang.math.NumberUtils;

import java.util.List;

import dto.PhysicianDTO;
import dto.RestrictionDTO;
import dto.UnityDTO;

public class Book {

    public Long id;
    public UnityDTO unity;
    public Integer slotSize;
    public List<RestrictionDTO> restrictions;
    public PhysicianDTO physician;

    public Book(Long id, UnityDTO unity, Integer slotSize, List<RestrictionDTO> restrictions, PhysicianDTO physician) {
        this.id = id;
        this.slotSize = slotSize;
        this.restrictions = restrictions;
        this.physician = physician;
        this.unity = unity;
    }

    public Integer getNumberOfNecessarySlots(Integer examProcedureDuration){

        if(examProcedureDuration <= slotSize){
            return NumberUtils.INTEGER_ONE;
        }


        return (int) Math.ceil(((double)examProcedureDuration / (double)slotSize));
    }

}

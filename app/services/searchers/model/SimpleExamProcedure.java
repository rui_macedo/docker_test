package services.searchers.model;

import java.io.Serializable;
import java.util.Set;

import dto.ExamModalityDTO;

public class SimpleExamProcedure implements Serializable {

    public Long id;
    public String name;
    public Set<String> warnings;
    public Duration duration;
    public ExamModalityDTO modality;

}

package services.searchers.model;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

public class Duration implements Serializable {

    public Integer time;
    public TimeUnit unit;

    public Duration(){}

    public Duration(Integer time){
        this.unit = TimeUnit.MINUTES;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Duration{time=" + time +" " + unit.name() + "}";
    }
}

package services.searchers.model;

import java.util.List;

import dto.InsuranceDTO;
import dto.InsurancePlanDTO;
import models.SchedulingBook;
import models.Slot;

public class ExamProcedureWithAvailableSlots {

    public SimpleExamProcedure examProcedure;
    public Book schedulingBook;
    public InsuranceDTO insurance;
    public InsurancePlanDTO insurancePlan;
    public List<Slot> slots;

    public ExamProcedureWithAvailableSlots(SimpleExamProcedure examProcedure) {
        this.examProcedure = examProcedure;
    }

    public ExamProcedureWithAvailableSlots(SimpleExamProcedure examProcedure, Book schedulingBook, List<Slot> slots) {
        this.examProcedure = examProcedure;
        this.schedulingBook = schedulingBook;
        this.slots = slots;
    }
}

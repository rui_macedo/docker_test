package services.searchers.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.deserializers.TimestampDateDeserializer;

import common.annotations.serializers.TimestampDateSerializer;

import dto.UnityDTO;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;

public class Card implements Serializable {

    public List<Item> items;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime start;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime end;

    public Duration duration;

    public UnityDTO unity;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime date;

    public Card() {}

    public Card(List<Item> items, DateTime start, DateTime end, Integer duration) {
        this.items = items;
        this.start = start;
        this.end = end;
        this.date = start;
        this.duration = new Duration(duration);
    }

    public Card(List<Item> items, DateTime start, DateTime end, Integer duration, UnityDTO unity) {
        this.items = items;
        this.start = start;
        this.end = end;
        this.date = start;
        this.duration = new Duration(duration);
        this.unity = unity;
    }

 }

package services.searchers.finder;

import com.google.inject.Inject;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.Minutes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import dto.InsuranceDTO;
import dto.InsurancePlanDTO;
import models.ExamProcedure;
import models.Insurance;
import models.InsurancePlan;
import models.Restriction;
import models.SchedulingBook;
import models.Slot;
import play.libs.F;
import queries.slots.PeriodQuery;
import queries.slots.SlotAvailableQuery;
import services.api.InsuranceService;
import services.external.api.ExamProcedureService;
import services.searchers.model.AgendaWithWastingTime;
import services.searchers.model.Book;
import services.searchers.model.Card;
import services.searchers.model.Duration;
import services.searchers.model.ExamProcedureWithAvailableSlots;
import services.searchers.model.Item;
import services.searchers.model.SimpleExamProcedure;
import services.searchers.model.SimpleInsurance;
import services.searchers.model.SimpleSlot;
import services.searchers.sorter.SlotSearchResultSorter;
import services.searchers.validator.SlotSearchResultValidator;
import utils.CollectionCombinationUtils;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

public class ShorterStayingFinder implements SlotFinder<Card>{

    @Inject
    public static ExamProcedureService examProcedureService;

    @Inject
    public static InsuranceService insuranceService;

    private SlotSearchResultValidator validator;

    private SlotSearchResultSorter sorter;


    public ShorterStayingFinder(SlotSearchResultValidator validator, SlotSearchResultSorter sorter) {
        this.validator = validator;
        this.sorter = sorter;
    }

    @Override
    public List<Card> find(SlotAvailableQuery query){

        Map<SimpleExamProcedure, F.Promise<List<AgendaWithWastingTime>>> epWithSchedulingBooks =
                getExamProcedureWithPossibleBooks(query);

        List<F.Promise<ExamProcedureWithAvailableSlots>>  wpWithAvailableSlots =
                getAvailableSlotsByExamProcedureAndBook(epWithSchedulingBooks, query.period);

        Map<SimpleExamProcedure, List<Item>> items = getItemByCombinations(wpWithAvailableSlots);

        List<Card> cards = buildCards(items);

        return sorter.sort(cards);
    }

    private Map<SimpleExamProcedure, F.Promise<List<AgendaWithWastingTime>>> getExamProcedureWithPossibleBooks(SlotAvailableQuery query){

        Map<SimpleExamProcedure, F.Promise<List<AgendaWithWastingTime>>> epWithSchedulingBooks = new HashMap<>();
        query.examProcedures.parallelStream().forEachOrdered(examProcedureId -> {

                    SimpleInsurance resolveInsurance = resolveInsurance(
                            query.institutionId,
                            examProcedureId,
                            query.insurance,
                            query.insurancePlan
                    );

                    ExamProcedure examProcedure = ExamProcedure.findById(examProcedureId);

                    epWithSchedulingBooks.put(examProcedure.wrapAsSimple(), F.Promise.pure(
                                    getBooksOrderedByLessWastingTime(examProcedure, query.institutionId,
                                            resolveInsurance.insurance, resolveInsurance.insurancePlan))
                    );
                }
        );
        return epWithSchedulingBooks;
    }


    private List<F.Promise<ExamProcedureWithAvailableSlots>> getAvailableSlotsByExamProcedureAndBook(
            Map<SimpleExamProcedure, F.Promise<List<AgendaWithWastingTime>>> epWithSchedulingBooks,
            PeriodQuery period){

        List<F.Promise<ExamProcedureWithAvailableSlots>>  result = new ArrayList<>();

        epWithSchedulingBooks.entrySet().parallelStream().forEachOrdered(entry -> {

            result.add(F.Promise.pure(
                    findSuitableSlotsByTimeTable(entry.getKey(), entry.getValue(), period.startAt, period.endAt)
            ));
        });
        return result;

    }

    private Map<SimpleExamProcedure, List<Item>>  getItemByCombinations(List<F.Promise<ExamProcedureWithAvailableSlots>> wpWithAvailableSlots ){
        Map<SimpleExamProcedure, List<Item>> list = new HashMap<>();
        for(F.Promise<ExamProcedureWithAvailableSlots> promise : wpWithAvailableSlots){
            ExamProcedureWithAvailableSlots availableSlots = promise.get(100, TimeUnit.MILLISECONDS);
            list.put(availableSlots.examProcedure,  buildItemAccordingSlotTime(availableSlots));
        }

        return list;
    }

    private List<Card> buildCards(Map<SimpleExamProcedure, List<Item>> items){

        for(Map.Entry<SimpleExamProcedure, List<Item>> item : items.entrySet()){
            System.out.println(" item: " + item.getKey().name + " slots " + item.getValue().size());
        }

        List<Card> cards = new ArrayList<>();
        CollectionCombinationUtils.ofCombinations(items.values())

                .forEach(item -> {

                    item = sorter.sortItems(item);
                    DateTime start = item.get(NumberUtils.INTEGER_ZERO).start;
                    DateTime end = item.get(item.size() - 1).end;
                    if (start.isAfter(end) || ObjectUtils.notEqual(start.dayOfMonth(), end.dayOfMonth())) {
                        return;
                    }

                    if (validator.isValid(item)) {

                        cards.add(new Card(item, start, end,
                                Minutes.minutesBetween(start, end).getMinutes(),
                                item.get(0).unity
                        ));
                    }

                });
        return cards;

    }

    public SimpleInsurance resolveInsurance(Long institutionId, Long examProcedureId, Long insuranceId, Long insurancePlanId){

        /**
         * As the user can choice a exam-procedure which is not covered by plan( it means that's covered by private default one)
         * we have to retrieve the identifier of the right insurance and insurance plan to seek available slots properly
         */
        if (!examProcedureService.examProcedureIsCoveredByPlan(examProcedureId, insuranceId, insurancePlanId)) {

            return new SimpleInsurance(insuranceService.getPrivateInsurance(institutionId).asDTO(),
                    insuranceService.getDefaultPlanForPrivateInsurance(institutionId).asDTO());
        }

        return new SimpleInsurance(Insurance.findById(insuranceId).wrapDTO(), InsurancePlan.findById(insurancePlanId).wrap());
    }


    private List<AgendaWithWastingTime> getBooksOrderedByLessWastingTime(ExamProcedure examProcedure, Long institutionId,
                                                                         InsuranceDTO insurance, InsurancePlanDTO insurancePlan){

        List<Book> books = SchedulingBook.getBooks(institutionId, examProcedure.id, insurance.id, insurancePlan.id);
        List<AgendaWithWastingTime> byWastingTime = new ArrayList<>();

        books.parallelStream().forEachOrdered(book -> {
            Integer numberOfSlots = book.getNumberOfNecessarySlots(examProcedure.duration);
            Integer wastingTime = (book.slotSize * numberOfSlots) - examProcedure.duration;
            byWastingTime.add(
                    new AgendaWithWastingTime(examProcedure.id, book, wastingTime,
                            numberOfSlots, book.slotSize, insurance, insurancePlan )
            );
        });

        return sortListByLessWastingTime(byWastingTime);
    }

    private List<AgendaWithWastingTime> sortListByLessWastingTime(List<AgendaWithWastingTime> byWastingTime){

        Comparator<AgendaWithWastingTime> byLessWastingTime = (AgendaWithWastingTime a1,AgendaWithWastingTime a2) ->
                Integer.compare(a1.wastingTime, a2.wastingTime);

        Comparator<AgendaWithWastingTime> byLessSlots = (AgendaWithWastingTime a1,AgendaWithWastingTime a2) ->
                Integer.compare(a1.numberNecessarySlots, a2.numberNecessarySlots);

        return byWastingTime
                .parallelStream()
                .sorted(byLessWastingTime.thenComparing(byLessSlots))
                .collect(toList());
    }


    private ExamProcedureWithAvailableSlots findSuitableSlotsByTimeTable(SimpleExamProcedure ep,F.Promise<List<AgendaWithWastingTime>> promiseAgendas ,
                                                                         DateTime start, DateTime end){
        /**
         * First position HAS to be the best book about less wasting time and less allocated number
         * of slots to run the exam-procedure
         */
        ExamProcedureWithAvailableSlots epWithAvailableSlots = new ExamProcedureWithAvailableSlots(ep);

        for(AgendaWithWastingTime agenda : promiseAgendas.get(100, TimeUnit.MILLISECONDS)){
            /**
             * Just take the first group of slot available and break the loop for performance purpose
             */
            epWithAvailableSlots.slots  = Slot.findAllAvailableByBookAndDateBetween(agenda.schedulingBook.id, start, end);
            if(CollectionUtils.isNotEmpty(epWithAvailableSlots.slots)){
                epWithAvailableSlots.schedulingBook = agenda.schedulingBook;
                epWithAvailableSlots.insurance = agenda.insurance;
                epWithAvailableSlots.insurancePlan = agenda.insurancePlan;
                break;
            }
        }

        return epWithAvailableSlots;

    }


    private List<Item> buildItemAccordingSlotTime(ExamProcedureWithAvailableSlots procedureWithAvailableSlots){

        if(isNull(procedureWithAvailableSlots.schedulingBook)){
            return Collections.emptyList();
        }

        int numberOfSlots = procedureWithAvailableSlots.schedulingBook.getNumberOfNecessarySlots(
                procedureWithAvailableSlots.examProcedure.duration.time
        );

        procedureWithAvailableSlots.schedulingBook.restrictions = Restriction.findByBook(procedureWithAvailableSlots.schedulingBook.id);

        List<Item> items = new ArrayList<>();
        int totalLimit = procedureWithAvailableSlots.slots.size() - 1;

        /**
         * If we need only one slot we can increase index by one (numberOfSlots = 1), but ...
         * if we need more than one, we have to increase the index by the number of necessary slots.
         * This way we can avoid to go through all list and can skip some positions
         */

        int index = 0;
        while(index < totalLimit){

            boolean increaseByOne = false;

            /**
             * Retrieving the first slot and build a valid card (the start date of card will be
             * the start date of the first slot that we will put into card)
             */
            Slot slot = procedureWithAvailableSlots.slots.get(index);

            Item item = new Item(
                    procedureWithAvailableSlots.schedulingBook.unity,
                    procedureWithAvailableSlots.examProcedure,
                    slot.startAt,
                    procedureWithAvailableSlots.schedulingBook.physician,
                    procedureWithAvailableSlots.schedulingBook.restrictions,
                    procedureWithAvailableSlots.insurance,
                    procedureWithAvailableSlots.insurancePlan);

            SimpleSlot slotDto = slot.wrapWithRestrictions();
            item.slots.add(slotDto);

            DateTime endDateReference = slotDto.end;

            /**
             * If i need only one slot to run the exam procedure, we have to iterate each slot and creating a single card
             * but if we need more than one slot we have to go through a subList
             */

            int toIndex = (index + (numberOfSlots-1));
            if(numberOfSlots > 1 && (index + (numberOfSlots-1)) <= totalLimit){

                int fromIndex = index + 1;
                for(int j = fromIndex; j <= toIndex ; j++){

                    Slot slotAux = procedureWithAvailableSlots.slots.get(j);
                    SimpleSlot subSlotDto = slotAux.wrapWithRestrictions();
                    /**
                     * Check to make sure there's no gap between slots.
                     */
                    if(subSlotDto.start.isEqual(endDateReference)){

                        endDateReference = subSlotDto.end;
                        item.slots.add(subSlotDto);

                        if(ObjectUtils.equals(j, toIndex)){
                            item.end = slotAux.endAt;
                            item.duration = new Duration(Minutes.minutesBetween(item.start, item.end).getMinutes());
                        }
                    } else {
                        increaseByOne = true;
                        break;
                    }
                }

            } else {
                /**
                 * When we need only one slot, we already put it into card, just need to build a end date (that
                 * will be the end date of the slot) and duration time ( end date - start date in minutes)
                 */
                item.end = slot.endAt;
                item.duration = new Duration(Minutes.minutesBetween(item.start, item.end).getMinutes());
            }
            /**
             * Sometimes we do not have the number of slots available to run the exam-procedure
             */
            if(item.slots.size() == numberOfSlots){
                items.add(item);
            }

/*
            index = increaseByOne ? index + NumberUtils.INTEGER_ONE : index + numberOfSlots;
*/
            index++;
        }
        return items;
    }


}

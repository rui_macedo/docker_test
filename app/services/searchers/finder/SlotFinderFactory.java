package services.searchers.finder;

import org.apache.commons.lang.ObjectUtils;

import services.searchers.SlotSearchType;
import services.searchers.sorter.ShorterStayingSorter;
import services.searchers.validator.ShorterStayingCardValidator;

public class SlotFinderFactory {

    public static SlotFinder getInstance(SlotSearchType type){

        if(ObjectUtils.equals(type, SlotSearchType.SHORTER_STAYING)){
            return new ShorterStayingFinder(
                    new ShorterStayingCardValidator(),
                    new ShorterStayingSorter()
            );
        }

        throw new IllegalArgumentException("Type " + type.name() + " not supported");
    }

}

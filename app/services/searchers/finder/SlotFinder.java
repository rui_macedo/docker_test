package services.searchers.finder;

import java.util.List;

import queries.slots.SlotAvailableQuery;

public interface SlotFinder<T> {

   public List<T> find(SlotAvailableQuery query);

}

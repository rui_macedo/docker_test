package services.searchers;

import java.util.List;

import queries.slots.SlotAvailableQuery;
import services.searchers.finder.SlotFinder;

public interface SearchProvider<T> {

    public List<T> getAll(SlotAvailableQuery query, SlotFinder strategy);

}

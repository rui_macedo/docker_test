package services.searchers;

import services.searchers.serializer.ByteSerializer;
import services.searchers.serializer.DummySerializer;
import services.searchers.serializer.ObjectSerializer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;


import play.Play;

public class SlotSearchConfiguration {

    private final String cacheKeyPrefix;

    private final int cacheTTL;

    private final boolean cacheEnabled;

    private final ObjectSerializer serializer;

    public SlotSearchConfiguration() {
        this.cacheKeyPrefix = "scheduling.query.";

        cacheTTL = Play.application()
                .configuration()
                .getInt("slots.search.query.results.cache.ttl", 120);

        cacheEnabled = Play.application()
                .configuration()
                .getBoolean("slots.search.query.results.cache.enabled", true);

        serializer = new ByteSerializer();
    }


    public SlotSearchConfiguration(String cacheKeyPrefix, int cacheTTL, boolean cacheEnabled,
                                   ObjectSerializer serializer) {
        this.cacheKeyPrefix = cacheKeyPrefix;
        this.cacheTTL = cacheTTL;
        this.cacheEnabled = cacheEnabled;
        this.serializer = serializer;
    }

    public static SlotSearchConfiguration DEFAULT_CONFIG() {
        return new SlotSearchConfiguration();
    }

    public static SlotSearchConfiguration DISABLE_CACHE() {
        return new SlotSearchConfiguration(StringUtils.EMPTY,
                NumberUtils.INTEGER_ZERO, false, new DummySerializer());
    }

    public String getCacheKeyPrefix() {
        return cacheKeyPrefix;
    }

    public int getCacheTTL() {
        return cacheTTL;
    }

    public boolean isCacheEnabled() {
        return cacheEnabled;
    }

    public ObjectSerializer getSerializer() {
        return serializer;
    }
}

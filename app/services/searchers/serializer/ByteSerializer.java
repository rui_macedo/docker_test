package services.searchers.serializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.List;

import play.Logger;
import services.searchers.model.Card;

public class ByteSerializer implements ObjectSerializer<Card> {

    @Override
    public byte[] serialize(List<Card> cards) {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        try {
            try {
                out = new ObjectOutputStream(bos);
                out.writeObject(cards);
            } catch (IOException e) {
                Logger.error("Deu ruim", e);
            }
            return bos.toByteArray();

        } finally {
            try{
                if (out != null) {
                    out.close();
                }
                bos.close();
            } catch (IOException e) {
                Logger.error("Deu ruim", e);
            }
        }
    }

    @Override
    public List<Card> deserialize(byte[] object) {

        ByteArrayInputStream bis = new ByteArrayInputStream(object);
        ObjectInput in = null;
        try {
            try {
                in = new ObjectInputStream(bis);
                return (List<Card>) in.readObject();
            } catch (IOException  | ClassNotFoundException e) {
                Logger.error("Error deserializing object", e);
            }
        } finally {
            try {
                bis.close();
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
        }
        return Collections.emptyList();

    }
}

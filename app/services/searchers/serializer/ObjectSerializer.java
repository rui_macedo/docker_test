package services.searchers.serializer;

import java.util.List;

public interface ObjectSerializer<T> {

    public byte[] serialize(List<T> cards);

    public List<T> deserialize(byte[] object);

}

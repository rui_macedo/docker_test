package services.searchers.serializer;

import java.util.Collections;
import java.util.List;

import services.searchers.model.Card;

public class DummySerializer implements ObjectSerializer<Card>{

    @Override
    public byte[] serialize(List<Card> cards) {
        //TODO do nothing
        return new byte[0];
    }

    @Override
    public List<Card> deserialize(byte[] object) {
        //TODO do nothing
        return Collections.emptyList();
    }
}

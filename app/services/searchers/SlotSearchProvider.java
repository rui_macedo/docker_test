package services.searchers;

import java.util.*;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import play.Logger;
import queries.slots.SlotAvailableQuery;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import services.searchers.finder.SlotFinder;
import services.searchers.model.Card;

import static java.util.Objects.isNull;


public class SlotSearchProvider implements SearchProvider<Card> {

    private JedisPool jedisPool;

    private SlotSearchConfiguration config;

    private final class PopulateCache implements Runnable {

        private final JedisPool jedisPool;
        private final String key;
        private final List<Card> cards;

        private PopulateCache(JedisPool jedisPool, String key, List<Card> cards) {
            this.jedisPool = jedisPool;
            this.cards = cards;
            this.key = key;
        }

        @Override
        public void run(){
            /**
             * Instead of blocking execution, populate cache async
             */
            if(config.isCacheEnabled() && CollectionUtils.isNotEmpty(cards)){

                Logger.debug("Populating cache with {} results", cards.size());

                try(Jedis jedis = jedisPool.getResource()){
                    jedis.setex(key.getBytes(), config.getCacheTTL(), config.getSerializer().serialize(cards));
                }catch (Exception ex) {
                    Logger.error("Could not populate cache.",  ex);
                }

            }
        }
    }

    public SlotSearchProvider(JedisPool jedisPool, SlotSearchConfiguration config){

        this.jedisPool = jedisPool;
        this.config = config;

        Logger.debug("Setting up SlotSearchProvider. CacheEnabled = {}, TTL ={}",
                config.isCacheEnabled(), config.getCacheTTL());
    }

    public SlotSearchProvider(JedisPool jedisPool) {

        this.config = SlotSearchConfiguration.DEFAULT_CONFIG();
        this.jedisPool = jedisPool;

        Logger.debug("Setting up SlotSearchProvider. CacheEnabled = {}, TTL ={}",
                config.isCacheEnabled(), config.getCacheTTL());
    }

    @Override
    public List<Card> getAll(SlotAvailableQuery query, SlotFinder strategy) {

        if(isNull(query)){
            return Collections.emptyList();
        }

        String cacheKey = getChecksum(query);
        List<Card> results = retrieveFromCache(cacheKey);

        if(CollectionUtils.isEmpty(results)){
           results = strategy.find(query);
           if(CollectionUtils.isNotEmpty(results)){
               Logger.info("Slots available search results got from DB");
               new PopulateCache(jedisPool, cacheKey, results).run();
           }
        }else{
            Logger.info("Slots available search results got from cache");
        }

        return limitResults(query.limit, query.offset, results);
    }

    private String getChecksum(SlotAvailableQuery query){

        if(! config.isCacheEnabled()){
            return StringUtils.EMPTY;
        }

        return new StringBuffer(config.getCacheKeyPrefix())
                .append(DigestUtils.md5Hex(query.toString()))
                .toString();
    }

    private List<Card> retrieveFromCache(String key){

        if(! config.isCacheEnabled()){
            return Collections.emptyList();
        }

        try(Jedis jedis = jedisPool.getResource()){

            byte[] result = jedis.get(key.getBytes());
            if(isNull(result)){
                return Collections.emptyList();
            }

            return config.getSerializer().deserialize(result);

        }catch (Exception ex) {
            Logger.error("Could not retrieve from cache.",  ex);
        }

        return Collections.emptyList();
    }

    private List<Card> limitResults(Integer limit, Integer offset, List<Card> result){

        if(isNull(limit) || isNull(offset) ||
              ObjectUtils.equals(limit, NumberUtils.INTEGER_ZERO) ||
                  ObjectUtils.equals(offset, NumberUtils.INTEGER_ZERO)){
            return result;
        }

        if(CollectionUtils.isEmpty(result)){
            return result;
        }

        int resultLength = result.size() - 1;
        int length = (limit + offset) - 1 > resultLength ? resultLength : limit;

        return result.subList(offset -1, length);

    }
}


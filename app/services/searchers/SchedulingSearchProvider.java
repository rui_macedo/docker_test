package services.searchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import models.ExamProcedure;
import models.Institution;
import models.Resource;
import models.SchedulingBook;
import models.SchedulingBookPeriod;
import models.Unity;
import models.searchalg.AlgorithmConfiguration;
import models.searchalg.DynamicSlotGroup;
import models.searchalg.ScheduleDailyCard;
import models.searchalg.analyzers.ListAllAnalyzer;
import models.searchalg.dayiterator.DayIteratorFactory;
import models.searchalg.dayiterator.DayIteratorStrategy;
import models.searchalg.finders.FreeSlotsByExamProcedureFinderStrategy;
import models.searchalg.finders.FreeSlotsByExamProcedureListFinderStrategy;
import models.searchalg.finders.ScheduledSlotsByResourceFinderStrategy;
import models.searchalg.validators.AlwaysTrueValidator;
import models.searchalg.validators.MaxDynamicSlotGroupDurationValidator;
import models.searchalg.DynamicSlot;
import models.searchalg.analyzers.SetOfExamsAnalyzer;
import models.searchalg.dayiterator.PlusDayStrategy;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import play.Logger;
import play.libs.F.Function;
import play.libs.F.Function0;
import play.libs.F.Promise;
import common.constants.CalendarDay;
import common.constants.DayShift;
import common.utils.DateUtils;

public class SchedulingSearchProvider {

  public static List<ScheduleDailyCard> search(Integer minResults, AlgorithmConfiguration algthConfig, List<DayShift> periods, ExamProcedure... examProcedures) {

    List<ScheduleDailyCard> cardResults = new ArrayList<ScheduleDailyCard>();
    int resultCont = 0;
    while (resultCont < minResults && algthConfig.getDayIteratorStrategy().hasNext()) {

      final DateTime finalDayToSearch =
          algthConfig.getDayIteratorStrategy().getCurrentIteratonDay().toDateTime();
      List<Promise<DynamicSlotGroup>> slotsByExamProcedure =
          algthConfig.getFinderStrategy().getPromiseListOfDynamicSlotGroup(finalDayToSearch, periods);
      Promise<ScheduleDailyCard> scheduleDailyCardPromisse =
          Promise.sequence(slotsByExamProcedure).map(
              new Function<List<DynamicSlotGroup>, ScheduleDailyCard>() {
                public ScheduleDailyCard apply(List<DynamicSlotGroup> dynamicSlotGroup) {
                  algthConfig.getDynamicSlotGroupAnalyzer().setStudySet(dynamicSlotGroup);
                  List<DynamicSlotGroup> slotsGroups =
                      algthConfig.getDynamicSlotGroupAnalyzer().analyze(
                          algthConfig.getDynamicSlotGroupValidator());
                  return new ScheduleDailyCard(
                      algthConfig.getCardInfoType().getPriorityInfoValue(), algthConfig
                          .getCardInfoType().getPriorityInfoLabel(), finalDayToSearch,
                      Arrays.asList(examProcedures), slotsGroups);
                }
              });

      ScheduleDailyCard scheduleDailyCard = scheduleDailyCardPromisse.get(60000);

      cardResults.add(scheduleDailyCard);
      algthConfig.getDayIteratorStrategy().next();
      resultCont += scheduleDailyCard.getSize();
    }
    return cardResults;
  }

  public static List<ScheduleDailyCard> searchAllByExamProcedure(ExamProcedure examProcedure,
      DateTime dtStart,  DateTime dtEnd,  List<DayShift> periods, List<CalendarDay> clDays, List<Unity> listUnity,
      Institution institution, Integer minResults) {
    final DayIteratorStrategy dayIteratorStrategy =
        DayIteratorFactory.createDayIteratorStrategy(dtStart, dtEnd, clDays);

   FreeSlotsByExamProcedureFinderStrategy strategy =    new FreeSlotsByExamProcedureFinderStrategy(
           examProcedure, listUnity, institution
   );

    AlgorithmConfiguration config = new AlgorithmConfiguration(
            new ListAllAnalyzer(),
            new AlwaysTrueValidator(),
            strategy,
            AlgorithmConfiguration.CardInfoType.LIST_OF_DAY, dayIteratorStrategy);
    return search(minResults, config, periods, examProcedure);
  }

  public static List<ScheduleDailyCard> searchGoodSetsOfSlotByExamProcedures(
      List<ExamProcedure> examProcedures, DateTime dtStart, DateTime dtEnd, List<DayShift> periods, List<CalendarDay> clDays, List<Unity> listUnity,
      Institution institution, Integer minResults, Long maxTolerance ) {
    final DayIteratorStrategy dayIteratorStrategy =
        DayIteratorFactory.createDayIteratorStrategy(dtStart, dtEnd, clDays);
    AlgorithmConfiguration config =
        new AlgorithmConfiguration(new SetOfExamsAnalyzer(),
            new MaxDynamicSlotGroupDurationValidator(new Duration(maxTolerance)), new FreeSlotsByExamProcedureListFinderStrategy(
                    examProcedures, listUnity, institution),
            AlgorithmConfiguration.CardInfoType.SET_OF_DATE, dayIteratorStrategy);
    /*
     * this service will search combinations of all exams in the same results, so, we need to
     * multiply the results, because one result (valid card) will contains a combination of N(number
     * of exams) slots
     */
    minResults = minResults * examProcedures.size();
    return search(minResults, config, periods, examProcedures.toArray(new ExamProcedure[examProcedures.size()]));
  }

  
  public static List<ScheduleDailyCard> searchScheduledSlotInDayByResource(Resource resource,
      DateTime dayToSearch) {
    AlgorithmConfiguration config =
        new AlgorithmConfiguration(new ListAllAnalyzer(), new AlwaysTrueValidator(),
            new ScheduledSlotsByResourceFinderStrategy(resource),
            AlgorithmConfiguration.CardInfoType.SCHEDULED_SLOTS,
            new PlusDayStrategy(dayToSearch, 1));
    List<ExamProcedure> fakeExam = Collections.emptyList();
    List<DayShift> fakeDayPeriods = Collections.emptyList();
    return search(1, config, fakeDayPeriods, fakeExam.toArray(new ExamProcedure[fakeExam.size()]));
  }
  
  public static List<ScheduleDailyCard> searchScheduledSlotInWeekByResource(Resource resource,
      DateTime referenceWeekDay) {
    AlgorithmConfiguration config =
        new AlgorithmConfiguration(new ListAllAnalyzer(), new AlwaysTrueValidator(),
            new ScheduledSlotsByResourceFinderStrategy(resource),
            AlgorithmConfiguration.CardInfoType.SCHEDULED_SLOTS,
            new PlusDayStrategy(DateUtils.getPrevioustDay(referenceWeekDay, 7), 7));
    List<ExamProcedure> fakeExam = Collections.emptyList();
    List<DayShift> fakeDayPeriods = Collections.emptyList();
    return search(100000, config, fakeDayPeriods, fakeExam.toArray(new ExamProcedure[fakeExam.size()]));
  }

  private static Promise<List<DynamicSlot>> getBookFreeSlotsByDay(DateTime dayToSearch, List<DayShift> namedPeriods,
      Interval searchIntervalParam, SchedulingBook book, ExamProcedure examProcedure) {

    List<Promise<List<DynamicSlot>>> promissesOfDynamicSlot =
        new ArrayList<Promise<List<DynamicSlot>>>();
    
    List<SchedulingBookPeriod> periods = SchedulingBookPeriod.findSpecificOverlapedPeriodsByCalendarDay(book, CalendarDay.getByDayIdx(dayToSearch.getDayOfWeek()), namedPeriods);
    for (SchedulingBookPeriod schedulingBookPeriod : periods) {
      promissesOfDynamicSlot.add(Promise.promise(new Function0<List<DynamicSlot>>() {
        public List<DynamicSlot> apply() {
          return DynamicSlot.convertList(
              schedulingBookPeriod.getAvaibleIntervalSlotsInADay(searchIntervalParam, namedPeriods),
              schedulingBookPeriod, examProcedure);
        }
      }));
      Promise<List<DynamicSlot>> promiseOfResult =
          Promise.sequence(promissesOfDynamicSlot).map(
              new Function<List<List<DynamicSlot>>, List<DynamicSlot>>() {
                public List<DynamicSlot> apply(List<List<DynamicSlot>> listOfListOfSlots) {
                  List<DynamicSlot> listOfSlots = new ArrayList<DynamicSlot>();
                  for (List<DynamicSlot> iList : listOfListOfSlots) {
                    listOfSlots.addAll(iList);
                  }
                  return listOfSlots;
                }
              });
      return promiseOfResult;
    }

    return Promise.promise(new Function0<List<DynamicSlot>>() {
      public List<DynamicSlot> apply() {
        return Collections.emptyList();
      }
    });
  }


  public static Promise<DynamicSlotGroup> findFreeSlotsInDayByExamProcedure(DateTime dateRef, List<DayShift> periods,
      ExamProcedure examProcedure, List<Unity> unities, Institution institution) {
    List<SchedulingBook> books;
    if (unities == null || unities.isEmpty()) {
      books = SchedulingBook.findByTenant(institution.getId());
    } else {
      books = SchedulingBook.findByUnities(unities);
    }
    if (books != null) {
      Interval searchIntervalParam = new Interval(dateRef, dateRef.withTime(23, 59, 59, 999));
      List<Promise<List<DynamicSlot>>> promiseOfSlotsByBook =
          new ArrayList<Promise<List<DynamicSlot>>>();
      for (SchedulingBook book : books) {
        promiseOfSlotsByBook.add(getBookFreeSlotsByDay(dateRef, periods, searchIntervalParam, book,
            examProcedure));
      }

      if (!promiseOfSlotsByBook.isEmpty()) {
        Promise<DynamicSlotGroup> dailySlots =
            Promise.sequence(promiseOfSlotsByBook).map(
                new Function<List<List<DynamicSlot>>, DynamicSlotGroup>() {
                  public DynamicSlotGroup apply(List<List<DynamicSlot>> slots) {
                    List<DynamicSlot> slotsOfDay = new ArrayList<DynamicSlot>();
                    for (List<DynamicSlot> listOfSlotsByBook : slots) {
                      slotsOfDay.addAll(listOfSlotsByBook);
                    }
                    return new DynamicSlotGroup(slotsOfDay);
                  }
                });
        return dailySlots;
      }
    }
    Logger.info("Unable to find any book for this category in this institution/unity");
    return Promise.pure(new DynamicSlotGroup(Collections.emptyList()));
  }
}

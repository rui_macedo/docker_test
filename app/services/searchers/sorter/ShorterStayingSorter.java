package services.searchers.sorter;

import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import services.searchers.model.Card;
import services.searchers.model.Item;

import static java.util.stream.Collectors.toList;

public class ShorterStayingSorter implements SlotSearchResultSorter {

    @Override
    public List<Card> sort(List<Card> cards) {
        if(CollectionUtils.isEmpty(cards)){
            return Collections.emptyList();
        }

        Comparator<Card> startRule = (Card a1, Card a2) -> a1.start.compareTo(a2.start);

        Comparator<Card> endRule = (Card a1, Card a2) -> a1.end.compareTo(a2.end);

        Comparator<Card> stayingTimeRule = (Card a1, Card a2) -> Integer.compare(a1.duration.time, a2.duration.time);


        return cards
                .parallelStream()
                .sorted(
                        startRule
                                .thenComparing(endRule)
                                .thenComparing(stayingTimeRule)
                ).collect(toList());
    }

    @Override
    public List<Item> sortItems(List<Item> items) {

        Comparator<Item> startRule = (Item a1, Item a2) -> a1.start.compareTo(a2.start);

        Comparator<Item> endRule = (Item a1, Item a2) -> a1.end.compareTo(a2.end);

        return items
                .parallelStream()
                .sorted(startRule.thenComparing(endRule))
                .collect(toList());
    }

}

package services.searchers.sorter;

import java.util.List;

import services.searchers.model.Card;
import services.searchers.model.Item;

public interface SlotSearchResultSorter {

    public List<Card> sort(List<Card> cards);

    public List<Item> sortItems(List<Item> items);

}

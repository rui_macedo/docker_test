package services.searchers;

import models.scheduling.SchedulingFilter;
import services.api.SchedulingFilterService;

import java.util.List;

public class SchedulingFilterServiceImpl implements SchedulingFilterService {


    @Override
    public List<SchedulingFilter> findSchedulingFiltersBySchedulingId(Long schedulingId) {
        return SchedulingFilter.findBySchedulingId(schedulingId);
    }

    @Override
    public void deleteBySchedulingId(Long schedulingId) {
        List<SchedulingFilter> list = this.findSchedulingFiltersBySchedulingId(schedulingId);
        list.forEach(item -> item.delete());
    }

}

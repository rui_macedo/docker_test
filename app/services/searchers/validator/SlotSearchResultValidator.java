package services.searchers.validator;

import java.util.List;

import services.searchers.model.Item;

public interface SlotSearchResultValidator {

    public boolean isValid(List<Item> items);

}

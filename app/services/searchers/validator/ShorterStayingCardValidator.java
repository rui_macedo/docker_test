package services.searchers.validator;

import org.joda.time.Interval;

import java.util.List;

import services.searchers.model.Item;

public class ShorterStayingCardValidator implements SlotSearchResultValidator{

    @Override
    public boolean isValid(List<Item> items) {

        if(containsCardsWithTimeConflicts(items)){
            return false;
        }

        if(containsElementEndedBeforeStarting(items)){
            return false;
        }

        return true;
    }

    private Boolean containsElementEndedBeforeStarting(List<Item> items){
        boolean exits = items.parallelStream()
                .anyMatch(f ->
                                /**
                                 * if card ends before it should start or if it ends in the follow day
                                 */
                                f.start.getDayOfMonth() != f.end.getDayOfMonth() ||
                                        f.start.isBefore(f.end.getMillis())
                );
        return ! exits;
    }

    private Boolean containsCardsWithTimeConflicts(List<Item> items){

        boolean isOverlap = false;

        for (int index = 0; index < items.size() - 1; index ++) {
            Item item = items.get(index);
            Item nextItem = null;
            if(index < items.size()){
                nextItem = items.get(index + 1);
                Interval reference = new Interval(item.start, item.end);
                Interval compare =  new Interval(nextItem.start, nextItem.end);
                isOverlap = reference.overlaps(compare) || compare.overlaps(reference);
                if(isOverlap){
                    break;
                }
            }
        }

        return isOverlap;
    }

}

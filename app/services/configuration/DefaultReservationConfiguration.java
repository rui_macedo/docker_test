package services.configuration;

import play.Logger;

public class DefaultReservationConfiguration implements SchedulingReservationConfiguration{

    private final Boolean autoEmailEnable;

    public DefaultReservationConfiguration(Boolean autoEmailEnable) {
        this.autoEmailEnable = autoEmailEnable;
        Logger.debug("Auto send voucher by email is {}", this.autoEmailEnable );
    }

    @Override
    public Boolean isAutoEmailEnable() {
        return autoEmailEnable;
    }
}

package services;

import dto.PrePatientDTO;
import forms.FormPrePatient;
import models.scheduling.PrePatient;
import services.api.PatientService;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

@Deprecated
public class PatientServiceImpl implements PatientService {

    @Override
    public PrePatientDTO savePreRegister(Long institutionId, FormPrePatient form) {
        PrePatient preRegister= new PrePatient();
        preRegister.institutionId = institutionId;
        preRegister.name = form.name;
        preRegister.save();
        return preRegister.wrap();
    }

    @Override
    public List<PrePatientDTO> getAllPreRegisters(Long institutionId) {
        return PrePatient.findAllByInstitution(institutionId)
                .parallelStream()
                .map(item -> item.wrap())
                .collect(toList());
    }

    @Override
    public Optional<PrePatientDTO> findPreRegisterByIdOptional(Long id) {
        return Optional.ofNullable(findPreRegisterById(id));
    }

    @Override
    public PrePatientDTO findPreRegisterById(Long id) {
        PrePatient prePatient = PrePatient.findById(id);
        return nonNull(prePatient) ? prePatient.wrap() : null;
    }
}

package services.external;

import play.Play;

public interface EndPointConfiguration {

    String GET_PATIENTS = Play.application().configuration().getString("getpatients.api.endpoint",
            "/v1/patient-data/institution/{institution}/ws/patients/");

}

package services.external.api;

import java.util.List;

public interface QueueService<T> {

    public void send(String message);

    public List<T> receive();

    public void delete(T message);

}

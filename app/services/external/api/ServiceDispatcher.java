package services.external.api;

import com.fasterxml.jackson.databind.JsonNode;
import services.external.impl.ServiceResponse;
import services.formatters.PathFormatter;

import java.util.List;
import java.util.Map;

public interface ServiceDispatcher {

  ServiceResponse doPost(PathFormatter pathFormatter, Long tenantId, JsonNode requestBodyJson);

  ServiceResponse doGet(PathFormatter pathFormatter);

  ServiceResponse doGetWithQueryParam(PathFormatter pathFormatter, Map<String, List<String>> queryParam);

  ServiceResponse doPost(PathFormatter pathFormatter);

  ServiceResponse doPut(PathFormatter pathFormatter, JsonNode requestBodyJson, boolean throwException);

}

package services.external.api;

import dto.EmployeeDTO;

public interface EmployeeData {

  public Long getCurrentEmployeeId(Long userId, Long tenant);

  public Long getCurrentEmployeeId();

  public EmployeeDTO findById(Long id);

}

package services.external.api;

import java.util.List;

import dto.ExamProcedureByPlanDTO;
import models.ExamProcedure;
import models.ExamProcedureFinancial;

public interface ExamProcedureService {

    public List<ExamProcedureByPlanDTO> getAllExamProcedureFinancial(Long institutionId, Long insurancePlanId);

    public ExamProcedureFinancial findByInsuranceAndInsurancePlan(Long examProcedureId, Long insuranceId, Long insurancePlanId);

    public ExamProcedure findById(Long id);

    public Boolean examProcedureIsCoveredByPlan(Long examProcedureId, Long insuranceId, Long insurancePlanId);

}

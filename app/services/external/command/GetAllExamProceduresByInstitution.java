package services.external.command;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.pixeon.cloudsecure.token.CloudIntraWSRequestHolder;
import com.pixeon.cloudsecure.token.WSSecureCallPlugin;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import dto.ExamProcedureByPlanDTO;
import play.Logger;
import play.Play;
import play.libs.F;
import play.libs.ws.WSResponse;
import services.external.EndpointConfiguration;

public class GetAllExamProceduresByInstitution extends HystrixCommand<List<ExamProcedureByPlanDTO>> {

    private final List<Long> coveredByPlan;
    private final Map<Long, BigDecimal> notCoveredByPlanWithPrice;
    private final ObjectMapper objectMapper;
    private final Long institutionId;
    private final boolean bla;

    public GetAllExamProceduresByInstitution(Long institutionId, List<Long> coveredByPlan,Map<Long, BigDecimal> notCoveredByPlanWithPrice ){

        super(HystrixCommand.Setter.withGroupKey(HystrixCommandGroupKey
                .Factory.asKey("GetAllExamProceduresByInstitution"))
                .andCommandPropertiesDefaults(
                        HystrixCommandProperties.Setter()
                                .withCircuitBreakerEnabled(true)
                                .withRequestLogEnabled(true)
                                .withFallbackEnabled(true)
                                .withExecutionTimeoutEnabled(true)
                                .withExecutionTimeoutInMilliseconds(20000)
                                .withExecutionIsolationThreadTimeoutInMilliseconds(20000)
                                .withCircuitBreakerSleepWindowInMilliseconds(20000)
                ));

        this.institutionId = institutionId;
        this.coveredByPlan = coveredByPlan;
        this.notCoveredByPlanWithPrice = notCoveredByPlanWithPrice;
        this.objectMapper = new ObjectMapper();
        this.objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        this.bla = false;
    }

    @Override
    protected List<ExamProcedureByPlanDTO> run() throws Exception {

        List<ExamProcedureByPlanDTO> examProcedures = getExamProceduresFromExternal();
        List<ExamProcedureByPlanDTO> result = new ArrayList<>();

        for(ExamProcedureByPlanDTO exByPlan : examProcedures){

            ExamProcedureByPlanDTO item = exByPlan.clone();
            item.isCoveredByPlan = coveredByPlan.contains(item.id);

            if(! item.isCoveredByPlan  && notCoveredByPlanWithPrice.containsKey(item.id)){
                item.price = notCoveredByPlanWithPrice.get(item.id);
            }

            result.add(item);
        }
        return result;
    }

    @Override
    protected List<ExamProcedureByPlanDTO> getFallback() {
        Logger.error("Returning fallback for GetAllExamProceduresByInstitution. ", getExecutionException());
        return Arrays.asList(new ExamProcedureByPlanDTO());
    }

    private List<ExamProcedureByPlanDTO> getExamProceduresFromExternal() throws IOException {

        WSSecureCallPlugin ws = Play.application().plugin(WSSecureCallPlugin.class);

        String url = EndpointConfiguration.GET_ALL_EXAM_PROCESURES
                .replace("{institutionId}", String.valueOf(institutionId));

        CloudIntraWSRequestHolder holder = ws.prepareAsGet(url);
        F.Promise<WSResponse> responsePromise = ws.callCloudIntraServiceAsync(holder);
        WSResponse wsResponse = responsePromise.get(3, TimeUnit.SECONDS);
        if (wsResponse.getStatus() != 200) {
             Logger.error("Error response from service call: {}", wsResponse.getStatus());
        }
        return Arrays.asList(objectMapper.readValue(wsResponse.getBody(), ExamProcedureByPlanDTO[].class));
    }

}

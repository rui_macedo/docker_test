package services.external.impl;

import com.google.inject.Inject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import dto.ExamProcedureByPlanDTO;
import models.ExamProcedure;
import models.ExamProcedureFinancial;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.Days;
import play.Logger;
import play.libs.Json;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import resource.InsurancePlanResource;
import services.api.InsuranceService;
import services.external.api.ExamProcedureService;
import services.external.command.GetAllExamProceduresByInstitution;
import utils.SerializeUtils;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

public class ExamProcedureServiceImpl implements ExamProcedureService {

    @Inject
    public static InsuranceService insuranceService;
    private JedisPool jedisPool;
    private Boolean cacheEnabled;

    public ExamProcedureServiceImpl(JedisPool jedisPool, Boolean cacheEnabled) {
        this.cacheEnabled = cacheEnabled;
        this.jedisPool = jedisPool;
        Logger.debug("ExamProcedures cache is enable = {}", this.cacheEnabled);
    }

    @Override
    public List<ExamProcedureByPlanDTO> getAllExamProcedureFinancial(Long institutionId, Long insurancePlanId) {


        List<ExamProcedureByPlanDTO> result = cacheEnabled ? findAllEnabledByInsurancePlanFromCache(institutionId, insurancePlanId) :
                Collections.EMPTY_LIST;

        if (CollectionUtils.isEmpty(result)) {

            Optional<InsurancePlanResource> plan = insuranceService.getDefaultPlanForPrivateInsuranceOptional(institutionId);
            List<Long> coveredByPlan = getAllExamProcedureIdsCoveredByPlan(institutionId, insurancePlanId);
            Map<Long, BigDecimal> notCoveredByPlanWithPrice = plan.isPresent() ?
                    getExamProcedureWithPrivatePriceAsMap(institutionId, plan.get().id) :
                    Collections.EMPTY_MAP;

            GetAllExamProceduresByInstitution command = new GetAllExamProceduresByInstitution(
                    institutionId,
                    coveredByPlan,
                    notCoveredByPlanWithPrice
            );

            result = command.execute();

            if(cacheEnabled)  {
                populateCache(result, institutionId, insurancePlanId);
            }
        }

        return result;
    }

    private void populateCache(List<ExamProcedureByPlanDTO> list, Long institutionId, Long insurancePlanId) {
        if(CollectionUtils.isNotEmpty(list)){
            try (Jedis jedis = jedisPool.getResource()) {
                for (ExamProcedureByPlanDTO item : list) {
                    String key = getCacheKey(institutionId, item.id, insurancePlanId);
                    jedis.setex(key, getCacheTTL(), Json.toJson(item).toString());
                }
            }
        }
    }

    private List<ExamProcedureByPlanDTO> findAllEnabledByInsurancePlanFromCache(Long institutionId, Long insurancePlanId) {
        try (Jedis jedis = jedisPool.getResource()) {
            Set<String> keys = jedis.keys(getCacheKey(institutionId, insurancePlanId));
            List<String> list = !keys.isEmpty() ? jedis.mget(keys
                    .stream()
                    .map(e -> e)
                    .toArray(String[]::new)) : Collections.EMPTY_LIST;
            Logger.info("Retrieved ExamProcedure from redis");
            return SerializeUtils.deserializeDataList(list, ExamProcedureByPlanDTO.class);
        } catch (IOException e) {
            return Collections.emptyList();
        }
    }

    private Map<Long, BigDecimal> getExamProcedureWithPrivatePriceAsMap(Long institutionId, Long insurancePlanId){

        List<ExamProcedureFinancial> list = ExamProcedureFinancial.findAllByInsurancePlanAndTenant(
                institutionId, insurancePlanId
        );

        return list.parallelStream()
                .collect(Collectors.toMap(item -> item.examProcedureId, item -> item.price));
    }

    private List<Long> getAllExamProcedureIdsCoveredByPlan(Long institutionId, Long insurancePlanId){
        return ExamProcedureFinancial.findAllByInsurancePlanAndTenant(
                institutionId, insurancePlanId)
                .parallelStream()
                .map(item -> item.examProcedureId)
                .collect(toList());
    }


    @Override
    public Boolean examProcedureIsCoveredByPlan(Long examProcedureId, Long insuranceId, Long insurancePlanId) {
        return nonNull(ExamProcedureFinancial.findByInsuranceAndPlanAndExamProcedure(examProcedureId, insuranceId, insurancePlanId));
    }


    @Override
    public ExamProcedureFinancial findByInsuranceAndInsurancePlan(Long examProcedureId, Long insuranceId, Long insurancePlanId) {
        return ExamProcedureFinancial.findByInsuranceAndInsurancePlan(examProcedureId, insuranceId, insurancePlanId);
    }

    public ExamProcedure findById(Long id){
        return ExamProcedure.findById(id);
    }

    private String getCacheKey(Long institutionId, Long procedureId, Long insurancePlanId) {
        return "scheduling.examProcedure." + institutionId + "." + insurancePlanId + "."+ procedureId;
    }

    private String getCacheKey(Long institutionId, Long insurancePlanId) {
        return "scheduling.examProcedure." + institutionId + "." + insurancePlanId + ".*";
    }

    private int getCacheTTL() {
       return Days.ONE.toStandardSeconds().getSeconds();
    }

}


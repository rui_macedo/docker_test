package services.external.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.pixeon.cloudsecure.token.CloudIntraWSRequestHolder;
import com.pixeon.cloudsecure.token.WSSecureCallPlugin;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.libs.F;
import play.libs.ws.WSResponse;
import play.mvc.Http;
import services.external.api.ServiceDispatcher;
import services.formatters.PathFormatter;
import utils.WSResponseUtils;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ServiceDispatcherImpl implements ServiceDispatcher {

  private final WSSecureCallPlugin wsSecureCall;

  private static final int TIMEOUT = 15;

  public ServiceDispatcherImpl(WSSecureCallPlugin wsSecureCall) {
      this.wsSecureCall = wsSecureCall;
  }

  private ServiceResponse doCall(CloudIntraWSRequestHolder wsHolder){
      F.Promise<WSResponse> responsePromise = wsSecureCall.callCloudIntraServiceAsync(wsHolder);
      WSResponse wsResponse = responsePromise.get(TIMEOUT, TimeUnit.SECONDS);
      String content = wsResponse.getBody();
      int status = wsResponse.getStatus();
      if (WSResponseUtils.isErrorResponse(wsResponse)) {
          Logger.error("Error response from service call: " + status);
      }
      return new ServiceResponse(content, status);
  }

  private CloudIntraWSRequestHolder doPrepare(String type, PathFormatter pathFormatter){
      try {
          if (type == "GET") {
              return wsSecureCall.prepareAsGet(pathFormatter.getFormatWSPath());
          } else if (type == "POST") {
              return wsSecureCall.prepareAsPost(pathFormatter.getFormatWSPath());
          } else if (type == "PUT") {
              return wsSecureCall.prepareAsPut(pathFormatter.getFormatWSPath());
          } else if (type == "DELETE") {
              return wsSecureCall.prepareAsDelete(pathFormatter.getFormatWSPath());
          }
      } catch (MalformedURLException e) {
          Logger.error("Error on service URL", e);
      }
      return null;
  }

  public ServiceResponse doPost(PathFormatter pathFormatter, Long tenantId, JsonNode requestBodyJson) {
      CloudIntraWSRequestHolder holder = this.doPrepare("POST", pathFormatter);
      if (holder == null){
          return new ServiceResponse(StringUtils.EMPTY, Http.Status.INTERNAL_SERVER_ERROR);
      }
      holder.setBodyAsJson(requestBodyJson);
      return this.doCall(holder);
  }

  @Override
  public ServiceResponse doPost(PathFormatter pathFormatter) {
      CloudIntraWSRequestHolder holder = this.doPrepare("POST", pathFormatter);
      if (holder == null){
        return new ServiceResponse(StringUtils.EMPTY, Http.Status.INTERNAL_SERVER_ERROR);
      }
      holder.setBodyAsString(StringUtils.EMPTY);
      return this.doCall(holder);
  }

  @Override
  public ServiceResponse doGet(PathFormatter pathFormatter) {
      CloudIntraWSRequestHolder holder = this.doPrepare("GET", pathFormatter);
      if (holder == null){
        return new ServiceResponse(StringUtils.EMPTY, Http.Status.INTERNAL_SERVER_ERROR);
      }
      return this.doCall(holder);
  }

    @Override
    public ServiceResponse doGetWithQueryParam(PathFormatter pathFormatter, Map<String, List<String>> queryParam) {
        CloudIntraWSRequestHolder holder = this.doPrepare("GET", pathFormatter);
        queryParam.entrySet().forEach(k -> {
            k.getValue().forEach(v -> {
                holder.addQueryParam(k.getKey(), v);
            });
        });

        if (holder == null){
            return new ServiceResponse(StringUtils.EMPTY, Http.Status.INTERNAL_SERVER_ERROR);
        }
        return this.doCall(holder);
    }

    @Override
  public ServiceResponse doPut(PathFormatter pathFormatter, JsonNode requestBodyJson, boolean throwException) {
      CloudIntraWSRequestHolder holder = this.doPrepare("PUT", pathFormatter);
      if (holder == null){
        return new ServiceResponse(StringUtils.EMPTY, Http.Status.INTERNAL_SERVER_ERROR);
      }
      holder.setBodyAsJson(requestBodyJson);
      return this.doCall(holder);
  }

}

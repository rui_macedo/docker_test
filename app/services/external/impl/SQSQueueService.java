package services.external.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;

import java.util.List;

import play.Logger;
import play.Play;
import services.external.api.QueueService;
import static com.google.common.base.Preconditions.checkNotNull;

@Singleton
public class SQSQueueService implements QueueService<Message>{

    @Inject
    private AmazonSQS sqs;

    private final String QUEUE_NAME = checkNotNull(Play.application().configuration().getString("aws.sqs.slot.name"),
            "Queue name is missing");

    @Override
    public void send(String message) {
        Logger.debug("Sending message[{}] to {}", message, QUEUE_NAME );
        sqs.sendMessage(QUEUE_NAME, message);
    }

    @Override
    public List<Message> receive() {
        return sqs.receiveMessage(QUEUE_NAME).getMessages();
    }

    @Override
    public void delete(Message message) {
        sqs.deleteMessage(QUEUE_NAME, message.getReceiptHandle());
    }

}

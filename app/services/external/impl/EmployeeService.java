package services.external.impl;

import com.pixeon.cloudsecure.utils.CloudSecureUtils;

import com.pixeon.cloudsecure.token.WSSecureCallPlugin;

import dto.EmployeeDTO;
import models.Employee;
import services.external.api.EmployeeData;

public class EmployeeService implements EmployeeData {

  @Override
  public Long getCurrentEmployeeId(Long userId, Long tenant) {
    return this.getCurrentEmployeeId();
  }

  @Override
  public Long getCurrentEmployeeId() {
    return CloudSecureUtils.getCurrentEmployeeId();
  }

  @Override
  public EmployeeDTO findById(Long id) {
    return Employee.findById(id).wrap();
  }
}

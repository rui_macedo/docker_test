package services.external.impl;

public class ServiceResponse {

    private String content;
    private int statusCode;

    public ServiceResponse(String content, int statusCode) {
        this.content = content;
        this.statusCode = statusCode;
    }

    public String getContent() {
        return content;
    }

    public int getStatusCode() {
        return statusCode;
    }
}

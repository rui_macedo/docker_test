package services;

import com.google.common.collect.Lists;
import models.InsurancePlan;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.Days;
import play.Logger;
import play.libs.Json;
import queries.InsurancePlanQuery;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;
import resource.InsurancePlanResource;
import services.api.InsurancePlanService;
import utils.SerializeUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

public class InsurancePlanServiceImpl implements InsurancePlanService {

    private JedisPool jedisPool;

    private Boolean cacheEnabled;

    public InsurancePlanServiceImpl(JedisPool jedisPool, Boolean cacheEnabled) {
        this.jedisPool = jedisPool;
        this.cacheEnabled = cacheEnabled;
        Logger.debug("InsurancePlan cache is enable = {}", cacheEnabled);
    }


    @Override
    public List<InsurancePlanResource> findAllEnabledByInsurance(InsurancePlanQuery query) {
        List<InsurancePlan> list = cacheEnabled ? findAllEnabledByInsuranceFromCache(query):Lists.newArrayList();

        if (CollectionUtils.isEmpty(list)) {
            list = InsurancePlan.findAllEnabledByInsurance(query.insuranceId);
            if(cacheEnabled){
                setJedisInsurancePlan(list);
            }
        }

        return list.parallelStream()
                .map(item -> item.wrapAsResource())
                .collect(toList());
    }

    private List<InsurancePlan> findAllEnabledByInsuranceFromCache(InsurancePlanQuery query) {
        Logger.info("Retrieved insurancesPlan from redis");
        try (Jedis jedis = jedisPool.getResource()) {
            Set<String> keys = jedis.keys(getCacheKey(query.institutionId, query.insuranceId));
            List<String> list = !keys.isEmpty() ? jedis.mget(keys
                    .stream()
                    .map(e -> e)
                    .toArray(String[]::new)) : Collections.EMPTY_LIST;
            return SerializeUtils.deserializeDataList(list, InsurancePlan.class);
        } catch (IOException e) {
            Logger.error("Exceptin occured for jedis " + e.getMessage());
            return Collections.EMPTY_LIST;
        }
    }

    private void setJedisInsurancePlan(List<InsurancePlan> list) {
        if(CollectionUtils.isNotEmpty(list)){
            try (Jedis jedis = jedisPool.getResource()) {
                for (InsurancePlan item : list) {
                    String key = getCacheKey(item.institution.getId(), item.insurance.getId(), item.getId());
                    jedis.setex(key, getCacheTTL(), Json.toJson(item).toString());
                }
            }
        }
    }

    @Override
    public InsurancePlanResource findById(Long id) {
        InsurancePlan plan = InsurancePlan.findById(id);
        if (isNull(plan)) {
            return null;
        }
        return plan.wrapAsResource();
    }

    @Override
    public Optional<InsurancePlanResource> findByIdOptional(Long id) {
        return Optional.ofNullable(findById(id));
    }

    @Override
    public void cleanCache(InsurancePlanQuery query) {
        Logger.info("Cleaning InsurancePlan cache for Insurance: " + query.insuranceId);
        try (Jedis jedis = jedisPool.getResource()) {
            Set<String> planKeys = jedis.keys(getCacheKey(query.institutionId, query.insuranceId));
            Transaction multi = jedis.multi();
            for (String aKeyToDelete : planKeys) {
                multi.del(aKeyToDelete);
            }
            multi.exec();
        }
    }

    private String getCacheKey(Long institutionId, Long insuranceId) {
        return "scheduling.insurancePlan." + institutionId + "." + insuranceId + ".*";
    }

    private String getCacheKey(Long institutionId, Long insuranceId, Number insurancePlanId) {
        return "scheduling.insurancePlan." + institutionId + "." + insuranceId + "." + insurancePlanId;
    }

    private int getCacheTTL() {
        return Days.ONE.toStandardSeconds().getSeconds();
    }

    public InsurancePlanResource findDefaultByInsurance(Long insuranceId) {
        InsurancePlan plan = InsurancePlan.findDefaultPlanByInsurance(insuranceId);
        if(isNull(plan)){
            return null;
        }
        return plan.wrapAsResource();
    }

    @Override
    public Optional<InsurancePlanResource> findDefaultByInsuranceOptional(Long insuranceId) {
        return Optional.ofNullable(findDefaultByInsurance(insuranceId));
    }
}

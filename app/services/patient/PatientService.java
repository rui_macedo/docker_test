package services.patient;

import dto.PatientDTO;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PatientService {

    public Optional<List<PatientDTO>> getPatients(Long tenantId, Set<Long> patientsId);

}

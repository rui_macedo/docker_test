package services.patient;

import com.google.inject.Inject;
import dto.PatientDTO;
import services.external.api.ServiceDispatcher;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public class PatientServiceImpl implements PatientService {

    private final ServiceDispatcher dispatcher;

    @Inject
    public PatientServiceImpl(ServiceDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }


    @Override
    public Optional<List<PatientDTO>> getPatients(Long tenantId, Set<Long> patientsId) {
        return new GetPatients(dispatcher, tenantId, patientsId).execute();
    }
}

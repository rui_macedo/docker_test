package services.patient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;
import dto.PatientDTO;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import services.external.api.ServiceDispatcher;
import services.formatters.PatientByID;

import java.util.*;
import java.util.stream.Collectors;

public class GetPatients  extends HystrixCommand<Optional<List<PatientDTO>>> {

    private final ServiceDispatcher dispatcher;

    private final Long tenantId;

    private final Set<Long> patientsId;

    private final ObjectMapper objectMapper;

    protected GetPatients(ServiceDispatcher dispatcher, Long tenantId, Set<Long> patientsId) {
        super(HystrixCommand.Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("GetPatients"))
                .andCommandPropertiesDefaults(
                        HystrixCommandProperties.Setter()
                                .withCircuitBreakerEnabled(true)
                                .withRequestLogEnabled(true)
                                .withRequestCacheEnabled(true)
                                .withFallbackEnabled(true)
                                .withExecutionTimeoutEnabled(false)
                                .withCircuitBreakerSleepWindowInMilliseconds(2000)
                ));

        this.dispatcher = dispatcher;
        this.tenantId = tenantId;
        this.patientsId = patientsId;
        this.objectMapper = new ObjectMapper();
        this.objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    @Override
    protected Optional<List<PatientDTO>> run() throws Exception {
        Map<String, List<String>> mapPatientId = new HashMap<>();
        mapPatientId.put("patient", patientsId
                .parallelStream()
                .map(p -> p.toString())
                .collect(Collectors.toList()));

        String response = dispatcher.doGetWithQueryParam(new PatientByID(tenantId), mapPatientId).getContent();
        if(StringUtils.isNotBlank(response)){
            return Optional.of(objectMapper.readValue(response, new TypeReference<List<PatientDTO>>(){}));
        }
        Logger.info("Institution " + tenantId + " patients list empty.");
        return Optional.empty();
    }

    @Override
    protected Optional<List<PatientDTO>> getFallback() {
        Logger.error("Returning fallback. ", getFailedExecutionException());
        return Optional.empty();
    }
}

package services;

import com.pixeon.utils.common.utils.DateUtils;
import dto.ContactDTO;
import dto.ContactPatientDTO;
import forms.FormContactPatient;
import models.scheduling.Contact;
import models.scheduling.ContactPatient;
import queries.ContactAllQuery;
import services.api.ContactService;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

public class ContactServiceImpl implements ContactService{

    @Override
    public ContactDTO findById(Long id) {
        Contact contact = Contact.findById(id);
        if(isNull(contact)){
            return null;
        }
        return contact.wrap();

    }

    @Override
    public Optional<ContactDTO> findByIdOptional(Long id) {
        return Optional.ofNullable(findById(id));
    }

    @Override
    public List<ContactDTO> list(ContactAllQuery query) {
        return Contact.findAllByInstitution(
                query.institutionId, query.name, query.phone
        ).parallelStream()
         .map(item -> item.wrap())
         .collect(toList());

    }

    @Override
    public ContactDTO create(Long institutionId, ContactDTO request) {
        Contact contact = Contact.create(request);
        contact.institutionId = institutionId;
        contact.save();
        return contact.wrap();
    }

    @Override
    public void update(Contact contact, ContactDTO request) {
        if(isNull(contact)){
            return ;
        }
        contact.unwrap(request);
        contact.updated = DateUtils.getCurrentDateTimeGMTZero();
        contact.update();
    }

    @Override
    public List<ContactPatientDTO> getPatientByContact(Long contactId) {
        return ContactPatient.findAllByContact(contactId).parallelStream()
                .map(item -> item.wrap())
                .collect(toList());
    }

    @Override
    public ContactPatientDTO addPatient(Long institutionId, FormContactPatient form) {
        ContactPatient contactPatient = new ContactPatient();
        contactPatient.contact = Contact.findById(form.contactId);
        contactPatient.notes = form.notes;
        contactPatient.patientId = form.patientId;
        contactPatient.prePatientId = form.prePatientId;
        contactPatient.institutionId = institutionId;
        contactPatient.save();
        return contactPatient.wrap();
    }

    @Override
    public ContactPatientDTO findContactPatientById(Long id) {
        ContactPatient cp = ContactPatient.findById(id);
        return isNull(cp) ? null : cp.wrap();
    }

    @Override
    public Optional<ContactPatientDTO> findContactPatientByIdOptional(Long id) {
        return Optional.ofNullable(findContactPatientById(id));
    }

    @Override
    public void updateContactPatient(ContactPatient cp, FormContactPatient form) {
        if(isNull(cp)){
            return ;
        }
        cp.notes = form.notes;
        cp.updatedDate = DateUtils.getCurrentDateTimeGMTZero();
        cp.update();
    }
}

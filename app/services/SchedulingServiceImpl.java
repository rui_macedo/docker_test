package services;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.inject.Inject;
import com.pixeon.utils.common.utils.DateUtils;
import dto.DurationDTO;
import dto.SchedulingDTO;
import dto.SchedulingOrderedDTO;
import forms.FormContactPatient;
import forms.FormScheduling;
import forms.FormSchedulingOrder;
import forms.FormSchedulingReservation;
import forms.voucher.FormVoucher;
import models.*;
import models.scheduling.Contact;
import models.scheduling.Scheduling;
import models.scheduling.SchedulingFilter;
import models.scheduling.SchedulingReservation;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import play.Logger;
import play.Play;
import resource.SchedulingFullReservationResource;
import services.api.ContactService;
import services.api.SchedulingFilterService;
import services.api.SchedulingService;
import services.api.SlotService;
import services.external.api.EmployeeData;
import services.external.api.ExamProcedureService;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

public class SchedulingServiceImpl implements SchedulingService {

    @Inject
    public static SlotService slotService;

    @Inject
    public static ExamProcedureService examProcedureService;

    @Inject
    public static ContactService contactService;

    @Inject
    public static EmployeeData employeeService;


    @Inject
    private static SchedulingFilterService schedulingFilterService;

    @Override
    public SchedulingDTO findSchedulingById(Long id) {
        Scheduling scheduling = Scheduling.findById(id);
        if (isNull(scheduling)) {
            return null;
        }
        return scheduling.wrap();
    }

    @Override
    public Optional<SchedulingDTO> findSchedulingByIdOptional(Long id) {
        return Optional.ofNullable(findSchedulingById(id));
    }

    @Override
    public SchedulingDTO startScheduling(Long institutionId, Long employeeId) {
        Scheduling scheduling = new Scheduling();
        scheduling.employeeId = employeeId;
        scheduling.institutionId = institutionId;
        scheduling.save();
        return scheduling.wrap();
    }

    @Override
    public void updateSchedulingOnlyInProgress(Scheduling scheduling, FormScheduling form) {

        if (isNull(scheduling) || ObjectUtils.notEqual(scheduling.status, Scheduling.Status.IN_PROGRESS)) {
            Logger.info("Scheduling process is null or it is not in progress anymore");
            return;
        }

        scheduling.notes = form.notes;

        if ((nonNull(scheduling.contact) && ObjectUtils.notEqual(form.contactId, scheduling.contact.getId())) ||
                isNull(scheduling.contact) && nonNull(form.contactId)) {
            scheduling.contact = Contact.findById(form.contactId);
        }

        if (nonNull(form.patientId)) {
            scheduling.patientId = form.patientId;
            scheduling.patientName = StringUtils.EMPTY;
        }

        if (StringUtils.isNotBlank(form.patientName) && isNull(form.patientId)) {
            scheduling.patientName = form.patientName;
            scheduling.patientId = null;
        }

        schedulingFilterService.deleteBySchedulingId(scheduling.getId());
        if(nonNull(form.filters)){
            form.filters.forEach(filter -> {
                SchedulingFilter schedulingFilter = new SchedulingFilter(scheduling.institutionId, filter);
                scheduling.schedulingFilters.add(schedulingFilter);
            });
        }

        scheduling.updated = DateUtils.getCurrentDateTimeGMTZero();
        scheduling.update();

    }

    @Override
    public void finishScheduling(Scheduling scheduling) {
        if (scheduling.isValuable()) {
            scheduling.finish();
        } else {
            scheduling.delete();
        }
    }

    @Override
    public void makeBooking(Scheduling scheduling, FormSchedulingReservation item) {

        for (FormSchedulingOrder order : item.orders) {

            slotService.slotsBooking(order.slots);
            ExamProcedureFinancial examProcedureFinancial = examProcedureService
                    .findByInsuranceAndInsurancePlan(order.examProcedureId, order.insuranceId, order.insurancePlanId);

            SchedulingReservation reservation = new SchedulingReservation(scheduling.institutionId,
                    scheduling, order.insuranceId, order.insurancePlanId, order.examProcedureId,
                    isNull(examProcedureFinancial) ? BigDecimal.ZERO : examProcedureFinancial.price,
                    order.physicianId, order.unityId
            );

            reservation.save();
            reservation.addSlots(order.slots);
            reservation.addRestrictions(order.restrictions);
        }

        /**
         * At the end of process we should check if the contact has been attached to patient
         */
        if (!scheduling.contact.hasPatient(scheduling.patientId)) {
            contactService.addPatient(scheduling.institutionId,
                    new FormContactPatient(scheduling.contact.id, scheduling.patientId)
            );
        }

        scheduling.booked();
        scheduling.refresh();
    }

    @Override
    public SchedulingFullReservationResource mapSchedulingToFullReservationResource(Scheduling scheduling) {

        if(isNull(scheduling)){
            return null;
        }

        SchedulingFullReservationResource full = new SchedulingFullReservationResource();

        full.contact = scheduling.contact.wrap();
        full.creationDate = scheduling.created;
        full.updatedDate = scheduling.updated;
        full.patient = Patient.findById(scheduling.patientId).wrap();
        full.protocol = scheduling.getProtocol();
        full.id = scheduling.id;
        full.employee = employeeService.findById(scheduling.employeeId);
        full.sessionTime = new DurationDTO(
                Minutes.minutesBetween(scheduling.created, scheduling.updated).getMinutes()
        );
        full.institutionId = scheduling.institutionId;
        full.notes = scheduling.notes;
        full.status = scheduling.status;
        full.requests = getOrdersByScheduling(full.id);
        full.unity = Unity.findById(full.requests.get(0).unityId.intValue()).wrap();
        full.serviceStart = full.requests.get(0).start;
        full.serviceEnd = full.requests.get(full.requests.size() - 1).end;

        full.amount = full.requests.parallelStream().map(SchedulingOrderedDTO::getPrice)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        full.duration = new DurationDTO(
                full.requests.parallelStream().mapToInt(i-> i.duration.time).sum()
        );

        return full;

    }

    public List<SchedulingOrderedDTO> getOrdersByScheduling(Long id){

        List<SchedulingReservation> reservations = SchedulingReservation.findBySchedulingId(id);
        List<SchedulingOrderedDTO> result  = new ArrayList<>();

        if(CollectionUtils.isEmpty(reservations)){
            return Collections.emptyList();
        }

        for(SchedulingReservation reservation : reservations){
            SchedulingOrderedDTO dto = new SchedulingOrderedDTO();
            dto.examProcedure = examProcedureService.findById(reservation.examProcedureId).wrap();
            dto.insurance = Insurance.findById(reservation.insuranceId).wrapDTO();
            dto.insurancePlan = InsurancePlan.findById(reservation.insurancePlanId).wrap();
            dto.price = reservation.price;
            dto.restrictions = Collections.emptyList();
            dto.physician = Physician.findById(reservation.physicianId).wrap();
            Map<String, DateTime> period = reservation.getDates();
            dto.start = period.get("startedAt");
            dto.end = period.get("endedAt");
            dto.duration = new DurationDTO(
                    Minutes.minutesBetween(dto.start, dto.end).getMinutes()
            );
            dto.restrictions = reservation.getRestrictionAsCheckDTO();
            dto.unityId = reservation.unityId;
            result.add(dto);
        }

        Comparator<SchedulingOrderedDTO> byStarting = (SchedulingOrderedDTO a1,SchedulingOrderedDTO a2) ->
                a1.start.compareTo(a2.start);

        return result
                .parallelStream()
                .sorted(byStarting)
                .collect(toList());

    }

    @Override
    public void sendVoucher(Long schedulingId, String email, String subject) {
        Scheduling scheduling = Scheduling.findById(schedulingId);
        SchedulingFullReservationResource schedulingReservation = mapSchedulingToFullReservationResource(scheduling);
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.to = email;
        emailMessage.htmlBody = buildHtml(schedulingReservation);
        emailMessage.subject = subject;
        this.sendVoucher(emailMessage);
    }

    @Override
    public void sendVoucher(EmailMessage email) {
        email.send();
    }

    @Override
    public void sendVoucher(Long schedulingId) {
        Scheduling scheduling = Scheduling.findById(schedulingId);
        sendVoucher(scheduling);
    }

    @Override
    public void sendVoucher(Scheduling scheduling) {
        Patient patient = nonNull(scheduling) ? Patient.findById(scheduling.patientId) : null;
        if(nonNull(patient)){
            List<PatientContact> emails = PatientContact.findEmailsByPatientId(patient.getId());
            emails.parallelStream().forEach(email ->
                    sendVoucher(scheduling.id, email.value, "Notificação de Agendamento")
            );
        } else {
            Logger.error("Not found a patient in the given scheduling");
        }
    }

    private String buildHtml(SchedulingFullReservationResource param){
        MustacheFactory mf = new DefaultMustacheFactory();

        Mustache mustache = mf.compile("public/voucher-template.mustache");
        StringWriter writer = new StringWriter();
        mustache.execute(writer, FormVoucher.create(param));
        return  writer.toString();

    }


}


package services;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import models.Insurance;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.Days;
import play.Logger;
import play.Play;
import play.libs.Json;
import queries.InsurancePlanQuery;
import queries.InsuranceQuery;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;
import resource.InsurancePlanResource;
import resource.InsuranceResource;
import services.api.InsurancePlanService;
import services.api.InsuranceService;
import utils.SerializeUtils;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class InsuranceServiceImpl implements InsuranceService {

    @Inject
    public InsurancePlanService planService;

    private JedisPool jedisPool;
    private Boolean cacheEnabled;

    public InsuranceServiceImpl(JedisPool jedisPool, Boolean cacheEnabled) {
        this.cacheEnabled = cacheEnabled;
        this.jedisPool = jedisPool;
        Logger.debug("InsuranceService cache is enable = {}", cacheEnabled);
    }

    @Override
    public List<InsuranceResource> list(Long institutionId, InsuranceQuery query) {

        List<Insurance> list = cacheEnabled ? listFromCache(query) : Lists.newArrayList();

        if (CollectionUtils.isEmpty(list)) {
            list = Insurance.findAllByTenant(institutionId, query);
            if(cacheEnabled){
                setJedisInsurance(list);
            }
        }
        return list.parallelStream()
                .map(insurance -> insurance.wrap())
                .collect(Collectors.toList());
    }

    private void setJedisInsurance(List<Insurance> list) {
        if(CollectionUtils.isNotEmpty(list)){
            try (Jedis jedis = jedisPool.getResource()) {
                for (Insurance insurance : list) {
                    String key = getCacheKey(insurance.institution.getId(), insurance.getId());
                    jedis.setex(key, getCacheTTL(), Json.toJson(insurance).toString());
                }
            }
        }
    }

    private List<Insurance> listFromCache(InsuranceQuery query) {
        Logger.info("Retrieved insurance from redis");
        try (Jedis jedis = jedisPool.getResource()) {
            Set<String> keys = jedis.keys(getCacheKey(query.institutionId));
            if (keys.isEmpty()) {
                return Collections.EMPTY_LIST;
            }
            List<String> list = jedis.mget(keys.stream().map(e -> e).toArray(String[]::new));
            return SerializeUtils.deserializeDataList(list, Insurance.class);
        } catch (IOException e) {
            Logger.error("Exceptin occured for jedis " + e.getMessage());
            return Collections.EMPTY_LIST;
        }
    }

    @Override
    public InsuranceResource findById(Long id) {
        Insurance insurance = Insurance.findById(id);
        if (isNull(insurance)) {
            return null;
        }
        return insurance.wrap();
    }

    @Override
    public Optional<InsuranceResource> findByIdOptional(Long id) {
        return Optional.ofNullable(findById(id));
    }

    @Override
    public List<InsurancePlanResource> findInsurancePlans(Long insuranceId) {
        InsuranceResource insurance = findById(insuranceId);
        return nonNull(insurance) ? planService.findAllEnabledByInsurance(new InsurancePlanQuery(insurance.institutionId, insurance.id)) : new ArrayList<>();
    }

    @Override
    public void cleanCache(Long idInstitution) {
        Logger.info("Cleaning Insurances cache for Institution: " + idInstitution);
        try (Jedis jedis = jedisPool.getResource()) {
            Set<String> planKeys = jedis.keys(getCacheKey(idInstitution));
            Transaction multi = jedis.multi();
            for (String aKeyToDelete : planKeys) {
                multi.del(aKeyToDelete);
            }
            multi.exec();
        }
    }

    private String getCacheKey(Long institutionId) {
        return "scheduling.insurance." + institutionId + ".*";
    }

    private String getCacheKey(Long institutionId, Long insuranceId) {
        return "scheduling.insurance." + institutionId + "." + insuranceId;
    }

    private int getCacheTTL() {
        return Days.ONE.toStandardSeconds().getSeconds();
    }

    @Override
    public Optional<InsuranceResource> getPrivateInsuranceOptional(Long institution) {
        return Optional.ofNullable(getPrivateInsurance(institution));
    }

    @Override
    public InsuranceResource getPrivateInsurance(Long institution) {

        Insurance insurance = Insurance.findPrivateInsurance(institution);
        if(isNull(insurance)){
            return null;
        }

        return insurance.wrap();
    }

    @Override
    public InsurancePlanResource getDefaultPlanForPrivateInsurance(Long institution) {

        Optional<InsuranceResource> insurance = getPrivateInsuranceOptional(institution);
        if(! insurance.isPresent()){
            return null;
        }

        return planService.findDefaultByInsurance(insurance.get().id);
    }

    @Override
    public Optional<InsurancePlanResource> getDefaultPlanForPrivateInsuranceOptional(Long institution) {
        return Optional.ofNullable(getDefaultPlanForPrivateInsurance(institution));
    }

}

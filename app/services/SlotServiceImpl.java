package services;

import com.google.inject.Inject;

import exceptions.SlotUnavailableToBookingException;
import models.SchedulingBook;
import models.SchedulingBookPeriod;
import models.SchedulingBookWeekAgenda;
import models.Slot;

import org.apache.commons.lang3.ObjectUtils;
import org.joda.time.DateTime;
import com.avaje.ebean.Ebean;
import common.constants.CalendarDay;
import common.constants.SlotStatus;

import java.util.Calendar;
import java.util.List;

import forms.FormMessageSlot;
import forms.FormMessageSlotMapper;
import play.Logger;
import queries.slots.SlotAvailableQuery;
import services.api.SlotService;
import services.searchers.SearchProvider;
import services.searchers.SlotSearchType;
import services.searchers.finder.SlotFinderFactory;
import services.searchers.model.Card;

import static java.util.Objects.isNull;

public class SlotServiceImpl implements SlotService{

  /**
   * If in the feature we need to change the whole business logic,
   * we just need to inject another implementation of SearchProvider
   */
  @Inject
  private static SearchProvider<Card> searchProvider;

  @Override
  public void generate(FormMessageSlot messageSlot) {
    Logger.debug("Creating Slots");
    SchedulingBook book = SchedulingBook.findById(messageSlot.schedulingBookId);
    if (book == null) {
      Logger.error("SchedulingBook Not Found");
      return;
    }
    if (!Slot.isValidCreationOfSlots(messageSlot)) {
      Logger.error("Slots já existe com o período determinado ou período informado é inválido");
      return;
    }
    book.processingSlots();
    if (generateSlots(messageSlot)) {
      book.successSlots();
    } else {
      Logger.error("Error creating slot for scheduling book id " + book.getId());
      book.failedSlots();
    }
  }

  private Boolean generateSlots(FormMessageSlot formMessageSlot) {

    Ebean.beginTransaction();
    try {

      SchedulingBook book = SchedulingBook.findById(formMessageSlot.schedulingBookId);
      DateTime startDateOfMessage = formMessageSlot.startAt;
      Slot slot = new Slot();
      slot.unwrapBook(book);
      FormMessageSlotMapper mapper = new FormMessageSlotMapper();
      mapper.unwrap(formMessageSlot, slot);

      while (startDateOfMessage.isBefore(formMessageSlot.endAt)) {

        mapper.startAtInMillis = formMessageSlot.startAt.getMillisOfDay();
        SchedulingBookWeekAgenda dayOfTheWeek = SchedulingBookWeekAgenda.getWeekAgendaByCalendarDay(
                CalendarDay.getByDayIdx(startDateOfMessage.getDayOfWeek()), book);

        if (dayOfTheWeek != null) {
          for (SchedulingBookPeriod period : dayOfTheWeek.schedulingBookPeriods) {
            if (mapper.startAtInMillis < period.getStartAt()) {
              mapper.startAtInMillis = period.getStartAt();
            }
            if (mapper.endAtInMillis > period.getStartAt()) {
              mapper.endAtInMillis = period.getEndAt();
            }
            while ((mapper.startAtInMillis + mapper.periodInMillis) < mapper.endAtInMillis) {
              Slot slotTemp = mapper.slot.clone();
              slotTemp.startAt = startDateOfMessage.dayOfMonth().roundFloorCopy().plusMillis(mapper.startAtInMillis);
              mapper.startAtInMillis += mapper.periodInMillis;
              slotTemp.endAt = startDateOfMessage.dayOfMonth().roundFloorCopy().plusMillis(mapper.startAtInMillis);
              slotTemp.calendarDay = dayOfTheWeek.calendarDay;
              slotTemp.save();
            }
          }
        } else {
          Logger.info("SchedulingBook does not have SchedulingBookWeekAgenda");
        }

        startDateOfMessage = startDateOfMessage.plusDays(1);
      }
      Ebean.commitTransaction();
      return true;
    } catch (Exception io) {
      Logger.error("Error processing slots ", io);
      Ebean.rollbackTransaction();
      return false;
    }
  }


  @Override
  public void slotsBooking(List<Long> ids) {

    ids.parallelStream().forEach( slotId ->{
        Slot slot = Slot.findById(slotId);
        if(isNull(slot)){
          throw new SlotUnavailableToBookingException("Slot " + slotId + " not found");
        }
        if(ObjectUtils.notEqual(slot.status, SlotStatus.AVAILABLE)){
          throw new SlotUnavailableToBookingException("Slot " + slot.getId() + " is not available anymore");
        }
        slot.booked();

    });

  }


  @Override
  public List<Card> getAvailableSlotsByShorterStayingTime(SlotAvailableQuery query) {

    return searchProvider.getAll(query,
            SlotFinderFactory.getInstance(SlotSearchType.SHORTER_STAYING)
    );

  }

}

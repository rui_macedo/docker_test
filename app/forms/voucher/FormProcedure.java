package forms.voucher;

import java.util.List;

public class FormProcedure {
    public String modality;
    public String name;
    public String startedAt;
    public List<String> preparation;

    public FormProcedure() {
    }

    public FormProcedure(String modality, String name, String startedAt) {
        this.modality = modality;
        this.name = name;
        this.startedAt = startedAt;
    }
}
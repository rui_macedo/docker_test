package forms.voucher;

public class FormPhysician {
    public String name;
    public String crm;

    public FormPhysician() {
    }

    public FormPhysician(String name, String crm) {
        this.name = name;
        this.crm = crm;
    }
}
package forms.voucher;

import com.google.common.collect.Lists;
import models.Patient;
import org.apache.commons.lang.BooleanUtils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import resource.SchedulingFullReservationResource;
import utils.DateUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.nonNull;

public class FormVoucher {

    private static final DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");

    public FormPatient patient;
    public List<FormProcedure> procedures = Lists.newArrayList();
    public String local;
    public String duration;
    public String insurance;
    public String insurancePlan;
    public FormPhysician physicianPerforming;
    public FormPhysician physicianRequesting;
    public BigDecimal amount;
    public String protocol;
    public String employeeName;
    public String dateScheduling;
    public Boolean hasPreparation = false;

    public static FormVoucher create(SchedulingFullReservationResource resource){
        FormVoucher voucher = new FormVoucher();
        voucher.patient = Patient.findById(resource.patient.id).wrapFormPatient();
        resource.requests.parallelStream().forEach(requests -> {
            FormProcedure procedure = new FormProcedure();
            procedure.name = requests.examProcedure.name;
            procedure.modality = nonNull(requests.examProcedure.examModality) ? requests.examProcedure.examModality.shortName : null;
            procedure.startedAt = nonNull(requests.start) ? fmt.print(requests.start) : null;
            procedure.preparation = buildPreparations(requests.examProcedure.preparation);
            voucher.hasPreparation = nonNull(procedure.preparation) || (BooleanUtils.isTrue(voucher.hasPreparation) ? voucher.hasPreparation : false);
            voucher.procedures.add(procedure);
        });

        voucher.local = resource.unity.name;
        voucher.duration = DateUtils.getPeriodByMinutes(resource.duration.time);
        resource.requests.parallelStream().findFirst().ifPresent(p -> {
            voucher.insurance = nonNull(p.insurance) ? p.insurance.name : null;
            voucher.insurancePlan = nonNull(p.insurancePlan) ? p.insurancePlan.name : null;
            voucher.physicianPerforming = new FormPhysician(p.physician.name, p.physician.crm);
            voucher.physicianRequesting = new FormPhysician("-", "-");
        });
        voucher.amount = resource.amount;
        voucher.employeeName = nonNull(resource.employee) ? resource.employee.name : null;
        voucher.dateScheduling = nonNull(resource.creationDate) ? fmt.print(resource.creationDate) : null;
        voucher.protocol = resource.protocol;

        return voucher;
    }

    public static List<String> buildPreparations(String preparations){
        if(nonNull(preparations)) {
            String[] allExamProcedurePreparation = preparations.split("\n");
            return Arrays.asList(allExamProcedurePreparation);
        }

        return null;
    }

}
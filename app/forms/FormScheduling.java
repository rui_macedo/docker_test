package forms;

import java.util.List;

public class FormScheduling {

    public Long contactId;
    public Long patientId;
    public String patientName;
    public String notes;
    public List<FormSchedulingFilter> filters;
}

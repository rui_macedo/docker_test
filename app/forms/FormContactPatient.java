package forms;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

public class FormContactPatient {

    @Constraints.Required
    public Long contactId;

    public Long patientId;

    public Long prePatientId;

    public String notes;

    public FormContactPatient(Long contactId, Long patientId, Long prePatientId, String notes) {
        this.contactId = contactId;
        this.patientId = patientId;
        this.prePatientId = prePatientId;
        this.notes = notes;
    }

    public FormContactPatient(Long contactId, Long patientId) {
        this.contactId = contactId;
        this.patientId = patientId;
    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();
        if (isNull(patientId) && isNull(prePatientId)) {
            errors.add(new ValidationError("patientId", "This contact should be attached to a patient or a patient pre register"));
        }
        return errors.isEmpty() ? null : errors;
    }

}


package forms;

import java.util.List;

import models.ExamProcedure;
import models.Resource;
import models.SchedulingBook;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import play.data.validation.Constraints.Required;

import common.annotations.BaseTenantModelFormatter;

public class FormSchedulingProcedure {

  @Required
  @BaseTenantModelFormatter(clazz = Resource.class, idFieldIsString = false, idFieldName = "id")
  public List<Resource> resources;

  @Required
  @BaseTenantModelFormatter(clazz = SchedulingBook.class, idFieldIsString = false, idFieldName = "id")
  public SchedulingBook book;

  @Required
  public Long referenceDate;

  @Required
  @BaseTenantModelFormatter(clazz = ExamProcedure.class, idFieldIsString = false, idFieldName = "id")
  public ExamProcedure examProcedure;

  @Required
  public int startAt;

  @Required
  public int endAt;

  public Interval boundsToInterval() {
    return new Interval(startAt, endAt);
  }

  public DateTime referenceDateToDateTime() {
    return new DateTime(referenceDate);
  }
}

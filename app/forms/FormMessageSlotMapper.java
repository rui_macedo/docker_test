package forms;

import models.Slot;
import play.data.validation.Constraints.Required;

public class FormMessageSlotMapper {

  @Required
  public Integer periodInMillis;

  @Required
  public Integer startAtInMillis;

  @Required
  public Integer endAtInMillis;
  
  @Required
  public Slot slot;
  
  public FormMessageSlotMapper() {}

  public FormMessageSlotMapper(Integer periodInMillis, Integer startAtInMillis, Integer endAtInMillis, Slot slot) {
    super();
    this.periodInMillis = periodInMillis;
    this.startAtInMillis = startAtInMillis;
    this.endAtInMillis = endAtInMillis;
    this.slot = slot;
  }
  
  public void unwrap(FormMessageSlot formMessageSlot, Slot slot){
    this.startAtInMillis = formMessageSlot.startAt.getMillisOfDay();
    this.endAtInMillis = formMessageSlot.endAt.getMillisOfDay();
    this.periodInMillis = (60000 * formMessageSlot.period);
    this.slot = slot;
  }

}

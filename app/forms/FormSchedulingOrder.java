package forms;

import java.util.List;

import common.annotations.BaseTenantModelFormatter;
import models.Patient;
import play.data.validation.Constraints.Required;

public class FormSchedulingOrder {


  @Required
  public Long insuranceId;

  @Required
  public Long insurancePlanId;

  @Required
  public Long examProcedureId;

  @Required
  public Long physicianId;

  @Required
  public Long unityId;

  @Required
  public List<Long> slots;

  public List<Long> restrictions;

  public FormSchedulingOrder() {  }

  public FormSchedulingOrder(Long insuranceId, Long insurancePlanId, Long examProcedureId, List<Long> slots) {
    this.insuranceId = insuranceId;
    this.insurancePlanId = insurancePlanId;
    this.examProcedureId = examProcedureId;
    this.slots = slots;
  }

  @Override
  public String toString() {
    return "FormSchedulingOrder{" +
            "insuranceId=" + insuranceId +
            ", insurancePlanId=" + insurancePlanId +
            ", examProcedureId=" + examProcedureId +
            ", slots=" + slots +
            ", physicianId=" + physicianId +
            '}';
  }

}

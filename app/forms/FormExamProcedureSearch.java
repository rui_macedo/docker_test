package forms;

import java.util.ArrayList;
import java.util.List;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;

import dto.ExamModalityDTO;

public class FormExamProcedureSearch {

  public Long idInstitution;

  public Long idProcedureTable;

  public String nameProcedureTable;

  public List<FormExamProcedureItens> examProcedures;

  public FormExamProcedureSearch(Long idInstitution, Long idProcedureTable, String nameProcedureTable) {
    super();
    this.idInstitution = idInstitution;
    this.idProcedureTable = idProcedureTable;
    this.nameProcedureTable = nameProcedureTable;
    examProcedures = new ArrayList<FormExamProcedureItens>();
  }
  
  public FormExamProcedureSearch(){
  }


  /**
   * Try to search originally from the institution's ExamProcedures. In case of not finding any
   * matched result, try the search on Global ExamProcedureTuss, indicating the procedure exists but
   * it is not in the Institution's register
   * 
   * @param code
   * @param name
   * @param mnemonic
   */
  public void findAllExamProcedures(String code, String name, String mnemonic) {
    if (code == null) {
      code = new String();
    }
    if (name == null) {
      name = new String();
    }
    if (mnemonic == null) {
      mnemonic = new String();
    }
    code = code.trim().replace("%", "");
    name = name.trim().replace("%", "");
    mnemonic = mnemonic.trim().replace("%", "");


    String addFilter = "";

    if (!code.isEmpty()) {
      addFilter += " and COALESCE(PTEP.Code,EP.Code) = '" + code + "'";
    }
    if (!name.isEmpty()) {
      addFilter += " and EP.Name like '" + name + "%'";
    }
    if (!mnemonic.isEmpty()) {
      addFilter += " and COALESCE(PTEP.Mnemonic,EP.Mnemonic) = '" + mnemonic + "'";
    }

    /* Searching in the Institution's ExamProcedures list */
    List<SqlRow> sqlList =
        Ebean
            .createSqlQuery(
                "Select EP.idExamProcedure as id, EP.name, " + " COALESCE(PTEP.Mnemonic,EP.Mnemonic) as mnemonic," + " COALESCE(PTEP.Code,EP.Code) as code, " + " EPT.IdExamProcedureTUSS, " + " EPT.Name as nameExamProcedureTUSS, "
                    + " EPT.code as codeExamProcedureTUSS, " + " EM.IdExamModality," + " EM.name as modalityName," + " EM.ShortName as modalityShortName" + " from ExamProcedure.ProcedureTableExamProcedure PTEP " + " inner join ExamProcedure.ExamProcedure EP "
                    + "     on PTEP.idExamProcedure = EP.idExamProcedure and PTEP.idProcedureTable = :idProcedureTable " + " inner join ExamProcedure.ExamProcedureTUSS EPT " + "     on EPT.IdExamProcedureTUSS = EP.IdExamProcedureTUSS "
                    + " left join ExamProcedure.ExamModality EM " + "     on EP.IDExamModality = EM.IDExamModality " + " where " + "    EP.idInstitution = :idInstitution " + addFilter + " order by EP.name ")
            .setParameter("idInstitution", this.idInstitution).setParameter("idProcedureTable", this.idProcedureTable).findList();


    for (SqlRow sqlRow : sqlList) {

      FormExamProcedureItens examProcedureItem = new FormExamProcedureItens();
      examProcedureItem.id = sqlRow.getLong("id").longValue();
      examProcedureItem.code = sqlRow.getString("code");
      examProcedureItem.name = sqlRow.getString("name");
      examProcedureItem.mnemonic = sqlRow.getString("mnemonic");
      examProcedureItem.IDExamProcedureTUSS = sqlRow.getLong("IDExamProcedureTUSS").longValue();
      examProcedureItem.nameExamProcedureTUSS = sqlRow.getString("nameExamProcedureTUSS");
      examProcedureItem.codeExamProcedureTUSS = sqlRow.getString("codeExamProcedureTUSS");
      if(sqlRow.getLong("IdExamModality") != null){
        examProcedureItem.examModality = new ExamModalityDTO();
        examProcedureItem.examModality.id = sqlRow.getLong("IdExamModality").longValue();
        examProcedureItem.examModality.name = sqlRow.getString("modalityName");
        examProcedureItem.examModality.shortName = sqlRow.getString("modalityShortName");  
      }
      this.examProcedures.add(examProcedureItem);
    }

    /* If couldnt find any examProcedure....try to find in the TUSS ExameProcedures list */
    if (this.examProcedures == null || this.examProcedures.isEmpty()) {

      addFilter = "";
      if (!code.isEmpty()) {
        addFilter += " where EPT.Code = '" + code + "'";
      }
      if (!name.isEmpty()) {
        if (addFilter.isEmpty()) {
          addFilter += " where ";
        } else {
          addFilter += " and ";
        }
        addFilter += " EPT.Name like '" + name + "%'";
      }

      if (!addFilter.isEmpty()) {

        /* Clear all information about Institution and ProcedureTable */
        this.idInstitution = null;
        this.idProcedureTable = null;
        this.nameProcedureTable = null;

        sqlList = Ebean.createSqlQuery("Select EPT.IdExamProcedureTUSS as id, EPT.name , " + " EPT.code " + " from  ExamProcedure.ExamProcedureTUSS EPT " + addFilter + " order by EPT.name ").findList();

        for (SqlRow sqlRow : sqlList) {

          FormExamProcedureItens examProcedureItem = new FormExamProcedureItens();
          examProcedureItem.id = sqlRow.getLong("id").longValue();
          examProcedureItem.code = sqlRow.getString("code");
          examProcedureItem.name = sqlRow.getString("name");
          examProcedureItem.IDExamProcedureTUSS = sqlRow.getLong("id").longValue();
          examProcedureItem.nameExamProcedureTUSS = sqlRow.getString("name");
          examProcedureItem.codeExamProcedureTUSS = sqlRow.getString("code");
          examProcedureItem.examModality = new ExamModalityDTO();
          examProcedureItem.examModality.id = -1L;
          examProcedureItem.examModality.name = "No Modality";
          examProcedureItem.examModality.shortName = "No Modality";
          this.examProcedures.add(examProcedureItem);
        }
      }

    }

  }


}

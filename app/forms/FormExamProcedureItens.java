package forms;

import dto.ExamModalityDTO;
import play.data.validation.Constraints;


/*Used to format searches for Exam procedures*/
public class FormExamProcedureItens {
      
	  @Constraints.Required
	  public Long id;
	
	  @Constraints.Required
	  public String name;
	  
	  @Constraints.Required
	  public String code;
	  
	  public String mnemonic;

	  public Long IDExamProcedureTUSS;
	  
	  public String nameExamProcedureTUSS;

	  public String codeExamProcedureTUSS;
	  
	  public ExamModalityDTO examModality;

	public FormExamProcedureItens() {
		super();
	}
	  
	  
	
}

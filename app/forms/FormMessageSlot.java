package forms;

import org.joda.time.DateTime;

import play.data.validation.Constraints.Required;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.deserializers.DateTimeISO8601Deserializer;
import com.pixeon.utils.common.serializers.DateTimeISO8601Serializer;


public class FormMessageSlot {

  @Required
  public Long schedulingBookId;

  @Required
  public Integer period;

  @Required
  @JsonSerialize(using = DateTimeISO8601Serializer.class)
  @JsonDeserialize(using = DateTimeISO8601Deserializer.class)
  public DateTime startAt;

  @Required
  @JsonSerialize(using = DateTimeISO8601Serializer.class)
  @JsonDeserialize(using = DateTimeISO8601Deserializer.class)
  public DateTime endAt;

  public FormMessageSlot() {}

  public FormMessageSlot(Long schedulingBookId, Integer period, DateTime startAt, DateTime endAt) {
    super();
    this.schedulingBookId = schedulingBookId;
    this.period = period;
    this.startAt = startAt;
    this.endAt = endAt;
  }

}

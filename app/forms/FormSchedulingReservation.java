package forms;

import java.util.List;

import javax.validation.Valid;

public class FormSchedulingReservation{

    @Valid
    public List<FormSchedulingOrder> orders;

    public FormSchedulingReservation(){}

    public FormSchedulingReservation(List<FormSchedulingOrder> orders) {
        this.orders = orders;
    }
}


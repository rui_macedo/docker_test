package utils;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ConverterUtils {

    public static List<Long> fromStringArrayToLongList(String[] items){
        return Arrays.asList(items)
                .parallelStream()
                .map(Long::valueOf)
                .collect(Collectors.toList());
    }

    public static <T extends Enum<T>> List<T> fromStringArrayToEnums(String[] items, Class<T> type){
        T[] result = (T[]) Array.newInstance(type, items.length);
        for (int i = 0; i < items.length; i++){
            result[i] = Enum.valueOf(type, items[i]);
        }
        return Arrays.asList(result);
    }

}

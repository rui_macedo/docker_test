package utils.email;

import com.typesafe.plugin.MailerAPI;
import com.typesafe.plugin.MailerPlugin;
import models.EmailMessage;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.libs.Akka;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

public class Mail {

    private static final TimeUnit DELAY_TIME_UNIT = TimeUnit.SECONDS;
    private static final long DELAY = 1;

    public static void sendMail(EmailMessage message) {
        EnvelopJob envelopJob = new EnvelopJob(message);
        final FiniteDuration delay = Duration.create(DELAY, DELAY_TIME_UNIT);
        Akka.system().scheduler().scheduleOnce(delay, envelopJob, Akka.system().dispatcher());
    }

    static class EnvelopJob implements Runnable {
        EmailMessage emailMessage;

        public EnvelopJob(EmailMessage envelop) {
            this.emailMessage = envelop;
        }

        public void run() {
            MailerAPI email = play.Play.application().plugin(MailerPlugin.class).email();

            email.setFrom(emailMessage.from);
            email.setSubject(emailMessage.subject);
            email.setRecipient(emailMessage.to);

            Logger.debug("Mail.sendMail: Mail will be sent to " + emailMessage.to);

            if (!StringUtils.isEmpty(emailMessage.textBody)) {
                if (!StringUtils.isEmpty(emailMessage.htmlBody)) {
                    email.send(emailMessage.textBody, emailMessage.htmlBody);
                    return;
                }
                email.send(emailMessage.textBody);
                return;
            }

            if (StringUtils.isEmpty(emailMessage.htmlBody)) {
                Logger.error("Mail is empty");
                return;
            }

            email.sendHtml(emailMessage.htmlBody);
            return;
        }
    }
}

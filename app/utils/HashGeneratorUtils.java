package utils;

import com.pixeon.utils.common.utils.DateUtils;

import org.joda.time.DateTime;

public class HashGeneratorUtils {

    public static String generateCodeBaseOnCurrtentTimeMillis(){

        DateTime now = DateUtils.getCurrentDateTimeGMTZero() ;
        return String.format("%s.%s.%s.%s",
                now.getYear(),
                now.getMonthOfYear(),
                now.getDayOfMonth(),
                now.getMillis());
    }
}

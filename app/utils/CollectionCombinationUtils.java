package utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class CollectionCombinationUtils {

    public static <T> Stream<List<T>> ofCombinations(Collection<? extends Collection<T>> collections) {

        return ofCombinations(new ArrayList<Collection<T>>(collections), Collections.emptyList());

    }

    private static <T> Stream<List<T>> ofCombinations(List<? extends Collection<T>> collections, List<T> current) {


        return collections.isEmpty() ? Stream.of(current) :
                collections.get(0).stream().flatMap(e ->
                {
                    List<T> list = new ArrayList<T>(current);
                    list.add(e);
                    return ofCombinations(collections.subList(1, collections.size()), list);
                });
    }

}

package utils;

import java.util.Map;

public class PathUtils {

    public static String getFormatted(String endpoint, Map<String, String> properties){
        for(Map.Entry<String, String> entry : properties.entrySet()){
            endpoint = endpoint.replaceAll("\\{" + entry.getKey() + "\\}", entry.getValue());
        }
        return endpoint;
    }
}

package utils;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import play.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SerializeUtils extends JsonSerializer<Float> {

    @Override public void serialize(Float arg0, JsonGenerator arg1, SerializerProvider arg2)
        throws IOException, JsonProcessingException {
        String str = arg0.toString();
        arg1.writeString(str);

    }

    public static <T> List<T> deserializeDataList(List<String> jsonNode, final Class<T> elementType) throws IOException {
        List<T>  result = new ArrayList<>();
        jsonNode.forEach(t -> {
            try {
                result.add(Jackson.getObjectMapper().readValue(t, elementType));
            } catch (IOException e) {
                Logger.error(e.getMessage());
            }
        });

        return result;
    }

    public static <T> List<T> deserializeDataList(JsonNode jsonNode, final Class<T> elementType) throws IOException {
        CollectionType listType = TypeFactory.defaultInstance().constructCollectionType(List.class, elementType);
        return (List<T>) Jackson.getObjectMapper().readValue(jsonNode.textValue(), listType);
    }
}

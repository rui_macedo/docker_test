package utils;

import static org.apache.commons.lang3.StringUtils.repeat;

import java.util.ArrayList;
import java.util.List;

public class CRMNormalizer {

  public static String normalize(String councilNumber) {
    if (councilNumber == null) {
      return councilNumber;
    }
    councilNumber = removeSpecialCharsButHyphen(removeZerosFromTheLeft(councilNumber.trim())).toUpperCase();
    return councilNumber;
  }
  
  private static String removeSpecialCharsButHyphen(String text) {
    if (text == null) {
      return null;
    }
    return text.replaceAll("[^a-zA-Z0-9-]", "");
  }
  
  private static String removeZerosFromTheLeft(String text) {
    if (text == null) {
      return null;
    }
    return text.replaceAll("^0*", "");
  }

  private static final int COUNCIL_NUMBER_MAX_LENGTH = 20;

  public static List<String> getPossibleVariations(String councilNumber) {
    String normalizedCRM = normalize(councilNumber);

    List<String> cRMVariations = new ArrayList<>();

    cRMVariations.add(normalizedCRM);
    
    for (int zerosToAdd = 1; zerosToAdd + normalizedCRM.length() < COUNCIL_NUMBER_MAX_LENGTH; zerosToAdd++) {
      cRMVariations.add(repeat("0", zerosToAdd)+normalizedCRM);
    }
    
    return cRMVariations;

  }
}

package utils;

import common.constants.DaysOfMonth;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class DateUtils {

    public static int getLastDayOfMonth(Integer year, Integer month){
        DateTime dateTime = new DateTime().withMonthOfYear(month).withYear(year);
        return dateTime.dayOfMonth().withMaximumValue().getDayOfMonth();
    }

    public static int getFirstDayOfMonth(Integer year, Integer month){

        DateTime now = new DateTime();
        DateTime dateTime = new DateTime().withMonthOfYear(month).withYear(year);

        //it means we are in the same month, so first day is today if we are before what we suppose to be the end of month
        if(now.getYear() == dateTime.getYear() && ( now.getMonthOfYear() == dateTime.getMonthOfYear())){
            return now.getDayOfMonth() <= DaysOfMonth.LAST_DAY_OF_BEGINNING_OF_MONTH ?
                    now.dayOfMonth().get() : dateTime.dayOfMonth().withMinimumValue().getDayOfMonth();
        }

        return dateTime.dayOfMonth().withMinimumValue().getDayOfMonth();

    }

    public static int getDayOfEndingMonth(Integer year, Integer month){
        DateTime now = new DateTime();
        DateTime dateTime = new DateTime().withMonthOfYear(month).withYear(year);
        //it means we are in the same month, so first day is today if we are before what we suppose to be the end of month
        if(now.getYear() == dateTime.getYear() && ( now.getMonthOfYear() == dateTime.getMonthOfYear())){
            return now.getDayOfMonth() >= DaysOfMonth.FIRST_DAY_OF_ENDING_OF_MONTH ? now.dayOfMonth().get() : DaysOfMonth.FIRST_DAY_OF_ENDING_OF_MONTH;
        }
        return DaysOfMonth.FIRST_DAY_OF_ENDING_OF_MONTH;
    }

    public static String getPeriodByMinutes(Integer minutes){
        PeriodFormatter format =
                new PeriodFormatterBuilder()
                        .printZeroAlways().minimumPrintedDigits(2).appendMinutes()
                        .appendSeparator(":")
                        .printZeroAlways().minimumPrintedDigits(2).appendSeconds()
                        .toFormatter();

        Period period = Period.minutes(minutes).normalizedStandard();
        return format.print(period);
    }

}


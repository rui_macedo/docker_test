package utils;

import play.libs.ws.WSResponse;

public class WSResponseUtils {

    public static boolean isErrorResponse(WSResponse wsResponse) {
        return wsResponse.getStatus() < 200 || wsResponse.getStatus() > 299;
    }
}

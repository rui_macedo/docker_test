package utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.util.json.Jackson;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class JsonFloatSerializer extends JsonSerializer<Float> {

    @Override public void serialize(Float arg0, JsonGenerator arg1, SerializerProvider arg2)
        throws IOException, JsonProcessingException {
        String str = arg0.toString();
        arg1.writeString(str);

    }
}

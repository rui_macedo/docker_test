package exceptions;

public class SlotUnavailableToBookingException extends RuntimeException {


    public SlotUnavailableToBookingException(String message) {
        super(message);
    }

    public SlotUnavailableToBookingException() {
    }
}

package controllers;

import com.google.inject.Inject;

import com.pixeon.cloudsecure.services.CloudSecureSkip;

import java.util.List;
import java.util.Optional;

import dto.ExamProcedureByPlanDTO;
import play.Logger;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.external.api.ExamProcedureService;


public class ExamProcedures extends Controller {

    @Inject
    private static ExamProcedureService epService;

    @CloudSecureSkip
    public static Result getAllExamProceduresWithPrice(Long institutionId, Long insurancePlanId){
        List<ExamProcedureByPlanDTO> dto =
                epService.getAllExamProcedureFinancial(institutionId, insurancePlanId);
        return ok(Json.toJson(dto));
    }

}

package controllers;

import static play.data.Form.form;

import java.util.Collections;
import java.util.List;

import com.pixeon.cloudsecure.services.CloudIntraServicesSecurity;
import models.Unity;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import com.pixeon.cloudsecure.services.CloudSecure;

import common.annotations.contenttype.ContentTypeInterceptor;
import common.utils.SerializeUtils;

public class Unities extends Controller {

    @CloudSecure(serviceScope = "agd_unity_list")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result all(Long idInstitution, List<String> fields) {
        List<Unity> unities = Unity.findAllByTenant(idInstitution);
        return ok(SerializeUtils.serializeObjectToJson(fields, unities));
    }

    @CloudIntraServicesSecurity
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result wsAll(Long idInstitution, List<String> fields) {
        return all(idInstitution, fields);
    }

    @CloudSecure(serviceScope = "agd_unity_list")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result byId(Long idInstitution,  Unity unity, List<String> fields) {
        if (unity == null) {
            return badRequest("Unity not exists!");
        }
        if (!unity.institution.id.equals(idInstitution)) {
            return unauthorized("You can't access this data from this tenant");
        }
        return ok(SerializeUtils.serializeObjectToJson(fields, unity));
    }

    @CloudSecure(serviceScope = "agd_unity_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result update(Long idInstitution,  Unity unity) {
        if (unity == null) {
            return badRequest("Unity not exists!");
        }

        if (!unity.institution.id.equals(idInstitution)) {
            return unauthorized("You can't save this data from this tenant");
        }

        Form<Unity> form = form(Unity.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        }

        Unity unityUpdated = form.get();

        if(! unity.unityCode.equals(unityUpdated.unityCode) && ! Unity.isValidUnityCode(unityUpdated.unityCode)){
          return badRequest("O código da unidade já existe");
        }

        if (! unityUpdated.validUnityName()) {
            return badRequest("O nome da unidade já existe");
        }

        unityUpdated.id = unity.id;
        unityUpdated.version = unity.version;
        unityUpdated.institution = unity.institution;
        unityUpdated.update();

        return ok(SerializeUtils.serializeObjectToJson(Collections.emptyList(), unityUpdated));
    }

    @CloudSecure(serviceScope = "agd_unity_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result save(Long idInstitution) {
        Form<Unity> form = form(Unity.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        }
        Unity unity = form.get();
        if (!unity.institution.id.equals(idInstitution)) {
            return unauthorized("You can't save this data from this tenant");
        }

        if(! Unity.isValidUnityCode(unity.unityCode)){
            return badRequest("O código da unidade já existe");
        }

        if (! unity.validUnityName()) {
            return badRequest("O nome da unidade já existe");
        }

        unity.save();
        return created(Json.toJson(unity));
    }

    @CloudSecure(serviceScope = "agd_unity_del")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result delete(Long idInstitution,Unity unity) {

        if (unity == null) {
            return badRequest("Unity not exists!");
        }
        if (!unity.institution.id.equals(idInstitution)) {
            return unauthorized("You can't delete this data from this tenant");
        }
        unity.delete();
        return ok("Unity Deleted");
    }

}

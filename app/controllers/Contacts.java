package controllers;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.pixeon.cloudsecure.services.CloudSecure;
import com.pixeon.cloudsecure.services.CloudSecureSkip;
import dto.ContactDTO;
import dto.PatientDTO;
import dto.ResponseError;
import models.scheduling.Contact;
import models.scheduling.ContactPatient;
import org.apache.commons.lang3.ObjectUtils;
import play.Logger;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import queries.ContactAllQuery;
import services.api.ContactService;
import services.patient.PatientService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class Contacts extends Controller {

    @Inject
    private static ContactService service;

    @Inject
    private static PatientService patientService;

    @CloudSecure(serviceScope = "list_contacts")
    public static Result all(Long institutionId, String phone, String name) {
        List<ContactDTO> result = service.list(new ContactAllQuery(institutionId, name, phone));
        return ok(Json.toJson(result));
    }

    @CloudSecure(serviceScope = "list_contacts")
    public static Result byId(Long institutionId, Long contactId) {

        Optional<ContactDTO> contact = service.findByIdOptional(contactId);

        if(! contact.isPresent()){
            return notFound(Json.toJson(ResponseError.create("Not found Contact with the given id",
                    ResponseError.Codes.NOT_FOUND)));
        }

        if(ObjectUtils.notEqual(contact.get().institutionId, institutionId)){
            return forbidden(Json.toJson(ResponseError.create("Contact with the given id" +
                    " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
        }

        return ok(Json.toJson(contact.get()));
    }

    @CloudSecure(serviceScope = "save_contact")
    public static Result save(Long institutionId) {

        if(isNull(request().body().asJson())){
            return badRequest(Json.toJson(ResponseError.create("Request payload is expected",
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        Form<ContactDTO> dto = Form.form(ContactDTO.class).bindFromRequest();
        if(dto.hasErrors()){
            return badRequest(Json.toJson(ResponseError.create(dto.errorsAsJson(),
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        try{
            ContactDTO contact = service.create(institutionId, dto.get());
            return created(Json.toJson(contact));
        }catch (Exception ex){
            Logger.error("Error creating a Contact. Request = {}", dto.get().toString(), ex);
            return status(SERVICE_UNAVAILABLE, Json.toJson(ResponseError.create("We are not able to " +
                    "process the request", ResponseError.Codes.SERVICE_UNAVAILABLE)) );
        }

    }

    @CloudSecure(serviceScope = "update_contact")
    public static Result update(Long institutionId, Long contactId) {

        if(isNull(request().body().asJson())){
            return badRequest(Json.toJson(ResponseError.create("Request payload is expected",
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        Form<ContactDTO> request = Form.form(ContactDTO.class).bindFromRequest();
        if(request.hasErrors()){
            return badRequest(Json.toJson(ResponseError.create(request.errorsAsJson(),
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        Contact contact = Contact.findById(contactId);
        if(isNull(contact)){
            return notFound(Json.toJson(ResponseError.create("Not found RequestedOrder with the given id",
                    ResponseError.Codes.NOT_FOUND)));
        }

        if(ObjectUtils.notEqual(contact.institutionId, institutionId)){
            return forbidden(Json.toJson(ResponseError.create("Contact with the given id" +
                    " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
        }

        ContactDTO dto  = request.get();
        dto.institutionId = institutionId;

        service.update(contact, dto);

        return ok();
    }

    @CloudSecure(serviceScope = "list_contacts")
    public static Result allPatients(Long institutionId, Long contactId) {
        Set<Long> patientsId = ContactPatient.findAllByContact(contactId).parallelStream().map(c -> c.patientId).collect(Collectors.toSet());

        Optional<List<PatientDTO>> optional = patientService.getPatients(institutionId, patientsId);
        if(optional.isPresent()){
            return ok(Json.toJson(optional.get().parallelStream().map(c -> c).collect(Collectors.toList())));
        }
        Logger.debug("Optional list of patient is not present");
        return ok();
    }



}

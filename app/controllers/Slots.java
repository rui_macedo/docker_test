package controllers;

import static play.data.Form.form;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;

import dto.SlotReservationDTO;
import models.Institution;
import models.SchedulingBook;
import models.Slot;

import org.joda.time.DateTime;

import play.data.Form;
import play.data.validation.Validation;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import queries.slots.SlotAvailableQuery;
import queries.slots.binder.SlotSearchQueryBinder;
import scala.Option;
import services.api.SlotService;
import services.external.api.EmployeeData;
import services.external.api.QueueService;
import services.searchers.model.Card;
import services.searchers.model.Item;
import validation.Validators;

import com.amazonaws.services.sqs.model.Message;
import com.google.common.base.Preconditions;
import com.google.inject.Inject;

import com.avaje.ebean.annotation.Transactional;
import com.pixeon.cloudsecure.services.CloudSecure;
import com.pixeon.cloudsecure.services.CloudSecureSkip;

import common.constants.SchedulingBookSlotStatus;
import dto.SchedulingBookSlotsDTO;
import forms.FormMessageSlot;

public class Slots extends Controller {

  @Inject
  private static QueueService<Message> sqs;

  @Inject
  private static EmployeeData employeeData;

  @Inject
  private static SlotService slotService;

  @CloudSecure(serviceScope = "agd_search_slots")
  public static Result all(Long tenantId, Option<Long> date, List<String> fields) {

    DateTime from = date.isDefined() ? new DateTime(date.get()) : new DateTime();
    // by the end of day
    DateTime to = new DateTime(from).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);

    List<Slot> slots = Slot.findAllByInstitutionAndDate(Institution.byId(tenantId), from, to);

    return ok(Json.toJson(slots.parallelStream().map(s -> s.wrapWithBook()).collect(Collectors.toList())));

  }

  @CloudSecure(serviceScope = "agd_search_slots")
  public static Result byBook(Long tenantId, Long schedulingBookId,
                              Option<Long> minDate, Option<Long> maxDate, List<String> fields) {

    SchedulingBook book = SchedulingBook.findById(schedulingBookId);
    if (book == null) {
      return notFound();
    }

    if (!book.belongsTo(Institution.byId(tenantId))) {
      return forbidden();
    }

    DateTime from = minDate.isDefined() ? new DateTime(minDate.get()) : new DateTime();
    DateTime to = maxDate.isDefined() ? new DateTime(maxDate.get()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59) : new DateTime(from).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);

    List<Slot> slots = Slot.findAllBySchedulingBookAndDate(book, from, to);

    SchedulingBookSlotsDTO dto = new SchedulingBookSlotsDTO();
    dto.schedulingBook = book.wrap();
    dto.slots = slots.parallelStream().map(s -> s.wrap()).collect(Collectors.toList());

    return ok(Json.toJson(dto));

  }

  @CloudSecure(serviceScope = "generate_slots")
  @Transactional
  public Result generateSlots(Long idInstitution) {

    Preconditions.checkNotNull(sqs, "SQS is missing");
    FormMessageSlot dto = Json.fromJson(request().body().asJson(), FormMessageSlot.class);
    Set<ConstraintViolation<FormMessageSlot>> dtoValidate = Validation.getValidator().validate(dto);

    if (!dtoValidate.isEmpty()) {
      return badRequest(Validators.getValidationResponse(dtoValidate));
    }

    if (!Slot.isValidCreationOfSlots(dto)) {
      return badRequest("Slots already exists with the given period or period informed is invalid");
    }

    SchedulingBook book = SchedulingBook.findById(dto.schedulingBookId);
    book.waitingSlots();
    try {
      sqs.send(Json.stringify(Json.toJson(dto)));
    } catch (Exception e) {
      return badRequest(e.getMessage());
    }
    return status(ACCEPTED);
  }

  @CloudSecure(serviceScope = "agd_save_slots")
  public static Result lock(Long tenantId, Long slotId){
    Slot slot = Slot.findById(slotId);
    if(slot == null ){
      return notFound("Slot doesn't exist");
    }

    if(! slot.belongsTo(tenantId)){
      return forbidden("You can not lock a slot from another tenant");
    }

    if(! slot.isAvailable()){
      return badRequest("This slot " + slotId + " is not available anymore for reservations");
    }

    slot.lock(employeeData.getCurrentEmployeeId());
    return ok(Json.toJson(slot.wrapReservation()));

  }

  @CloudSecure(serviceScope = "agd_save_slots")
  public static Result unlock(Long tenantId, Long slotId){

    Slot slot = Slot.findById(slotId);

    if(slot == null ){
      return notFound("Slot doesn't exist");
    }

    if(slot.reservation == null){
      return badRequest("The slot " + slot.id + " has no reservations, it is available");
    }

    if(! slot.belongsTo(tenantId)) {
      return forbidden("You can not unlock a slot from another tenant");
    }

    if (request().body() == null || request().body().asJson() == null) {
      return badRequest("A valid token was expected");
    }

    Form<SlotReservationDTO> form = form(SlotReservationDTO.class).bindFromRequest();
    if(form.hasErrors()){
      return badRequest(form.errorsAsJson());
    }

    SlotReservationDTO request = form.get();
    request.employeeId = employeeData.getCurrentEmployeeId();
    if ( !  slot.reservation.canUnlock(request.employeeId, request.token)) {
      return unauthorized("The slot " + slotId + " was locked by someone else or your token is invalid");
    }
    slot.unlock(request.employeeId, request.token);

    return ok();
  }

  @CloudSecureSkip
  public static Result listAvailable(Long institutionId){

    SlotAvailableQuery query = new SlotSearchQueryBinder()
            .bindFromQueryString(institutionId, request().queryString());

    List<Card> cards =
            slotService.getAvailableSlotsByShorterStayingTime(query);

    System.out.println("");
    System.out.println("");
    System.out.println("***************************************************");

    int index = 0;
    for(Card cluster : cards){
      System.out.println("Start at:  " + cluster.start + " .End at: " + cluster.end);
      System.out.println("Duration: " + cluster.duration);
      for(Item card : cluster.items){
        System.out.println("  Card Exam-Proceudre " + card.examProcedure.id +". Start at:  " + card.start + " .End at: " + card.end);
      }
      index++;
    }

    System.out.println("");
    System.out.println("***************************************************");


    return ok(Json.toJson(cards));

  }

}

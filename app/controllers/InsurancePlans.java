package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.google.inject.Inject;
import com.pixeon.cloudsecure.services.CloudSecureSkip;
import common.utils.SerializeUtils;

import dto.InsurancePlanDTO;
import dto.ResponseError;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;

import models.Insurance;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import com.pixeon.cloudsecure.services.CloudSecure;
import queries.InsurancePlanQuery;
import resource.InsurancePlanResource;
import resource.InsuranceResource;
import services.api.InsurancePlanService;
import services.api.InsuranceService;

public class InsurancePlans extends Controller {

  @Inject
  public static InsurancePlanService planService;

  @Inject
  public static InsuranceService insuranceService;

  @CloudSecure(serviceScope = "agd_insurancecarrierplan_all")
  public static Result byInsurances(Long idInstitution, List<Insurance> insurances) {
    if (insurances == null) {
      return badRequest("Insurance does not exists");
    }
    List<InsurancePlanDTO> plans = new ArrayList<InsurancePlanDTO>();
    insurances.parallelStream().forEach(i -> i.insurancePlans.parallelStream().forEach(is -> plans.add(is.wrap())));
    return ok(Json.toJson(plans));

  }


  @CloudSecure(serviceScope = "agd_insurancecarrierplan_all")
  public static Result getPlans(Long institutionId, Long id, List<String> fields) {

    Optional<InsuranceResource> insurance = insuranceService.findByIdOptional(id);

    if(! insurance.isPresent()){
      return notFound(Json.toJson(ResponseError.create("Not found Insurance with the given id",
              ResponseError.Codes.NOT_FOUND)));
    }

    if (ObjectUtils.notEqual(insurance.get().institutionId, institutionId)){
      return forbidden(Json.toJson(ResponseError.create("Insurance with the given id" +
              " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
    }

    List<InsurancePlanResource> resources = planService.findAllEnabledByInsurance(new InsurancePlanQuery(institutionId, id));
    if(CollectionUtils.isEmpty(resources)){
      return status(NO_CONTENT, Json.toJson(ResponseError.create("The given Insurance has no plans",
              ResponseError.Codes.ENTITY_HAS_NO_RELATION)));
    }

    return ok(SerializeUtils.toJson(resources, fields));
  }

  @CloudSecure(serviceScope = "agd_insurancecarrierplan_all")
  public static Result byInsuranceAndId(Long institutionId, Long insuranceId, Long insurancePlanId, List<String> fields){

    Optional<InsurancePlanResource> plan = planService.findByIdOptional(insurancePlanId);

    if(! plan.isPresent()){
      return notFound(Json.toJson(ResponseError.create("Not found InsurancePlan with the given id",
              ResponseError.Codes.NOT_FOUND)));
    }

    if(ObjectUtils.notEqual(plan.get().insuranceId, insuranceId)){
      return notFound(Json.toJson(ResponseError.create("Not found InsurancePlan with the given id for the given insuranceId",
              ResponseError.Codes.NOT_FOUND)));
    }

    if(ObjectUtils.notEqual(plan.get().institutionId, institutionId)){
      return forbidden(Json.toJson(ResponseError.create("InsurancePlan with the given id" +
              " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
    }

    return ok(SerializeUtils.toJson(plan.get(), fields));
  }


}

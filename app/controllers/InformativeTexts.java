package controllers;

import static play.data.Form.form;

import java.util.Collections;
import java.util.List;

import models.InformativeText;

import com.pixeon.cloudsecure.services.CloudSecure;

import common.annotations.contenttype.ContentTypeInterceptor;
import common.utils.SerializeUtils;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class InformativeTexts extends Controller {

    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    @CloudSecure(serviceScope = "agd_Inftext_all")
    public static Result all(Long idInstitution, List<String> fields) {
        List<InformativeText> informativetext = InformativeText.findAllByInstitution(idInstitution);
        return ok(SerializeUtils.serializeObjectToJson(fields, informativetext));
    }

    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    @CloudSecure(serviceScope = "agd_Inftext_all")
    public static Result byId(Long idInstitution, InformativeText informativetext, List<String> fields) {


        if (informativetext != null) {
            if (informativetext.institution.id.equals(idInstitution)) {
                return ok(SerializeUtils.serializeObjectToJson(fields, informativetext));
            } else {
                return ok(SerializeUtils.serializeObjectToJson(fields, "You can't access this data using this tenant"));
            }
        } else {
            return badRequest(Json.toJson("InformativeText doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_Inftext_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result save(Long idInstitution) {
        Form<InformativeText> form = form(InformativeText.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        }
        InformativeText informativetext = form.get();
        if (informativetext.institution.id.equals(idInstitution)) {
            informativetext.save();
            return ok(Json.toJson(informativetext));
        } else {
            return badRequest(Json.toJson("InformativeText doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_Inftext_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result update(Long idInstitution, InformativeText informativetext) {
        if (informativetext != null) {
            if (informativetext.institution.id.equals(idInstitution)) {
                Form<InformativeText> form = form(InformativeText.class).bindFromRequest();
                if (form.hasErrors()) {
                    return badRequest(form.errorsAsJson());
                }
                InformativeText informativetextUpdated = form.get();
                informativetextUpdated.id = informativetext.id;
                informativetextUpdated.version = informativetext.version;
                informativetextUpdated.institution = informativetext.institution;
                informativetextUpdated.update();
                List<String> fields = Collections.emptyList();
                return ok(SerializeUtils.serializeObjectToJson(fields, informativetextUpdated));
            } else {
                return badRequest(Json.toJson("InformativeText doesn't exist or You can't access this data using this tenant!"));
            }
        } else {
            return badRequest(Json.toJson("InformativeText doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_Inftext_del")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result delete(Long idInstitution, InformativeText informativetext) {
        if (informativetext != null) {
            if (informativetext.institution.id.equals(idInstitution)) {
                informativetext.delete();
                return ok(Json.toJson("InformativeText Deleted"));
            } else {
                return badRequest(Json.toJson("You can't delete this data using this tenant"));
            }
        } else {
            return badRequest(Json.toJson("InformativeText doesn't exist or You can't access this data using this tenant!"));
        }
    }
}
 
 
 
 
 
 
 
 
 
 
 

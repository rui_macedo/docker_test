package controllers;

import com.pixeon.cloudsecure.services.CloudSecureSkip;

import play.mvc.Controller;
import play.mvc.Result;

public class Healthchecks extends Controller {

    @CloudSecureSkip
    public static Result ping() {
        return ok("OK");
    }
}

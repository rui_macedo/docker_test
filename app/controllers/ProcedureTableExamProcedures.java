package controllers;

import static play.data.Form.form;

import java.util.Collections;
import java.util.List;

import models.ProcedureTable;
import models.ProcedureTableExamProcedure;

import com.pixeon.cloudsecure.services.CloudSecure;
import common.annotations.contenttype.ContentTypeInterceptor;
import common.utils.SerializeUtils;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class ProcedureTableExamProcedures extends Controller {

    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    @CloudSecure(serviceScope = "agd_proctableexamprocedure_all")
    public static Result all(Long idInstitution, ProcedureTable procedureTable,List<String> fields) {
        List<ProcedureTableExamProcedure> proceduretableexamprocedure = ProcedureTableExamProcedure.findAllByInstitution(idInstitution, procedureTable);
        return ok(SerializeUtils.serializeObjectToJson(fields, proceduretableexamprocedure));
    }

    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    @CloudSecure(serviceScope = "agd_proctableexamprocedure_all")
    public static Result byId(Long idInstitution, ProcedureTableExamProcedure proceduretableexamprocedure, List<String> fields) {


        if (proceduretableexamprocedure != null) {
            if (proceduretableexamprocedure.institution.id.equals(idInstitution)) {
                return ok(SerializeUtils.serializeObjectToJson(fields, proceduretableexamprocedure));
            } else {
                return ok(SerializeUtils.serializeObjectToJson(fields, "You can't access this data using this tenant"));
            }
        } else {
            return badRequest(Json.toJson("ProcedureTableExamProcedure doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_proctableexamprocedure_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result save(Long idInstitution) {
        Form<ProcedureTableExamProcedure> form = form(ProcedureTableExamProcedure.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        }
        ProcedureTableExamProcedure proceduretableexamprocedure = form.get();
        if (proceduretableexamprocedure.institution.id.equals(idInstitution)) {
            proceduretableexamprocedure.save();
            return ok(Json.toJson(proceduretableexamprocedure));
        } else {
            return badRequest(Json.toJson("ProcedureTableExamProcedure doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_proctableexamprocedure_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result update(Long idInstitution, ProcedureTableExamProcedure proceduretableexamprocedure) {
        if (proceduretableexamprocedure != null) {
            if (proceduretableexamprocedure.institution.id.equals(idInstitution)) {
                Form<ProcedureTableExamProcedure> form = form(ProcedureTableExamProcedure.class).bindFromRequest();
                if (form.hasErrors()) {
                    return badRequest(form.errorsAsJson());
                }
                ProcedureTableExamProcedure proceduretableexamprocedureUpdated = form.get();
                proceduretableexamprocedureUpdated.id = proceduretableexamprocedure.id;
                proceduretableexamprocedureUpdated.version = proceduretableexamprocedure.version;
                proceduretableexamprocedureUpdated.institution = proceduretableexamprocedure.institution;
                proceduretableexamprocedureUpdated.update();
                List<String> fields = Collections.emptyList();
                return ok(SerializeUtils.serializeObjectToJson(fields, proceduretableexamprocedureUpdated));
            } else {
                return badRequest(Json.toJson("ProcedureTableExamProcedure doesn't exist or You can't access this data using this tenant!"));
            }
        } else {
            return badRequest(Json.toJson("ProcedureTableExamProcedure doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_proctableexamprocedure_del")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result delete(Long idInstitution,  ProcedureTableExamProcedure proceduretableexamprocedure) {
        if (proceduretableexamprocedure != null) {
            if (proceduretableexamprocedure.institution.id.equals(idInstitution)) {
                proceduretableexamprocedure.delete();
                return ok(Json.toJson("ProcedureTableExamProcedure Deleted"));
            } else {
                return badRequest(Json.toJson("You can't delete this data using this tenant"));
            }
        } else {
            return badRequest(Json.toJson("ProcedureTableExamProcedure doesn't exist or You can't access this data using this tenant!"));
        }
    }
}
 
 
 
 
 
 
 

package controllers;

import java.util.List;
import java.util.Optional;

import com.avaje.ebean.Ebean;
import com.google.inject.Inject;

import com.pixeon.cloudsecure.services.CloudSecure;
import com.pixeon.cloudsecure.services.CloudSecureSkip;
import dto.ContactDTO;
import dto.ContactPatientDTO;
import dto.ResponseError;

import common.utils.SerializeUtils;
import forms.FormContactPatient;
import models.scheduling.ContactPatient;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import play.Logger;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.api.ContactService;

import static java.util.Objects.isNull;

public class ContactPatients extends Controller {

    @Inject
    private static ContactService service;

    @CloudSecure(serviceScope = "agd_contpac_all")
    public static Result byContactId(Long institutionId, Long contactId, List<String> fields){

        Optional<ContactDTO> contact = service.findByIdOptional(contactId);

        if(! contact.isPresent()){
            return notFound(Json.toJson(ResponseError.create("Not found Contact with the given id",
                    ResponseError.Codes.NOT_FOUND)));
        }

        if(ObjectUtils.notEqual(contact.get().institutionId, institutionId)){
            return forbidden(Json.toJson(ResponseError.create("Contact with the given id" +
                    " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
        }

        List<ContactPatientDTO> result = service.getPatientByContact(contactId);
        if(CollectionUtils.isEmpty(result)){
            return status(NO_CONTENT, Json.toJson(ResponseError.create("The given Contact has no patients" +
                    " attached yet ", ResponseError.Codes.ENTITY_HAS_NO_RELATION)));
        }

        return ok(SerializeUtils.toJson(result, fields));
    }


    @CloudSecure(serviceScope = "agd_contpac_all")
    public static Result byId(Long institutionId, Long contactPatientId, List<String> fields) {

        Optional<ContactPatientDTO> cp = service.findContactPatientByIdOptional(contactPatientId);
        if(! cp.isPresent()){
            return notFound(Json.toJson(ResponseError.create("Not found ContactPatient with the given id",
                    ResponseError.Codes.NOT_FOUND)));
        }

        if(ObjectUtils.notEqual(cp.get().institutionId, institutionId)){
            return forbidden(Json.toJson(ResponseError.create("Contact with the given id" +
                    " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
        }

        return ok(SerializeUtils.toJson(cp.get(), fields));
    }

    @CloudSecure(serviceScope = "agd_contpac_sv")
    public static Result save(Long institutionId){

        if(isNull(request().body().asJson())){
            return badRequest(Json.toJson(ResponseError.create("Request payload is expected",
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        Form<FormContactPatient> form = Form.form(FormContactPatient.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(Json.toJson(ResponseError.create(form.errorsAsJson(),
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));        }

        Ebean.beginTransaction();
        try{

            ContactPatientDTO dto = service.addPatient(institutionId, form.get());
            Ebean.commitTransaction();
            return ok(Json.toJson(dto));

        }catch (Exception ex){
            Logger.error("Error attaching a patient to Contact. Request = {}", form.get().toString(), ex);
            return status(SERVICE_UNAVAILABLE, Json.toJson(ResponseError.create("We are not able to " +
                    "process the request", ResponseError.Codes.SERVICE_UNAVAILABLE)) );
        }finally {
            Ebean.endTransaction();
        }
    }


    @CloudSecure(serviceScope = "agd_contpac_sv")
    public static Result update(Long institutionId, Long contatPatientId) {

        if(isNull(request().body().asJson())){
            return badRequest(Json.toJson(ResponseError.create("Request payload is expected",
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        Form<FormContactPatient> request = Form.form(FormContactPatient.class).bindFromRequest();
        if(request.hasErrors()){
            return badRequest(Json.toJson(ResponseError.create(request.errorsAsJson(),
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        ContactPatient cp = ContactPatient.findById(contatPatientId);
        if(isNull(cp)){
            return notFound(Json.toJson(ResponseError.create("Not found RequestedOrder with the given id",
                    ResponseError.Codes.NOT_FOUND)));
        }

        if(ObjectUtils.notEqual(cp.institutionId, institutionId)){
            return forbidden(Json.toJson(ResponseError.create("Contact with the given id" +
                    " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
        }

        service.updateContactPatient(cp, request.get());

        return ok();
    }

}
 

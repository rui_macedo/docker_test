package controllers;

import static play.data.Form.form;

import java.util.List;

import models.Equipment;
import models.Institution;
import play.data.Form;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Controller;

import com.pixeon.cloudsecure.services.CloudSecure;

import common.utils.SerializeUtils;

public class Equipments extends Controller {

    @CloudSecure(serviceScope = "agd_equipment_list")
    public static Result findAll(Long tenant, List<String> fields) {
        if (Institution.byId(tenant) == null)
            return notFound();
        List<Equipment> all = Equipment.findAll(tenant);
        return ok(SerializeUtils.serializeObjectToJson(fields, all));
    }

    @CloudSecure(serviceScope = "agd_equipment_list")
    public static Result byId(Equipment equipment, Long tenant, List<String> fields) {
        if (equipment == null)
            return notFound();

        return ok(SerializeUtils.serializeObjectToJson(fields, equipment));
    }

    @CloudSecure(serviceScope = "agd_equipment_sv")
    public static Result save(Long tenant) {
        Form<Equipment> filledForm = form(Equipment.class).bindFromRequest();
        if (filledForm.hasErrors()) {
            return badRequest(filledForm.errorsAsJson());
        }

        Equipment equipment = filledForm.get();
        if (!equipment.institution.id.equals(tenant)) {
            return unauthorized();
        }
        equipment.save();
        return created(Json.toJson(equipment));
    }

    @CloudSecure(serviceScope = "agd_equipment_sv")
    public static Result update( Equipment equipment, Long tenant) {

        Form<Equipment> filledForm = form(Equipment.class).bindFromRequest();
        if (filledForm.hasErrors()) {
            return badRequest(filledForm.errorsAsJson());
        }

        if (equipment == null) {
            return badRequest();
        }
        if (!equipment.institution.id.equals(tenant)) {
            return unauthorized();
        }

        Equipment newEquip = filledForm.get();
        newEquip.id = equipment.id;
        newEquip.version = equipment.version;
        newEquip.update();
        return ok(Json.toJson(newEquip));

    }

    @CloudSecure(serviceScope = "agd_equipment_del")
    public static Result delete(Equipment equipment, Long tenant) {

        if (equipment == null) {
            return badRequest("Equipment not found");
        }
        if (!equipment.institution.id.equals(tenant)) {
            return unauthorized();
        }
        equipment.delete();
        return ok("Deleted");
    }
}

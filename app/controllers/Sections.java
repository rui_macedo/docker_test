package controllers;

import static play.data.Form.form;

import java.util.Collections;
import java.util.List;

import models.Section;
import models.Unity;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import com.pixeon.cloudsecure.services.CloudSecure;

import common.annotations.contenttype.ContentTypeInterceptor;
import common.utils.SerializeUtils;

public class Sections extends Controller {

    @CloudSecure(serviceScope = "agd_section_list")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result all(Long idInstitution, List<String> fields) {
        List<Section> sections = Section.findAllByTenant(idInstitution);
        return ok(SerializeUtils.serializeObjectToJson(fields, sections));
    }

    @CloudSecure(serviceScope = "agd_section_list")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result allByUnity(Long idInstitution, Unity unity, List<String> fields) {
        if (unity != null) {
            if (unity.institution.id.equals(idInstitution)) {
                List<Section> sections = Section.findAllByUnity(unity);
                return ok(SerializeUtils.serializeObjectToJson(fields, sections));
            } else {
                return unauthorized("You can't access this data from this tenant");
            }
        } else {
            return badRequest("Unity not exists!");
        }
    }

    @CloudSecure(serviceScope = "agd_section_list")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result byId(Long idInstitution,Section section, List<String> fields) {
        if (section != null) {
            if (section.institution.id.equals(idInstitution)) {
                return ok(SerializeUtils.serializeObjectToJson(fields, section));
            } else {
                return unauthorized("You can't access this data from this tenant");
            }
        } else {
            return badRequest("Section not exists!");
        }
    }

    @CloudSecure(serviceScope = "agd_section_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result save(Long idInstitution) {
        Form<Section> form = form(Section.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        }
        Section section = form.get();
        if (section.institution.id.equals(idInstitution)) {
            section.save();
            return ok(Json.toJson(section));
        } else {
            return unauthorized("You can't save this data from this tenant");
        }
    }

    @CloudSecure(serviceScope = "agd_section_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result update(Long idInstitution, Section section) {
        if (section != null) {
            if (section.institution.id.equals(idInstitution)) {
                Form<Section> form = form(Section.class).bindFromRequest();
                if (form.hasErrors()) {
                    return badRequest(form.errorsAsJson());
                }
                Section sectionUpdated = form.get();
                sectionUpdated.id = section.id;
                sectionUpdated.version = section.version;
                sectionUpdated.institution = section.institution;
                sectionUpdated.update();
                List<String> fields = Collections.emptyList();
                return ok(SerializeUtils.serializeObjectToJson(fields, sectionUpdated));
            } else {
                return unauthorized("You can't save this data from this tenant");
            }
        } else {
            return badRequest("Section not exists!");
        }
    }

    @CloudSecure(serviceScope = "agd_section_del")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result delete(Long idInstitution, Section section) {
        if (section != null) {
            if (section.institution.id.equals(idInstitution)) {
                section.delete();
                return ok("Section Deleted");
            } else {
                return unauthorized("You can't delete this data from this tenant");
            }
        } else {
            return badRequest("Section not exists!");
        }
    }


}

package controllers;

import static play.data.Form.form;

import java.util.List;
import java.util.stream.Collectors;
import models.Institution;
import models.Restriction;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import com.pixeon.cloudsecure.services.CloudSecure;
import common.utils.SerializeUtils;

import dto.RestrictionDTO;

public class Restrictions extends Controller {

  @CloudSecure(serviceScope = "list_restrictions")
  public static Result all(Long idInstitution,  List<String> fields) {
    List<Restriction> restrictions = Restriction.findAllByInstitutionId(idInstitution);
    return ok(SerializeUtils.serializeObjectToJson(fields, restrictions.parallelStream().map(r -> r.wrap()).collect(Collectors.toList())));

  }

  @CloudSecure(serviceScope = "list_restrictions")
  public static Result byId(Long idInstitution, Restriction restriction, List<String> fields) {
    if (restriction == null) {
      return notFound();
    }
    if (restriction.institution.getId() != idInstitution) {
      return unauthorized();
    }
    return ok(SerializeUtils.serializeObjectToJson(fields, restriction.wrap()));
  }

  @CloudSecure(serviceScope = "save_restriction")
  public static Result save(Long idInstitution) {
    if (request().body() == null || request().body().asJson() == null) {
      return badRequest("JSON body expected");
    }

    Form<RestrictionDTO> filledForm = form(RestrictionDTO.class).bindFromRequest();
    if (filledForm.hasErrors()) {
      return badRequest(filledForm.errorsAsJson());
    }

    Restriction restriction = new Restriction();
    restriction.unwrap(filledForm.get());
    restriction.institution = Institution.byId(idInstitution);
    restriction.isDeleted = false;
    restriction.save();
    return created(Json.toJson(restriction.wrap()));
  }
}

package controllers;

import com.avaje.ebean.Ebean;
import com.google.inject.Inject;

import com.fasterxml.jackson.core.type.TypeReference;
import com.pixeon.cloudsecure.services.CloudSecure;
import com.pixeon.cloudsecure.services.CloudSecureSkip;

import dto.ResponseError;
import exceptions.SlotUnavailableToBookingException;
import forms.FormSchedulingReservation;
import models.scheduling.Scheduling;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Arrays;
import java.util.List;

import models.searchalg.DynamicSlotGroup;
import play.Logger;
import play.Play;
import play.data.Form;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.api.SchedulingService;
import services.configuration.SchedulingReservationConfiguration;
import services.external.api.EmployeeData;

import static java.util.Objects.isNull;

public class Reservations extends Controller {

    @Inject
    public static SchedulingService service;

    @Inject
    public static EmployeeData employeeService;

    @Inject
    public static SchedulingReservationConfiguration reservationConfig;

    @CloudSecure(serviceScope = "agd_search_slots")
    public static Result reservation(Long institutionId, Long schedulingId) {
 
        if (isNull(request().body().asJson())) {
            return badRequest(Json.toJson(ResponseError.create("Request payload is expected",
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        Form<FormSchedulingReservation> request = Form.form(FormSchedulingReservation.class).bindFromRequest();
        if (request.hasErrors()) {
            return badRequest(Json.toJson(ResponseError.create(request.errorsAsJson(),
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        Scheduling scheduling = Scheduling.findById(schedulingId);
        if (isNull(scheduling)) {
            return notFound(Json.toJson(ResponseError.create("Not found a scheduling with the given id",
                    ResponseError.Codes.NOT_FOUND)));
        }

        if (ObjectUtils.notEqual(scheduling.institutionId, institutionId)) {
            return forbidden(Json.toJson(ResponseError.create("The scheduling process with the given id" +
                    " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
        }

        if (ObjectUtils.notEqual(scheduling.employeeId, employeeService.getCurrentEmployeeId())) {
            return forbidden(Json.toJson(ResponseError.create("You are not the owner of this booking",
                    ResponseError.Codes.NOT_OWNER)));
        }

        if(ObjectUtils.notEqual(scheduling.status, Scheduling.Status.IN_PROGRESS)){
            return badRequest(Json.toJson(ResponseError.create("The given scheduling is no longer available." +
                            " It might be finished or booked already",
                    ResponseError.Codes.NOT_AVAILABLE)));
        }

        if(isNull(scheduling.contact) ){
            Logger.debug("BODY_CONTENT_PROBLEM " + scheduling.toString());
            return forbidden(Json.toJson(ResponseError.create("The given scheduling has no contact yet " +
                            "to be able to book",
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        if(isNull(scheduling.patientId) ){
            Logger.debug("BODY_CONTENT_PROBLEM " + scheduling.toString());
            return forbidden(Json.toJson(ResponseError.create("The given scheduling has no patientId yet " +
                            "to be able to book ",
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        Ebean.beginTransaction();
        try {
            service.makeBooking(scheduling, request.get());
            Ebean.commitTransaction();
            // By default we are setting false, to avoid high bounce rate on developing or staging mode
            if(reservationConfig.isAutoEmailEnable()){
                sendVoucher(scheduling.getId());
            } else {
                Logger.info("Not enable auto-email");
            }
            return ok();
        }catch (SlotUnavailableToBookingException se){
            Logger.error("Slot already booked", se);
            return badRequest( Json.toJson(ResponseError.create(se.getMessage(),
                            ResponseError.Codes.SLOT_BOOKED)));
        } catch (Exception ex) {
            Logger.error("Error booked a Scheduling", ex);
            return status(SERVICE_UNAVAILABLE,
                    Json.toJson(ResponseError.create("We are not able to process the request. Try again later",
                            ResponseError.Codes.SERVICE_UNAVAILABLE)));
        }finally{
            Ebean.endTransaction();
        }

    }

    private static void sendVoucher(Long schedulingId){
        try{
            service.sendVoucher(schedulingId);
        }catch (RuntimeException re){
            Logger.error("We could not send email to patient.", re);
        }
    }

}

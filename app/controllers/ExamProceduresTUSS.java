package controllers;

import java.util.List;

import models.ExamProcedureTUSS;
import play.mvc.Controller;
import play.mvc.Result;

import com.pixeon.cloudsecure.services.CloudSecure;
import common.utils.SerializeUtils;

public class ExamProceduresTUSS extends Controller {

  @CloudSecure(serviceScope = "list_examproceduretuss")
  @Deprecated
  public static Result all( Long idInstitution,  List<String> fields) {
    return listAll(fields);
  }

  @Deprecated
  @CloudSecure(serviceScope = "list_examproceduretuss")
  public static Result byId( Long idInstitution, ExamProcedureTUSS examProcedureTUSS, List<String> fields) {
    return retrieve(examProcedureTUSS, fields);
  }

  @CloudSecure(serviceScope = "list_examproceduretuss")
  public static Result listAll(List<String> fields) {
    return ok(SerializeUtils.serializeObjectToJson(fields, ExamProcedureTUSS.findAll()));
  }

  @CloudSecure(serviceScope = "list_examproceduretuss")
  public static Result retrieve( ExamProcedureTUSS examProcedureTUSS, List<String> fields) {
    if (examProcedureTUSS == null) {
      return notFound();
    }
    return ok(SerializeUtils.serializeObjectToJson(fields, examProcedureTUSS));
  }

}

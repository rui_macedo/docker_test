package controllers;

import static play.data.Form.form;

import java.util.Collections;
import java.util.List;

import models.InformativeTextType;

import com.pixeon.cloudsecure.services.CloudSecure;

import common.annotations.contenttype.ContentTypeInterceptor;
import common.utils.SerializeUtils;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class InformativeTextTypes extends Controller {

    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    @CloudSecure(serviceScope = "agd_Inftexttype_all")
    public static Result all(Long idInstitution,
                             String name,
                             List<String> fields) {
        List<InformativeTextType> informativetexttype = InformativeTextType.findAllByInstitution(idInstitution, name);
        return ok(SerializeUtils.serializeObjectToJson(fields, informativetexttype));
    }

    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    @CloudSecure(serviceScope = "agd_Inftexttype_all")
    public static Result byId(Long idInstitution, InformativeTextType informativetexttype, List<String> fields) {


        if (informativetexttype != null) {
            if (informativetexttype.institution.id.equals(idInstitution)) {
                return ok(SerializeUtils.serializeObjectToJson(fields, informativetexttype));
            } else {
                return ok(SerializeUtils.serializeObjectToJson(fields, "You can't access this data using this tenant"));
            }
        } else {
            return badRequest(Json.toJson("InformativeTextType doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_Inftexttype_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result save( Long idInstitution) {
        Form<InformativeTextType> form = form(InformativeTextType.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        }
        InformativeTextType informativetexttype = form.get();
        if (informativetexttype.institution.id.equals(idInstitution)) {
            informativetexttype.save();
            return ok(Json.toJson(informativetexttype));
        } else {
            return badRequest(Json.toJson("InformativeTextType doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_Inftexttype_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result update( Long idInstitution,  InformativeTextType informativetexttype) {
        if (informativetexttype != null) {
            if (informativetexttype.institution.id.equals(idInstitution)) {
                Form<InformativeTextType> form = form(InformativeTextType.class).bindFromRequest();
                if (form.hasErrors()) {
                    return badRequest(form.errorsAsJson());
                }
                InformativeTextType informativetexttypeUpdated = form.get();
                informativetexttypeUpdated.id = informativetexttype.id;
                informativetexttypeUpdated.version = informativetexttype.version;
                informativetexttypeUpdated.institution = informativetexttype.institution;
                informativetexttypeUpdated.update();
                List<String> fields = Collections.emptyList();
                return ok(SerializeUtils.serializeObjectToJson(fields, informativetexttypeUpdated));
            } else {
                return badRequest(Json.toJson("InformativeTextType doesn't exist or You can't access this data using this tenant!"));
            }
        } else {
            return badRequest(Json.toJson("InformativeTextType doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_Inftexttype_del")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result delete( Long idInstitution, InformativeTextType informativetexttype) {
        if (informativetexttype != null) {
            if (informativetexttype.institution.id.equals(idInstitution)) {
                informativetexttype.delete();
                return ok(Json.toJson("InformativeTextType Deleted"));
            } else {
                return badRequest(Json.toJson("You can't delete this data using this tenant"));
            }
        } else {
            return badRequest(Json.toJson("InformativeTextType doesn't exist or You can't access this data using this tenant!"));
        }
    }
}
 
 
 
 
 
 
 
 
 
 
 

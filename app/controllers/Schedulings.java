package controllers;

import com.google.inject.Inject;

import static java.util.Objects.isNull;

import java.util.List;
import java.util.Optional;

import dto.ResponseError;
import dto.SchedulingDTO;
import forms.FormScheduling;
import models.EmailMessage;
import models.ExamProcedure;

import models.Resource;
import models.scheduling.Scheduling;
import play.Logger;
import resource.SchedulingFullReservationResource;
import services.api.SchedulingService;
import services.external.api.EmployeeData;
import models.Unity;


import org.apache.commons.lang3.ObjectUtils;
import play.data.Form;
import play.db.ebean.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import scala.Option;

import com.avaje.ebean.Ebean;
import com.pixeon.cloudsecure.services.CloudSecure;
import com.pixeon.cloudsecure.services.CloudSecureSkip;

import common.annotations.contenttype.ContentTypeInterceptor;

public class Schedulings extends Controller {

  @Inject
  public static SchedulingService service;

  @Inject
  public static EmployeeData employeeService;


  @CloudSecure(serviceScope = "agd_search_slots")
  public static Result start(Long institutionId){

    final Long employeeId = employeeService.getCurrentEmployeeId();
    Ebean.beginTransaction();

    try{

      SchedulingDTO scheduling = service.startScheduling(institutionId, employeeId);
      Ebean.commitTransaction();
      return created(Json.toJson(scheduling));

    }catch (Exception ex){
      Ebean.rollbackTransaction();
      Logger.error("Error creating a Scheduling", ex);
      return status(SERVICE_UNAVAILABLE,
              Json.toJson(ResponseError.create("We are not able to process the request. Try again later",
                      ResponseError.Codes.SERVICE_UNAVAILABLE)));
    }
  }

  @CloudSecure(serviceScope = "agd_search_slots")
  public static Result updateScheduling(Long institutionId, Long schedulingId){

    if(isNull(request().body().asJson())){
      return badRequest(Json.toJson(ResponseError.create("Request payload is expected",
              ResponseError.Codes.BODY_CONTENT_PROBLEM)));
    }

    Form<FormScheduling> request = Form.form(FormScheduling.class).bindFromRequest();
    if(request.hasErrors()){
      return badRequest(Json.toJson(ResponseError.create(request.errorsAsJson(),
              ResponseError.Codes.BODY_CONTENT_PROBLEM)));
    }

    Scheduling scheduling = Scheduling.findById(schedulingId);
    if(isNull(scheduling)){
      return notFound(Json.toJson(ResponseError.create("Not found a scheduling with the given id",
              ResponseError.Codes.NOT_FOUND)));
    }

    if(ObjectUtils.notEqual(scheduling.institutionId, institutionId)){
      return forbidden(Json.toJson(ResponseError.create("The scheduling process with the given id" +
              " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
    }

    if(ObjectUtils.notEqual(scheduling.employeeId, employeeService.getCurrentEmployeeId())){
      return forbidden(Json.toJson(ResponseError.create("You are not the owner of this booking",
              ResponseError.Codes.NOT_OWNER)));
    }

    Ebean.beginTransaction();
    try{
      service.updateSchedulingOnlyInProgress(scheduling, request.get());
      Ebean.commitTransaction();
      return ok();
    }catch (Exception ex){
      Ebean.rollbackTransaction();
      Logger.error("Error creating a Scheduling", ex);
      return status(SERVICE_UNAVAILABLE,
              Json.toJson(ResponseError.create("We are not able to process the request. Try again later",
                      ResponseError.Codes.SERVICE_UNAVAILABLE)));
    }
  }


  @CloudSecure(serviceScope = "agd_search_slots")
  public static Result finishScheduling(Long institutionId, Long schedulingId){

    Scheduling scheduling = Scheduling.findById(schedulingId);
    if(isNull(scheduling)){
      return notFound(Json.toJson(ResponseError.create("Not found a Scheduling with the given id",
              ResponseError.Codes.NOT_FOUND)));
    }

    if(ObjectUtils.notEqual(scheduling.institutionId, institutionId)){
      return forbidden(Json.toJson(ResponseError.create("Scheduling  with the given id" +
              " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
    }

    if(ObjectUtils.notEqual(scheduling.employeeId, employeeService.getCurrentEmployeeId())){
      return forbidden(Json.toJson(ResponseError.create("You are not the employee responsable for this process",
              ResponseError.Codes.NOT_OWNER)));
    }

    Ebean.beginTransaction();
    try{
      service.finishScheduling(scheduling);
      Ebean.commitTransaction();
      return ok();
    }catch (Exception ex){
      Ebean.rollbackTransaction();
      Logger.error("Error creating a Scheduling", ex);
      return status(SERVICE_UNAVAILABLE,
              Json.toJson(ResponseError.create("We are not able to process the request. Try again later",
                      ResponseError.Codes.SERVICE_UNAVAILABLE)));
    }

  }

  @CloudSecure(serviceScope = "agd_search_slots")
  public static Result getScheduling(Long institutionId, Long schedulingId){

    Optional<SchedulingDTO> scheduling = service.findSchedulingByIdOptional(schedulingId);
    if(! scheduling.isPresent()){
      return notFound(Json.toJson(ResponseError.create("Not found a Scheduling with the given id",
              ResponseError.Codes.NOT_FOUND)));
    }

    SchedulingDTO dto = scheduling.get();

    if(ObjectUtils.notEqual(dto.institutionId, institutionId)){
      return forbidden(Json.toJson(ResponseError.create("The scheduling process with the given id" +
              " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
    }

    return ok(Json.toJson(dto));

  }

  @CloudSecure(serviceScope = "agd_search_slots")
  public static Result getSchedulingBooked(Long institutionId, Long schedulingId){

    Scheduling scheduling = Scheduling.findById(schedulingId);
    if(isNull(scheduling)){
      return notFound(Json.toJson(ResponseError.create("Not found a Scheduling with the given id",
              ResponseError.Codes.NOT_FOUND)));
    }

    if(ObjectUtils.notEqual(scheduling.institutionId, institutionId)){
      return forbidden(Json.toJson(ResponseError.create("Scheduling  with the given id" +
              " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
    }

    if(ObjectUtils.notEqual(scheduling.status, Scheduling.Status.BOOKED)){
      return badRequest(Json.toJson(ResponseError.create("This scheduling has not been booked yet." +
                      " Status " + scheduling.status,
              ResponseError.Codes.NOT_AVAILABLE)));
    }

    SchedulingFullReservationResource resource = service.mapSchedulingToFullReservationResource(scheduling);

    return ok(Json.toJson(resource));

  }

  @CloudSecure(serviceScope = "agd_search_slots")
  @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
  public static Result searchAvaibleSlots(Long idInstitution, Option<Unity> unity, Integer maxResults,
      Option<Long> sDate, Option<Long> eDate, List<String> weekDays,List<String> periods,  List<ExamProcedure> examprocedure) {

    return status(GONE, "The resource that you are trying to access is no longer used");

  }

  @CloudSecure(serviceScope = "agd_search_slots")
  @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
  public static Result searchGoodSetsOfExams(Long idInstitution, Option<Unity> unity, Integer maxResults,
                                                      Option<Long> sDate, Option<Long> eDate,  Long maxToleranceDuration,
                                                      List<String> weekDays, List<String> periods,
                                                      List<ExamProcedure> examprocedure) {
    return status(GONE, "The resource that you are trying to access is no longer used");
  }

  @CloudSecure(serviceScope = "agd_search_slots")
  @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
  public static Result searchDailyScheduledExamsByResource(Long idInstitution,Long lDate, String vtype,
      Resource resource) {
    return status(GONE, "The resource that you are trying to access is no longer used");
  }


  @Transactional
  @CloudSecure(serviceScope = "agd_save_slots")
  public static Result save(Long idInstitution) {
    return status(GONE, "The resource that you are trying to access is no longer used");
  }

  @CloudSecure(serviceScope = "agd_search_slots")
  public static Result sendEmail(Long institutionId, Long schedulingId) {
    if (isNull(request().body().asJson())) {
      return badRequest(Json.toJson(ResponseError.create("Request payload is expected",
              ResponseError.Codes.BODY_CONTENT_PROBLEM)));
    }
    Form<EmailMessage> request = Form.form(EmailMessage.class).bindFromRequest();
    if (request.hasErrors()) {
      return badRequest(Json.toJson(ResponseError.create(request.errorsAsJson(),
              ResponseError.Codes.BODY_CONTENT_PROBLEM)));
    }

    Scheduling scheduling = Scheduling.findById(schedulingId);
    if(isNull(scheduling)){
      return notFound(Json.toJson(ResponseError.create("Not found the given scheduling ",
              ResponseError.Codes.NOT_FOUND)));
    }

    EmailMessage emailMessage = request.get();
    service.sendVoucher(schedulingId, emailMessage.to, emailMessage.subject);

    return ok();
  }

}


package controllers;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.google.inject.Inject;
import com.pixeon.cloudsecure.services.CloudIntraServicesSecurity;
import com.pixeon.cloudsecure.services.CloudSecure;
import com.pixeon.cloudsecure.services.CloudSecureSkip;
import dto.ResponseError;
import org.apache.commons.lang3.ObjectUtils;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import queries.InsuranceQuery;

import common.utils.SerializeUtils;
import resource.InsuranceResource;
import services.api.InsuranceService;

public class Insurances extends Controller {

  @Inject
  private static InsuranceService service;

  @CloudSecure(serviceScope = "agd_insurancecarrier_all")
  public static Result all(Long institutionId, String name, String enabled, String paymentType, String hasPlans, List<String> fields) {

    List<InsuranceResource> resources = service.list(institutionId, new InsuranceQuery(enabled, paymentType, name, hasPlans));

    return ok(SerializeUtils.toJson(resources, fields));
  }

  @CloudIntraServicesSecurity
  public static Result wsAll(Long insitutionId, String name) {
    return all(insitutionId, name, null, null, null, Collections.emptyList());
  }


  @CloudSecure(serviceScope = "agd_insurancecarrier_all")
  public static Result byId(Long institutionId,  Long id,  List<String> fields) {

    Optional<InsuranceResource> insurance = service.findByIdOptional(id);

    if(! insurance.isPresent()){
      return notFound(Json.toJson(ResponseError.create("Not found Insurance with the given id",
              ResponseError.Codes.NOT_FOUND)));
    }

    if(ObjectUtils.notEqual(insurance.get().institutionId, institutionId)){
      return forbidden(Json.toJson(ResponseError.create("Insurance with the given id" +
              " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
    }

    return ok(SerializeUtils.toJson(insurance.get(), fields));

  }

}

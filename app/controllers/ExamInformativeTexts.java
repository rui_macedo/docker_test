package controllers;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.List;

import models.ExamInformativeText;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import com.pixeon.cloudsecure.services.CloudSecure;
import common.annotations.contenttype.ContentTypeInterceptor;
import common.utils.SerializeUtils;

public class ExamInformativeTexts extends Controller {

  @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
  @CloudSecure(serviceScope = "exminf_all")
  public static Result all( Long idInstitution, Long idInsuranceCarrier, Long idInsuranceCarrierPlan,
                            List<Long> examProcedures, List<String> fields) {

    List<ExamInformativeText> examInformativeTexts = ExamInformativeText.findAllByInstitution(idInstitution, idInsuranceCarrier, idInsuranceCarrierPlan, examProcedures);

    ArrayList<String> field = new ArrayList<>();
    field.add("informativeText.content");

    return ok(SerializeUtils.serializeObjectToJson(field, examInformativeTexts));
  }

  @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
  @CloudSecure(serviceScope = "exminf_all")
  public static Result byId(Long idInstitution,  ExamInformativeText examinformativetext,  List<String> fields) {

    if (examinformativetext == null) {
      return badRequest(Json.toJson("ExamInformativeText doesn't exist or You can't access this data using this tenant!"));
    }

    if (!examinformativetext.institution.id.equals(idInstitution)) {
      return ok(SerializeUtils.serializeObjectToJson(fields, "You can't access this data using this tenant"));
    }
    return ok(SerializeUtils.serializeObjectToJson(fields, examinformativetext));

  }

  @CloudSecure(serviceScope = "exminf_sv")
  @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
  public static Result save(Long idInstitution) {

    Form<ExamInformativeText> form = form(ExamInformativeText.class).bindFromRequest();
    if (form.hasErrors()) {
      return badRequest(form.errorsAsJson());
    }
    ExamInformativeText examinformativetext = form.get();
    if (examinformativetext.institution.id.equals(idInstitution)) {
      examinformativetext.save();
      return ok(Json.toJson(examinformativetext));
    } else {
      return badRequest(Json.toJson("ExamInformativeText doesn't exist or You can't access this data using this tenant!"));
    }
  }


  @CloudSecure(serviceScope = "exminf_sv")
  @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
  public static Result update( Long idInstitution, ExamInformativeText examinformativetext) {
    if (examinformativetext == null) {
      return badRequest(Json.toJson("ExamInformativeText doesn't exist or You can't access this data using this tenant!"));
    }
    if (!examinformativetext.institution.id.equals(idInstitution)) {
      return badRequest(Json.toJson("ExamInformativeText doesn't exist or You can't access this data using this tenant!"));
    }

    Form<ExamInformativeText> form = form(ExamInformativeText.class).bindFromRequest();
    if (form.hasErrors()) {
      return badRequest(form.errorsAsJson());
    }
    ExamInformativeText examInformativeTextUpdated = form.get();
    examInformativeTextUpdated.id = examinformativetext.id;
    examInformativeTextUpdated.version = examinformativetext.version;
    examInformativeTextUpdated.institution = examinformativetext.institution;
    examInformativeTextUpdated.update();

    return ok(Json.toJson(examInformativeTextUpdated));
  }


  @CloudSecure(serviceScope = "exminf_del")
  @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
  public static Result delete(Long idInstitution, ExamInformativeText examInformativeText) {
    if (examInformativeText == null) {
      return badRequest(Json.toJson("ExamInformativeText doesn't exist or You can't access this data using this tenant!"));
    }
    if (!examInformativeText.institution.id.equals(idInstitution)) {
      return badRequest(Json.toJson("You can't delete this data using this tenant"));
    }
    examInformativeText.delete();
    return ok(Json.toJson("ExamInformativeText Deleted"));

  }
}

package controllers;

import static play.data.Form.form;

import java.util.Collections;
import java.util.List;

import models.ProcedureTable;

import com.pixeon.cloudsecure.services.CloudSecure;

import common.annotations.contenttype.ContentTypeInterceptor;
import common.utils.SerializeUtils;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class ProcedureTables extends Controller {

    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    @CloudSecure(serviceScope = "agd_proceduretable_all")
    public static Result all(Long idInstitution, List<String> fields) {
        List<ProcedureTable> proceduretable = ProcedureTable.findAllByInstitution(idInstitution);
        return ok(SerializeUtils.serializeObjectToJson(fields, proceduretable));
    }

    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    @CloudSecure(serviceScope = "agd_proceduretable_all")
    public static Result byId(Long idInstitution, ProcedureTable proceduretable, List<String> fields) {


        if (proceduretable != null) {
            if (proceduretable.institution.id.equals(idInstitution)) {
                return ok(SerializeUtils.serializeObjectToJson(fields, proceduretable));
            } else {
                return ok(SerializeUtils.serializeObjectToJson(fields, "You can't access this data using this tenant"));
            }
        } else {
            return badRequest(Json.toJson("ProcedureTable doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_proceduretable_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result save( Long idInstitution) {
        Form<ProcedureTable> form = form(ProcedureTable.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        }
        ProcedureTable proceduretable = form.get();
        if (proceduretable.institution.id.equals(idInstitution)) {
            proceduretable.save();
            return ok(Json.toJson(proceduretable));
        } else {
            return badRequest(Json.toJson("ProcedureTable doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_proceduretable_sv")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result update(Long idInstitution, ProcedureTable proceduretable) {
        if (proceduretable != null) {
            if (proceduretable.institution.id.equals(idInstitution)) {
                Form<ProcedureTable> form = form(ProcedureTable.class).bindFromRequest();
                if (form.hasErrors()) {
                    return badRequest(form.errorsAsJson());
                }
                ProcedureTable proceduretableUpdated = form.get();
                proceduretableUpdated.id = proceduretable.id;
                proceduretableUpdated.version = proceduretable.version;
                proceduretableUpdated.institution = proceduretable.institution;
                proceduretableUpdated.update();
                List<String> fields = Collections.emptyList();
                return ok(SerializeUtils.serializeObjectToJson(fields, proceduretableUpdated));
            } else {
                return badRequest(Json.toJson("ProcedureTable doesn't exist or You can't access this data using this tenant!"));
            }
        } else {
            return badRequest(Json.toJson("ProcedureTable doesn't exist or You can't access this data using this tenant!"));
        }
    }

    @CloudSecure(serviceScope = "agd_proceduretable_del")
    @ContentTypeInterceptor(mimeType = Http.MimeTypes.JSON)
    public static Result delete( Long idInstitution, ProcedureTable proceduretable) {
        if (proceduretable != null) {
            if (proceduretable.institution.id.equals(idInstitution)) {
                proceduretable.delete();
                return ok(Json.toJson("ProcedureTable Deleted"));
            } else {
                return badRequest(Json.toJson("You can't delete this data using this tenant"));
            }
        } else {
            return badRequest(Json.toJson("ProcedureTable doesn't exist or You can't access this data using this tenant!"));
        }
    }
}
 
 
 
 
 
 
 

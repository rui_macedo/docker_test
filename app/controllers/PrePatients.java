package controllers;

import com.avaje.ebean.Ebean;
import com.google.inject.Inject;

import com.pixeon.cloudsecure.services.CloudSecure;
import com.pixeon.cloudsecure.services.CloudSecureSkip;
import common.utils.SerializeUtils;
import dto.PrePatientDTO;
import dto.ResponseError;
import forms.FormPrePatient;
import org.apache.commons.lang3.ObjectUtils;
import play.Logger;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.api.PatientService;

import java.util.List;
import java.util.Optional;

public class PrePatients extends Controller {

    @Inject
    private static PatientService service;

    @CloudSecure(serviceScope = "agd_contpac_all")
    public static Result all(Long institutionId, List<String> fields){
        List<PrePatientDTO> list = service.getAllPreRegisters(institutionId);
        return ok(SerializeUtils.toJson(list, fields));
    }

    @CloudSecure(serviceScope = "agd_contpac_all")
    public static Result byId(Long institutionId, Long preRegisterId, List<String> fields){

        Optional<PrePatientDTO> contact = service.findPreRegisterByIdOptional(preRegisterId);

        if(! contact.isPresent()){
            return notFound(Json.toJson(ResponseError.create("Not found a PatientPreRegister with the given id",
                    ResponseError.Codes.NOT_FOUND)));
        }

        if(ObjectUtils.notEqual(contact.get().institutionId, institutionId)){
            return forbidden(Json.toJson(ResponseError.create("PatientPreRegister with the given id" +
                    " belongs to another tenant", ResponseError.Codes.CROSS_TENANT)));
        }
        return ok(SerializeUtils.toJson(contact.get(), fields));
    }


    @CloudSecure(serviceScope = "agd_contpac_sv")
    public static Result save(Long institutionId){

        Form<FormPrePatient> form = Form.form(FormPrePatient.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(Json.toJson(ResponseError.create(form.errorsAsJson(),
                    ResponseError.Codes.BODY_CONTENT_PROBLEM)));
        }

        Ebean.beginTransaction();
        try{
            PrePatientDTO dto = service.savePreRegister(institutionId, form.get());
            Ebean.commitTransaction();
            return created(Json.toJson(dto));

        }catch (Exception ex){
            Logger.error("Error creating a pre-patient. Request = {}", form.get().toString(), ex);
            return status(SERVICE_UNAVAILABLE, Json.toJson(ResponseError.create("We are not able to " +
                    "process the request", ResponseError.Codes.SERVICE_UNAVAILABLE)) );
        }finally {
            Ebean.endTransaction();
        }
    }

}

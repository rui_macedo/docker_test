package controllers;

import static play.data.Form.form;

import java.util.List;
import java.util.stream.Collectors;

import dto.SchedulingBookDTO;
import models.Institution;
import models.SchedulingBook;
import play.data.Form;
import play.db.ebean.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import com.pixeon.cloudsecure.services.CloudSecure;

import common.constants.SchedulingBookSlotStatus;
import common.utils.SerializeUtils;
import dto.SchedulingBookDetailsDTO;

public class SchedulingBooks extends Controller {

  @CloudSecure(serviceScope = "agd_list_schedulingbook")
  public static Result findAll(Long idInstitution, List<String> fields) {
    List<SchedulingBookDTO> result = SchedulingBook.findByTenant(idInstitution)
            .parallelStream()
            .map(book -> book.wrap())
            .collect(Collectors.toList());
    return ok(SerializeUtils.toJson(result, fields));
  }

  @CloudSecure(serviceScope = "agd_list_schedulingbook")
  public static Result byId( SchedulingBook schedulingBook,
      Long idInstitution,
       List<String> fields) {
    if (schedulingBook == null) {
      return notFound();
    }
    if (!schedulingBook.institution.id.equals(idInstitution)) {
      return unauthorized();
    }
    return ok(SerializeUtils.serializeObjectToJson(fields, schedulingBook.wrapDetails()));
  }

  @CloudSecure(serviceScope = "agd_create_schedulingbook")
  @Transactional
  public static Result save(Long tenant) {
    Form<SchedulingBookDetailsDTO> filledForm = form(SchedulingBookDetailsDTO.class).bindFromRequest();
    if (filledForm.hasErrors()) {
      return badRequest(filledForm.errorsAsJson());
    }

    SchedulingBookDetailsDTO dto = filledForm.get();
    SchedulingBook book = new SchedulingBook();
    book.isDeleted = false;
    book.statusSlots = SchedulingBookSlotStatus.PENDING;
    book.institution = Institution.byId(tenant);
    book.unwrap(dto, tenant);
    book.save();
    return created(Json.toJson(book.wrapDetails()));
  }

  @CloudSecure(serviceScope = "agd_update_schedulingbook")
  @Transactional
  public static Result update(SchedulingBook schedulingBook, Long tenant) {
    Form<SchedulingBookDetailsDTO> filledForm = form(SchedulingBookDetailsDTO.class).bindFromRequest();
    if (filledForm.hasErrors()) {
      return badRequest(filledForm.errorsAsJson());
    }

    if (schedulingBook == null) {
      return badRequest();
    }
    if (!schedulingBook.institution.id.equals(tenant)) {
      return unauthorized();
    }

    SchedulingBookDetailsDTO newBook = filledForm.get();
    SchedulingBook book = new SchedulingBook();
    book.unwrap(newBook, tenant);
    book.id = schedulingBook.id;
    book.institution = Institution.byId(tenant);
    book.version = schedulingBook.version;
    book.update();
    return ok(Json.toJson(SchedulingBook.findById(book.getId()).wrapDetails()));
  }

  @CloudSecure(serviceScope = "agd_search_slots")
  public static Result delete(SchedulingBook schedulingBook, Long tenant) {
    if (schedulingBook == null) {
      return badRequest("SchedulingBook  not found");
    }
    if (!schedulingBook.institution.id.equals(tenant)) {
      return unauthorized();
    }
    schedulingBook.delete();
    return ok();
  }

}

package queries.slots.binder;

import org.joda.time.DateTime;

import queries.slots.PeriodQuery;

public class PeriodExactDayBinder implements PeriodBinder {

    @Override
    public PeriodQuery bind(int year, int month, int day) {

        DateTime startAt = new DateTime()
                .withMonthOfYear(month)
                .withYear(year)
                .withDayOfMonth(day)
                .withTimeAtStartOfDay();

        DateTime  endAt = new DateTime()
                .withMonthOfYear(month)
                .withYear(year)
                .withDayOfMonth(day)
                .withTime(23, 59, 59, 999);

        return new PeriodQuery(startAt, endAt);

    }
}


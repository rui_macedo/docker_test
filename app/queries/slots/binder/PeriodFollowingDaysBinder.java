package queries.slots.binder;

import org.joda.time.DateTime;

import queries.slots.PeriodQuery;

public class PeriodFollowingDaysBinder implements PeriodBinder {

    @Override
    public PeriodQuery bind(int year, int month, int day) {

        DateTime startAt = new DateTime();
        DateTime endAt = new DateTime().plusDays(day);

        return new PeriodQuery(startAt, endAt);
    }

}

package queries.slots.binder;

import com.google.common.base.Joiner;

import queries.slots.SlotAvailableQuery.PeriodRangeEnum ;

public class PeriodBinderFactory {

    public static PeriodBinder getInstance(PeriodRangeEnum  range){

        if(range.equals(PeriodRangeEnum.BEGINNING_OF_MONTH)){
            return new PeriodBeginningOfMonthBinder();
        }

        if(range.equals(PeriodRangeEnum.ENDING_OF_MONTH)){
            return new PeriodEndingOfMonthBinder();
        }

        if(range.equals(PeriodRangeEnum.ANY_DAY_OF_MONTH)){
            return new PeriodAnyDayOfMonthBinder();
        }

        if(range.equals(PeriodRangeEnum.EXACT_DAY_OF_MONTH)){
            return new PeriodExactDayBinder();
        }

        if(range.equals(PeriodRangeEnum.FOLLOW_DAYS)){
            return new PeriodFollowingDaysBinder();
        }

        throw new IllegalArgumentException("We only support period such as " +
                Joiner.on(",").join(PeriodRangeEnum.values()));
    }

}

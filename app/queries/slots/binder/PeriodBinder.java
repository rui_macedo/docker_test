package queries.slots.binder;

import queries.slots.PeriodQuery;

public interface PeriodBinder {

    public PeriodQuery bind(int year, int month, int day);

}

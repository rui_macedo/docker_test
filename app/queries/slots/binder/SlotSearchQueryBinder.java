package queries.slots.binder;

import com.fasterxml.jackson.databind.JsonNode;
import com.pixeon.utils.common.constants.CalendarDay;

import common.constants.DayShift;

import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;

import java.util.Map;

import play.Play;
import queries.slots.PeriodQuery;
import queries.slots.SlotAvailableQuery;
import queries.slots.SlotAvailableQuery.PeriodRangeEnum;
import utils.ConverterUtils;

public class SlotSearchQueryBinder {

    public SlotAvailableQuery query;

    private final int FOLLOWING_DAYS = Play.application()
            .configuration()
            .getInt("slots.search.query.days", 3);

    public SlotSearchQueryBinder() {
        this.query = new SlotAvailableQuery();
    }

    public SlotAvailableQuery bindFromQueryString(Long institutionId, Map<String, String[]> params) {

        query.institutionId = institutionId;

        if (params.containsKey("offset")) {
            query.offset = Integer.parseInt(params.get("offset")[NumberUtils.INTEGER_ZERO]);
        }

        if (params.containsKey("limit")) {
            query.limit = Integer.parseInt(params.get("limit")[NumberUtils.INTEGER_ZERO]);
        }

        if (params.containsKey("insurance")) {
            query.insurance = Long.valueOf(params.get("insurance")[NumberUtils.INTEGER_ZERO]);
        }

        if (params.containsKey("insurancePlan")) {
            query.insurancePlan = Long.valueOf(params.get("insurancePlan")[NumberUtils.INTEGER_ZERO]);
        }

        if (params.containsKey("examProcedures")) {
            query.examProcedures = ConverterUtils.fromStringArrayToLongList(params.get("examProcedures"));
        }

        if (params.containsKey("weekDays")) {
            query.weekDays = ConverterUtils.fromStringArrayToEnums(params.get("weekDays"), CalendarDay.class);
        }

        if (params.containsKey("dayShifts")) {
            query.shifts = ConverterUtils.fromStringArrayToEnums(params.get("dayShifts"), DayShift.class);
        }

        if(params.containsKey("referenceDate")){

            final int year = params.containsKey("year") ?
                    Integer.valueOf(params.get("year")[0]).intValue() : NumberUtils.INTEGER_ONE.intValue();
            final int month = params.containsKey("month") ?
                    Integer.valueOf(params.get("month")[0]).intValue() : NumberUtils.INTEGER_ONE.intValue();
            final int day = params.containsKey("day") ? Integer.valueOf(params.get("day")[0]).intValue() :
                    NumberUtils.INTEGER_ONE.intValue();
            PeriodRangeEnum range = PeriodRangeEnum.valueOf(params.get("referenceDate")[0]);
            query.period = PeriodBinderFactory.getInstance(range).bind(year, month, day);

        } else {

            //If they didnt choice a reference date we are setting from now until the following next 3 days;
            query.period = new PeriodQuery(new DateTime().plusMinutes(15),
                    new DateTime().plusDays(FOLLOWING_DAYS).withTimeAtStartOfDay()) ;
        }

        return query;
    }

    public SlotAvailableQuery bindFromRequest(Long institutionId, JsonNode request){
        //TODO if we have to change request's parameters from queryString to a payload for any reason
        return query;
    }

}


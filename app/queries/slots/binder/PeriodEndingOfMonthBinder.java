package queries.slots.binder;

import common.constants.DaysOfMonth;

import org.joda.time.DateTime;

import queries.slots.PeriodQuery;
import utils.DateUtils;

public class PeriodEndingOfMonthBinder implements PeriodBinder{

    @Override
    public PeriodQuery bind(int year, int month, int day) {

        final int firstDayOfEndingOfMonth = DateUtils.getDayOfEndingMonth(year, month);

        DateTime startAt = new DateTime()
                .withMonthOfYear(month)
                .withYear(year)
                .withDayOfMonth(firstDayOfEndingOfMonth)
                .withTimeAtStartOfDay();

        //until what supposed to be the last day of beginning of month
        DateTime  endAt = new DateTime()
                .withMonthOfYear(month)
                .withYear(year)
                .withDayOfMonth(DaysOfMonth.LAST_DAY_OF_ENDING_OF_MONTH)
                .withTime(23, 59, 59, 999);

        return new PeriodQuery(startAt, endAt);

    }
}

package queries.slots;

import org.joda.time.DateTime;

public class PeriodQuery {

    public DateTime startAt;
    public DateTime endAt;

    public PeriodQuery(DateTime startAt, DateTime endAt) {
        this.startAt = startAt;
        this.endAt = endAt;
    }

    @Override
    public String toString() {
        return "PeriodQuery{" +
                "startAt=" + startAt.getMillis() +
                ", endAt=" + endAt.getMillis() +
                '}';
    }
}

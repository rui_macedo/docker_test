package queries.slots;

import com.pixeon.utils.common.constants.CalendarDay;

import common.constants.DayShift;

import java.util.List;

public class SlotAvailableQuery {

    public enum PeriodRangeEnum{
        BEGINNING_OF_MONTH, //We are taking that beginning of month goes till 15ht
        ENDING_OF_MONTH, //We are taking that ending of month starts at 16th
        EXACT_DAY_OF_MONTH,
        ANY_DAY_OF_MONTH,
        FOLLOW_DAYS;
    }

    public Integer offset;
    public Integer limit;
    public Long institutionId;
    public Long insurance;
    public Long insurancePlan;
    public List<Long> examProcedures;
    public List<CalendarDay> weekDays;
    public List<DayShift> shifts;
    public PeriodQuery period;

    @Override
    public String toString() {
        return "SlotAvailableQuery{" +
                "period=" + period +
                ", examProcedures=" + examProcedures +
                ", insurancePlan=" + insurancePlan +
                ", insurance=" + insurance +
                ", institutionId=" + institutionId +
                '}';
    }
}

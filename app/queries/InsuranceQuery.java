package queries;

import common.constants.InsurancePaymentType;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

public class InsuranceQuery {

    public Long insuranceId;
    public Boolean enabled;
    public InsurancePaymentType paymentType;
    public String name;
    public Boolean hasPlans;

    public Long institutionId;
    public InsuranceQuery(String enabled, String paymentType, String name, Long institutionId, Long insuranceId) {
        this.enabled = StringUtils.isNotEmpty(enabled) ? BooleanUtils.toBoolean(enabled) : true;
        this.institutionId = institutionId;
        this.paymentType = StringUtils.isNotEmpty(paymentType) ? InsurancePaymentType.valueOf(paymentType.toUpperCase()) : null;
        this.name = name;
        this.insuranceId = insuranceId;
    }

    public InsuranceQuery(String enabled, String paymentType, String name, String hasPlans) {
        //We are taking enable as default;
        this.enabled = StringUtils.isNotEmpty(enabled) ? BooleanUtils.toBoolean(enabled) : true;
        this.hasPlans = StringUtils.isNotEmpty(hasPlans) ? BooleanUtils.toBoolean(hasPlans) : null;
    }
    public InsuranceQuery(String enabled, String paymentType, String name, Long idInstitution) {
        this.enabled = StringUtils.isNotEmpty(enabled) ? BooleanUtils.toBoolean(enabled) : true;
        this.institutionId = idInstitution;
        this.paymentType = StringUtils.isNotEmpty(paymentType) ? InsurancePaymentType.valueOf(paymentType.toUpperCase()) : null;
        this.name = name;
    }

    public InsuranceQuery(String enabled, String name, Long institutionId) {
        this.enabled = StringUtils.isNotEmpty(enabled) ? BooleanUtils.toBoolean(enabled) : true;
        this.institutionId = institutionId;
        this.name = name;
    }
}

package queries;

public class ContactAllQuery {

    public Long institutionId;
    public String phone;
    public String name;

    public ContactAllQuery(Long institutionId, String phone, String name) {
        this.institutionId = institutionId;
        this.phone = phone;
        this.name = name;
    }
}

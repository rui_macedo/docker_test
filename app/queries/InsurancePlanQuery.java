package queries;

public class InsurancePlanQuery {

    public Long insurancePlanId;
    public Long insuranceId;
    public Long institutionId;

    public InsurancePlanQuery(Long insurancePlanId, Long institutionId, Long insuranceId) {
        this.insurancePlanId = insurancePlanId;
        this.institutionId = institutionId;
        this.insuranceId = insuranceId;
    }

    public InsurancePlanQuery(Long institutionId, Long insuranceId) {
        this.institutionId = institutionId;
        this.insuranceId = insuranceId;
    }
}

package validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import models.Error;
import flexjson.JSONSerializer;

public final class Validators {

  public static List<Error> getValidationErrors(Set<? extends ConstraintViolation<?>> validate) {
    List<Error> errors = new ArrayList<>();
    for (ConstraintViolation<?> violation : validate) {
      errors.add(Error.create(violation.getMessage(), violation.getPropertyPath().toString()));
    }
    return errors;
  }

  public static String getValidationResponse(Set<? extends ConstraintViolation<?>> validate) {
    return new JSONSerializer().rootName("errors").exclude("class").serialize(getValidationErrors(validate));
  }
}

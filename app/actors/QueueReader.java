package actors;

import java.util.List;

import play.Logger;
import play.libs.Json;
import services.api.SlotService;
import services.external.api.QueueService;
import akka.actor.UntypedActor;

import com.amazonaws.services.sqs.model.Message;
import com.google.inject.Inject;

import forms.FormMessageSlot;

public class QueueReader extends UntypedActor {

  @Inject
  private static QueueService<Message> sqs;

  @Inject
  private static SlotService service;

  @Override
  public void onReceive(Object receivedMessage) throws Exception {

    if (receivedMessage == null || !(receivedMessage instanceof String)) {
      Logger.error("Unrecognized message: " + receivedMessage);
      return;
    }
    String textualMessage = (String) receivedMessage;
    if (textualMessage.equals("readQueue")) {
      Logger.debug("Reading queue: " + textualMessage);
      readQueue();
      return;
    }
    Logger.error("Unrecognized textual message: " + receivedMessage);
  }

  private void readQueue() {
    List<Message> messagesToProcess = sqs.receive();
    for (Message m : messagesToProcess) {
      Logger.debug("Received: " + m.getBody() + " " + m.getMessageId());
      delegateWork(m);
    }
  }

  private void delegateWork(final Message message) {
    service.generate(Json.fromJson(Json.parse(message.getBody()), FormMessageSlot.class));
    deleteMessage(message);
  }

  public void deleteMessage(final Message message) {
    Logger.debug("Deleting well processed message: ");
    sqs.delete(message);
  }

}

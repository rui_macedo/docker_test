package common.annotations.validations;

import java.lang.reflect.Field;

import javax.validation.ConstraintValidator;

import models.BaseSimpleModel;

import org.apache.commons.lang3.StringUtils;

import play.Logger;
import play.data.validation.Constraints;
import play.libs.F;
import play.libs.F.Tuple;

public class ModelFieldBooleanCheckImpl extends Constraints.Validator<BaseSimpleModel> implements ConstraintValidator<ModelFieldBooleanCheck, BaseSimpleModel> {

  private String messageKey;
  private boolean valueToCheck; 
  private String fieldName;
  
  @Override
  public void initialize(ModelFieldBooleanCheck arg0) {
    if(StringUtils.isNotBlank(arg0.fieldName())){
      fieldName = arg0.fieldName();
      valueToCheck = arg0.valueToCheck();
    }else{
      throw new IllegalArgumentException("You need to declare fieldName and validValue on ModelFieldBooleanCheck");
    }
  }

  @Override
  public Tuple<String, Object[]> getErrorMessageKey() {
    Object[] objs = {fieldName, valueToCheck};
    return new F.Tuple<String, Object[]>(messageKey, objs);
  }

  @Override
  public boolean isValid(BaseSimpleModel arg0) {
    boolean validate = false;
    //test only if the BaseSimpleModel field is valid, not if is required!
    if(arg0 != null){
      try {
        Field field = arg0.getClass().getField(fieldName);
        Object objValue = field.get(arg0);
        //test only if the BaseSimpleModel field is valid, not if is required!
        if(objValue != null){
          if(Boolean.class.isAssignableFrom(objValue.getClass())){
            Boolean bvalue = (Boolean) objValue;
            if(bvalue != null){
              return valueToCheck == bvalue;
            }
          }else{
            Logger.error("fieldName " + fieldName + " declared on ModelFieldBooleanCheck at " + arg0.getClass().getSimpleName() + " is not boolean!!" );
            throw new IllegalArgumentException("You need to declare fieldName of a boolean value");
          }
        }else{
          validate = true;//field value is empty
        }
      } catch (IllegalArgumentException e) {
        Logger.error("fieldName " + fieldName + " declared on ModelFieldBooleanCheck exists on " + arg0.getClass().getSimpleName() + "??",e );
      } catch (IllegalAccessException e) {
        Logger.error("fieldName " + fieldName + " declared on ModelFieldBooleanCheck is public on " + arg0.getClass().getSimpleName() + "??",e );
      } catch (NoSuchFieldException e) {
        Logger.error("fieldName " + fieldName + " declared on ModelFieldBooleanCheck exists ont " + arg0.getClass().getSimpleName() + "??", e );
      e.printStackTrace();
      } catch (SecurityException e) {
        Logger.error("fieldName " + fieldName + " declared on ModelFieldBooleanCheck is public on " + arg0.getClass().getSimpleName() + "??",e );
      }
    }else{
      validate = true;//field is empty
    }
    return validate;
  }


}

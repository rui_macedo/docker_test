package common.annotations.validations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = ModelFieldBooleanCheckImpl.class)
@Target( {ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ModelFieldBooleanCheck {
  String message() default "error.booleanfield.invalid.value";
  String fieldName();
  boolean valueToCheck() default true;
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
}

package common.annotations;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.Locale;

import javax.persistence.PersistenceException;

import models.BaseSimpleModel;

import org.apache.commons.lang3.StringUtils;

import play.Logger;
import play.data.format.Formatters;

import com.avaje.ebean.Ebean;

public class BaseSimpleModelFormatterImpl extends
    Formatters.AnnotationFormatter<BaseSimpleModelFormatter, BaseSimpleModel> {

  @Override
  public BaseSimpleModel parse(BaseSimpleModelFormatter annotation, String value, Locale locale)
      throws ParseException {
    //does not require the field to be mandatory
    if(StringUtils.isEmpty(value)){
      return null;
    }
    try {
      BaseSimpleModel obj = null;
      if (annotation.idFieldIsString()) {
        obj = Ebean.find(annotation.clazz()).where().eq(annotation.idFieldName(), value).findUnique();
      } else {
          Number id = new Long(value);
          obj = Ebean.find(annotation.clazz()).where().eq(annotation.idFieldName(), id).findUnique();
      }
      if(obj == null){
        throw new ParseException("Can't found " + annotation.clazz().getSimpleName() + " with " + annotation.idFieldName() + " value: "
          + value + ". Entity not found in database", 1);
      }else{
        return annotation.clazz().cast(obj);
      }
    }catch (NumberFormatException ex) {
      throw new ParseException(" Can't parse idFieldName to number into " + annotation.idFieldName()
          + " annotation. Are this idField String? You can set this with idFieldIsString=true", 1); 
    } catch (ClassCastException e) {
      throw new ParseException("Can't parse " + annotation.clazz() + " with " + annotation.idFieldName() + " value: "
          + value + ". Are this field the same class of clazz annotation atribute?", 1); 
    }catch(PersistenceException e){
      Logger.error("Error trying to execute parse",e);
      throw new ParseException("Can't parse " + annotation.clazz() + " with " + annotation.idFieldName() + " value: "
          + value + ". This field are key field or return more than one" + annotation.clazz().getSimpleName() + "?", 1); 
    }
  }

  @Override
  public String print(BaseSimpleModelFormatter annotation, BaseSimpleModel object, Locale locale) {
    try {
      Field field = object.getClass().getField(annotation.idFieldName());
      return field.get(object).toString();
    } catch (IllegalArgumentException e) {
      Logger.error("Can't print " + object.getClass() + " using " + annotation.idFieldName()
          + " value. Is this field public and valid?", e);
    } catch (IllegalAccessException e) {
      Logger.error("Can't print " + object.getClass() + " using " + annotation.idFieldName()
          + " value. Is this field public and valid?", e);
    } catch (NoSuchFieldException e) {
      Logger.error("Can't print " + object.getClass() + " using " + annotation.idFieldName()
          + " value. Is this field public and valid?", e);
    } catch (SecurityException e) {
      Logger.error("Can't print " + object.getClass() + " using " + annotation.idFieldName()
          + " value. Is this field public and valid?", e);
    }
    return "";
  }
}

package common.annotations.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;

public class TimestampDateSerializer extends JsonSerializer<DateTime> {
  // unlike java regular formatters this one is thread-safe
  private static final DateTimeFormatter ISO8601_FORMATTER =
      DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z");
  
  @Override
  public void serialize(DateTime value, JsonGenerator generator, SerializerProvider provider)
      throws IOException, JsonProcessingException {
    generator.writeString(ISO8601_FORMATTER.print(value));
  }

}

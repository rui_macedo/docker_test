package common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import models.BaseTenantModel;
import play.data.Form;

/**
 * @author abner.rolim Parse a html form key field value, if is not null, into a existing
 *         BaseTenantModel representation. This parser always will search in the current tenant. The
 *         key field needs to be unique and the clazz needs to be the same type of field key field
 *         needs to be public throws parse exception if find more than one or none, considering the
 *         current tenant
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Form.Display(name = "error.bind.object", attributes = {"clazz", "keyField"})
public @interface BaseTenantModelFormatter {
  Class<? extends BaseTenantModel> clazz();

  String idFieldName();

  boolean idFieldIsString();
}

package common.annotations.contenttype;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import play.mvc.Http.Request;
import play.mvc.With;

/**
 * @author abner.rolim
 *Filter request asking by {@link Request#accepts(String mimeType)} and rejects with 415 Unsupported Media Type if false
 *Use on methods that don't implement content negotiation
 *Sets the response to the given mimetype to.
 */
@With(ContentTypeInterceptorImpl.class)
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ContentTypeInterceptor {
  String mimeType();
}

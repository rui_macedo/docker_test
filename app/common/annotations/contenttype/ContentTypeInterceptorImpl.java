package common.annotations.contenttype;


import play.libs.F;
import play.libs.F.Promise;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Result;


public class ContentTypeInterceptorImpl extends Action<ContentTypeInterceptor> {


  @Override
  public Promise<Result> call(Context context) throws Throwable {
    if (context.request().accepts(configuration.mimeType())) {
      context.response().setContentType(configuration.mimeType());
      return delegate.call(context);
    } else {
      return F.Promise.pure((Result) status(415,
          "This method just respond on " + configuration.mimeType()
              + ". Check your Accept http header on your request"));
    }
  }
}

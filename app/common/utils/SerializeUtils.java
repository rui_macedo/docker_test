package common.utils;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import play.libs.Json;
import flexjson.JSONSerializer;

public class SerializeUtils {

  public static String serializeObjectToJson(List<String> fields, Object t){
    if(fields == null || fields.isEmpty()){
      return Json.toJson(t).toString();
    }
    return(SerializeUtils.getJSONSerializerForFields(fields).serialize(t));
  }

  public static JsonNode toJson(Object t, List<String> fields){

    if(CollectionUtils.isEmpty(fields)){
      return Json.toJson(t);
    }

    JSONSerializer serializer = SerializeUtils.getJSONSerializerForFields(fields);
    return Json.parse(serializer.serialize(t));
  }

  private static JSONSerializer getJSONSerializerForFields (List<String> fields) {
    JSONSerializer serializer = new JSONSerializer();
    serializer.exclude("password");
    serializer.exclude("version");
    serializer.exclude("class");
    serializer.exclude("formCleanPassword");
    for (String field : fields) {
      serializer.include(field);
    }
    serializer.exclude("*");
    return serializer;

  }

}

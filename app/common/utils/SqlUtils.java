package common.utils;

import play.Logger;

import com.avaje.ebean.CallableSql;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.config.ServerConfig;
import com.avaje.ebean.config.dbplatform.H2Platform;
import com.avaje.ebeaninternal.api.SpiEbeanServer;
import com.avaje.ebeaninternal.server.ddl.DdlGenerator;

import java.util.Arrays;
import java.util.List;

public class SqlUtils {

  public static EbeanServer server;
  public static DdlGenerator ddl;

  public static void createDDLToMemoryDB() {
    server = Ebean.getServer("default");
    ServerConfig config = new ServerConfig();

    config.setDdlGenerate(true);
    config.setDdlRun(true);
    ddl = new DdlGenerator();
    ddl.setup((SpiEbeanServer) server, new H2Platform(), config);
    Ebean.execute(Ebean.createCallableSql("CREATE SCHEMA IF NOT EXISTS resources"));
    Ebean.execute(Ebean.createCallableSql("CREATE SCHEMA IF NOT EXISTS global"));
    Ebean.execute(Ebean.createCallableSql("CREATE SCHEMA IF NOT EXISTS patient"));
    Ebean.execute(Ebean.createCallableSql("CREATE SCHEMA IF NOT EXISTS examprocedure"));
    Ebean.execute(Ebean.createCallableSql("CREATE SCHEMA IF NOT EXISTS scheduling"));
    Ebean.execute(Ebean.createCallableSql("CREATE SCHEMA IF NOT EXISTS auditing"));
    Ebean.execute(Ebean.createCallableSql("CREATE SCHEMA IF NOT EXISTS workflow"));
    Ebean.execute(Ebean.createCallableSql("CREATE SCHEMA IF NOT EXISTS imaging"));
    Ebean.execute(Ebean.createCallableSql("CREATE SCHEMA IF NOT EXISTS billing"));
    Ebean.execute(Ebean.createCallableSql("CREATE SCHEMA IF NOT EXISTS integration"));

    Logger.debug("generating create ddl");
    String createScript = ddl.generateCreateDdl();
    List<String> schemas = Arrays.asList("resources", "global", "patient", "examprocedure", "scheduling", "auditing", "workflow", "imaging", "billing", "integration");
    for (String schema : schemas) {
      createScript = createScript.replaceAll("uq_" + schema + ".", "uq_")
              .replaceAll("fk_" + schema + ".", "fk_")
              .replaceAll("ix_" + schema + ".", "ix_");
    }
    CallableSql createCallableSql = Ebean.createCallableSql(createScript);
    Ebean.execute(Ebean.createCallableSql(createScript));
  }
}

package common.utils;

public class FormattingUtils {
  
  public final static String NUMBERREGEX = "\\d+";
  
  public static String removeNonNumericCharacters(String string) {
    return string.replaceAll("[^0-9]", "");
  }
  
  public static boolean isNumberCompatible(String value){
    if(value != null && value.matches(NUMBERREGEX)){
      return true;
    }
    return false;
  }

  /*
   * Used on formatting decimal numbers
   * */
  public static float formatFloatNumber(Float value, int decimalPlaces){
	  if (value == null){
		  value = 0F;
	  }

	  float a = value;
	  double temp = Math.pow(10.0, decimalPlaces);
	  a *= temp;
	  a = Math.round(a);

	  return (a / (float)temp);

  }
  
}

package common.utils;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.joda.time.Years;

import common.constants.CalendarDay;


public class DateUtils {


  public static Date getCurrentDate() {
    return new Date();
  }

  public static DateTime getCurrentDateTime() {
    return new DateTime();
  }

  public static Date nowMinusMinutes(int minutes) {
    DateTime dateTime = new DateTime();
    return dateTime.minusMinutes(minutes).toDate();
  }

  public static Date nowPlusMinutes(int minutes) {
    DateTime dateTime = new DateTime();
    return dateTime.plusMinutes(minutes).toDate();
  }

  public static DateTime nowDateTimeMinusMinutes(int minutes) {
    DateTime dateTime = new DateTime();
    return dateTime.minusMinutes(minutes);
  }

  public static DateTime nowDateTimePlusMinutes(int minutes) {
    DateTime dateTime = new DateTime();
    return dateTime.plusMinutes(minutes);
  }

  public static java.sql.Date convertToSqlDate(DateTime dateTime) {
    return new java.sql.Date(dateTime.getMillis());
  }

  public static java.sql.Time convertToSqlTime(DateTime dateTime) {
    return new Time(dateTime.millisOfDay().get());
  }

  public static List<Interval> splitInterval(Interval interval, int chunckSizeMillis) {
    List<Interval> splited = new ArrayList<Interval>();
    for (long i = interval.getStartMillis(); i + chunckSizeMillis <= interval.getEndMillis(); i =
        i + chunckSizeMillis) {
      splited.add(new Interval(i, i + chunckSizeMillis));
    }
    return splited;
  }

  public static DateTime calcNextDayOfWeek(CalendarDay calendarDay) {
    DateTime d = DateTime.now();
    return (d.getDayOfWeek() <= calendarDay.getCalendarDayIdx()) ? d.withDayOfWeek(calendarDay
        .getCalendarDayIdx()) : d.plusWeeks(1).withDayOfWeek(calendarDay.getCalendarDayIdx());
  }

  public static int calcAgeByBirthDate(DateTime birthDate) {
    DateTime now = new DateTime();
    Years age = Years.yearsBetween(birthDate, now);
    return age.getYears();
  }

  public static DateTime convertDateTimeToDateTimeGMTZero(DateTime dateTime) {
    return dateTime.toDateTime(DateTimeZone.UTC);
  }

  public static DateTime getCurrentDateTimeGMTZero() {
    return new DateTime(DateTimeZone.UTC);
  }

  public static Date getCurrentDateGMTZero() {
    return getCurrentDateTimeGMTZero().toDate();
  }

  /**
   * Interval is respect a chronology and timezone. With you need to convert a certain hour of day
   * for current chronology and timezone, you need to consider the current day. Eg: when you
   * instantiate a interval with 9h-10h with the default constructor, new Interval(32400000,
   * 36000000), the interval will be built with UTC 0 time and current gmt: 6h -3GMT to 7h -3GMT
   * This method will construct the interval using {@link DateTime#now()}
   * 
   * @param startAt current start time in millis after midnight considering Unix Epoch eg:32400000 =
   *        9h AM
   * @param endAt current end time in millis after midnight considering Unix Epoch eg:36000000 = 10h
   *        AM
   * @return a interval with correct current gmt time
   * 
   * 
   */
  public static Interval convertIntervalToCurrentChronology(Long startAt, Long endAt) {
    if (startAt > 86400000 || endAt > 86400000) {
      throw new IllegalArgumentException(
          "Can only accept values refering at hours, min, sec, millis into a day. Eg: 32400000, 36000000 = interval on today that mean 9h-10h AM");
    }
    DateTime now = getCurrentDateTime();
    return new Interval(now.withTimeAtStartOfDay().plusMillis(startAt.intValue()), now
        .withTimeAtStartOfDay().plusMillis(endAt.intValue()));
  }

  public static Interval overlapIntervalsWithSlotRound(Interval reference, Interval toCompare,
      int slotRoundMillis) {
    if (!reference.overlaps(toCompare)) {
      return null;
    }
    long realStart;
    long realEnd;
    long refStartMillis = reference.getStartMillis();
    long toCStartMillis = toCompare.getStartMillis();
    long refEndMillis = reference.getEndMillis();
    long toCEndMillis = toCompare.getEndMillis();
    if (toCStartMillis <= refStartMillis) {
      realStart = refStartMillis;
    } else {
      while (toCStartMillis > refStartMillis) {
        refStartMillis += slotRoundMillis;
      }
      realStart = refStartMillis;
    }
    if (toCEndMillis >= refEndMillis) {
      realEnd = refEndMillis;
    } else {
      realEnd = reference.getStartMillis();// need to assume that the base is the start
      while (toCEndMillis > (realEnd + slotRoundMillis)) {
        realEnd += slotRoundMillis;
      }
    }
    return new Interval(realStart, realEnd);
  }

  public static Interval subtractIntervalWithSlotRound(Interval reference, Interval toSubtract,
      int slotRoundMillis) {
    if (!reference.overlaps(toSubtract)) {
      return reference;
    }
    if (toSubtract.contains(reference)) {
      return null;
    }
    if (reference.contains(toSubtract)) {
      throw new IllegalArgumentException("Can't split and merge reference interval, not yet.");
    }
    long realStart = 0;
    long realEnd = 0;
    long refStartMillis = reference.getStartMillis();
    long toSubStartMillis = toSubtract.getStartMillis();
    long refEndMillis = reference.getEndMillis();
    long toSubEndMillis = toSubtract.getEndMillis();
    if (toSubStartMillis > refStartMillis) {
      realStart = refStartMillis;
      realEnd = realStart;
      while (toSubStartMillis > (realEnd + slotRoundMillis)) {
        realEnd += slotRoundMillis;
      }
    } else {
      while (realStart < toSubEndMillis) {
        realStart += slotRoundMillis;
      }
      realEnd = refEndMillis;
    }
    return new Interval(realStart, realEnd);
  }

  public static Interval convertIntervalToIntervalGMTZero(Interval interval) {
    Long start = interval.getStart().getMillis();
    Long end = interval.getEnd().getMillis();
    return new Interval(start, end, DateTimeZone.UTC);
  }
  
  public static DateTime getNextWeekDay(int dayOfWeek){
    return getNextWeekDay(getCurrentDateTime(), dayOfWeek);
  }

  /**
   * @param start the base date
   * @param dayOfWeek the int associated with the day, i.e monday=1,..., sunday=7
   * @return the next day of week from the parameter start
   */
  public static DateTime getNextWeekDay(DateTime start, int dayOfWeek) {
    do {
      start = start.plusDays(1);
    } while (start.dayOfWeek().get() != dayOfWeek);
    return start;
  }
  
  /**
   * @param start the base date
   * @param dayOfWeek the int associated with the day, i.e monday=1,..., sunday=7
   * @return the next day of week from the parameter start
   */
  public static DateTime getPrevioustDay(DateTime start, int dayOfWeek) {
    if(start.dayOfWeek().get() == dayOfWeek){
      return start.minusWeeks(1);
    }
    do {
      start = start.minusDays(1);
    } while (start.dayOfWeek().get() != dayOfWeek);
    return start;
  }
}

package common.constants;

public interface IHttpQueryParamKeys {
  
  public String WWW_AUTHENTICATE_ERROR = "error";
  public String WWW_AUTHENTICATE_ERROR_DESCRIPTION = "error_description";
  public String WWW_AUTHENTICATE_REALM = "realm";
  public String ACCESS_TOKEN = "accessToken";
  public String EXPIRES_IN = "expireIn";
  public String REFRESH_TOKEN = "refreshToken";
  public String CLIENT_APP_RAW_PARAM = "rawParam";
  public String CLIENT_APP_KEY = "appKey";
  
}

package common.constants;

public enum SlotStatus {

    AVAILABLE, UNAVAILABLE, BOOKED

}

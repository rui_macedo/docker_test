package common.constants;

public enum SchedulingBookSlotStatus {
  PENDING, WAITING, SUCCESS, PROCESSING, FAILED;
}

package common.constants;

public interface DaysOfMonth {

    public final int FIRST_DAY_OF_BENNING_OF_MONTH = 1;
    public final int LAST_DAY_OF_BEGINNING_OF_MONTH = 15;
    public final int FIRST_DAY_OF_ENDING_OF_MONTH = 16;
    public final int LAST_DAY_OF_ENDING_OF_MONTH = 31;

}

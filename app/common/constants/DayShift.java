package common.constants;

import org.joda.time.Interval;

public enum DayShift {
  MOR("dayperiods.morning",21600000, 43200000), //6h-12h
  AFT("dayperiods.afternoon", 43200000, 64800000),//12h-18h
  NIG("dayperiods.night",64800000, 86399999);//18h-23h59m59s999
  
  private String i18nKey;
  private int startAt;
  private int endAt;
  private Interval interval;
  private DayShift(String i18nKey, int startAt, int endAt){
    this.i18nKey = i18nKey;
    this.startAt = startAt;
    this.endAt = endAt;
  }
  
  public String getI18nKey(){
    return this.i18nKey;
  }
  
  public int getStartAt(){
    return this.startAt;
  }
  
  public int getEndAt(){
    return this.endAt;
  }
  
  public Interval getInterval(){
    if(interval == null){
      this.interval = new Interval(this.startAt, this.endAt);
    }
    return interval;
  }
  
  public static DayShift byTimeInDay(int utcDayTime){
    if(MOR.getInterval().contains(utcDayTime)){
      return MOR;
    }else if(AFT.getInterval().contains(utcDayTime)){
      return AFT;
    }else{
      return NIG;
    }
  }
}

package common.constants;

public enum CalendarDay {
  MON("calendarday.monday", 1),
  TUE("calendarday.tuesday", 2),
  WED("calendarday.wednesday", 3),
  THU("calendarday.thursday", 4),
  FRI("calendarday.friday", 5),
  SAT("calendarday.saturday", 6),
  SUN("calendarday.sunday", 7);
  
  private String i18nKey;
  private int calendarDayIdx;
  private CalendarDay(String i18nKey, int calendarDayIdx){
    this.i18nKey = i18nKey;
    this.calendarDayIdx = calendarDayIdx;
  }
  public String getI18nKey(){
    return this.i18nKey;
  }
  public int getCalendarDayIdx(){
    return this.calendarDayIdx;
  }
  public static CalendarDay getByDayIdx(int idx){
    if(idx > 7 || idx < 1){
      throw new IllegalArgumentException("Invalid index (1 for monday, 7 to sunday");
    }
    CalendarDay  cl;
    switch (idx) {
      case 1: cl = MON;
        break;
      case 2: cl = TUE;
        break;
      case 3: cl = WED;
        break;
      case 4: cl = THU;
        break;
      case 5: cl = FRI;
        break;
      case 6: cl = SAT;
        break;
      case 7: cl = SUN;
        break;
      default: cl = MON;
        break;
    }
    return cl;
  }
  public static CalendarDay getNext(CalendarDay today){
    if(today.calendarDayIdx < 7){
      return getByDayIdx(today.calendarDayIdx+1);
    }
    return MON;
  }
}

package common.constants;

public interface IHttpHeaderParamKeys {
  
  public String AUTHORIZATION = "Authorization";
  public String AUTHORIZATION_BEARER = "Bearer";
  public String WWW_AUTHENTICATE = "WWW-Authenticate";

}

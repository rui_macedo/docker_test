package common.constants;

import com.avaje.ebean.annotation.EnumValue;

   /**
	 * The Enum IndexerType.
	 * 
	 *   Used to set the type of Indexer to be applied in the ExamProcedures indexes
	 */
	public enum IndexerType {
		  
  		/** The ch. */
		@EnumValue("CH")
  		CH("Custo de Honorários Médicos","CH"),
		  
  		/** The co. */
		@EnumValue("CO")
  		CO("Custo Operacional","CO"),
		  
  		/** The fil. */
		@EnumValue("FIL")
  		FIL("Filme","FIL"),
		  
  		/** The mat. */
		@EnumValue("MAT")
  		MAT("Materiais e Medicamentos","MAT"),
		  
  		/** The out. */
		@EnumValue("OTR")
  		OTR("Outros Custos","OTR"),
		  
  		/** The ant. */
		@EnumValue("ANT")
  		ANT("Anestesista","ANT");
		  
		  /** The description. */
  		private String description;
		  
  		/** The short name. */
  		private String shortName;
		  
		  /**
  		 * Instantiates a new indexer type.
  		 *
  		 * @param description the description
  		 * @param shortName the short name
  		 */
  		private IndexerType(String description, String shortName){
			  this.description = description;
			  this.shortName = shortName;
		  }
		  
		  /**
  		 * Gets the description.
  		 *
  		 * @return the description
  		 */
  		public String getDescription(){
			  return this.description;
		  }
		  
  		/**
  		 * Gets the short name.
  		 *
  		 * @return the short name
  		 */
  		public String getShortName(){
			  return this.shortName;
		  }
		  
		  /**
  		 * Gets the indexer type.
  		 *
  		 * @param shortName the short name
  		 * @return the indexer type
  		 */
  		public static IndexerType getIndexerType(String shortName){
			  IndexerType  indType;
			  

			  switch (shortName) {
			      case "CH": indType = CH;
			        break;
			      case "CO": indType = CO;
			        break;
			      case "FIL": indType = FIL;
			        break;
			      case "MAT": indType = MAT;
			        break;
			      case "OTR": indType = OTR;
			        break;
			      case "ANT": indType = ANT;
			        break;
			      default : indType = CH;    
			    }
			    return indType;
			  }
		  
	}



package common.constants;

public enum InsurancePaymentType {

    HEALTHCARE, PRIVATE, COMPANY;

}

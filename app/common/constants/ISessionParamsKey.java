package common.constants;

public interface ISessionParamsKey {
  public String AUTH_ATTEMPTS_COUNTER = "auth.attempts.counter";
}

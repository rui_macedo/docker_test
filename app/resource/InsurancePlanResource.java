package resource;

import dto.InsurancePlanDTO;

public class InsurancePlanResource {

    public Long id;
    public Long institutionId;
    public Long insuranceId;
    public String name;
    public Boolean enabled;
    public String description;
    public ProcedureTableResource procedureTable;

    public InsurancePlanDTO asDTO(){
        InsurancePlanDTO dto = new InsurancePlanDTO();
        dto.id = id;
        dto.name = name;
        return dto;
    }

}


package resource;

public class ProcedureTableResource {

    public Long id;
    public String name;
    public Float percentageAUX1;
    public Float percentageAUX2;
    public Float percentageAUX3;
    public Float percentagePatientCH;
    public Float percentagePatientCO ;
    public Float percentagePatientFIL;
    public Float percentagePatientMAT;
    public Float percentagePatientANT ;
    public Float percentagePatientOTR;



}

package resource;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.deserializers.TimestampDateDeserializer;
import com.pixeon.utils.common.serializers.TimestampDateSerializer;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;

import dto.ContactDTO;
import dto.DurationDTO;
import dto.EmployeeDTO;
import dto.PatientDTO;
import dto.SchedulingOrderedDTO;
import dto.UnityDTO;
import models.scheduling.Scheduling;

public class SchedulingFullReservationResource {


    public Long id;

    public String protocol;

    public UnityDTO unity;

    public EmployeeDTO employee;

    public Long institutionId;

    public ContactDTO contact;

    public String notes;

    public Scheduling.Status status;

    public PatientDTO patient;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime creationDate;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime updatedDate;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime serviceStart;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime serviceEnd;

    public BigDecimal amount;

    public DurationDTO sessionTime;

    public DurationDTO duration;

    public List<SchedulingOrderedDTO> requests;



}

package resource;

import common.constants.InsurancePaymentType;

import dto.InsuranceDTO;

public class InsuranceResource {

    public Long id;
    public Long institutionId;
    public String name;
    public ProcedureTableResource procedureTable;
    public String description;
    public Boolean isEnabled;
    public InsurancePaymentType paymentType;

    public InsuranceDTO asDTO(){
        InsuranceDTO dto = new InsuranceDTO();
        dto.id = id;
        dto.name = name;
        dto.description = description;
        dto.isEnabled = isEnabled;
        return dto;
    }

}

package dto;


import java.io.Serializable;

public class UnityDTO implements Serializable {
  public Integer id;
  public String name;
  public String unityCode;

  public UnityDTO() {}

  public UnityDTO(Integer id, String name, String unityCode) {
    super();
    this.id = id;
    this.name = name;
    this.unityCode = unityCode;
  }

}

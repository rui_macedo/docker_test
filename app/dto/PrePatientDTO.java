package dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.deserializers.DateTimeISO8601Deserializer;
import com.pixeon.utils.common.serializers.DateTimeISO8601Serializer;
import org.joda.time.DateTime;

public class PrePatientDTO {

    public Long id;

    public Long institutionId;

    public String name;

    @JsonSerialize(using = DateTimeISO8601Serializer.class)
    @JsonDeserialize(using = DateTimeISO8601Deserializer.class)
    public DateTime creationDate;

    @JsonSerialize(using = DateTimeISO8601Serializer.class)
    @JsonDeserialize(using = DateTimeISO8601Deserializer.class)
    public DateTime updatedDate;

}

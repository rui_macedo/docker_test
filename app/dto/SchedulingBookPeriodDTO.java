package dto;


public class SchedulingBookPeriodDTO {
  public Long id;
  public Integer startedAt;
  public Integer endedAt;
}

package dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import common.annotations.serializers.TimestampDateSerializer;
import common.constants.SlotStatus;

import org.joda.time.DateTime;

import java.util.List;

public class SimpleSlotDTO {

    public Long id;

    @JsonSerialize(using = TimestampDateSerializer.class)
    public DateTime startedAt;

    @JsonSerialize(using = TimestampDateSerializer.class)
    public DateTime endedAt;

    public SlotStatus status;

    public SchedulingBookPeriodDTO period;

    public List<InsuranceDTO> insurances;

    public List<InsurancePlanDTO> insurancePlans;

    public List<ExamProcedureDTO> examProcedures;

    public List<RestrictionDTO> restrictions;



}

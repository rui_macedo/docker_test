package dto;

import java.io.Serializable;

import play.data.validation.Constraints.Required;

public class RestrictionDTO implements Serializable{

  @Required
  public String name;
  public Long id;

  public RestrictionDTO() {}

  public RestrictionDTO(Long id, String name) {
    this.id = id;
    this.name = name;
  }

}

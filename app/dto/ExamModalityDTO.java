package dto;


import java.io.Serializable;

public class ExamModalityDTO implements Serializable{
  public Long id;
  public String name;
  public String shortName;
}

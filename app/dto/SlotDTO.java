package dto;

/**
 * It's built for CLD-3438 but as nobody knows what I should return to frontend
 *
 * I kept it as simple as possible !
 */
public class SlotDTO extends SimpleSlotDTO{

    public SchedulingBookDTO schedulingBook;

    public SlotDTO(){}

    public SlotDTO(SimpleSlotDTO simple){
        this.endedAt = simple.endedAt;
        this.startedAt = simple.startedAt;
        this.examProcedures = simple.examProcedures;
        this.id = simple.id;
        this.period = simple.period;
        this.status = simple.status;
    }

}

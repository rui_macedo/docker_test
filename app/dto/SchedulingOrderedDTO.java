package dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.deserializers.TimestampDateDeserializer;
import com.pixeon.utils.common.serializers.TimestampDateSerializer;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Objects.isNull;

public class SchedulingOrderedDTO {

    public ExamProcedureDTO examProcedure;

    public InsuranceDTO insurance;

    public InsurancePlanDTO insurancePlan;

    public PhysicianDTO physician;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime start;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime end;

    public DurationDTO duration;

    public BigDecimal price;

    public List<RestrictionCheckDTO> restrictions;

    @JsonIgnore
    public Long unityId;

    public ExamProcedureDTO getExamProcedure() {
        return examProcedure;
    }

    public void setExamProcedure(ExamProcedureDTO examProcedure) {
        this.examProcedure = examProcedure;
    }

    public InsuranceDTO getInsurance() {
        return insurance;
    }

    public void setInsurance(InsuranceDTO insurance) {
        this.insurance = insurance;
    }

    public InsurancePlanDTO getInsurancePlan() {
        return insurancePlan;
    }

    public void setInsurancePlan(InsurancePlanDTO insurancePlan) {
        this.insurancePlan = insurancePlan;
    }

    public PhysicianDTO getPhysician() {
        return physician;
    }

    public void setPhysician(PhysicianDTO physician) {
        this.physician = physician;
    }

    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    public DurationDTO getDuration() {
        return duration;
    }

    public void setDuration(DurationDTO duration) {
        this.duration = duration;
    }

    public BigDecimal getPrice() {
        if(isNull(price)){
            return BigDecimal.ZERO;
        }
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<RestrictionCheckDTO> getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(List<RestrictionCheckDTO> restrictions) {
        this.restrictions = restrictions;
    }

    public Long getUnityId() {
        return unityId;
    }

    public void setUnityId(Long unityId) {
        this.unityId = unityId;
    }
}

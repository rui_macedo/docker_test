package dto;

public class EquipmentDTO {
  public Long id;
  public String name;

  public EquipmentDTO() {}

  public EquipmentDTO(Long id, String name) {
    this.id = id;
    this.name = name;
  }

}

package dto;


import common.constants.InsurancePaymentType;

import java.io.Serializable;

public class InsuranceDTO implements Serializable {
  public Long id;
  public String name;
  public String description;
  public Boolean isEnabled;
  public InsurancePaymentType paymentType;

  public InsuranceDTO() {}

  public InsuranceDTO(Long id, String name, String description, Boolean isEnabled, InsurancePaymentType paymentType) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.isEnabled = isEnabled;
    this.paymentType = paymentType;
  }

}

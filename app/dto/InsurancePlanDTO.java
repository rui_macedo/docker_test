package dto;

import java.io.Serializable;

public class InsurancePlanDTO implements Serializable {
  public String name;
  public Long id;

  public InsurancePlanDTO() {}

  public InsurancePlanDTO(Long id, String name) {
    this.id = id;
    this.name = name;
  }

}

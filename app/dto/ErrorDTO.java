package dto;

public class ErrorDTO {


    public ErrorDTO() {}

    public ErrorDTO(String code, Object errors) {
        this.code = code;
        this.errors = errors;
    }

    public ErrorDTO(String code) {
        this.code = code;
    }

    public String code;

    //this could be a simple string or a list of errors
    public Object errors;

}

package dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.deserializers.DateTimeISO8601Deserializer;
import com.pixeon.utils.common.serializers.DateTimeISO8601Serializer;
import org.joda.time.DateTime;

import play.data.validation.Constraints.Required;


public class SlotReservationDTO {

    public Long slotId;
    @Required
    public String token;
    @JsonSerialize(using = DateTimeISO8601Serializer.class)
    @JsonDeserialize(using = DateTimeISO8601Deserializer.class)
    public DateTime expirationTime;
    public Long employeeId;

    public SlotReservationDTO(){}

    public SlotReservationDTO(String token, DateTime expirationTime, Long employeeId) {
        this.token = token;
        this.expirationTime = expirationTime;
        this.employeeId = employeeId;
    }
}

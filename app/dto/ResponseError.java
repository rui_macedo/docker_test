package dto;

public class ResponseError {

    public enum Codes {
        UNDEFINED, BODY_CONTENT_PROBLEM, CROSS_TENANT, NOT_FOUND,
        ENTITY_HAS_NO_RELATION, SERVICE_UNAVAILABLE, NOT_OWNER,
        NOT_AVAILABLE, SLOT_BOOKED;
    }

    public static ErrorDTO create(String error) {
        return new ErrorDTO(Codes.UNDEFINED.toString(), error);
    }

    public static ErrorDTO create(Object errors, Codes code) {
        return new ErrorDTO(code.toString(), errors);
    }

}

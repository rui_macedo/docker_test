package dto;

import java.util.concurrent.TimeUnit;

public class DurationDTO {

    public Integer time;
    public TimeUnit unit;

    public DurationDTO(Integer time) {
        this.time = time;
        this.unit = TimeUnit.MINUTES;
    }
}

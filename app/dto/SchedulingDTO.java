package dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Lists;
import com.pixeon.utils.common.deserializers.DateTimeISO8601Deserializer;
import com.pixeon.utils.common.serializers.DateTimeISO8601Serializer;

import org.joda.time.DateTime;

import models.scheduling.Scheduling;

import java.util.List;

public class SchedulingDTO {

    public Long id;

    public String protocol;

    public ContactDTO contact;

    public Long patientId;

    public Long institutionId;

    public Long employeeId;

    public String patientName;

    public String notes;

    public Scheduling.Status status;

    @JsonSerialize(using = DateTimeISO8601Serializer.class)
    @JsonDeserialize(using = DateTimeISO8601Deserializer.class)
    public DateTime creationDate;

    @JsonSerialize(using = DateTimeISO8601Serializer.class)
    @JsonDeserialize(using = DateTimeISO8601Deserializer.class)
    public DateTime updatedDate;

    public List<SchedulingFilterDTO> filters;

    public SchedulingDTO(){
        filters = Lists.newArrayList();
    }

    @Override
    public String toString() {
        return "SchedulingDTO{" +
                "id=" + id +
                ", contact=" + contact +
                ", patientId=" + patientId +
                ", institutionId=" + institutionId +
                ", employeeId=" + employeeId +
                ", patientName='" + patientName + '\'' +
                ", notes='" + notes + '\'' +
                ", status=" + status +
                ", creationDate=" + creationDate +
                ", updatedDate=" + updatedDate +
                '}';
    }
}

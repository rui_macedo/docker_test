package dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.deserializers.TimestampDateDeserializer;
import com.pixeon.utils.common.serializers.TimestampDateSerializer;

import org.joda.time.DateTime;

import java.util.List;

public class PatientWithDocumentDTO {

    public Long id;

    public String name;

    public String externalId;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @JsonDeserialize(using = TimestampDateDeserializer.class)
    public DateTime dateOfBirth;

    public String gender;

    public List<PatientDocumentDTO> documents;

    public String email;

}

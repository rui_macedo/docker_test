package dto;

import java.util.List;

public class SchedulingBookDetailsDTO extends SchedulingBookDTO{

  public EquipmentDTO equipment;
  public Integer slotSizeMin;
  public PhysicianDTO physician;
  public Boolean suitable;
  public Boolean status;
  public List<ExamProcedureDTO> examProcedures;
  public List<SchedulingBookWeekAgendaDTO> schedulingBookWeekAgenda;
  public List<InsuranceDTO> insurances;
  public List<InsurancePlanDTO> insurancePlans;
  public List<RestrictionDTO> restrictions;

  public SchedulingBookDetailsDTO(){

  }
  
}

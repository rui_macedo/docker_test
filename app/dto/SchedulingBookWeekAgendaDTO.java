package dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import common.constants.CalendarDay;

public class SchedulingBookWeekAgendaDTO {

  public Long id;
  public CalendarDay calendarDay;
  public List<SchedulingBookPeriodDTO> schedulingBookPeriods;
  @JsonIgnore
  public Long idInstitution;

}

package dto;

import models.ExamModality;
import play.data.validation.Constraints.Required;

public class ExamProcedureDTO {
  public Long id;
  @Required
  public String name;
  @Required
  public String code;
  public String mnemonic;
  public Boolean examProcedureEnabled;
  public ExamModality examModality;
  public String preparation;
}

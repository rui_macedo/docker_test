package dto;

import java.math.BigDecimal;

public class ExamProcedureByPlanDTO implements Cloneable{

    public Long id;
    public String name;
    public Boolean isCoveredByPlan;
    public BigDecimal price;

    @Override
    public ExamProcedureByPlanDTO clone(){
        ExamProcedureByPlanDTO clone = new ExamProcedureByPlanDTO();
        clone.id = id;
        clone.name = name;
        clone.isCoveredByPlan = isCoveredByPlan;
        clone.price = price;
        return clone;
    }

}




package dto;

public class RestrictionCheckDTO {

    public Long id;
    public String name;
    public Boolean checked;

    public RestrictionCheckDTO(Long id, String name, Boolean checked) {
        this.id = id;
        this.name = name;
        this.checked = checked;
    }
}

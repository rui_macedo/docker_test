package dto;

import java.io.Serializable;

public class PhysicianDTO implements Serializable {

  public String name;
  public Long id;
  public String crm;

  public PhysicianDTO() {}

  public PhysicianDTO(Long id, String name) {
    this.id = id;
    this.name = name;
  }

  public PhysicianDTO(Long id, String name,  String crm) {
    this.name = name;
    this.id = id;
    this.crm = crm;
  }
}

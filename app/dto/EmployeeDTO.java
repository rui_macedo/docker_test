package dto;

public class EmployeeDTO {

    public Long id;
    public String name;

    public EmployeeDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}

package dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.deserializers.DateTimeISO8601Deserializer;
import com.pixeon.utils.common.serializers.DateTimeISO8601Serializer;
import org.joda.time.DateTime;

public class ContactPatientDTO {

    public Long id;

    public Long institutionId;

    public Boolean isValidPatient;

    public PatientDTO patient;

    public PrePatientDTO patientPreRegister;

    public String notes;

    @JsonSerialize(using = DateTimeISO8601Serializer.class)
    @JsonDeserialize(using = DateTimeISO8601Deserializer.class)
    public DateTime creationDate;

    @JsonSerialize(using = DateTimeISO8601Serializer.class)
    @JsonDeserialize(using = DateTimeISO8601Deserializer.class)
    public DateTime updatedDate;

}

package dto;

import java.util.List;

public class SchedulingBookSlotsDTO {

    public SchedulingBookDTO schedulingBook;
    public List<SimpleSlotDTO> slots;

}

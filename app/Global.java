import actors.QueueReader;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.Message;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import com.pixeon.cloudsecure.remote.authorization.CloudCredentialsProvider;
import com.pixeon.cloudsecure.remote.authorization.DefaultHttpsCloudAuthProviderV2;
import com.pixeon.cloudsecure.token.WSSecureCallPlugin;
import com.typesafe.plugin.RedisPlugin;
import common.annotations.BaseSimpleModelFormatterImpl;
import common.annotations.BaseTenantModelFormatterImpl;
import common.utils.SqlUtils;
import controllers.*;
import models.BaseSimpleModel;
import models.BaseTenantModel;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.Play;
import play.data.format.Formatters;
import play.libs.Akka;
import play.libs.Json;
import scala.concurrent.duration.Duration;
import services.*;
import services.api.*;
import services.configuration.DefaultReservationConfiguration;
import services.configuration.SchedulingReservationConfiguration;
import services.external.api.EmployeeData;
import services.external.api.ExamProcedureService;
import services.external.api.QueueService;
import services.external.api.ServiceDispatcher;
import services.external.impl.EmployeeService;
import services.external.impl.ExamProcedureServiceImpl;
import services.external.impl.SQSQueueService;
import services.external.impl.ServiceDispatcherImpl;
import services.patient.PatientService;
import services.searchers.SchedulingFilterServiceImpl;
import services.searchers.SearchProvider;
import services.searchers.SlotSearchProvider;
import services.searchers.finder.ShorterStayingFinder;
import services.searchers.model.Card;

import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;

public class Global extends GlobalSettings {

  private Injector injector;

  //FIXME Create an abstract global settings and extends it just to set the DI

  @Override
  public void onStart(Application app) {

    Formatters.register(BaseSimpleModel.class, new BaseSimpleModelFormatterImpl());
    Formatters.register(BaseTenantModel.class, new BaseTenantModelFormatterImpl());

    if (("jdbc:h2:mem:play".equals(app.configuration().getString("db.default.url")) && app.isDev()) || app.isTest()) {
      SqlUtils.createDDLToMemoryDB();
    }

    Json.setObjectMapper(new ObjectMapper()
                    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
    );

    injector = Guice.createInjector(new AbstractModule() {

      @Override
      protected void configure() {
        bind(EmployeeData.class).toInstance(getSecureCall());
        bind(AmazonSQS.class).toInstance(createSQSClient());
        bind(new TypeLiteral<QueueService<Message>>() {}).to(SQSQueueService.class);
        bind(CloudCredentialsProvider.class).to(DefaultHttpsCloudAuthProviderV2.class);
        bind(SlotService.class).to(SlotServiceImpl.class);
        bind(ContactService.class).to(ContactServiceImpl.class);
        bind(InsuranceService.class).toInstance(getInsuranceService());
        bind(InsurancePlanService.class).toInstance(getInsurancePlanService());
        bind(ExamProcedureService.class).toInstance(getExamProcedureService());
        bind(SchedulingService.class).to(SchedulingServiceImpl.class);
        bind(new TypeLiteral<SearchProvider<Card>>() {}).toInstance(getShorterStayingSearchProvider());
        bind(services.api.PatientService.class).to(PatientServiceImpl.class);
        bind(PatientService.class).toInstance(getPatientService());
        bind(SchedulingFilterService.class).to(SchedulingFilterServiceImpl.class);
        bind(SchedulingReservationConfiguration.class).toInstance(getReservationConfig());
        requestStaticInjection(Insurances.class, Slots.class, SQSQueueService.class,
                QueueReader.class, Contacts.class, InsuranceServiceImpl.class, InsurancePlans.class,
                ContactPatients.class, PrePatients.class, ExamProcedures.class, ExamProcedureServiceImpl.class,
                Schedulings.class, SlotServiceImpl.class, ShorterStayingFinder.class, Reservations.class,
                SchedulingServiceImpl.class
        );
      }
    });

    if (!app.isTest()) {
      ActorRef myActor = Akka.system().actorOf(Props.create(QueueReader.class));
      Akka.system().scheduler()
              .schedule(Duration.create(3, TimeUnit.SECONDS), Duration.create(2, TimeUnit.SECONDS),
                      myActor, "readQueue", Akka.system().dispatcher(), null);
    }

    super.onStart(app);
  }

  private ExamProcedureService getExamProcedureService() {
    return new ExamProcedureServiceImpl(tryToGetRedisPlugin().jedisPool(),
            Play.application().configuration().getBoolean("scheduling.cache.enabled", true)
    );
  }

  private SearchProvider<Card> getShorterStayingSearchProvider() {
    return new SlotSearchProvider(tryToGetRedisPlugin().jedisPool());
  }


  public Injector getInjector() {
    checkNotNull(this.injector, "Injector is null");
    return this.injector;
  }

  private RedisPlugin tryToGetRedisPlugin() {
    RedisPlugin redisPlugin = play.Play.application().plugin(RedisPlugin.class);
    if (redisPlugin == null || redisPlugin.jedisPool() == null) {
      throw new RuntimeException("Trying to started reliable Queue. No jedisPool could be acquired.");
    }
    return redisPlugin;
  }

  @Override
  public <T> T getControllerInstance(Class<T> aClass) throws Exception {
    return injector.getInstance(aClass);
  }

  protected ServiceDispatcher getIntraServiceDispatcher() {
    return new ServiceDispatcherImpl(Play.application().plugin(WSSecureCallPlugin.class));
  }

  protected PatientService getPatientService(){
    return new services.patient.PatientServiceImpl(this.getIntraServiceDispatcher());
  }

  protected EmployeeData getSecureCall() {
    return new EmployeeService();
  }

  protected AmazonSQS createSQSClient() {
    String accessKey = checkNotNull(Play.application().configuration().getString("aws.access.key"), "AWS Access Key is missing");
    String secretKey = checkNotNull(Play.application().configuration().getString("aws.secret.key"), "AWS Secret Key is missing");
    AmazonSQSClient amazonSQSClient = new AmazonSQSClient(new BasicAWSCredentials(accessKey, secretKey));
    amazonSQSClient.setRegion(Region.getRegion(Regions.SA_EAST_1));

    if (Play.application().configuration().getBoolean("scheduling.api.offline.enabled", false)) {
      String endpoint = Play.application().configuration().getString("sqs.jpr.endpoint", "http://localhost:4569");
      Logger.warn("JPR parameter is true, setting SQS' endpoint to " + endpoint);
      amazonSQSClient.setEndpoint(endpoint);
    }

    return amazonSQSClient;

  }

  protected InsurancePlanService getInsurancePlanService(){
    return new InsurancePlanServiceImpl(tryToGetRedisPlugin().jedisPool(),
            Play.application().configuration().getBoolean("scheduling.cache.enabled", true)
    );
  }

  protected InsuranceService getInsuranceService() {
    return new InsuranceServiceImpl(tryToGetRedisPlugin().jedisPool(),
            Play.application().configuration().getBoolean("scheduling.cache.enabled", true)
    );
  }

  protected SchedulingReservationConfiguration getReservationConfig() {
    return new DefaultReservationConfiguration(
            Play.application().configuration()
                    .getBoolean("scheduling.reservation.auto.email.enable", false)
    );

  }

}


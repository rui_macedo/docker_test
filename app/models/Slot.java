package models;

import common.constants.CalendarDay;
import common.constants.DayShift;
import common.constants.SlotStatus;
import common.utils.FormattingUtils;
import dto.*;
import services.searchers.model.SimpleSlot;
import forms.FormMessageSlot;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import play.Logger;
import play.Play;
import play.db.ebean.Model;
import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.avaje.ebean.Expr.between;

@Entity
@Table(name = "Slot", schema = "scheduling")
public class Slot extends BaseTenantModel implements PathBindable<Slot>, QueryStringBindable<Slot>, Cloneable {

    private static final long serialVersionUID = 2631916108277997820L;
    public static Model.Finder<Long, Slot> finder = new Model.Finder<>(Long.class, Slot.class);

    @Id
    @Column(name = "IDSlot")
    public Long id;

    @Column(name = "IsDeleted", nullable = false)
    public Boolean isDeleted;

    @Column(name = "Name", nullable = false)
    public String name;

    @ManyToOne
    @JoinColumn(name = "IDUnity", nullable = true)
    public Unity unity;

    @ManyToOne
    @JoinColumn(name = "IDSchedulingBook", nullable = true)
    public SchedulingBook book;

    @Column(name = "CalendarDay")
    @Enumerated(EnumType.STRING)
    public CalendarDay calendarDay;

    @Column(name = "dayShift")
    @Enumerated(EnumType.STRING)
    public DayShift dayShift;

    @Column(name = "Status")
    @Enumerated(EnumType.STRING)
    public SlotStatus status;

    @Column(name = "StartedAt", nullable = false)
    public DateTime startAt;

    @Column(name = "EndedAt", nullable = false)
    public DateTime endAt;

    @OneToMany(mappedBy = "slot", cascade = CascadeType.ALL)
    public List<SlotInsurance> insurances;

    @OneToMany(mappedBy = "slot", cascade = CascadeType.ALL)
    public List<SlotInsurancePlan> insurancePlans;

    @OneToMany(mappedBy = "slot", cascade = CascadeType.ALL)
    public List<SlotExamProcedure> examProcedures;

    @OneToMany(mappedBy = "slot", cascade = CascadeType.ALL)
    public List<SlotRestriction> restrictions;

    @OneToOne(mappedBy = "slot")
    public SlotReservation reservation;

    public Slot() {
    }

    public Slot(Institution institution, SchedulingBook book, List<SlotInsurance> insurances,
                List<SlotInsurancePlan> insurancePlans, List<SlotExamProcedure> examProcedures,
                List<SlotRestriction> restrictions) {
        super(institution);
        this.book = book;
        this.name = book.name;
        this.institution = book.institution;
        this.isDeleted = false;
        this.status = SlotStatus.AVAILABLE;
        this.unity = book.unity;
        this.insurances = insurances;
        this.insurancePlans = insurancePlans;
        this.examProcedures = examProcedures;
        this.restrictions = restrictions;
    }

    public static Slot findById(Long id) {
        return id != null ? finder.byId(id) : null;
    }

    public static List<Slot> findAllByInstitutionAndDate(Institution institution, DateTime from, DateTime to) {
        return finder.where().eq("institution", institution).between("startedAt", from, to).findList();
    }

    public static List<Slot> findAllBySchedulingBookAndDate(SchedulingBook book, DateTime from, DateTime to) {
        return finder.where().eq("book", book).between("startedAt", from, to).findList();
    }

    public static boolean isValidCreationOfSlots(FormMessageSlot message) {
        try {
            Interval intervalo = new Interval(message.startAt, message.endAt);
            if (intervalo.toDuration().getMillis() < (60000 * message.period)) {
                return false;
            }
        } catch (IllegalArgumentException e) {
            return false;
        }
        return finder.where()
                .eq("book.id", message.schedulingBookId)
                .or( between("StartedAt", message.startAt, message.endAt),
                     between("EndedAt", message.startAt, message.endAt))
                .findList()
                .isEmpty();
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public F.Option<Slot> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("slot")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return F.Option.None();
        }
        Slot obj = findById(new Long(pathId));
        return obj != null ? F.Option.Some(obj) : F.Option.None();
    }

    @Override
    public Slot bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

    public SlotDTO wrapWithBook() {
        SlotDTO dto = new SlotDTO(this.wrap());
        dto.schedulingBook = this.book.wrap();
        return dto;
    }

    public SimpleSlot wrapWithRestrictions(){
        long starting = System.currentTimeMillis();
        SimpleSlot dto = new SimpleSlot();
        dto.id = id;
        dto.start = startAt;
        dto.end = endAt;
        return dto;
    }

    public dto.SimpleSlotDTO wrap() {
        dto.SimpleSlotDTO dto = new dto.SimpleSlotDTO();
        dto.id = this.id;
        dto.endedAt = new DateTime(this.endAt);
        dto.startedAt = new DateTime(this.startAt);
        dto.status = this.status;
        dto.insurancePlans = this.wrapInsurancePlans();
        dto.insurances = this.wrapInsurances();
        dto.examProcedures = this.wrapExamProcedures();
        dto.restrictions = this.wrapRestrictions();
        return dto;
    }

    public void unwrapBook(SchedulingBook book) {
        this.book = book;
        this.name = book.name;
        this.institution = book.institution;
        this.isDeleted = false;
        this.status = SlotStatus.AVAILABLE;
        this.unity = book.unity;

        for (SchedulingBookRestriction rest : book.schedulingBookRestriction) {
            SlotRestriction slotRestriction = new SlotRestriction(this, rest.restriction);
            this.restrictions.add(slotRestriction);
        }

        for (SchedulingBookExamProcedure exam : book.schedulingBookExamProcedures) {
            SlotExamProcedure slotExamProcedure = new SlotExamProcedure(this, exam.examProcedure);
            this.examProcedures.add(slotExamProcedure);
        }

        for (SchedulingBookInsurance ins : book.schedulingBookInsurances) {
            SlotInsurance slotInsurance = new SlotInsurance(this, ins.insurance);
            this.insurances.add(slotInsurance);
        }

        for (SchedulingBookInsurancePlan ins : book.schedulingBookInsurancePlan) {
            SlotInsurancePlan slotInsurancePlan = new SlotInsurancePlan(this, ins.insurancePlan);
            this.insurancePlans.add(slotInsurancePlan);
        }

    }

    public List<InsuranceDTO> wrapInsurances() {
        if (this.insurances == null) {
            return Collections.EMPTY_LIST;
        }
        List<InsuranceDTO> dto = new ArrayList<>();
        for (SlotInsurance slotInsurance : this.insurances) {
            dto.add(slotInsurance.insurance.wrapDTO());

        }
        return dto;
    }

    public List<InsurancePlanDTO> wrapInsurancePlans() {
        if (this.insurancePlans == null) {
            return Collections.EMPTY_LIST;
        }
        List<InsurancePlanDTO> dto = new ArrayList<>();
        for (SlotInsurancePlan slotInsurancePlan : this.insurancePlans) {
            dto.add(slotInsurancePlan.insurancePlan.wrap());

        }
        return dto;
    }

    public List<RestrictionDTO> wrapRestrictions() {
        if (CollectionUtils.isEmpty(this.restrictions)) {
            return Collections.EMPTY_LIST;
        }
        List<RestrictionDTO> dto = new ArrayList<>();
        for (SlotRestriction slotRestriction : this.restrictions) {
            dto.add(slotRestriction.restriction.wrap());

        }
        return dto;
    }

    public List<ExamProcedureDTO> wrapExamProcedures() {
        if (CollectionUtils.isEmpty(this.examProcedures)) {
            return Collections.EMPTY_LIST;
        }
        List<ExamProcedureDTO> dto = new ArrayList<>();
        for (SlotExamProcedure slotRestriction : this.examProcedures) {
            dto.add(slotRestriction.examProcedure.wrap());

        }
        return dto;
    }

    public Slot clone() {
        return new Slot(this.institution, this.book, this.insurances, this.insurancePlans, this.examProcedures, this.restrictions);
    }

    public Boolean isAvailable() {
        return this.status == SlotStatus.AVAILABLE && this.reservation == null;
    }

    public void lock(Long employeeId) {
        if (!this.isAvailable()) {
            Logger.debug("Slot " + this.getId() + " is unavailable." + (this.status == SlotStatus.BOOKED ? " It has been already booked " : " It's in the middle of booked process by someone else"));
            return;
        }

        this.reservation = SlotReservation.lock(this, employeeId, Play.application().configuration().getInt("slot.booked.expiration.time"));
        this.unavailable();
    }

    public void unlock(Long employeeId, String token) {
        if (this.reservation == null) {
            Logger.info("Slot " + this.id + " is not locked to make unlock");
            return;
        }
        if (this.reservation.canUnlock(employeeId, token)) {
            reservation.delete();
            available();
        }
    }

    public SlotReservationDTO wrapReservation() {
        SlotReservationDTO dto = reservation.wrap();
        dto.slotId = this.getId();
        return dto;
    }

    public Boolean belongsTo(Long tenantId) {
        return this.institution.getId().equals(tenantId);
    }

    public void unavailable() {
        this.status = SlotStatus.UNAVAILABLE;
        update();
    }

    public void available() {
        this.status = SlotStatus.AVAILABLE;
        this.update();
    }

    public static List<Slot> findAllAvailableByBookAndDateBetween(Long schedulingBookId, DateTime from, DateTime to){
        return finder.where().eq("book.id", schedulingBookId)
                .eq("status", SlotStatus.AVAILABLE)
                .between("startedAt", from, to)
                .setMaxRows(80)
                .findList();
    }

    public void booked() {
        this.status = SlotStatus.BOOKED;
        this.update();
    }

}

package models;

import common.annotations.BaseTenantModelFormatter;
import play.data.validation.Constraints.Required;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("HCPROFESSIONAL")
public class InstitutionHealthCareProfessionalResource extends Resource {

    private static final long serialVersionUID = 1173053341327635628L;


    @OneToOne
    @JoinColumn(name = "IDInstitutionHealthCareProfessional", nullable = true)
    @BaseTenantModelFormatter(clazz = InstitutionHealthCareProfessional.class,
            idFieldIsString = false, idFieldName = "id")
    @Required
    public InstitutionHealthCareProfessional institutionHealthCareProfessional;

  /*
   * Attributes
   */

  /*
   * Constructors
   */

  /*
   * Abstract methods
   */

  /*
   * Get/setters/modifiers
   */


  /*
   * Repository methods
   */
}

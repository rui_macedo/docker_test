package models;

import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import dto.ExamProcedureDTO;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "SchedulingBookExamProcedure", schema = "scheduling")
public class SchedulingBookExamProcedure extends BaseSimpleModel implements PathBindable<SchedulingBookExamProcedure>, QueryStringBindable<SchedulingBookExamProcedure> {

    private static final long serialVersionUID = -6966356062836993028L;
    private static Model.Finder<Long, SchedulingBookExamProcedure> finder = new Model.Finder<Long, SchedulingBookExamProcedure>(Long.class, SchedulingBookExamProcedure.class);
    @Id
    @Column(name = "IDSchedulingBookExamProcedure")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDExamProcedure")
    @BaseTenantModelFormatter(clazz = ExamProcedure.class, idFieldIsString = false, idFieldName = "id")
    public ExamProcedure examProcedure;

    @ManyToOne
    @JoinColumn(name = "IDSchedulingBook")
    @BaseTenantModelFormatter(clazz = SchedulingBook.class, idFieldIsString = false, idFieldName = "id")
    public SchedulingBook schedulingBook;


    public SchedulingBookExamProcedure() {
    }

    public SchedulingBookExamProcedure(ExamProcedure examProcedure) {
        super();
        this.examProcedure = examProcedure;
    }

    public static SchedulingBookExamProcedure findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<SchedulingBookExamProcedure> unwrapByExamProcedure(List<ExamProcedureDTO> examProcedures) {
        List<SchedulingBookExamProcedure> list = new ArrayList<SchedulingBookExamProcedure>();
        if (examProcedures == null) {
            return list;
        }
        for (ExamProcedureDTO exam : examProcedures) {
            list.add(new SchedulingBookExamProcedure(ExamProcedure.findById(exam.id)));
        }
        return list;
    }

    @Override
    public Option<SchedulingBookExamProcedure> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            SchedulingBookExamProcedure obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public SchedulingBookExamProcedure bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }
}

package models;


import common.utils.FormattingUtils;
import play.db.ebean.Model;
import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "SlotRestriction", schema = "scheduling")
public class SlotRestriction extends BaseSimpleModel implements PathBindable<SlotRestriction>, QueryStringBindable<SlotRestriction> {

    private static final long serialVersionUID = -7342598583389116542L;

    public static Model.Finder<Long, SlotRestriction> finder = new Model.Finder<>(Long.class, SlotRestriction.class);

    @Id
    @Column(name = "IDSlotRestriction")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDSlot", nullable = true)
    public Slot slot;

    @ManyToOne
    @JoinColumn(name = "IDRestriction", nullable = true)
    public Restriction restriction;

    public SlotRestriction() {
    }

    public SlotRestriction(Slot slot, Restriction restriction) {
        super();
        this.slot = slot;
        this.restriction = restriction;
    }

    public static SlotRestriction findById(Long id) {
        return id != null ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public F.Option<SlotRestriction> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("slotExamProcedure")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return F.Option.None();
        }
        SlotRestriction obj = findById(new Long(pathId));
        return obj != null ? F.Option.Some(obj) : F.Option.None();
    }

    @Override
    public SlotRestriction bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

}

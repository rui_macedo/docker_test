package models;

import com.avaje.ebean.ExpressionList;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "MedicalCouncil", schema = "global")
public class MedicalCouncil extends BaseSimpleModel implements PathBindable<MedicalCouncil>,
        QueryStringBindable<MedicalCouncil> {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, MedicalCouncil> finder = new Model.Finder<Long, MedicalCouncil>(
            Long.class, MedicalCouncil.class);
    @Id
    @Column(name = "IDMedicalCouncil")
    public Long id;
    @Column(name = "Name", nullable = false, length = 100)
    @Required
    public String name;
    @Column(name = "ShortName", nullable = false, length = 10)
    @Required
    public String shortName;
    @Column(name = "RegisterByState", nullable = false)
    @Required
    public boolean registerByState;


    /*Constructor*/
    public MedicalCouncil(String name, String shortName) {
        super();
        this.name = name;
        this.shortName = shortName;
        this.registerByState = true;
    }

    public static MedicalCouncil findById(Long id) {
        if (id == null) {
            return null;
        }
        return finder.byId(id);
    }

    /**
     * Find all.
     *
     * @param shortname the shortname
     * @param name      the name
     * @return the list
     */
    public static List<MedicalCouncil> findAll(String shortname, String name) {
        if (shortname == null) {
            shortname = new String();
        }
        if (name == null) {
            name = new String();
        }
        shortname = shortname.trim().replace("%", "");
        name = name.trim().replace("%", "");

        ExpressionList<MedicalCouncil> search = null;

        if (!shortname.isEmpty()) {
            search = finder.where().ilike("shortname", shortname + "%");
        }
        if (!name.isEmpty()) {
            search = finder.where().ilike("name", name + "%");
        }


        if (search == null) {
            return finder.findList();
        } else {
            return search.findList();
        }
    }

    public static List<MedicalCouncil> findAllByName(String search) {
        search = search.replace("%", "");
        search = search.trim();
        if (search == null || search.isEmpty()) {
            return finder.findList();
        } else {
            return finder.where().ilike("Name", "%" + search + "%").findList();
        }
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<MedicalCouncil> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("medicalcouncil")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            MedicalCouncil obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public MedicalCouncil bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }


}
 
 
 

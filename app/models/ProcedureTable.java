package models;

import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;
import resource.ProcedureTableResource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Map;

/**
 * Procedure Table contains all the examprocedures for a given contract
 * <p>
 * Its complemented by a list of Indexers and its values.
 */

@Entity
@Table(name = "ProcedureTable", schema = "examprocedure")
public class ProcedureTable extends BaseTenantModel implements PathBindable<ProcedureTable>, QueryStringBindable<ProcedureTable> {

    private static final long serialVersionUID = 1L;

    public static Model.Finder<Long, ProcedureTable> finder = new Model.Finder<>(Long.class, ProcedureTable.class);

    @Id
    @Column(name = "IDProcedureTable")
    public Long id;

    @Column(name = "Name", length = 100, nullable = false)
    @Required
    public String name;

    @Column(name = "PercentageAUX1", columnDefinition = "float default 0")
    private Float percentageAUX1 = 0F;

    @Column(name = "PercentageAUX2", columnDefinition = "float default 0")
    private Float percentageAUX2 = 0F;

    @Column(name = "PercentageAUX3", columnDefinition = "float default 0")
    private Float percentageAUX3 = 0F;

    @Column(name = "PercentagePatientCH", columnDefinition = "float default 0")
    private Float percentagePatientCH = 0F;

    @Column(name = "PercentagePatientCO", columnDefinition = "float default 0")
    private Float percentagePatientCO = 0F;

    @Column(name = "PercentagePatientFIL", columnDefinition = "float default 0")
    private Float percentagePatientFIL = 0F;

    @Column(name = "PercentagePatientMAT", columnDefinition = "float default 0")
    private Float percentagePatientMAT = 0F;

    @Column(name = "PercentagePatientANT", columnDefinition = "float default 0")
    private Float percentagePatientANT = 0F;

    @Column(name = "PercentagePatientOTR", columnDefinition = "float default 0")
    private Float percentagePatientOTR = 0F;

    public ProcedureTable(Institution institution, String name) {
        super(institution);
        this.name = name;
    }

    public static ProcedureTable findById(Long id) {
        return finder.byId(id);
    }

    public static List<ProcedureTable> findAllByInstitution(Long idInstitution) {
        return finder.where().eq("idInstitution", idInstitution).findList();
    }

    public Float getPercentageAUX1() {
        if (percentageAUX1 == null) {
            percentageAUX1 = 0F;
        }
        return percentageAUX1;
    }

    public void setPercentageAUX1(Float percentageAUX1) {
        this.percentageAUX1 = percentageAUX1;
    }

    public Float getPercentageAUX2() {
        if (percentageAUX2 == null) {
            percentageAUX2 = 0F;
        }
        return percentageAUX2;
    }

    public void setPercentageAUX2(Float percentageAUX2) {
        this.percentageAUX2 = percentageAUX2;
    }

    public Float getPercentageAUX3() {
        if (percentageAUX3 == null) {
            percentageAUX3 = 0F;
        }
        return percentageAUX3;
    }

    public void setPercentageAUX3(Float percentageAUX3) {
        this.percentageAUX3 = percentageAUX3;
    }

    public Float getPercentagePatientCH() {
        if (percentagePatientCH == null) {
            percentagePatientCH = 0F;
        }
        return percentagePatientCH;
    }

    public void setPercentagePatientCH(Float percentagePatientCH) {
        this.percentagePatientCH = percentagePatientCH;
    }

    public Float getPercentagePatientCO() {
        if (percentagePatientCO == null) {
            percentagePatientCO = 0F;
        }
        return percentagePatientCO;
    }

    public void setPercentagePatientCO(Float percentagePatientCO) {
        this.percentagePatientCO = percentagePatientCO;
    }

    public Float getPercentagePatientFIL() {
        if (percentagePatientFIL == null) {
            percentagePatientFIL = 0F;
        }
        return percentagePatientFIL;
    }

    public void setPercentagePatientFIL(Float percentagePatientFIL) {
        this.percentagePatientFIL = percentagePatientFIL;
    }

    public Float getPercentagePatientMAT() {
        if (percentagePatientMAT == null) {
            percentagePatientMAT = 0F;
        }
        return percentagePatientMAT;
    }

    public void setPercentagePatientMAT(Float percentagePatientMAT) {
        this.percentagePatientMAT = percentagePatientMAT;
    }

    public Float getPercentagePatientANT() {
        if (percentagePatientANT == null) {
            percentagePatientANT = 0F;
        }
        return percentagePatientANT;
    }

    public void setPercentagePatientANT(Float percentagePatientANT) {
        this.percentagePatientANT = percentagePatientANT;
    }

    public Float getPercentagePatientOTR() {
        if (percentagePatientOTR == null) {
            percentagePatientOTR = 0F;
        }
        return percentagePatientOTR;
    }

    public void setPercentagePatientOTR(Float percentagePatientOTR) {
        this.percentagePatientOTR = percentagePatientOTR;
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<ProcedureTable> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("proceduretable")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return Option.None();
        }
        ProcedureTable obj = findById(new Long(pathId));
        return obj != null ? Option.Some(obj) : Option.None();

    }

    @Override
    public ProcedureTable bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    public ProcedureTableResource wrap() {
        ProcedureTableResource resource = new ProcedureTableResource();
        resource.id = this.id;
        resource.name = this.name;
        resource.percentageAUX1 = this.percentageAUX1;
        resource.percentageAUX2 = this.percentageAUX2;
        resource.percentageAUX3 = this.percentageAUX3;
        resource.percentagePatientANT = this.getPercentagePatientANT();
        resource.percentagePatientCH = this.getPercentagePatientCH();
        resource.percentagePatientCO = this.getPercentagePatientCO();
        resource.percentagePatientFIL = this.getPercentagePatientFIL();
        resource.percentagePatientMAT = this.getPercentagePatientMAT();
        resource.percentagePatientMAT = this.getPercentagePatientMAT();
        resource.percentagePatientOTR = this.getPercentagePatientOTR();
        return resource;
    }


}
 
 

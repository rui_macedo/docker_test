package models;

import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import dto.InsuranceDTO;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "SchedulingBookInsurance", schema = "scheduling")
public class SchedulingBookInsurance extends BaseSimpleModel implements PathBindable<SchedulingBookInsurance>, QueryStringBindable<SchedulingBookInsurance> {

    private static final long serialVersionUID = 5986766481205137991L;
    private static Model.Finder<Long, SchedulingBookInsurance> finder = new Model.Finder<Long, SchedulingBookInsurance>(Long.class, SchedulingBookInsurance.class);
    @Id
    @Column(name = "IDSchedulingBookInsurance")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDInsurance")
    @BaseTenantModelFormatter(clazz = Insurance.class, idFieldIsString = false, idFieldName = "id")
    public Insurance insurance;

    @ManyToOne
    @JoinColumn(name = "IDSchedulingBook")
    @BaseTenantModelFormatter(clazz = SchedulingBook.class, idFieldIsString = false, idFieldName = "id")
    public SchedulingBook schedulingBook;


    public SchedulingBookInsurance(Insurance insurance) {
        super();
        this.insurance = insurance;
    }

    public static SchedulingBookInsurance findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<SchedulingBookInsurance> unwrapByInsurance(List<InsuranceDTO> insurances) {
        List<SchedulingBookInsurance> list = new ArrayList<SchedulingBookInsurance>();
        if (insurances == null) {
            return list;
        }
        for (InsuranceDTO ins : insurances) {
            list.add(new SchedulingBookInsurance(Insurance.findById(ins.id)));
        }
        return list;
    }

    @Override
    public Option<SchedulingBookInsurance> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            SchedulingBookInsurance obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public SchedulingBookInsurance bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }
}

package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pixeon.utils.common.serializers.TimestampDateSerializer;
import dto.PatientDocumentDTO;
import org.joda.time.DateTime;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PatientDocument", schema = "patient")
public class PatientDocument extends BaseSimpleModel {
    private static final Model.Finder<Long, PatientDocument> finder = new Model.Finder<Long, PatientDocument>(
            Long.class, PatientDocument.class);

    private static final long serialVersionUID = 5826936622362667473L;

    @Id
    @Column(name = "IDPatientDocument", nullable = false)
    public Long id;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "IDPatient")
    public Patient patient;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "IDPatientDocumentType")
    public PatientDocumentType type;

    @Column(name = "DocumentNumber", length = 20, nullable = false)
    public String value;

    @JsonSerialize(using = TimestampDateSerializer.class)
    @Column(name = "ExpireDate")
    public DateTime expireDate;

    @Override
    public Long getId() {
        return this.id;
    }

    protected Model.Finder<Long, PatientDocument> getFinder() {
        return finder;
    }


    public static PatientDocument findByPatientAndType(Long id, String type) {
        return finder.where().eq("patient.id", id).eq("type.shortName", type).setMaxRows(1).findUnique();
    }

    public static List<PatientDocument> findByPatient(Long id) {
        return finder.where().eq("patient.id", id).findList();
    }

    public PatientDocumentDTO wrap(){
        PatientDocumentDTO dto = new PatientDocumentDTO();
        dto.number = this.value;
        dto.type = type.shortName;
        return dto;
    }

}

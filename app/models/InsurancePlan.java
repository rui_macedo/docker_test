package models;

import com.avaje.ebean.ExpressionList;
import com.fasterxml.jackson.annotation.JsonBackReference;
import common.annotations.BaseSimpleModelFormatter;
import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import dto.InsurancePlanDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.context.annotation.Lazy;

import play.Logger;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;
import resource.InsurancePlanResource;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "InsurancePlan", schema = "billing")
public class InsurancePlan extends BaseTenantModel implements PathBindable<InsurancePlan>, QueryStringBindable<InsurancePlan> {

    private static final long serialVersionUID = 1L;

    public static Model.Finder<Long, InsurancePlan> finder = new Model.Finder<>(Long.class, InsurancePlan.class);

    @Id
    @Column(name = "IDInsurancePlan")
    public Long id;

    @Column(name = "Name", nullable = false, length = 100)
    @Required
    public String name;

    @Lazy
    @ManyToOne
    @JoinColumn(name = "IDInsurance", nullable = false)
    @BaseTenantModelFormatter(clazz = Insurance.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public Insurance insurance;

    @Lazy
    @ManyToOne
    @JoinColumn(name = "IDInsuranceHealthCarePlan", nullable = true)
    @BaseSimpleModelFormatter(clazz = InsuranceHealthCarePlan.class, idFieldIsString = false, idFieldName = "id")
    public InsuranceHealthCarePlan insuranceHealthCarePlan;

    @Lazy
    @ManyToOne
    @JoinColumn(name = "IDProcedureTable", nullable = false)
    @BaseTenantModelFormatter(clazz = ProcedureTable.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public ProcedureTable procedureTable;


    @Column(name = "Description", nullable = true, length = 300)
    public String description;

    @Column(name = "Enabled", columnDefinition = "bit default 1")
    public Boolean enabled;

    @Column(name = "IsDefault", columnDefinition = "bit default 1")
    public Boolean standard;


    public InsurancePlan(Institution tenant, Insurance insurance) {
        this.insurance = insurance;
        this.institution = tenant;
        this.enabled = true;
    }

    public InsurancePlan(Institution tenant, String name, Insurance insurance) {
        this.insurance = insurance;
        this.institution = tenant;
        this.name = name;
        this.enabled = true;
    }

    private static ExpressionList<InsurancePlan> getDefaultInstitutionQuery(Long institutionId){
        return finder.where().eq("institution.id", institutionId)
                .eq("enabled", true);
    }

    private static ExpressionList<InsurancePlan> getDefaultInsuranceQuery(Long insuranceId){
        return finder.where().eq("insurance.id", insuranceId).eq("enabled", true);
    }


    public static InsurancePlan findById(Long id) {
        return finder.byId(id);
    }

    public static List<InsurancePlan> findAllByInstitution(Long idInstitution, Insurance insurance, String name) {
        ExpressionList<InsurancePlan> search = getDefaultInstitutionQuery(idInstitution).eq("insurance", insurance);

        if (name != null) {
            search = search.icontains("name", name);
        }

        return search.orderBy("name").findList();
    }

    /**
     * Find a list of all possible HealthCarePlans from the given Carrier
     */
    public static List<InsurancePlan> findAllByHealthCareInstitution(Long idInstitution, Insurance insurance, String name) {
        ExpressionList<InsurancePlan> search = finder.where()
                .eq("idInstitution", idInstitution)
                .eq("insurance", insurance)
                .isNotNull("idInsuranceHealthCarePlan");

        if (StringUtils.isNotEmpty(name)) {
            search = search.icontains("name", name);
        }

        return search.orderBy("name").findList();
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<InsurancePlan> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("insurancePlan")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            InsurancePlan obj = findById(new Long(pathId));
            return obj != null ? Option.Some(obj) : Option.None();
        }
        return Option.None();
    }

    @Override
    public InsurancePlan bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    public InsurancePlanDTO wrap() {
        return new InsurancePlanDTO(this.id, this.name);
    }

    public InsurancePlanResource wrapAsResource(){
        InsurancePlanResource resource = new InsurancePlanResource();
        resource.description = this.description;
        resource.enabled = this.enabled;
        resource.id = this.id;
        resource.name = this.name;
        resource.procedureTable = procedureTable.wrap();
        resource.institutionId = this.institution.getId();
        resource.insuranceId = this.insurance != null ? this.insurance.getId(): null;
        return resource;
    }

    public static List<InsurancePlan> findAllEnabledByInsurance(Long insuranceId){
        return getDefaultInsuranceQuery(insuranceId).findList();
    }

    public static InsurancePlan findDefaultPlanByInsurance(Long insuranceId){

        List<InsurancePlan> plans = getDefaultInsuranceQuery(insuranceId).eq("standard", true).findList();

        if(plans.size() > NumberUtils.INTEGER_ONE){
            Logger.error("Insurance {} has more than one default Plan. Returning the first one", insuranceId);
        }

        if(plans.size() == NumberUtils.INTEGER_ZERO ){
            Logger.info("Insurance {} has no default Plan.", insuranceId);
            return null;
        }

        return plans.get(NumberUtils.INTEGER_ZERO);

    }

}
 

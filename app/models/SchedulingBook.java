package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;

import common.annotations.BaseSimpleModelFormatter;
import common.annotations.BaseTenantModelFormatter;
import common.constants.CalendarDay;
import common.constants.SchedulingBookSlotStatus;
import common.utils.FormattingUtils;

import org.apache.commons.lang.math.NumberUtils;

import dto.*;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;
import services.searchers.model.Book;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "SchedulingBook", schema = "scheduling")
public class SchedulingBook extends BaseTenantModel implements PathBindable<SchedulingBook>, QueryStringBindable<SchedulingBook> {
    /*
     * Attributes
     */
    private static final long serialVersionUID = 273183558753774801L;
    private static Model.Finder<Long, SchedulingBook> finder = new Model.Finder<Long, SchedulingBook>(Long.class, SchedulingBook.class);

    @Id
    @Column(name = "IDSchedulingBook")
    public Long id;

    @Column(name = "Name")
    @Required
    public String name;

    @Column(name = "SlotSizeMin")
    public Integer slotSizeMin;

    @OneToMany(mappedBy = "schedulingBook", cascade = CascadeType.ALL)
    @BaseTenantModelFormatter(clazz = SchedulingBookWeekAgenda.class, idFieldIsString = false, idFieldName = "id")
    public Set<SchedulingBookWeekAgenda> schedulingBookWeekAgendas;

    @ManyToOne
    @JoinColumn(name = "IDUnity")
    @BaseTenantModelFormatter(clazz = Unity.class, idFieldIsString = false, idFieldName = "id")
    public Unity unity;

    @ManyToOne
    @JoinColumn(name = "IDPhysician")
    @BaseSimpleModelFormatter(clazz = Physician.class, idFieldIsString = false, idFieldName = "id")
    public Physician physician;

    @Column(name = "Status")
    public Boolean status;

    @Column(name = "Suitable")
    public Boolean suitable;

    @Column(name = "StatusSlots")
    @Enumerated(EnumType.STRING)
    public SchedulingBookSlotStatus statusSlots;

    @ManyToOne
    @JoinColumn(name = "IDEquipment")
    @BaseTenantModelFormatter(clazz = Equipment.class, idFieldIsString = false, idFieldName = "id")
    public Equipment equipment;

    @Column(name = "isDeleted")
    public Boolean isDeleted;

    @OneToMany(mappedBy = "schedulingBook", cascade = CascadeType.ALL)
    @BaseSimpleModelFormatter(clazz = SchedulingBookExamProcedure.class, idFieldIsString = false, idFieldName = "id")
    public List<SchedulingBookExamProcedure> schedulingBookExamProcedures;

    @OneToMany(mappedBy = "schedulingBook", cascade = CascadeType.ALL)
    @BaseSimpleModelFormatter(clazz = SchedulingBookInsurance.class, idFieldIsString = false, idFieldName = "id")
    public List<SchedulingBookInsurance> schedulingBookInsurances;

    @OneToMany(mappedBy = "schedulingBook", cascade = CascadeType.ALL)
    @BaseSimpleModelFormatter(clazz = SchedulingBookInsurancePlan.class, idFieldIsString = false, idFieldName = "id")
    public List<SchedulingBookInsurancePlan> schedulingBookInsurancePlan;

    @OneToMany(mappedBy = "schedulingBook", cascade = CascadeType.ALL)
    @BaseSimpleModelFormatter(clazz = SchedulingBookRestriction.class, idFieldIsString = false, idFieldName = "id")
    public List<SchedulingBookRestriction> schedulingBookRestriction;

  /*
   * Constructors
   */

    public SchedulingBook(Institution institution, String name, Integer slotSizeMin) {
        super(institution);
        this.name = name;
        this.slotSizeMin = slotSizeMin;
    }

    public SchedulingBook() {
    }

    ;

  /*
   * Abstract methods
   */

    /*
     * Repository methods
     */
    public static SchedulingBook findById(Long id) {
        return id != null ? finder.byId(id) : null;
    }

    public static List<SchedulingBook> findByUnities(List<Unity> unities) {
        return finder.where().in("unity", unities).findList();
    }

    public static List<SchedulingBook> findByTenant(Long tenant) {
        return finder.where().eq("institution.id", tenant.toString()).findList();
    }

    /*
     * Override methods
     */
    @Override
    public Option<SchedulingBook> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("examProcedureDictionany")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            SchedulingBook obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public SchedulingBook bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

  /*
   * Get/setters/modifiers
   */

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void addSchedulingBookPeriod(Integer startAt, Integer endAt, CalendarDay dayOfWeek) {
        SchedulingBookWeekAgenda agenda = getWeekAgenda(dayOfWeek);
        if (agenda == null) {
            agenda = new SchedulingBookWeekAgenda(this.institution, this, dayOfWeek);
            agenda.save();
            this.schedulingBookWeekAgendas.add(agenda);
            this.update();
        }
        agenda.addSchedulingBookPeriod(startAt, endAt);
    }

    public SchedulingBookWeekAgenda getWeekAgenda(CalendarDay calendarDay) {
        SchedulingBookWeekAgenda tmpBookAgenda = null;
        for (SchedulingBookWeekAgenda bookAgenda : schedulingBookWeekAgendas) {
            if (bookAgenda.calendarDay.equals(calendarDay)) {
                tmpBookAgenda = bookAgenda;
                break;
            }
        }
        return tmpBookAgenda;
    }

    public SchedulingBookDTO wrap() {
        SchedulingBookDTO dto = new SchedulingBookDTO();
        dto.name = this.name;
        dto.id = this.id;
        dto.unity = unity != null ? unity.wrap() : null;
        return dto;
    }

    public SchedulingBookDTO wrapDetails() {
        SchedulingBookDetailsDTO dto = new SchedulingBookDetailsDTO();
        dto.name = this.name;
        dto.id = this.id;
        dto.unity = unity != null ? unity.wrap() : null;
        dto.equipment = this.equipment != null ? equipment.wrap() : null;
        dto.slotSizeMin = this.slotSizeMin;
        dto.physician = this.physician != null ? this.physician.wrap() : null;
        dto.suitable = this.suitable;
        dto.status = this.status;
        dto.examProcedures = getExamProcedureDTO();
        dto.insurances = getInsuranceDTO();
        dto.insurancePlans = getInsurancePlanDTO();
        dto.restrictions = getRestrictionDTO();
        dto.schedulingBookWeekAgenda = getSchedulingBookWeekAgendaDTO();
        return dto;
    }

    private List<ExamProcedureDTO> getExamProcedureDTO() {
        List<ExamProcedureDTO> list = new ArrayList<>();
        if (this.schedulingBookExamProcedures != null) {
            for (SchedulingBookExamProcedure bookExamProcedure : this.schedulingBookExamProcedures) {
                list.add(bookExamProcedure.examProcedure.wrap());
            }
        }
        return list;
    }

    private List<InsuranceDTO> getInsuranceDTO() {
        List<InsuranceDTO> list = new ArrayList<>();
        if (this.schedulingBookInsurances != null) {
            for (SchedulingBookInsurance bookInsurance : this.schedulingBookInsurances) {
                list.add(bookInsurance.insurance.wrapDTO());
            }
        }
        return list;
    }

    private List<InsurancePlanDTO> getInsurancePlanDTO() {
        List<InsurancePlanDTO> list = new ArrayList<>();
        if (this.schedulingBookInsurancePlan != null) {
            for (SchedulingBookInsurancePlan bookInsurancePlan : this.schedulingBookInsurancePlan) {
                list.add(bookInsurancePlan.insurancePlan.wrap());
            }
        }
        return list;
    }

    private List<RestrictionDTO> getRestrictionDTO() {
        List<RestrictionDTO> list = new ArrayList<>();
        if (this.schedulingBookRestriction != null) {
            for (SchedulingBookRestriction bookRestrictions : this.schedulingBookRestriction) {
                list.add(bookRestrictions.restriction.wrap());
            }
        }
        return list;
    }

    private List<SchedulingBookWeekAgendaDTO> getSchedulingBookWeekAgendaDTO() {
        List<SchedulingBookWeekAgendaDTO> list = new ArrayList<>();
        if (this.schedulingBookWeekAgendas != null) {
            for (SchedulingBookWeekAgenda bookAgenda : this.schedulingBookWeekAgendas) {
                list.add(bookAgenda.wrap());
            }
        }
        return list;
    }

    public void unwrap(SchedulingBookDetailsDTO dto, Long IdInstitution) {
        this.id = dto.id;
        this.name = dto.name;
        this.unity = dto.unity != null ? Unity.findById(dto.unity.id) : null;
        this.equipment = dto.equipment != null ? Equipment.findById(dto.equipment.id) : null;
        this.slotSizeMin = dto.slotSizeMin;
        this.physician = dto.physician != null ? Physician.findById(dto.physician.id) : null;
        this.suitable = dto.suitable == null ? false : dto.suitable;
        this.status = dto.status == null ? false : dto.status;
        this.institution = Institution.byId(IdInstitution);
        this.schedulingBookExamProcedures = SchedulingBookExamProcedure.unwrapByExamProcedure(dto.examProcedures);
        this.schedulingBookInsurances = SchedulingBookInsurance.unwrapByInsurance(dto.insurances);
        this.schedulingBookInsurancePlan = SchedulingBookInsurancePlan.unwrapByInsurancePlan(dto.insurancePlans);
        this.schedulingBookRestriction = SchedulingBookRestriction.unwrapByRestriction(dto.restrictions);
        this.schedulingBookWeekAgendas = SchedulingBookWeekAgenda.unwrap(dto.schedulingBookWeekAgenda, IdInstitution);
    }

    public Boolean belongsTo(Institution institution) {
        return institution.equals(this.institution);
    }

    public void waitingSlots() {
        this.statusSlots = SchedulingBookSlotStatus.WAITING;
        this.update();
    }

    public void processingSlots() {
        this.statusSlots = SchedulingBookSlotStatus.PROCESSING;
        this.update();
    }

    public void successSlots() {
        this.statusSlots = SchedulingBookSlotStatus.SUCCESS;
        this.update();
    }

    public void failedSlots() {
        this.statusSlots = SchedulingBookSlotStatus.FAILED;
        this.update();
    }


    public static List<Book> getBooks(Long institutionId, Long examProcedureId,
                                      Long insuranceId, Long insurancePlanId) {

        String query = "select sb.IDSchedulingBook, sb.SlotSizeMin, p.IDPhysician, p.Name, p.MedicalCouncilRegistration, \n" +
                " u.IDUnity as IDUnity, u.Name as unityName, u.UnityCode as unityCode " +
                "from \n" +
                "   Scheduling.SchedulingBook sb\n" +
                "   join Global.Unity u on u.IDUnity = sb.IDUnity " +
                "   join Scheduling.SchedulingBookExamProcedure sbe on sb.IDSchedulingBook = sbe.IDSchedulingBook\n" +
                "   join Scheduling.SchedulingBookInsurance sbi on sb.IDSchedulingBook = sbi.IDSchedulingBook   \n" +
                "   join Scheduling.SchedulingBookInsurancePlan sbip on sb.IDSchedulingBook = sbip.IDSchedulingBook   \n" +
                "   join Global.Physician p on sb.IDPhysician = p.IDPhysician\n" +
                "where\n" +
                "  sb.IDInstitution = " + institutionId + " and\n" +
                "  sbe.IDExamProcedure = " + examProcedureId + " and\n" +
                "  sbi.IDInsurance = " + insuranceId + " and\n" +
                "  sbip.IDInsurancePlan = " + insurancePlanId;

        List<SqlRow> rows = Ebean.createSqlQuery(query).findList();

        List<Book> books = new ArrayList<>();

        for(SqlRow row : rows){

            books.add(
              new Book(row.getLong("IDSchedulingBook"),
                      new UnityDTO(row.getInteger("IDUnity"), row.getString("unityName"), row.getString("unityCode")),
                      row.getInteger("SlotSizeMin"), Collections.emptyList(),
                      new PhysicianDTO(row.getLong("IDPhysician"), row.getString("Name"), row.getString("MedicalCouncilRegistration")))
            );

        }
        return books;
    }

}

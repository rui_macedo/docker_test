package models;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import resource.TenantResource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Institution", schema = "global")
public class Institution extends BaseSimpleModel {
    /*
     * Attributes
     */
    private static final long serialVersionUID = -5320077601305075791L;
    public static Model.Finder<Long, Institution> finder = new Model.Finder<>(Long.class, Institution.class);
    @Id
    @Column(name = "IDInstitution")
    public Long id;
    @Column(name = "TenantName", nullable = false, unique = true, length = 20)
    @Required
    public String tenantName;
    @Column(name = "BusinessName", length = 100, nullable = false)
    @Required
    public String businessName;
    @Column(name = "BusinessSocialName", length = 100, nullable = false)
    @Required
    public String businessSocialName;
  
/*  @ManyToOne
  @JoinColumn(name = "IDInstitutionGroup")
  public InstitutionGroup group;*/
    @Column(name = "CNPJ", length = 14, nullable = false)
    @Required
    public String cnpj;

    public Institution(String tenantName, String businessName, String businessSocialName, String cnpj) {
        super();
        this.tenantName = tenantName;
        this.businessName = businessName;
        this.businessSocialName = businessSocialName;
        this.cnpj = cnpj;
    }

    Institution() {
        super();
    }

    public static Institution byTenantName(String tenant) {
        return finder.where().eq("tenantName", tenant).findUnique();
    }

    public static Institution byId(Long tenant) {
        return finder.where().eq("id", tenant).findUnique();
    }

    @Override
    public Long getId() {
        return id;
    }

    public TenantResource wrap() {
        TenantResource resource = new TenantResource();
        resource.id = this.id;
        resource.name = this.tenantName;
        return resource;
    }
}

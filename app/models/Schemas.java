package models;

public interface Schemas {

    public String INTEGRATION_DATA = "integrationdata";
    public String BILLING = "billing";
    public String GLOBAL = "global";
    public String EXAM_PROCEDURE = "examprocedure";
    public String REPORT = "report";
    public String PATIENT = "patient";
    public String IMAGING = "imaging";
    public String SCHEDULING = "scheduling";

}




package models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.security.GeneralSecurityException;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "ScheduledOrder", schema = "scheduling")
public class ScheduledOrder extends BaseTenantModel implements PathBindable<ScheduledOrder>,
        QueryStringBindable<ScheduledOrder> {
    /*
     * Attributes
     */
    private static final long serialVersionUID = 3505089357129054138L;
    private static Model.Finder<Long, ScheduledOrder> finder =
            new Model.Finder<Long, ScheduledOrder>(Long.class, ScheduledOrder.class);

    @Id
    @Column(name = "IDScheduledOrder")
    public Long id;

    @OneToMany(mappedBy = "scheduledOrder", cascade = CascadeType.ALL)
    @JsonManagedReference
    @Required
    public Set<ScheduledProcedure> scheduledProcedures;

    @ManyToOne
    @JoinColumn(name = "IDPatient", nullable = false)
    @BaseTenantModelFormatter(clazz = Patient.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public Patient patient;

  /*
   * Constructors
   */

    public ScheduledOrder(Patient patient, Institution institution) throws GeneralSecurityException {
        super();
        if (patient.institution.equals(institution)) {
            this.patient = patient;
            this.institution = institution;
        } else {
            throw new GeneralSecurityException(
                    "Illegal access security: Cant instantiate ScheduledOrder with patient of another tenant");
        }
    }

    public ScheduledOrder() {
        super();
    }

  /*
   * Abstract methods
   */

    /*
     * Get/setters/modifiers
     */
  /*
   * Repository methods
   */
    public static ScheduledOrder findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    /*
     * Override methods
     */
    @Override
    public Option<ScheduledOrder> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            ScheduledOrder obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public ScheduledOrder bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }
}

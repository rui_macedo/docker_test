package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.avaje.ebean.validation.NotNull;
import common.annotations.BaseTenantModelFormatter;
import common.constants.PacsType;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "Pacs", schema = "imaging", uniqueConstraints = @UniqueConstraint(
        columnNames = {"IDUnity"}))
public class Pacs extends BaseTenantModel implements PathBindable<Pacs>, QueryStringBindable<Pacs> {
    private static final long serialVersionUID = -6990728970264670739L;
    private static final Model.Finder<Long, Pacs> finder = new Model.Finder<Long, Pacs>(Long.class,
            Pacs.class);

    @Id
    @Column(name = "IDPacs", nullable = false)
    public Long id;

    // TODO: AETITLE

    @Column(name = "retrieveAETitle", length = 16)
    @Required
    public String retrieveAETitle;
    //
    @Column(name = "ip", length = 50)
    @Required
    public String ip;

    @Column(name = "port", nullable = false)
    @Required
    public int port;

    @Column(name = "host", length = 45, nullable = false)
    public String host;

    @OneToOne
    @Required
    @JoinColumn(name = "IDUnity", nullable = false, unique = true)
    @BaseTenantModelFormatter(clazz = Unity.class, idFieldIsString = false, idFieldName = "id")
    @NotNull
    public Unity unity;

    @Column(name = "CreationDate")
    @Temporal(TemporalType.DATE)
    @CreatedTimestamp
    public Date creationDate;

    @Column(name = "LastUpdate")
    @Temporal(TemporalType.DATE)
    @UpdatedTimestamp
    public Date updateDate;

    @Column(name = "PacsType")
    @Enumerated(EnumType.STRING)
    @Required
    public PacsType type;
    @Column(name = "PacsEnabled", nullable = true)
    public Boolean enabled = true;
    @Column(name = "isAurora")
    private Boolean aurora;

    Pacs() {
    }

    public Pacs(Institution institution, Unity unity, String aeTitle, String ip, Integer port,
                String host, PacsType type, Boolean enabled) {
        super(institution);
        this.unity = unity;
        this.retrieveAETitle = aeTitle;
        this.ip = ip;
        this.port = port;
        this.host = host;
        this.type = type;
        this.aurora = type.equals(PacsType.AURORA) ? true : false;
        this.enabled = enabled;
    }

    public static Pacs findByUnity(Unity unity) {
        return finder.where().eq("unity", unity).findUnique();
    }

    public static List<Pacs> findAllByTenant(Long tenant) {
        return finder.where().eq("institution.id", tenant).findList();
    }

    public Boolean isAurora() {
        return this.aurora;
    }

    public void setType(PacsType type) {
        this.type = type;
        this.aurora = (this.type.equals(PacsType.AURORA)) ? true : false;
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<Pacs> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("pacs")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Pacs obj = finder.byId(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Pacs bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return finder.byId(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    public Boolean canSave() {
        return (finder.where().eq("unity", this.unity).findRowCount() > 0) ? false : true;
    }

    public Boolean canUpdate(Pacs oldPacs) {
        if (this.unity.equals(oldPacs.unity)) {
            return true;
        } else if (this.canSave()) {
            return true;
        } else {
            return false;
        }
    }
}

package models;

import common.annotations.BaseSimpleModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "InsuranceHealthCare", schema = "global")
public class InsuranceHealthCare extends BaseSimpleModel implements PathBindable<InsuranceHealthCare>, QueryStringBindable<InsuranceHealthCare> {

    private static final long serialVersionUID = 1L;
    public static Finder<Long, InsuranceHealthCare> finder = new Finder<Long, InsuranceHealthCare>(
            Long.class, InsuranceHealthCare.class);
    @Id
    @Column(name = "IDInsuranceHealthCare")
    public Long id;
    @Column(name = "IdentificationANS", nullable = false, length = 10)
    @Required
    public String identificationANS;
    @Column(name = "IdentificationCNPJ", nullable = false, length = 20)
    @Required
    public String identificationCNPJ;
    @Column(name = "BusinessName", length = 100, nullable = false)
    @Required
    public String businessName;
    @Column(name = "BusinessSocialName", length = 100, nullable = false)
    @Required
    public String businessSocialName;
    @ManyToOne
    @JoinColumn(name = "IDAddress", nullable = true)
    @BaseSimpleModelFormatter(clazz = Address.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public Address address;
    @Column(name = "AddressNumber", length = 6)
    @Required
    public String addressNumber;
    @Column(name = "AddressComplement", length = 30)
    public String addressComplement;
    @Column(name = "phoneNumber", length = 30)
    public String phoneNumber;
    @Column(name = "faxNumber", length = 30)
    public String faxNumber;
    @Column(name = "email", length = 100)
    public String email;
    @Column(name = "InsuranceHealthCareEnabled", columnDefinition = "bit default 1")
    @Required
    public Boolean insuranceHealthCareEnabled = true;


    /*Constructor*/
    public InsuranceHealthCare(String identificationANS,
                               String identificationCNPJ, String businessName,
                               String businessSocialName) {
        super();
        this.identificationANS = identificationANS;
        this.identificationCNPJ = identificationCNPJ;
        this.businessName = businessName;
        this.businessSocialName = businessSocialName;
        this.insuranceHealthCareEnabled = true;
    }

    public static InsuranceHealthCare findById(Long id) {
        return finder.byId(id);
    }

    public static List<InsuranceHealthCare> findAll() {
        return finder.findList();
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<InsuranceHealthCare> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            InsuranceHealthCare obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public InsuranceHealthCare bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }


}
 
 

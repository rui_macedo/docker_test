package models;

import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PatientDocumentType", schema="patient")
public class PatientDocumentType extends BaseSimpleModel  {

  private static final Model.Finder<Long, PatientDocumentType> finder = new Model.Finder<Long, PatientDocumentType>(
      Long.class, PatientDocumentType.class);

  private static final long serialVersionUID = 639378937834959932L;

  @Id
  @Column(name = "IDPatientDocumentType", nullable = false)
  public Long id;

  @Column(name = "ShortName", length = 10)
  public String shortName;

  @Column(name = "Name", length = 50, nullable = false)
  public String name;

  @Override
  public Number getId() {
    return id;
  }
}

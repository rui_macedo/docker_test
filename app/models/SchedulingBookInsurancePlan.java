package models;

import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import dto.InsurancePlanDTO;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "SchedulingBookInsurancePlan", schema = "scheduling")
public class SchedulingBookInsurancePlan extends BaseSimpleModel implements PathBindable<SchedulingBookInsurancePlan>, QueryStringBindable<SchedulingBookInsurancePlan> {

    private static final long serialVersionUID = 1585597317980807004L;
    private static Model.Finder<Long, SchedulingBookInsurancePlan> finder = new Model.Finder<Long, SchedulingBookInsurancePlan>(Long.class, SchedulingBookInsurancePlan.class);
    @Id
    @Column(name = "IDSchedulingBookInsurancePlan")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDInsurancePlan")
    @BaseTenantModelFormatter(clazz = InsurancePlan.class, idFieldIsString = false, idFieldName = "id")
    public InsurancePlan insurancePlan;

    @ManyToOne
    @JoinColumn(name = "IDSchedulingBook")
    @BaseTenantModelFormatter(clazz = SchedulingBook.class, idFieldIsString = false, idFieldName = "id")
    public SchedulingBook schedulingBook;

    public SchedulingBookInsurancePlan(InsurancePlan insurancePlan) {
        super();
        this.insurancePlan = insurancePlan;
    }

    public static SchedulingBookInsurancePlan findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<SchedulingBookInsurancePlan> unwrapByInsurancePlan(List<InsurancePlanDTO> insurancePlans) {
        List<SchedulingBookInsurancePlan> list = new ArrayList<SchedulingBookInsurancePlan>();
        if (insurancePlans == null) {
            return list;
        }
        for (InsurancePlanDTO ins : insurancePlans) {
            list.add(new SchedulingBookInsurancePlan(InsurancePlan.findById(ins.id)));
        }
        return list;
    }

    @Override
    public Option<SchedulingBookInsurancePlan> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            SchedulingBookInsurancePlan obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public SchedulingBookInsurancePlan bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }
}

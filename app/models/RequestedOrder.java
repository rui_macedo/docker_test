package models;

import common.utils.FormattingUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;

@Entity
@Table(name = "RequestedOrder", schema = "examprocedure")
public class RequestedOrder extends BaseTenantModel implements PathBindable<RequestedOrder>, QueryStringBindable<RequestedOrder> {

    private static final long serialVersionUID = 3729949978299751476L;


    public static Model.Finder<Long, RequestedOrder> finder = new Model.Finder<Long, RequestedOrder>(Long.class, RequestedOrder.class);


    @Id
    @Column(name = "IDRequestedOrder")
    public Long id;

    @Column(name = "AccessionNumber", length = 32)
    public String accessionNumber;

    @Column(name = "DateTime")
    public DateTime dateTime;

    @Column(name = "EnteredBy", length = 50)
    public String enteredBy;


    public RequestedOrder(Institution institution, String accessionNumber, DateTime dateTime, String enteredBy) {
        super(institution);
        this.accessionNumber = accessionNumber;
        this.dateTime = dateTime;
        this.enteredBy = enteredBy;
    }

    public static RequestedOrder findById(Long id) {
        return finder.byId(id);
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    public String getDateTime() {
        if (this.dateTime == null) {
            return null;
        }
        DateTimeFormatter formatDate = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z");
        try {
            return formatDate.print(this.dateTime);
        } catch (Exception e) {
            return "Invalid Date";
        }
    }

    @Override
    public Option<RequestedOrder> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            RequestedOrder obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            }
        }
        return Option.None();
    }

    @Override
    public RequestedOrder bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public Number getId() {
        return this.id;
    }


}

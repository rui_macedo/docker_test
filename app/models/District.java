package models;

import common.annotations.BaseSimpleModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "District", schema = "global")
public class District extends BaseSimpleModel implements PathBindable<District>,
        QueryStringBindable<District> {

    private static final long serialVersionUID = -6663525861061356045L;

    private static Model.Finder<Long, District> finder = new Model.Finder<Long, District>(Long.class, District.class);
    /*
     * Attributes
     */
    @Id
    @Column(name = "IDDistrict")
    public Long id;

    @Column(name = "OriginalId", nullable = true)
    public Long originalId;

    @Column(name = "Name", nullable = true, length = 50)
    public String name;

    @Column(name = "ShortName", nullable = true, length = 30)
    public String shortName;

    @ManyToOne
    @JoinColumn(name = "IDCity", nullable = false)
    @BaseSimpleModelFormatter(clazz = City.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public City city;

    /*
     * Constructors
     */
    public District(Long originalId, String name, City city) {
        this.originalId = originalId;
        this.name = name;
        this.city = city;
    }

    District() {
        super();
    }
  /*
   * Abstract methods
   */


  /*
   * Override methods
   */

    public static District findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<District> findAllByCity(City city) {
        return finder.where().eq("city", city).findList();
    }

    @Override
    public Option<District> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("district")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            District obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public District bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }
  /*
   * Get/setters/modifiers
   */


  /*
   * Repository methods
   */

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }

}

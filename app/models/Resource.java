package models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import common.annotations.BaseTenantModelFormatter;
import common.constants.CalendarDay;
import common.utils.FormattingUtils;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "Resource", schema = "scheduling")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ResourceType")
public class Resource extends BaseTenantModel implements PathBindable<Resource>,
        QueryStringBindable<Resource> {

    private static final long serialVersionUID = -7674776596822130869L;

    /*
     * Attributes
     */
    private static Model.Finder<Long, Resource> finder = new Model.Finder<Long, Resource>(Long.class,
            Resource.class);

    @Id
    @Column(name = "IDResource")
    public Long id;

    @OneToMany(mappedBy = "resource", cascade = CascadeType.ALL)
    @BaseTenantModelFormatter(clazz = ResourceWeekAgenda.class, idFieldIsString = false,
            idFieldName = "id")
    @JsonManagedReference
    public Set<ResourceWeekAgenda> resourceWeekAgendas;


    @ManyToOne
    @JoinColumn(name = "IDUnity")
    @BaseTenantModelFormatter(clazz = Unity.class, idFieldIsString = false, idFieldName = "id")
    public Unity unity;

    public Resource() {

    }

    /*
     * Repository methods
     */
    public static Resource findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<Resource> findByScheduledProcedure(ScheduledProcedure scheduledProcedure) {
        return null;
    }

    @Override
    public Option<Resource> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("resource")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Resource obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Resource bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

  /*
   * Get/setters/modifiers
   */

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }

    public ResourceWeekAgenda getWeekAgendaByCalendarDay(CalendarDay calendarDay) {
        ResourceWeekAgenda selected = null;
        for (ResourceWeekAgenda resourceWeekAgenda : resourceWeekAgendas) {
            if (calendarDay.equals(resourceWeekAgenda.calendarDay)) {
                selected = resourceWeekAgenda;
                break;
            }
        }
        return selected;
    }

    public void addResourceWeekAgenda(CalendarDay calendarDay, int startAt, int endAt) {
        ResourceWeekAgenda agenda = getWeekAgendaByCalendarDay(calendarDay);
        if (agenda == null) {
            agenda = new ResourceWeekAgenda(this.institution, this, calendarDay, startAt, endAt);
            agenda.save();
            this.resourceWeekAgendas.add(agenda);
            this.update();
        }
        agenda.startAt = startAt;
        agenda.endAt = endAt;
        agenda.update();
    }

}

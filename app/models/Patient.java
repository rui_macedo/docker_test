package models;

import com.google.common.base.Strings;

import com.avaje.ebean.ExpressionList;
import common.utils.FormattingUtils;
import dto.PatientDTO;
import forms.voucher.FormPatient;

import org.apache.commons.lang3.StringUtils;
import org.fest.util.Collections;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.data.validation.Constraints.MaxLength;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "Patient", schema = "patient")
public class Patient extends BaseTenantModel implements PathBindable<Patient>,
        QueryStringBindable<Patient> {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, Patient> finder = new Model.Finder<Long, Patient>(
            Long.class, Patient.class);
    @Id
    @Column(name = "IDPatient")
    public Long id;
    @Column(name = "Name")
    @MaxLength(value = 150)
    @Required
    public String name;
    @Column(name = "InternalID")
    @MaxLength(value = 50)
    @Required
    public String internalID;
    @Column(name = "Sex")
    @MaxLength(value = 1)
    public String sex;
    @Column(name = "Active", nullable = false)
    public boolean active;
    @Column(name = "DateOfBirth")
    private DateTime dateOfBirth;

    /*Constructor*/
    public Patient(Institution institution, String name, String internalID) {
        super(institution);
        this.name = name;
        this.internalID = internalID;
    }

    public static Patient findById(Long id) {
        return finder.byId(id);
    }

    public static List<Patient> findAllByInstitution(Long idInstitution, String internalId, String name) {
        if (internalId == null) {
            internalId = new String();
        }
        if (name == null) {
            name = new String();
        }
        internalId = internalId.trim().replace("%", "");
        name = name.trim().replace("%", "");

        ExpressionList<Patient> search = finder.where().eq("idInstitution", idInstitution);

        if (!internalId.isEmpty()) {
            search = search.eq("InternalId", internalId);
        }

        if (!name.isEmpty()) {
            search = search.ilike("name", name + "%");
        }
        return search.findList();
    }

    //Getter and Setter for DateOfBirth in order to Bind Json from a given String
    public String getDateOfBirth() {
        if (this.dateOfBirth == null) {
            return null;
        }
        DateTimeFormatter formatDate = DateTimeFormat.forPattern("dd/MM/yyyy");
        try {
            return formatDate.print(this.dateOfBirth);
        } catch (Exception e) {
            return "Invalid Date";
        }
    }

    //Getter and Setter for DateOfBirth in order to Bind Json from a given String
    public void setDateOfBirth(String dateOfBirth) {
        if (dateOfBirth == null || dateOfBirth.isEmpty()) {
            this.dateOfBirth = null;
            return;
        }

        DateTimeFormatter formatDate = DateTimeFormat.forPattern("dd/MM/yyyy");
        try {
            this.dateOfBirth = formatDate.parseDateTime(dateOfBirth);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<Patient> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("patient")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Patient obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Patient bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    public PatientDTO wrap(){
        PatientDTO dto = new PatientDTO();
        dto.externalId = this.internalID;
        dto.id = this.id;
        dto.name = this.name;
        dto.dateOfBirth = this.dateOfBirth;
        dto.gender = this.sex;
        for (PatientDocument doc : PatientDocument.findByPatient(this.id)) {
            dto.documents.add(doc.wrap());
        }
        PatientContact email = Collections.isEmpty(PatientContact.findEmailsByPatientId(this.id)) ? null : PatientContact.findEmailsByPatientId(this.id).get(0);
        dto.email = Objects.nonNull(email) ? email.value : null;
        return dto;
    }

    public FormPatient wrapFormPatient() {
        final DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
        FormPatient form = new FormPatient();
        form.id = this.id;
        form.name = this.name;
        form.sex = this.sex;
        form.dateOfBirth = Objects.nonNull(this.dateOfBirth) ? fmt.print(this.dateOfBirth) : null;
        Period period = new Period(this.dateOfBirth, new DateTime());
        form.age = period.getYears() + "A" + period.getMonths() + "M";
        PatientDocument rg = PatientDocument.findByPatientAndType(this.id, "rg");
        form.rg = Objects.nonNull(rg) ? rg.value : null;
        form.canonicalName = buildCanonicalName() ;
        return form;
    }

    private String buildCanonicalName(){
        if (StringUtils.isEmpty(this.name)) {
            return StringUtils.EMPTY;
        }

        StringBuilder builder = new StringBuilder();
        builder.append(this.name.substring(0, 1));

        if(StringUtils.containsWhitespace(this.name)){
            builder.append(this.name.split(" ")[1].substring(0,1));
        }

        return builder.toString().toUpperCase();
    }

}
 
 
 
 






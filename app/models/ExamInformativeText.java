package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.ExpressionList;
import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.avaje.ebean.Expr.*;

@Entity
@Table(name = "ExamInformativeText", schema = "examprocedure")
public class ExamInformativeText extends BaseTenantModel implements PathBindable<ExamInformativeText>, QueryStringBindable<ExamInformativeText> {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, ExamInformativeText> finder = new Model.Finder<Long, ExamInformativeText>(Long.class, ExamInformativeText.class);
    @Id
    @Column(name = "IDExamInformativeText")
    public Long id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "IDInformativeText")
    @Required
    @BaseTenantModelFormatter(clazz = InformativeText.class, idFieldIsString = false, idFieldName = "id")
    public InformativeText informativeText;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDExamProcedure", nullable = true)
    @BaseTenantModelFormatter(clazz = ExamProcedure.class, idFieldIsString = false, idFieldName = "id")
    public ExamProcedure examProcedure;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDInsuranceCarrier", nullable = true)
    @BaseTenantModelFormatter(clazz = Insurance.class, idFieldIsString = false, idFieldName = "id")
    public Insurance insurance;

  /*
   * Optionally you can choose about any set of examProcedure, InsuranceCarrier or Plan but at least
   * one of them should be filled
   */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDInsuranceCarrierPlan", nullable = true)
    @BaseTenantModelFormatter(clazz = InsurancePlan.class, idFieldIsString = false, idFieldName = "id")
    public InsurancePlan insurancePlan;

    ExamInformativeText() {
        super();
    }


    /* Constructor */
    public ExamInformativeText(Institution institution, InformativeText informativeText, Insurance insurance, InsurancePlan insurancePlan, ExamProcedure examProcedure) {
        super(institution);
        this.examProcedure = examProcedure;
        this.informativeText = informativeText;
        this.insurance = insurance;
        this.insurancePlan = insurancePlan;
    }

    public static ExamInformativeText findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    /**
     * Find all Informative texts from a given Exam Procedure
     *
     * @param idInstitution
     * @param idExamProcedure
     * @return
     */
    public static List<ExamInformativeText> findAllByInstitution(Long idInstitution, Long idInsuranceCarrier, Long idInsuranceCarrierPlan, List<Long> examProcedures) {
        ExpressionList<ExamInformativeText> query = Ebean.find(ExamInformativeText.class).where();
        query.eq("idInstitution", idInstitution).or(eq("idInsuranceCarrier", idInsuranceCarrier), isNull("idInsuranceCarrier")).or(eq("idInsuranceCarrierPlan", idInsuranceCarrierPlan), isNull("idInsuranceCarrierPlan"));
        if (examProcedures != null && !examProcedures.isEmpty()) {
            query = query.or(in("idExamProcedure", examProcedures), isNull("idExamProcedure"));
        } else {
            query = query.isNull("idExamProcedure");
        }
        HashMap<Long, ExamInformativeText> map = new HashMap<Long, ExamInformativeText>();
        List<ExamInformativeText> listTemp = query.findList();
        for (ExamInformativeText examInformativeText : listTemp) {
            map.put(examInformativeText.informativeText.id, examInformativeText);
        }
        if (listTemp != null) {
            listTemp.clear();
            listTemp.addAll(map.values());
        }
        return listTemp;
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<ExamInformativeText> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("examinformativetext")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            ExamInformativeText obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public ExamInformativeText bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

}

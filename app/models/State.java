package models;

import common.annotations.BaseSimpleModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "State", schema = "global")
public class State extends BaseSimpleModel implements PathBindable<State>,
        QueryStringBindable<State> {

    /*
     * Attributes
     */
    private static final long serialVersionUID = -7121824162686242216L;


    private static Model.Finder<Integer, State> finder = new Model.Finder<Integer, State>(Integer.class, State.class);

    @Id
    @Column(name = "IDState")
    public Integer id;

    @Column(name = "OriginalId", nullable = true)
    public Long originalId;

    @Column(name = "Name", nullable = true, length = 50)
    public String name;

    @Column(name = "ShortName", nullable = true, length = 30)
    public String shortName;

    @ManyToOne
    @JoinColumn(name = "IDCountry", nullable = false)
    @BaseSimpleModelFormatter(clazz = Country.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public Country country;


    /*
     * Constructors
     */
    public State(Long originalId, String name, Country country) {
        this.originalId = originalId;
        this.name = name;
        this.country = country;
    }

    public State() {
        super();
    }
  /*
   * Abstract methods
   */


  /*
   * Override methods
   */

    public static State findById(Integer id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<State> findAllByCountry(Country country) {
        return finder.where().eq("country", country).findList();
    }

    @Override
    public Option<State> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("state")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            State obj = findById(new Integer(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public State bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Integer(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }
  /*
   * Get/setters/modifiers
   */


  /*
   * Repository methods
   */

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Integer getId() {
        return id;
    }
}

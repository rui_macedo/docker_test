package models;

import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "InformativeText", schema = "examprocedure")
public class InformativeText extends BaseTenantModel implements PathBindable<InformativeText>, QueryStringBindable<InformativeText> {

    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, InformativeText> finder = new Model.Finder<Long, InformativeText>(
            Long.class, InformativeText.class);
    @Id
    @Column(name = "IDInformativeText")
    public Long id;
    @Column(name = "Name", length = 50)
    @Required
    public String name;
    @Lob
    @Column(name = "Content")
    public String content;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDInformativeTextType", nullable = false)
    @BaseTenantModelFormatter(clazz = InformativeTextType.class, idFieldIsString = false, idFieldName = "id")
    public InformativeTextType informativeTextType;

    /*Constructor*/
    public InformativeText(Institution institution, String name, InformativeTextType informativeTextType) {
        super(institution);
        this.name = name;
        this.informativeTextType = informativeTextType;
    }

    public static InformativeText findById(Long id) {
        if (id == null) {
            return null;
        }
        return finder.byId(id);
    }

    public static List<InformativeText> findAllByInstitution(Long idInstitution) {
        return finder.where().eq("idInstitution", idInstitution).findList();
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<InformativeText> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("informativetext")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            InformativeText obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public InformativeText bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }


}
 
 
 
 

package models;

import com.avaje.ebean.ExpressionList;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Map;

/**
 * @author rodrigo.silva
 */
@Entity
@Table(name = "InformativeTextType", schema = "examprocedure")
public class InformativeTextType extends BaseTenantModel implements PathBindable<InformativeTextType>,
        QueryStringBindable<InformativeTextType> {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, InformativeTextType> finder = new Model.Finder<Long, InformativeTextType>(
            Long.class, InformativeTextType.class);
    @Id
    @Column(name = "IDInformativeTextType")
    public Long id;
    @Column(name = "Name", length = 50)
    @Required
    public String name;
    @Column(name = "IsPrepare")
    public Boolean isPrepare;

    /*Constructor*/
    public InformativeTextType(Institution institution, String name) {
        super(institution);
        this.name = name;
    }

    public static InformativeTextType findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    /**
     * Search by and InformationType by name or part of it
     *
     * @param idInstitution
     * @param name
     * @return
     */
    public static List<InformativeTextType> findAllByInstitution(Long idInstitution, String name) {
        if (name == null) {
            name = new String();
        }
        name = name.trim().replace("%", "");

        ExpressionList<InformativeTextType> search = finder.where().eq("idInstitution", idInstitution);

        if (!name.isEmpty()) {
            search = search.ilike("name", name + "%");
        }

        return finder.findList();
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<InformativeTextType> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("informativetexttype")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            InformativeTextType obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public InformativeTextType bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }


}
 
 
 
 

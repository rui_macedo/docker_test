package models;

import common.utils.FormattingUtils;
import play.db.ebean.Model;
import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "SlotExamProcedure", schema = "scheduling")
public class SlotExamProcedure extends BaseSimpleModel implements PathBindable<SlotExamProcedure>, QueryStringBindable<SlotExamProcedure> {

    private static final long serialVersionUID = 4122689346720931937L;

    public static Model.Finder<Long, SlotExamProcedure> finder = new Model.Finder<>(Long.class, SlotExamProcedure.class);

    @Id
    @Column(name = "IDSlotExamProcedure")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDSlot", nullable = true)
    public Slot slot;

    @ManyToOne
    @JoinColumn(name = "IDExamProcedure", nullable = true)
    public ExamProcedure examProcedure;

    public SlotExamProcedure() {
    }

    public SlotExamProcedure(Slot slot, ExamProcedure examProcedure) {
        super();
        this.slot = slot;
        this.examProcedure = examProcedure;
    }

    public static SlotExamProcedure findById(Long id) {
        return id != null ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public F.Option<SlotExamProcedure> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("slotExamProcedure")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return F.Option.None();
        }
        SlotExamProcedure obj = findById(new Long(pathId));
        return obj != null ? F.Option.Some(obj) : F.Option.None();
    }

    @Override
    public SlotExamProcedure bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

}

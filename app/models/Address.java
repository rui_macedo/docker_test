package models;

import common.annotations.BaseSimpleModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "Address", schema = "global")
public class Address extends BaseSimpleModel implements PathBindable<Address>,
        QueryStringBindable<Address> {

    /*
     * Attributes
     */
    private static final long serialVersionUID = 1L;

    private static Model.Finder<Long, Address> finder = new Model.Finder<Long, Address>(Long.class,
            Address.class);

    @Id
    @Column(name = "IDAddress")
    public Long id;

    @Column(name = "OriginalId", nullable = true)
    public Long originalId;

    @Column(name = "Name", nullable = true, length = 150)
    public String name;

    @Column(name = "ShortName", nullable = true, length = 100)
    public String shortName;

    @Column(name = "Type", nullable = true, length = 30)
    public String type;

    @Column(name = "PostCode", nullable = true, length = 20)
    public String postalCode;

    @ManyToOne
    @JoinColumn(name = "IDDistrict", nullable = false)
    @BaseSimpleModelFormatter(clazz = District.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public District district;

  /*
   * Constructors
   */

    public Address(Long originalId, String name, String type, String postalCode, District district) {
        this.originalId = originalId;
        this.name = name;
        this.type = type;
        this.postalCode = postalCode;
        this.district = district;
    }

    public Address() {
        super();
    }

  /*
   * Abstract methods
   */


  /*
   * Override methods
   */

    public static Address findById(Long id) {
        return (id != null) ? finder.byId(id) : null;
    }

    public static List<Address> findAllByDistrict(District district) {
        return finder.where().eq("district", district).findList();
    }

    public static List<Address> findByPostalCode(String postalCode) {
        return finder.where().eq("postalCode", postalCode).findList();
    }

    public static List<Address> findLikeAddress(String address) {
        return finder.where().ilike("name", "%" + address + "%").findList();
    }

    public static List<Address> findByCityAndNameLike(City city, String address) {
        return finder.where().eq("district.city", city).ilike("name", "%" + address + "%").findList();
    }

  /*
   * Get/setters/modifiers
   */


  /*
   * Repository methods
   */

    @Override
    public Option<Address> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("address")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Address obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Address bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }
}

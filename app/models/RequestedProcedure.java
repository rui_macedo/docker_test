package models;

import common.utils.FormattingUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "RequestedProcedure", schema = "examprocedure")
public class RequestedProcedure extends BaseTenantModel implements
        PathBindable<RequestedProcedure>, QueryStringBindable<RequestedProcedure> {

    private static final long serialVersionUID = 4659426933070803436L;

    public static Model.Finder<Long, RequestedProcedure> finder =
            new Model.Finder<Long, RequestedProcedure>(Long.class, RequestedProcedure.class);


    @Id
    @Column(name = "IDRequestedProcedure")
    public Long id;

    @Column(name = "RequestedProcedureID", length = 32)
    public String requestedProcedureID;

    @Column(name = "DateTime")
    public DateTime dateTime;

    @ManyToOne
    @JoinColumn(name = "IDExamModality")
    public ExamModality examModality;

    public RequestedProcedure(Institution institution, String requestedProcedureID) {
        super(institution);
        this.requestedProcedureID = requestedProcedureID;
    }

    public RequestedProcedure(Institution institution, String requestedProcedureID, DateTime dateTime) {
        super(institution);
        this.requestedProcedureID = requestedProcedureID;
        this.dateTime = dateTime;
    }

    public static RequestedProcedure findById(Long id) {
        return finder.byId(id);
    }

    public static List<RequestedProcedure> findAllByExamModality(ExamModality modality) {
        return finder.where().eq("examModality", modality).findList();
    }

    @Override
    public Option<RequestedProcedure> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            RequestedProcedure obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            }
        }
        return Option.None();
    }

    @Override
    public RequestedProcedure bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    public String getDateTime() {
        if (this.dateTime == null) {
            return null;
        }
        DateTimeFormatter formatDate = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z");
        try {
            return formatDate.print(this.dateTime);
        } catch (Exception e) {
            return "Invalid Date";
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Number getId() {
        return this.id;
    }

}

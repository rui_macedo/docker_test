package models;

import common.annotations.BaseSimpleModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "InsuranceHealthCarePlan", schema = "global")
public class InsuranceHealthCarePlan extends BaseSimpleModel implements PathBindable<InsuranceHealthCarePlan>, QueryStringBindable<InsuranceHealthCarePlan> {

    private static final long serialVersionUID = 1L;
    public static Finder<Long, InsuranceHealthCarePlan> finder = new Finder<>(Long.class, InsuranceHealthCarePlan.class);
    @Id
    @Column(name = "IDInsuranceHealthCarePlan")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDInsuranceHealthCare", nullable = false)
    @BaseSimpleModelFormatter(clazz = InsuranceHealthCare.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public InsuranceHealthCare insuranceHealthCare;

    @Column(name = "IdentificationANS", nullable = false, length = 10)
    @Required
    public String identificationANS;

    @Column(name = "Name", nullable = false, length = 100)
    @Required
    public String name;

    @Column(name = "Description", length = 300)
    public String description;

    @Column(name = "InsuranceHealthCarePlanEnabled", columnDefinition = "bit default 1")
    public Boolean insuranceHealthCarePlanEnabled = true;


    public InsuranceHealthCarePlan(InsuranceHealthCare insuranceHealthCare, String identificationANS, String name) {
        super();
        this.insuranceHealthCare = insuranceHealthCare;
        this.identificationANS = identificationANS;
        this.name = name;
        this.insuranceHealthCarePlanEnabled = true;
    }

    public static InsuranceHealthCarePlan findById(Long id) {
        return finder.byId(id);
    }

    public static List<InsuranceHealthCarePlan> findAll(InsuranceHealthCare insuranceHealthCare) {
        return finder.where().eq("id", insuranceHealthCare.getId()).findList();
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Option<InsuranceHealthCarePlan> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return Option.None();
        }
        InsuranceHealthCarePlan obj = findById(new Long(pathId));
        return obj != null ? Option.Some(obj) : Option.None();
    }

    @Override
    public InsuranceHealthCarePlan bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }


}
 
 

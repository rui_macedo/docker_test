package models;

import common.utils.FormattingUtils;
import dto.SlotReservationDTO;
import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import play.db.ebean.Model;
import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "SlotReservation", schema = "scheduling")
public class SlotReservation extends BaseSimpleModel implements PathBindable<SlotReservation>, QueryStringBindable<SlotReservation> {

    private static final long serialVersionUID = -7342598583389116542L;

    public static Model.Finder<Long, SlotReservation> finder = new Model.Finder<>(Long.class, SlotReservation.class);

    @Id
    @Column(name = "IDSlotReservation")
    public Long id;

    @Column(name = "Token")
    public String token;

    @OneToOne
    @JoinColumn(name = "IDSlot")
    public Slot slot;

    @Column(name = "IDEmployee")
    public Long employeeId;

    @Column(name = "CreationDate")
    public DateTime creationDate;

    @Column(name = "ExpirationDate")
    public DateTime expirationDate;

    public SlotReservation(Slot slot, Long employeeId) {
        this.employeeId = employeeId;
        this.slot = slot;
    }

    public static SlotReservation findById(Long id) {
        return id != null ? finder.byId(id) : null;
    }

    public static SlotReservation lock(Slot slot, Long employeeId, int expirationTime) {
        SlotReservation reservation = new SlotReservation(slot, employeeId);
        reservation.creationDate = new DateTime(DateTimeZone.UTC);
        reservation.expirationDate = new DateTime(reservation.creationDate).plusMinutes(expirationTime);
        reservation.token = reservation.tokenFormatBuilder();
        reservation.save();
        return reservation;
    }

    public static SlotReservation findBySlot(Slot slot) {
        return finder.where().eq("slot", slot).findUnique();
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public F.Option<SlotReservation> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("slotExamProcedure")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return F.Option.None();
        }
        SlotReservation obj = findById(new Long(pathId));
        return obj != null ? F.Option.Some(obj) : F.Option.None();
    }

    @Override
    public SlotReservation bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

    public Boolean canUnlock(Long employeeId, String token) {
        return this.employeeId.equals(employeeId) && this.token.equals(token);
    }

    public String tokenFormatBuilder() {
        String tokenKey = String.format("{employeeId:%s,creationDate:%s,expirationDate:%s,slotId:%s }",
                employeeId, creationDate, expirationDate, slot.getId());
        return new String(DigestUtils.md5Hex(tokenKey));
    }

    public SlotReservationDTO wrap() {
        return new SlotReservationDTO(token, expirationDate, employeeId);
    }

}


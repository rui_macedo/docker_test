package models;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

import static java.util.Objects.nonNull;

@Entity
@Table(name = "ExamProcedureFinancial", schema = Schemas.SCHEDULING)
public class ExamProcedureFinancial extends BaseSimpleModel  {

    private static Model.Finder<Long, ExamProcedureFinancial> finder = new Model.Finder<>(Long.class, ExamProcedureFinancial.class);

    @Id
    @Column(name = "IDExamProcedureFinancial")
    public Long id;

    @Column(name = "IDInstitution")
    public Long institutionId;

    @Column(name = "IDInsurance")
    public Long insuranceId;

    @Column(name = "IDInsurancePlan")
    public Long insurancePlanId;

    @Column(name = "IDExamProcedure")
    public Long examProcedureId;

    @Column(name = "Price")
    public BigDecimal price;

    @Column(name = "CreationDate")
    public DateTime created;

    @Column(name = "UpdatedDate")
    public DateTime updated;

    public static ExamProcedureFinancial findById(Long id) {
        return nonNull(id) ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public static List<ExamProcedureFinancial> findAllByInsurancePlanAndTenant(Long institutionId, Long insurancePlanId){
        return finder.where().eq("institutionId", institutionId).eq("insurancePlanId", insurancePlanId).findList();
    }

    public static ExamProcedureFinancial findByInsuranceAndPlanAndExamProcedure(Long examProcedureId, Long insuranceId, Long insurancePlanId){
        return finder.where()
                .eq("insuranceId", insuranceId)
                .eq("examProcedureId", examProcedureId)
                .eq("insurancePlanId", insurancePlanId)
                .findUnique();
    }

    public static ExamProcedureFinancial findByInsuranceAndInsurancePlan(Long examProcedureId, Long insuranceId, Long insurancePlanId){
        return finder.where().eq("insuranceId", insuranceId).eq("insurancePlanId", insurancePlanId).eq("examProcedureId", examProcedureId).findUnique();
    }

    @Override
    public String toString() {
        return "ExamProcedureFinancial{" +
                "id=" + id +
                ", institutionId=" + institutionId +
                ", insuranceId=" + insuranceId +
                ", insurancePlanId=" + insurancePlanId +
                ", examProcedureId=" + examProcedureId +
                ", price=" + price +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }
}

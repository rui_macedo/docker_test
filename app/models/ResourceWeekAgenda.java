package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import common.annotations.BaseTenantModelFormatter;
import common.constants.CalendarDay;
import org.joda.time.Interval;
import play.data.validation.Constraints.Required;

import javax.persistence.*;

@Entity
@Table(name = "ResourceWeekAgenda", schema = "scheduling")
public class ResourceWeekAgenda extends BaseTenantModel {

    /*
     * Attributes
     */
    private static final long serialVersionUID = -6380107762646932028L;

    @Id
    @Column(name = "IDResourceWeekAgenda")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDResource")
    @BaseTenantModelFormatter(clazz = Resource.class, idFieldIsString = false, idFieldName = "id")
    @Required
    @JsonBackReference
    public Resource resource;

    @Column(name = "CalendarDay")
    @Enumerated(EnumType.STRING)
    public CalendarDay calendarDay;

    @Column(name = "StartAt", nullable = false)
    Integer startAt;

    @Column(name = "EndAt", nullable = false)
    Integer endAt;

    @Transient
    private Interval zeroEpochInterval;


    /*
     * Constructors
     */
    public ResourceWeekAgenda(Institution institution, Resource resource, CalendarDay calendarDay,
                              Integer startAt, Integer endAt) {
        super(institution);
        this.resource = resource;
        this.calendarDay = calendarDay;
        this.startAt = startAt;
        this.endAt = endAt;
    }

    public ResourceWeekAgenda() {
    }

    ;
  /*
   * Abstract methods
   */


    /*
     * Override methods
     */
    @Override
    public Long getId() {
        return id;
    }

    /*
     * Get/setters/modifiers
     */
    public Interval getZeroEpochInterval() {
        if (this.zeroEpochInterval == null) {
            this.zeroEpochInterval = new Interval(startAt, endAt);
        }
        return zeroEpochInterval;
    }

    public void settInterval(Interval interval) {
        Long st = interval.getStartMillis();
        Long en = interval.getEndMillis();
        if (st > 86400000 || en > 86400000) {
            throw new IllegalArgumentException(
                    "This interval it's only valid to hours in one day. You only can set at max 24 hours (in millis)");
        }
        this.startAt = (int) interval.getStartMillis();
        this.endAt = (int) interval.getEndMillis();
    }

  /*
   * Repository methods
   */
}

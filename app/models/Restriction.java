package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import common.annotations.BaseSimpleModelFormatter;
import common.utils.FormattingUtils;

import dto.PhysicianDTO;
import dto.RestrictionDTO;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;
import services.searchers.model.Book;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "Restriction", schema = "scheduling")
public class Restriction extends BaseTenantModel implements PathBindable<Restriction>, QueryStringBindable<Restriction> {
    private static final long serialVersionUID = 273183558753774801L;
    private static Model.Finder<Long, Restriction> finder = new Model.Finder<Long, Restriction>(Long.class, Restriction.class);

    @Id
    @Column(name = "IDRestriction")
    public Long id;

    @Column(name = "Name")
    @Required
    public String name;

    @Column(name = "isDeleted")
    @Required
    public Boolean isDeleted;

    @OneToMany(mappedBy = "restriction")
    @BaseSimpleModelFormatter(clazz = SchedulingBookRestriction.class, idFieldIsString = false, idFieldName = "id")
    @JsonManagedReference
    public List<SchedulingBookRestriction> schedulingBookRestriction;

    public static Restriction findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<Restriction> findByTenant(Long tenant) {
        return finder.where().eq("institution.id", tenant.toString()).findList();
    }

    public static List<Restriction> findAllByInstitutionId(Long idInstitution) {
        return finder.where().eq("institution.id", idInstitution).eq("isDeleted", false).findList();
    }

    @Override
    public Option<Restriction> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Restriction obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Restriction bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }

    public RestrictionDTO wrap() {
        return new RestrictionDTO(this.id, this.name);
    }

    public void unwrap(RestrictionDTO dto) {
        this.id = dto.id;
        this.name = dto.name;
    }

    public static List<RestrictionDTO> findByBook(Long schedulingBookId){

        String query = "select r.IDRestriction, r.Name\t  \n" +
                "from \n" +
                "   Scheduling.SchedulingBookRestriction sbr\n" +
                "   join Scheduling.Restriction r on sbr.IDRestriction = r.IDRestriction\n" +
                "where\n" +
                "   sbr.IDSchedulingBook = "  + schedulingBookId + " and\n" +
                "   r.isDeleted = 0";

        List<SqlRow> rows = Ebean.createSqlQuery(query).findList();

        List<RestrictionDTO> restrictions = new ArrayList<>();

        for(SqlRow row : rows){
            restrictions.add(
                    new RestrictionDTO(row.getLong("IDRestriction"), row.getString("Name"))
            );
        }
        return restrictions;
    }

    public static List<Restriction> findByIds(List<Long> ids){
        return finder.where().in("id", ids).findList();
    }
}

package models;

import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.Map;

@Entity
@Table(name = "ScheduledProcedureResource", schema = "scheduling")
public class ScheduledProcedureResource extends BaseTenantModel implements
        PathBindable<ScheduledProcedureResource>, QueryStringBindable<ScheduledProcedureResource> {
    private static final long serialVersionUID = -5947397536205423571L;
    /*
     * Attributes
     */
    private static Model.Finder<Long, ScheduledProcedureResource> finder =
            new Model.Finder<Long, ScheduledProcedureResource>(Long.class,
                    ScheduledProcedureResource.class);
    @Id
    @Column(name = "IDScheduleProcedureResource")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDResource")
    @BaseTenantModelFormatter(clazz = Resource.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public Resource resource;

    @ManyToOne
    @JoinColumn(name = "IDSchedulingBook")
    @BaseTenantModelFormatter(clazz = SchedulingBook.class, idFieldIsString = false, idFieldName = "id")
    @NotNull
    public SchedulingBook schedulingBook;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDScheduledProcedure")
    @BaseTenantModelFormatter(clazz = ScheduledProcedure.class, idFieldIsString = false,
            idFieldName = "id")
    @Required
    public ScheduledProcedure scheduledProcedure;

    @Column(name = "ScheduledDate", nullable = false)
    public DateTime scheduledDate;

    @Column(name = "StartAt", nullable = false)
    Integer startAt;

    @Column(name = "EndAt", nullable = false)
    Integer endAt;

    @Transient
    private Interval zeroEpochInterval;

    /*
     * Constructors
     */
    public ScheduledProcedureResource() {
        super();
    }

    public ScheduledProcedureResource(Resource resource, ScheduledProcedure scheduledProcedure, SchedulingBook book,
                                      DateTime startDate, Interval interval) throws GeneralSecurityException {
        if (resource.institution.equals(scheduledProcedure.institution)) {
            this.schedulingBook = book;
            this.resource = resource;
            this.institution = resource.institution;
            this.scheduledProcedure = scheduledProcedure;
            this.scheduledDate = startDate;
            this.zeroEpochInterval = interval;
            this.startAt = (int) interval.getStartMillis();
            this.endAt = (int) interval.getEndMillis();
        } else {
            throw new GeneralSecurityException("Illegal access security: Cant instantiate ScheduledProcedureResource with resource of another tenant");
        }
    }

  /*
   * Abstract methods
   */

    /*
     * Repository methods
     */
    public static ScheduledProcedureResource findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static Collection<ScheduledProcedureResource> findScheduledProcedureResourceByResource(
            Resource resource, Interval interval) {
        return finder.where().eq("resource", resource).between("scheduledDate", interval.getStart(), interval.getEnd()).findList();
    }

    public static Collection<ScheduledProcedureResource> findScheduledProcedureResourceByResource(Resource resource) {
        return finder.where().eq("resource", resource).findList();
    }

    public static Collection<ScheduledProcedureResource> findDaylyScheduledProcedureResourceByResource(Resource resource, DateTime reference) {
        return finder.where().eq("resource", resource).between("scheduledDate", reference.withTimeAtStartOfDay(), reference.withTime(23, 59, 59, 999)).findList();
    }

    public static Collection<ScheduledProcedureResource> findScheduledProcedureResourceByScheduledProcedure(ScheduledProcedure scheduledProcedure) {
        return finder.where().eq("scheduledProcedure", scheduledProcedure).findList();
    }

  /*
   * Get/setters/modifiers
   */

    /*
     * Override methods
     */
    @Override
    public Option<ScheduledProcedureResource> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            ScheduledProcedureResource obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public ScheduledProcedureResource bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }

    public Interval getZeroEpochInterval() {
        if (this.zeroEpochInterval == null) {
            this.zeroEpochInterval = new Interval(startAt, endAt);
        }
        return zeroEpochInterval;
    }

    public void settInterval(Interval interval) {
        Long st = interval.getStartMillis();
        Long en = interval.getEndMillis();
        if (st > 86400000 || en > 86400000) {
            throw new IllegalArgumentException(
                    "This interval it's only valid to hours in one day. You only can set at max 24 hours (in millis)");
        }
        this.startAt = (int) interval.getStartMillis();
        this.endAt = (int) interval.getEndMillis();
    }
}

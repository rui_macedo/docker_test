package models;

import common.utils.FormattingUtils;

import dto.ExamModalityDTO;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "ExamModality", schema = "examprocedure")
public class ExamModality extends BaseTenantModel implements PathBindable<ExamModality>,
        QueryStringBindable<ExamModality> {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, ExamModality> finder = new Model.Finder<Long, ExamModality>(
            Long.class, ExamModality.class);
    @Id
    @Column(name = "IDExamModality")
    public Long id;
    @Column(name = "Name", length = 100)
    public String name;
    @Column(name = "ShortName", length = 50)
    public String shortName;

    /*Constructor*/
    public ExamModality(Institution institution) {
        super(institution);
    }

    public static ExamModality findById(Long id) {
        return id == null ? null : finder.byId(id);
    }

    public static List<ExamModality> findAllByInstitution(Long idInstitution) {
        return finder.where().eq("idInstitution", idInstitution).findList();
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<ExamModality> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("exammodality")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            ExamModality obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public ExamModality bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    public List<RequestedProcedure> findRequestedProcedures() {
        return RequestedProcedure.findAllByExamModality(this);
    }

    public List<ExamProcedure> findExamProcedures() {
        return ExamProcedure.findAllByExamModality(this);
    }

    public ExamModalityDTO wrap(){
        ExamModalityDTO dto = new ExamModalityDTO();
        dto.id = this.id;
        dto.name = this.name;
        dto.shortName = this.shortName;
        return dto;
    }
}







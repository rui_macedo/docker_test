package models;

import common.utils.FormattingUtils;
import org.joda.time.DateTime;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "Visit", schema = "examprocedure")
public class Visit extends BaseTenantModel implements PathBindable<Visit>, QueryStringBindable<Visit> {

    private static final long serialVersionUID = 1L;

    private static Model.Finder<Long, Visit> finder = new Model.Finder<Long, Visit>(Long.class, Visit.class);

    @Id
    @Column(name = "IDVisit")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDPatient")
    public Patient patient;

    @Column(name = "datetime")
    public DateTime dateTime;

    public Visit() {

    }

    public Visit(Institution institution, Patient patient, DateTime dateTime) {
        super(institution);
        this.patient = patient;
        this.dateTime = dateTime;
    }

    public static Visit findById(Long id) {
        return finder.byId(id);
    }

    @Override
    public Option<Visit> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return Option.None();
        }
        Visit obj = findById(new Long(pathId));
        return obj != null ? Option.Some(obj) : Option.None();
    }

    @Override
    public Visit bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;

    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Number getId() {
        return this.id;
    }

}

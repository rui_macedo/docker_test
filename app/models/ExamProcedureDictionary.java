package models;

import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "ExamProcedureDictionary", schema = "integration")
public class ExamProcedureDictionary extends BaseTenantModel implements PathBindable<ExamProcedureDictionary>, QueryStringBindable<ExamProcedureDictionary> {

    private static final long serialVersionUID = -349104773834249351L;

    private static Model.Finder<Long, ExamProcedureDictionary> finder = new Model.Finder<Long, ExamProcedureDictionary>(Long.class, ExamProcedureDictionary.class);

    @Id
    @Column(name = "IDExamProcedureDictionary")
    public Long id;

    @Column(name = "Word")
    public String word;

    @Required
    @ManyToOne
    @JoinColumn(name = "IDExamProcedure", nullable = false)
    @BaseTenantModelFormatter(clazz = ExamProcedure.class, idFieldIsString = false, idFieldName = "id")
    public ExamProcedure examProcedure;

    public ExamProcedureDictionary(String word, ExamProcedure examProcedure) {
        this.word = word;
        this.examProcedure = examProcedure;
    }

    ExamProcedureDictionary() {
        super();
    }

    public static ExamProcedureDictionary findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<ExamProcedureDictionary> findAllByInstitution(Long idInstitution) {
        return idInstitution != null ? finder.where().eq("institution.id", String.valueOf(idInstitution)).findList() : null;
    }

    public Number getExamProcedure() {
        return examProcedure.getId();
    }

    @Override
    public Option<ExamProcedureDictionary> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("examproceduredictionary")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            ExamProcedureDictionary obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public ExamProcedureDictionary bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }
}

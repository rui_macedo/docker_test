package models;

import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Minutes;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.security.GeneralSecurityException;
import java.util.Map;


@Entity
@Table(name = "ScheduledProcedureSchedulingBook", schema = "scheduling")
public class ScheduledProcedureSchedulingBook extends BaseTenantModel implements
        PathBindable<ScheduledProcedureSchedulingBook>,
        QueryStringBindable<ScheduledProcedureSchedulingBook> {

    private static final long serialVersionUID = -5947397536205423571L;
    /*
     * Attributes
     */
    private static Model.Finder<Long, ScheduledProcedureSchedulingBook> finder =
            new Model.Finder<Long, ScheduledProcedureSchedulingBook>(Long.class,
                    ScheduledProcedureSchedulingBook.class);
    @Id
    @Column(name = "IDScheduledProcedureSchedulingBook")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDSchedulingBook")
    @BaseTenantModelFormatter(clazz = SchedulingBook.class, idFieldIsString = false,
            idFieldName = "id")
    @Required
    public SchedulingBook schedulingBook;

    @OneToOne
    @JoinColumn(name = "IDScheduledProcedure")
    @BaseTenantModelFormatter(clazz = ScheduledProcedure.class, idFieldIsString = false,
            idFieldName = "id")
    @Required
    public ScheduledProcedure scheduledProcedure;

    @Column(name = "ScheduledDate", nullable = false)
    public DateTime scheduledDate;
    @Column(name = "DurationMinutes", nullable = false)
    public Integer durationMinutes;
    @Column(name = "StartAt", nullable = false)
    Integer startAt;
    @Column(name = "EndAt", nullable = false)
    Integer endAt;
    @Transient
    private Interval zeroEpochInterval;

    /*
     * Constructors
     */
    public ScheduledProcedureSchedulingBook() {
        super();
    }

    public ScheduledProcedureSchedulingBook(SchedulingBook schedulingBook,
                                            ScheduledProcedure scheduledProcedure, DateTime referenceDate, Interval interval)
            throws GeneralSecurityException {
        if (schedulingBook.institution.equals(scheduledProcedure.institution)) {
            this.schedulingBook = schedulingBook;
            this.institution = schedulingBook.institution;
            this.scheduledProcedure = scheduledProcedure;
            this.scheduledDate = referenceDate;
            this.startAt = (int) interval.getStartMillis();
            this.endAt = (int) interval.getEndMillis();
            this.zeroEpochInterval = interval;
            this.durationMinutes = Minutes.minutesIn(interval).getMinutes();
        } else {
            throw new GeneralSecurityException(
                    "Illegal Access: Cant instantiate ScheduledProcedureSchedulingBook with book of another tenant");
        }
    }

  /*
   * Abstract methods
   */

    /*
     * Repository methods
     */
    public static ScheduledProcedureSchedulingBook findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static ScheduledProcedureSchedulingBook findByScheduledProcedure(
            ScheduledProcedure scheduledProcedure) {
        return finder.where().eq("scheduledProcedure", scheduledProcedure).findUnique();
    }

    /*
     * Override methods
     */
    @Override
    public Option<ScheduledProcedureSchedulingBook> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            ScheduledProcedureSchedulingBook obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public ScheduledProcedureSchedulingBook bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }

    /*
     * Get/setters/modifiers
     */
    public Interval getZeroEpochInterval() {
        if (this.zeroEpochInterval == null) {
            this.zeroEpochInterval = new Interval(startAt, endAt);
        }
        return zeroEpochInterval;
    }

    public void settInterval(Interval interval) {
        Long st = interval.getStartMillis();
        Long en = interval.getEndMillis();
        if (st > 86400000 || en > 86400000) {
            throw new IllegalArgumentException(
                    "This interval it's only valid to hours in one day. You only can set at max 24 hours (in millis)");
        }
        this.startAt = (int) interval.getStartMillis();
        this.endAt = (int) interval.getEndMillis();
    }
}

package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import com.avaje.ebean.Query;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.List;
//import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * @author abner.rolim
 * @{http://www.avaje.org/occ.html
 * @{http://blog.matthieuguillermin.fr/2012/11/ebean-and-the-optimisticlockexception/
 */
@MappedSuperclass
public abstract class BaseSimpleModel extends Model implements Serializable {
    /*
     * Attributes
     */
    private static final long serialVersionUID = 1L;

    @Version
    @Column(name = "Version")
    //@JsonIgnore
    public int version;

    /*
     * Constructors
     */
    public BaseSimpleModel() {
        super();
    }

  /*
   * Abstract methods
   */

    public static <T extends BaseSimpleModel> Page<T> findAll(Class<T> clazz, int pageSize, int page) {
        Query<T> query = Ebean.createQuery(clazz);
        return query.findPagingList(pageSize).getPage(page);
    }
  /*
   * Override methods
   */

    public static <T extends BaseSimpleModel> List<T> findAll(Class<T> clazz) {
        Query<T> query = Ebean.createQuery(clazz);
        return query.findList();
    }

    /**
     * Find all with field to order with asc
     */
    public static <T extends BaseSimpleModel> Page<T> findAll(Class<T> clazz, int pageSize, int page,
                                                              String fieldToOrder) {
        Query<T> query = Ebean.createQuery(clazz).order().asc(fieldToOrder);
        return query.findPagingList(pageSize).getPage(page);
    }

    public abstract Number getId();

  
  /*
   * Get/setters/modifiers
   */


  /*
   * Repository methods
   */

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    /**
     * @see Object#equals(java.lang.Object)
     */
    public boolean equals(Object object) {
        return object instanceof Serializable && equals((BaseSimpleModel) object);
    }

    public boolean equals(BaseSimpleModel baseModel) {
        if (this == baseModel) {
            return true;
        }

        return baseModel == null ? false
                : (baseModel.getId() == null || baseModel.getClass() == null ? this.getId() == null
                && this.getClass() == null : baseModel.getId().equals(this.getId())
                && this.getClass().equals(baseModel.getClass()));
    }

}

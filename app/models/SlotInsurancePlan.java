package models;

import common.utils.FormattingUtils;
import play.db.ebean.Model;
import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "SlotInsurancePlan", schema = "scheduling")
public class SlotInsurancePlan extends BaseSimpleModel implements PathBindable<SlotInsurancePlan>, QueryStringBindable<SlotInsurancePlan> {

    private static final long serialVersionUID = 8460984853967220603L;

    public static Model.Finder<Long, SlotInsurancePlan> finder = new Model.Finder<>(Long.class, SlotInsurancePlan.class);

    @Id
    @Column(name = "IDSlotInsurancePlan")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDSlot", nullable = true)
    public Slot slot;

    @ManyToOne
    @JoinColumn(name = "IDInsurancePlan", nullable = true)
    public InsurancePlan insurancePlan;

    public SlotInsurancePlan() {
    }

    public SlotInsurancePlan(Slot slot, InsurancePlan insurancePlan) {
        super();
        this.slot = slot;
        this.insurancePlan = insurancePlan;
    }

    public static SlotInsurancePlan findById(Long id) {
        return id != null ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public F.Option<SlotInsurancePlan> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("slotInsurancePlan")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return F.Option.None();
        }
        SlotInsurancePlan obj = findById(new Long(pathId));
        return obj != null ? F.Option.Some(obj) : F.Option.None();
    }

    @Override
    public SlotInsurancePlan bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

}

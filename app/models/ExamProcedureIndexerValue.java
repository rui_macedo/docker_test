package models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import common.utils.FormattingUtils;
import utils.JsonFloatSerializer;

public class ExamProcedureIndexerValue {

    @JsonSerialize(using = JsonFloatSerializer.class)
    private float qtde;

    private IndexerValue indexerValue;

    @JsonSerialize(using = JsonFloatSerializer.class)
    private float percentageIndexerValue = 100;

    @JsonSerialize(using = JsonFloatSerializer.class)
    private float patientValue = 0F;

    @JsonSerialize(using = JsonFloatSerializer.class)
    private float carrierValue = 0F;


    public IndexerValue getIndexerValue() {
        return this.indexerValue;
    }

    public void setIndexerValue(IndexerValue indexerValue) {
        this.indexerValue = indexerValue;
    }

    public float getQtde() {
        return FormattingUtils.formatFloatNumber(this.qtde, 4);
    }

    public void setQtde(float qtde) {
        this.qtde = FormattingUtils.formatFloatNumber(qtde, 4);
    }

    public float getPercentageIndexerValue() {
        return FormattingUtils.formatFloatNumber(this.percentageIndexerValue, 2);
    }

    public void setPercentageIndexerValue(float percentageIndexerValue) {
        this.percentageIndexerValue = FormattingUtils.formatFloatNumber(percentageIndexerValue, 2);
    }

    public float getPatientValue() {
        return FormattingUtils.formatFloatNumber(this.patientValue, 2);
    }

    public void setPatientValue(float patientValue) {
        this.patientValue = FormattingUtils.formatFloatNumber(patientValue, 2);
    }

    public float getCarrierValue() {
        return FormattingUtils.formatFloatNumber(this.carrierValue, 2);
    }

    public void setCarrierValue(float carrierValue) {
        this.carrierValue = FormattingUtils.formatFloatNumber(carrierValue, 2);
    }


}

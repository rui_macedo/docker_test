package models;

import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "Section", schema = "global")
public class Section extends BaseTenantModel implements PathBindable<Section>,
        QueryStringBindable<Section> {

  /*
   * Attributes
   */

    private static final long serialVersionUID = -9170679200673143226L;

    private static Model.Finder<Integer, Section> finder = new Model.Finder<Integer, Section>(
            Integer.class, Section.class);

    @Id
    @Column(name = "IDSection")
    public Integer id;

    @Column(name = "Name", length = 50, nullable = false)
    @Required
    public String name;

    @ManyToOne
    @JoinColumn(name = "IDUnity", nullable = false)
    @BaseTenantModelFormatter(clazz = Unity.class, idFieldIsString = false, idFieldName = "id")
    public Unity unity;

    /*
     * Constructors
     */
    public Section() {
        super();
    }

    public Section(Institution institution, String name) {
        super(institution);
        this.name = name;
        this.institution = institution;
    }

    public Section(Unity unity, String name) {
        this.name = name;
        this.institution = unity.institution;
        this.unity = unity;
    }

  /*
   * Abstract methods
   */


  /*
   * Override methods
   */

    /*
     * Repository methods
     */
    public static Section findById(Integer id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<Section> findAllByTenant(Long tenantId) {
        return finder.where().eq("institution.id", tenantId).findList();
    }

    public static List<Section> findAllByUnity(Unity unity) {
        if (unity != null) {
            return finder.where().eq("unity", unity).findList();
        }
        return Collections.emptyList();
    }

    @Override
    public Option<Section> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("section")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Section obj = findById(new Integer(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Section bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Integer(arg1));
        } else {
            return null;
        }
    }

  /*
   * Get/setters/modifiers
   */

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Integer getId() {
        return id;
    }

}

package models;

import com.avaje.ebean.ExpressionList;
import common.annotations.BaseTenantModelFormatter;
import common.constants.IndexerType;
import common.utils.FormattingUtils;
import org.joda.time.DateTime;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;


@Entity
@Table(name = "Indexer", schema = "global")
public class Indexer extends BaseTenantModel implements PathBindable<Indexer>,
        QueryStringBindable<Indexer> {
    /**
     *
     */

    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, Indexer> finder = new Model.Finder<Long, Indexer>(
            Long.class, Indexer.class);
    @Id
    @Column(name = "IDIndexer")
    public Long id;
    @Column(name = "Name", length = 50, nullable = false)
    @Required
    public String name;
    /* Procedure table owner */
    @ManyToOne(fetch = FetchType.LAZY)
    @Required
    @JoinColumn(name = "IDProcedureTable", nullable = false)
    @BaseTenantModelFormatter(clazz = ProcedureTable.class, idFieldIsString = false,
            idFieldName = "id")
    public ProcedureTable procedureTable;
    //Indicate the Type of Indexer to be allocated.(CH, CO, FIL, MAT, OUT, ANT)
    @Column(name = "IndexerType", length = 3, nullable = true)
    private IndexerType indexerType;

    /*Constructor*/
    public Indexer(Institution institution, ProcedureTable procedureTable, String name) {
        super(institution);
        this.procedureTable = procedureTable;
        this.name = name;
    }

    public static Indexer findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<Indexer> findAllByInstitution(Long idInstitution, ProcedureTable procedureTable, String name) {
        if (name == null) {
            name = new String();
        }
        name = name.trim().replace("%", "");

        ExpressionList<Indexer> search =
                finder.where()
                        .eq("idInstitution", idInstitution)
                        .eq("idProcedureTable", procedureTable.id);

        if (!name.isEmpty()) {
            search = search.ilike("name", name + "%");
        }
        return search.findList();
    }

    public String getIndexerType() {
        return indexerType.getShortName();
    }

    public void setIndexerType(String indexerType) {
        this.indexerType = IndexerType.getIndexerType(indexerType);
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<Indexer> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("indexer")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Indexer obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Indexer bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    /*Return the value of the Indexer in a given Data*/
    public IndexerValue getIndexerValue(DateTime date) {

        IndexerValue indexerValue = IndexerValue.finder
                .where()
                .eq("idInstitution", this.institution.getId())
                .eq("idIndexer", this.id)
                .lt("COALESCE(validFromDate,0)", date)
                .orderBy("validFromDate desc")
                .setMaxRows(1)
                .findUnique();

        if (indexerValue == null) {
            return null;
        }

        return indexerValue;
    }


}
 
 
 
 
 
 

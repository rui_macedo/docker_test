package models;

import com.avaje.ebean.ExpressionList;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import common.annotations.BaseSimpleModelFormatter;
import common.annotations.BaseTenantModelFormatter;
import common.constants.InsurancePaymentType;
import common.utils.FormattingUtils;
import dto.InsuranceDTO;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import play.Logger;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;
import queries.InsuranceQuery;
import resource.InsuranceResource;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

import static java.util.Objects.nonNull;

@Entity
@Table(name = "Insurance", schema = Schemas.BILLING)
public class Insurance extends BaseTenantModel implements PathBindable<Insurance>, QueryStringBindable<Insurance> {

    private static final long serialVersionUID = 1L;

    public static Model.Finder<Long, Insurance> finder = new Model.Finder<>(Long.class, Insurance.class);

    @Id
    @Column(name = "IDInsurance")
    public Long id;

    @Column(name = "Name", nullable = false, length = 100)
    @Required
    public String name;

    @ManyToOne
    @JoinColumn(name = "IDProcedureTable", nullable = false)
    @BaseTenantModelFormatter(clazz = ProcedureTable.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public ProcedureTable procedureTable;

    @ManyToOne
    @JoinColumn(name = "IDInsuranceHealthCare", nullable = true)
    @BaseSimpleModelFormatter(clazz = InsuranceHealthCare.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public InsuranceHealthCare insuranceHealthCare;

    @Column(name = "PaymentType")
    @Enumerated(EnumType.STRING)
    public InsurancePaymentType paymentType;

    @Column(name = "IdentificationProvider", length = 30)
    public String identificationProvider;

    @Column(name = "Description", nullable = true, length = 300)
    @Required
    public String description;

    @Column(name = "Enabled", columnDefinition = "bit default 1")
    @Required
    public Boolean enabled;

    @Column(name = "IsDEleted", columnDefinition = "bit default 1")
    @Required
    public Boolean deleted;

    @OneToMany(mappedBy = "insurance")
    @JsonManagedReference
    @JsonIgnore
    public List<InsurancePlan> insurancePlans;

    public Insurance(Institution institution, String description) {
        super(institution);
        this.description = description;
        this.enabled = true;
        this.deleted = false;
    }

    public Insurance(Institution institution, String name, String description) {
        super(institution);
        this.description = description;
        this.name = name;
        this.enabled = true;
        this.deleted = false;
    }

    public Insurance(Institution institution, InsuranceHealthCare insuranceHealthCare, String identificationProvider, String description) {
        super(institution);
        this.name = insuranceHealthCare.businessName;
        this.insuranceHealthCare = insuranceHealthCare;
        this.identificationProvider = identificationProvider;
        this.description = description;
        this.enabled = true;
        this.deleted = false;
    }

    public static Insurance findById(Long id) {
        return id != null ? finder.byId(id) : null;
    }

    private static ExpressionList<Insurance> getDefaultQuery(Long institutionId){
        return finder.where().eq("institution.id", institutionId)
                .eq("enabled", true)
                .eq("isDeleted", false);
    }

    public static List<Insurance> findAllByTenant(Long tenantId, InsuranceQuery query) {
        ExpressionList<Insurance> search = getDefaultQuery(tenantId);
        if (nonNull(query)) {
            if (StringUtils.isNotEmpty(query.name)) {
                search = search.icontains("name", query.name);
            }
            if (nonNull(query.enabled)) {
                search = search.eq("enabled", query.enabled);
            }
            if (nonNull(query.paymentType)) {
                search = search.eq("paymentType", query.paymentType);
            }
            if(BooleanUtils.isTrue(query.hasPlans)){
                search = search.raw(" exists( select 1 from Billing.InsurancePlan ip where ip.IDInsurance = id) ");
            }
        }
        return search.findList();
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Option<Insurance> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("insurance")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return Option.None();
        }
        Insurance obj = findById(new Long(pathId));
        return obj != null ? Option.Some(obj) : Option.None();
    }

    @Override
    public Insurance bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

    public InsuranceResource wrap() {
        InsuranceResource resource = new InsuranceResource();
        resource.description = this.description;
        resource.institutionId = institution.getId();
        resource.id = this.id;
        resource.isEnabled = this.enabled;
        resource.name = this.name;
        resource.procedureTable = nonNull(this.procedureTable) ? this.procedureTable.wrap() : null;
        resource.paymentType = this.paymentType;
        return resource;
    }

    public InsuranceDTO wrapDTO() {
        return new InsuranceDTO(this.id, this.name, this.description, this.enabled, this.paymentType);
    }

    public static Insurance findPrivateInsurance(Long institutionId){

        List<Insurance> insurances = getDefaultQuery(institutionId)
                .eq("paymentType", InsurancePaymentType.PRIVATE)
                .findList();

        if(insurances.size() > NumberUtils.INTEGER_ONE){
            Logger.error("Institution {} has more than one private insurance. Returning the first one", institutionId);
        }

        if(insurances.size() == NumberUtils.INTEGER_ZERO ){
            return null;
        }

        return insurances.get(NumberUtils.INTEGER_ZERO);

    }



}

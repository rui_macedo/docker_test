package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import common.annotations.BaseTenantModelFormatter;
import common.constants.CalendarDay;
import dto.SchedulingBookWeekAgendaDTO;
import org.joda.time.Interval;

import play.Logger;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "SchedulingBookWeekAgenda", schema = "scheduling")
public class SchedulingBookWeekAgenda extends BaseTenantModel {

    /*
     * Attributes
     */
    private static final long serialVersionUID = -6245608615769129159L;
    private static Model.Finder<Long, SchedulingBookWeekAgenda> finder = new Model.Finder<Long, SchedulingBookWeekAgenda>(Long.class, SchedulingBookWeekAgenda.class);

    @Id
    @Column(name = "IDSchedulingBookWeekAgenda")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDSchedulingBook")
    @BaseTenantModelFormatter(clazz = SchedulingBook.class, idFieldIsString = false, idFieldName = "id")
    @Required
    @JsonBackReference
    public SchedulingBook schedulingBook;

    @Column(name = "CalendarDay")
    @Enumerated(EnumType.STRING)
    public CalendarDay calendarDay;

    @OneToMany(mappedBy = "schedulingBookWeekAgenda", cascade = CascadeType.ALL)
    @BaseTenantModelFormatter(clazz = SchedulingBookPeriod.class, idFieldIsString = false, idFieldName = "id")
    @JsonManagedReference
    public Set<SchedulingBookPeriod> schedulingBookPeriods;

    /*
     * Constructors
     */
    public SchedulingBookWeekAgenda(Institution institution, SchedulingBook schedulingBook, CalendarDay calendarDay) {
        super(institution);
        this.schedulingBook = schedulingBook;
        this.calendarDay = calendarDay;
    }

    SchedulingBookWeekAgenda() {
    }

    ;

  /*
   * Abstract methods
   */

    /*
     * Repository methods
     */
    public static SchedulingBookWeekAgenda getWeekAgendaByCalendarDay(CalendarDay calendarDay, SchedulingBook schedulingBook) {
        Logger.debug("Seeking bookWeekAgenda for book {}, CalendarDay {}", schedulingBook.getId(), calendarDay);
        return finder.where().eq("calendarDay", calendarDay).eq("schedulingBook", schedulingBook).findUnique();
    }

    public static Set<SchedulingBookWeekAgenda> unwrap(List<SchedulingBookWeekAgendaDTO> schedulingBookWeekAgenda, Long idInstitution) {
        Set<SchedulingBookWeekAgenda> list = new HashSet<>();
        if (schedulingBookWeekAgenda == null) {
            return list;
        }
        for (SchedulingBookWeekAgendaDTO ins : schedulingBookWeekAgenda) {
            SchedulingBookWeekAgenda agenda = new SchedulingBookWeekAgenda();
            agenda.calendarDay = ins.calendarDay;
            agenda.schedulingBookPeriods = SchedulingBookPeriod.unwrapByListPeriod(ins.schedulingBookPeriods, idInstitution);
            agenda.institution = Institution.byId(idInstitution);
            list.add(agenda);
        }
        return list;
    }

    ;

    /*
     * Override methods
     */
    @Override
    public Long getId() {
        return this.id;
    }

  /*
   * Get/setters/modifiers
   */

    @Override
    public void save() {
        if (this.institution.equals(this.schedulingBook.institution)) {
            super.save();
            schedulingBook.schedulingBookWeekAgendas.add(this);
            schedulingBook.update();
        } else {
            throw new IllegalArgumentException("Book allows to another institution!");
        }
    }

    @Override
    public void save(String arg0) {
        // TODO Auto-generated method stub
        super.save(arg0);
    }

    public void addSchedulingBookPeriod(Integer startAt, Integer endAt) {
        if (this.validatePeriodInterval(startAt, endAt)) {
            SchedulingBookPeriod period = new SchedulingBookPeriod(institution, startAt, endAt, this);
            period.save();
            this.schedulingBookPeriods.add(period);
            this.update();
        }
    }

    public boolean validatePeriodInterval(Integer startAt, Integer endAt) {
        Interval interval = new Interval(startAt, endAt);
        boolean isValid = true;
        for (SchedulingBookPeriod schedulingBookPeriod : schedulingBookPeriods) {
            if (schedulingBookPeriod.getZeroEpochInterval().overlaps(interval)) {
                isValid = false;
                break;
            }
        }
        return isValid;
    }

    public SchedulingBookWeekAgendaDTO wrap() {
        SchedulingBookWeekAgendaDTO dto = new SchedulingBookWeekAgendaDTO();
        dto.calendarDay = this.calendarDay;
        dto.id = this.id;
        if (this.schedulingBookPeriods != null) {
            dto.schedulingBookPeriods = new ArrayList<>();
            for (SchedulingBookPeriod period : this.schedulingBookPeriods) {
                dto.schedulingBookPeriods.add(period.wrap());
            }
        }
        return dto;
    }

}

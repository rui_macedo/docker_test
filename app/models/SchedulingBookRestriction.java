package models;

import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import dto.RestrictionDTO;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "SchedulingBookRestriction", schema = "scheduling")
public class SchedulingBookRestriction extends BaseSimpleModel implements PathBindable<SchedulingBookRestriction>, QueryStringBindable<SchedulingBookRestriction> {

    private static final long serialVersionUID = 4633289281844580144L;

    private static Model.Finder<Long, SchedulingBookRestriction> finder = new Model.Finder<Long, SchedulingBookRestriction>(Long.class, SchedulingBookRestriction.class);
    @Id
    @Column(name = "IDSchedulingBookRestriction")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDRestriction")
    @BaseTenantModelFormatter(clazz = Restriction.class, idFieldIsString = false, idFieldName = "id")
    public Restriction restriction;

    @ManyToOne
    @JoinColumn(name = "IDSchedulingBook")
    @BaseTenantModelFormatter(clazz = SchedulingBook.class, idFieldIsString = false, idFieldName = "id")
    public SchedulingBook schedulingBook;

    public SchedulingBookRestriction(Restriction restriction) {
        super();
        this.restriction = restriction;
    }

    public static SchedulingBookRestriction findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<SchedulingBookRestriction> unwrapByRestriction(List<RestrictionDTO> restrictions) {
        List<SchedulingBookRestriction> list = new ArrayList<SchedulingBookRestriction>();
        if (restrictions == null) {
            return list;
        }
        for (RestrictionDTO res : restrictions) {
            list.add(new SchedulingBookRestriction(Restriction.findById(res.id)));
        }
        return list;
    }

    @Override
    public Option<SchedulingBookRestriction> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            SchedulingBookRestriction obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public SchedulingBookRestriction bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }
}

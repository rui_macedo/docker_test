package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import forms.FormSchedulingProcedure;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Transactional;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.security.GeneralSecurityException;
import java.util.Map;

@Entity
@Table(name = "ScheduledProcedure", schema = "scheduling")
public class ScheduledProcedure extends BaseTenantModel implements
        PathBindable<ScheduledProcedure>, QueryStringBindable<ScheduledProcedure> {

    /*
     * Attributes
     */
    private static final long serialVersionUID = 135928062224830600L;
    private static Model.Finder<Long, ScheduledProcedure> finder =
            new Model.Finder<Long, ScheduledProcedure>(Long.class, ScheduledProcedure.class);

    @Id
    @Column(name = "IDScheduledProcedure")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDScheduledOrder", nullable = false)
    @BaseTenantModelFormatter(clazz = ScheduledOrder.class, idFieldIsString = false,
            idFieldName = "id")
    @JsonBackReference
    @Required
    public ScheduledOrder scheduledOrder;

    @ManyToOne
    @JoinColumn(name = "IDPatient", nullable = false)
    @BaseTenantModelFormatter(clazz = Patient.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public Patient patient;

    @ManyToOne
    @JoinColumn(name = "IDHealthCareProfessionalExecuter")
    @BaseTenantModelFormatter(clazz = InstitutionHealthCareProfessional.class,
            idFieldIsString = false, idFieldName = "id")
    public InstitutionHealthCareProfessional healthCareProfissionalExecuter;

    @ManyToOne
    @JoinColumn(name = "IDHealthCareProfessionalReporter")
    @BaseTenantModelFormatter(clazz = InstitutionHealthCareProfessional.class,
            idFieldIsString = false, idFieldName = "id")
    public InstitutionHealthCareProfessional healthCareProfessionalReporter;

    @ManyToOne
    @JoinColumn(name = "IDExamProcedure", nullable = false)
    @BaseTenantModelFormatter(clazz = ExamProcedure.class,
            idFieldIsString = false, idFieldName = "id")
    @Required
    public ExamProcedure examProcedure;

    @Transient
    private Interval interval;

    // TODO: another atributtes

    /*
     * Constructors
     */
    public ScheduledProcedure(ScheduledOrder order, ExamProcedure examProcedure) throws GeneralSecurityException {
        super();
        if (order.institution.equals(examProcedure.institution)) {
            this.institution = order.institution;
            this.patient = order.patient;
            this.scheduledOrder = order;
            this.examProcedure = examProcedure;
        } else {
            throw new GeneralSecurityException("Illegal access security: Cant instantiate ScheduledProcedure with order, examProcedure of another tenant");
        }
    }

    public ScheduledProcedure() {
        super();
    }
  /*
   * Abstract methods
   */

    /*
     * Repository methods
     */
    public static ScheduledProcedure findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    @Transactional //FIXME: revisar exceção
    public static ScheduledProcedure createAndSaveAScheduling(FormSchedulingProcedure procForm, ScheduledOrder order) throws GeneralSecurityException {
        ScheduledProcedure proc = new ScheduledProcedure(order, procForm.examProcedure);
        order.scheduledProcedures.add(proc);
        proc.save();
        Interval interval = procForm.boundsToInterval();
        DateTime refDateTime = procForm.referenceDateToDateTime();
        if (!procForm.book.institution.equals(proc.institution)) {
            throw new GeneralSecurityException("Illegal access security: order tenant must to be the same of all entities to scheduliung");
        }
        ScheduledProcedureSchedulingBook scheduledInBook = new ScheduledProcedureSchedulingBook(procForm.book, proc, refDateTime, interval);
        scheduledInBook.save();
        for (Resource res : procForm.resources) {
            res = Resource.findById(res.id);
            ScheduledProcedureResource scheduledProcedureResource = new ScheduledProcedureResource(res, proc, procForm.book, refDateTime, interval);
            scheduledProcedureResource.save();
        }
        return proc;
    }

    /*
     * Override methods
     */
    @Override
    public Option<ScheduledProcedure> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            ScheduledProcedure obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public ScheduledProcedure bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

  /*
   * Get/setters/modifiers
   */

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return id;
    }

    public Interval getZeroEpochInterval() {
        if (interval == null) {
            interval = ScheduledProcedureSchedulingBook.findByScheduledProcedure(this).getZeroEpochInterval();
        }
        return interval;
    }
}

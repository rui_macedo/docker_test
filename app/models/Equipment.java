package models;

import common.utils.FormattingUtils;
import dto.EquipmentDTO;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "Equipment", schema = "global")
public class Equipment extends BaseTenantModel implements PathBindable<Equipment>,
        QueryStringBindable<Equipment> {
    /*
     * Attributes
     */
    private static final long serialVersionUID = -1L;
    private static Model.Finder<Long, Equipment> finder = new Model.Finder<Long, Equipment>(
            Long.class, Equipment.class);
    @Id
    @Column(name = "IDEquipment")
    public Long id;
  /*
  @Column
  @Required
  public String producer;

  @Column
  @Required
  public String aeTitle;

  @ManyToOne
  @JoinColumn(name = "IDSection", nullable = false)
  @BaseTenantModelFormatter(clazz = Section.class, idFieldIsString = false, idFieldName = "id")
  public Section section;

  @ManyToOne
  @JoinColumn(name = "IDRoom", nullable = true)
  @BaseTenantModelFormatter(clazz = Room.class, idFieldIsString = false, idFieldName = "id")
  public Room room;*/

    // TODO: (Modalidade : DICOM)
    @Column(name = "Name")
    @Required
    public String name;

  /*
   * Constructors
   */

    public Equipment(Institution institution, String name, String producer, String aeTitle,
                     Section section, Room room) {
        super(institution);
        this.name = name;
//    this.producer = producer;
//    this.aeTitle = aeTitle;
//    this.section = section;
//    this.room = room;
    }

  /*
   * Abstract methods
   */

    /*
     * Repository methods
     */
    public static List<Equipment> findAll(Long tenant) {
        return finder.where().eq("institution.id", tenant).findList();
    }

    public static Equipment findById(Long id) {
        if (id == null) {
            return null;
        }
        return finder.where().eq("id", id).findUnique();
    }

    /*
     * Override methods
     */
    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public Option<Equipment> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("equipment")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Equipment obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Equipment bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

  /*
   * Get/setters/modifiers
   */

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    public EquipmentDTO wrap() {
        return new EquipmentDTO(this.id, this.name);
    }
}

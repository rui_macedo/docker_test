package models;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import common.utils.FormattingUtils;
import org.joda.time.DateTime;
import utils.JsonFloatSerializer;

import java.util.HashMap;
import java.util.Map;


/**
 * The Class ExamProcedureValues.
 * Used to calculate values for a given ExamProcedure in a Carrier/Plan
 */

/**
 * @author rodrigo.silva
 *
 *  Contains all the rules for a ExamProcedure calculation
 */
public class ExamProcedureValue {

    public ProcedureTableExamProcedure procedureTableExamProcedure;

    public ProcedureTable procedureTable;

    /*Indexers*/
    public ExamProcedureIndexerValue indexerCH;

    public ExamProcedureIndexerValue indexerCO;

    public ExamProcedureIndexerValue indexerFIL;

    public ExamProcedureIndexerValue indexerMAT;

    public ExamProcedureIndexerValue indexerOTR;

    public ExamProcedureIndexerValue indexerANT;

    public ExamProcedureIndexerValue indexerAUX1;

    public ExamProcedureIndexerValue indexerAUX2;

    public ExamProcedureIndexerValue indexerAUX3;

    @JsonSerialize(using = JsonFloatSerializer.class)
    private Float patientValue = 0F;

    @JsonSerialize(using = JsonFloatSerializer.class)
    private Float carrierValue = 0F;

    private DateTime date;

    private Map<Long, IndexerValue> indexersDateValue;


    public ExamProcedureValue() {
        super();
        indexersDateValue = new HashMap<Long, IndexerValue>();
    }

    public Float getPatientValue() {
        return FormattingUtils.formatFloatNumber(patientValue, 4);
    }

    public void setPatientValue(Float patientValue) {
        this.patientValue = FormattingUtils.formatFloatNumber(patientValue, 4);
    }

    public Float getCarrierValue() {
        return FormattingUtils.formatFloatNumber(carrierValue, 4);
    }

    public void setCarrierValue(Float carrierValue) {
        this.carrierValue = FormattingUtils.formatFloatNumber(carrierValue, 4);
    }

    public void calculateExamProcedureValue(Insurance insurance,
                                            InsurancePlan insurancePlan,
                                            ExamProcedure examProcedure, DateTime dt) {

        this.procedureTable = null;
        this.patientValue = 0F;
        this.carrierValue = 0F;
        this.date = dt;

        indexersDateValue.clear();

        //Decide which table will be used
        if (insurancePlan != null && insurancePlan.procedureTable != null) {
            this.procedureTable = insurancePlan.procedureTable;
        }
        if (procedureTable == null) {
            if (insurance != null && insurance.procedureTable != null) {
                this.procedureTable = insurance.procedureTable;
            }
        }
        if (this.procedureTable == null) {
            return;
        }

        //Find the very first ExamProcedure in the given Table
        this.procedureTableExamProcedure = ProcedureTableExamProcedure
                .finder
                .where()
                .eq("idInstitution", examProcedure.institution.getId())
                .eq("idProcedureTable", procedureTable.getId())
                .eq("idExamProcedure", examProcedure.getId())
                .findUnique();

        if (procedureTableExamProcedure == null) {
            return;
        }


        //Add values for each different indexer on date
        addIndexerValue(indexersDateValue, procedureTableExamProcedure.indexerCH);
        addIndexerValue(indexersDateValue, procedureTableExamProcedure.indexerCO);
        addIndexerValue(indexersDateValue, procedureTableExamProcedure.indexerFIL);
        addIndexerValue(indexersDateValue, procedureTableExamProcedure.indexerMAT);
        addIndexerValue(indexersDateValue, procedureTableExamProcedure.indexerANT);
        addIndexerValue(indexersDateValue, procedureTableExamProcedure.indexerOTR);

        //CH
        this.indexerCH = setIndexerValue(procedureTableExamProcedure.indexerCH,
                procedureTableExamProcedure.getQtdeCH(),
                procedureTable.getPercentagePatientCH(), 100);

        //CO
        this.indexerCO = setIndexerValue(procedureTableExamProcedure.indexerCO,
                procedureTableExamProcedure.getQtdeCO(),
                procedureTable.getPercentagePatientCO(), 100);

        //FIL
        this.indexerFIL = setIndexerValue(procedureTableExamProcedure.indexerFIL,
                procedureTableExamProcedure.getQtdeFIL(),
                procedureTable.getPercentagePatientFIL(), 100);

        //MAT
        this.indexerMAT = setIndexerValue(procedureTableExamProcedure.indexerMAT,
                procedureTableExamProcedure.getQtdeMAT(),
                procedureTable.getPercentagePatientMAT(), 100);

        //ANT
        this.indexerANT = setIndexerValue(procedureTableExamProcedure.indexerANT,
                procedureTableExamProcedure.getQtdeANT(),
                procedureTable.getPercentagePatientANT(), 100);

        //OTR
        this.indexerOTR = setIndexerValue(procedureTableExamProcedure.indexerOTR,
                procedureTableExamProcedure.getQtdeOTR(),
                procedureTable.getPercentagePatientOTR(), 100);


        //Setting auxiliars - Percentage over the CH
        if (procedureTableExamProcedure.getQtdeAUX() > 0) {
            this.indexerAUX1 = setIndexerValue(procedureTableExamProcedure.indexerCH,
                    procedureTableExamProcedure.getQtdeCH(),
                    procedureTable.getPercentagePatientCH(),
                    procedureTable.getPercentageAUX1());
        }
        if (procedureTableExamProcedure.getQtdeAUX() > 1) {
            this.indexerAUX2 = setIndexerValue(procedureTableExamProcedure.indexerCH,
                    procedureTableExamProcedure.getQtdeCH(),
                    procedureTable.getPercentagePatientCH(),
                    procedureTable.getPercentageAUX2());
        }
        if (procedureTableExamProcedure.getQtdeAUX() > 2) {
            this.indexerAUX3 = setIndexerValue(procedureTableExamProcedure.indexerCH,
                    procedureTableExamProcedure.getQtdeCH(),
                    procedureTable.getPercentagePatientCH(),
                    procedureTable.getPercentageAUX3());
        }


    }


    /**
     * Sets the indexer value finding a valid value for it.
     *
     * @param indexer the indexer
     * @param qtde the qtde
     * @param percentagePatient the percentage patient
     * @param percentageIndexerValue the percentage indexer value
     * @return the exam procedure indexer value
     */
    private ExamProcedureIndexerValue setIndexerValue(Indexer indexer, float qtde,
                                                      float percentagePatient, float percentageIndexerValue) {

        //Adjust percentage to the range[0..100]
        if (percentageIndexerValue > 100) {
            percentageIndexerValue = 100;
        }
        if (percentageIndexerValue < 0) {
            percentageIndexerValue = 0;
        }

        qtde = FormattingUtils.formatFloatNumber(qtde, 4);
        percentagePatient = FormattingUtils.formatFloatNumber(percentagePatient, 2);
        percentageIndexerValue = FormattingUtils.formatFloatNumber(percentageIndexerValue, 2);

        ExamProcedureIndexerValue indx = new ExamProcedureIndexerValue();

        indx.setQtde(qtde);
        indx.setPercentageIndexerValue(percentageIndexerValue);

        if (indexer != null) {
            if (indexersDateValue.containsKey(indexer.getId())) {
                indx.setIndexerValue(indexersDateValue.get(indexer.getId()));
            }
        }
        float total = 0F;

        //If there are an indexer and a value for it...calculate
        if (indx.getIndexerValue() != null) {

            total = qtde * indx.getIndexerValue().getValue();
            //Apply the reduction (if exists)
            if (percentageIndexerValue > 0) {
                total = total * (percentageIndexerValue / 100);
            }

            //Apply the reduction for patient(if exists)
            if (percentagePatient > 0) {
                indx.setPatientValue(total * (percentagePatient / 100));
            }
            total -= indx.getPatientValue();
            //Set total for Carrier
            indx.setCarrierValue(total);
        }

        //AddUp values
        this.patientValue += indx.getPatientValue();
        this.carrierValue += indx.getCarrierValue();

        return indx;
    }

    //Avoid searching/calculating for unnecessary values on DB
    private void addIndexerValue(Map<Long, IndexerValue> indexerList, Indexer indexer) {
        if (indexer == null) {
            return;
        }
        if (!indexerList.containsKey(indexer.getId())) {
            indexerList.put(indexer.getId().longValue(), indexer.getIndexerValue(this.date));
        }
    }


}

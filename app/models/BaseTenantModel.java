package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import com.avaje.ebean.Query;
import common.annotations.BaseSimpleModelFormatter;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@MappedSuperclass
public abstract class BaseTenantModel extends Model implements Serializable {
    /*
     * Attributes
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "IDInstitution", nullable = false)
    @BaseSimpleModelFormatter(clazz = Institution.class, idFieldIsString = false, idFieldName = "id")
    @NotNull
    public Institution institution;


    @Version
    @Column(name = "Version")
    // @JsonIgnore
    public int version;

  /*
   * Constructors
   */

    public BaseTenantModel(Institution institution) {
        super();
        this.institution = institution;
    }


    public BaseTenantModel() {
        super();
    }

  /*
   * Abstract methods
   */

    public static <T extends BaseTenantModel> Page<T> findAll(Class<T> clazz,
                                                              Institution institution, int pageSize, int page) {
        Query<T> query = Ebean.createQuery(clazz);
        return query.where().eq("idInstitution", institution.getId()).findPagingList(pageSize)
                .getPage(page);
    }

  /*
   * Override methods
   */

    public static <T extends BaseTenantModel> List<T> findAll(Class<T> clazz, Institution institution) {
        Query<T> query = Ebean.createQuery(clazz);
        return query.where().eq("idInstitution", institution.getId()).findList();
    }

    /**
     * Find all with field to order with asc
     */
    public static <T extends BaseTenantModel> Page<T> findAll(Class<T> clazz,
                                                              Institution institution, int pageSize, int page, String fieldToOrder) {
        Query<T> query =
                Ebean.createQuery(clazz).where().eq("idInstitution", institution.getId()).order()
                        .asc(fieldToOrder);
        return query.findPagingList(pageSize).getPage(page);
    }

    public abstract Number getId();


  /*
   * Override methods
   */


  /*
   * Get/setters/modifiers
   */

  /*
   * Repository methods
   */

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    /**
     * @see Object#equals(java.lang.Object)
     */
    public boolean equals(Object object) {
        return object instanceof Serializable && equals((BaseTenantModel) object);
    }

    public boolean equals(BaseTenantModel baseModel) {
        if (this == baseModel) {
            return true;
        }

        return baseModel == null ? false
                : (baseModel.getId() == null || baseModel.getClass() == null ? this.getId() == null
                && this.getClass() == null : baseModel.getId().equals(this.getId())
                && this.getClass().equals(baseModel.getClass()));
    }


}

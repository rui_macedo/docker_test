package models;

import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import com.fasterxml.jackson.annotation.JsonIgnore;
import common.annotations.BaseSimpleModelFormatter;
import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import dto.ExamProcedureDTO;
import services.searchers.model.Duration;
import services.searchers.model.SimpleExamProcedure;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

import static java.util.Objects.nonNull;

/**
 * The Class ExamProcedure.
 */
@Entity
@Table(name = "ExamProcedure", schema = "examprocedure")
public class ExamProcedure extends BaseTenantModel implements PathBindable<ExamProcedure>,
        QueryStringBindable<ExamProcedure> {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The finder.
     */
    public static Model.Finder<Long, ExamProcedure> finder = new Model.Finder<Long, ExamProcedure>(
            Long.class, ExamProcedure.class);
    /**
     * The id.
     */
    @Id
    @Column(name = "IDExamProcedure")
    public Long id;
    /**
     * The name.
     */
    @Column(name = "Name", length = 200, nullable = false)
    @Required
    public String name;
    /**
     * The code.
     */
    @Column(name = "Code", length = 30, nullable = false)
    @Required
    public String code;
    @Column(name = "Mnemonic", length = 10)
    public String mnemonic;
    @Column(name = "ExamProcedureEnabled", columnDefinition = "bit default 1")
    public Boolean examProcedureEnabled = true;
    /**
     * The exam procedure tuss.
     */
    @ManyToOne
    @JoinColumn(name = "IDExamProcedureTUSS", nullable = true)
    @BaseSimpleModelFormatter(clazz = ExamProcedureTUSS.class, idFieldIsString = false,
            idFieldName = "id")
    @JsonIgnore
    public ExamProcedureTUSS examProcedureTUSS;

    @OneToOne
    @JoinColumn(name = "IDExamModality")
    @BaseTenantModelFormatter(clazz = ExamModality.class, idFieldIsString = false, idFieldName = "id")
    @JsonIgnore
    public ExamModality examModality;

    @JsonIgnore
    @OneToMany(mappedBy = "examProcedure")
    public List<ExamProcedureDictionary> examProcedureDictionary;

    @Column(name = "IsDeleted")
    public Boolean deleted = false;

    @Column(name = "duration")
    public Integer duration;

    @Column(name = "preparation")
    public String preparation;
    /**
     * Instantiates a new exam procedure.
     *
     * @param institution       the institution
     * @param name              the name
     * @param code              the code
     * @param examProcedureTUSS the exam procedure tuss
     * @param examCategory      the exam category
     */
    public ExamProcedure(Institution institution, String name, String code,
                         ExamProcedureTUSS examProcedureTUSS) {
        super(institution);
        this.name = name;
        this.code = code;
        this.examProcedureTUSS = examProcedureTUSS;
        this.examProcedureEnabled = true;
    }

    public ExamProcedure(Institution institution, String name, String code) {
        super(institution);
        this.name = name;
        this.code = code;
    }

    public ExamProcedure() {
    }

    /**
     * Find by id.
     *
     * @param id the id
     * @return the exam procedure
     */
    public static ExamProcedure findById(Long id) {
        return id == null ? null : finder.where().eq("id", id).or(Expr.eq("deleted", false), Expr.eq("deleted", null)).findUnique();
    }

    /**
     * Find all by institution.
     *
     * @param idInstitution the id institution
     * @return the list
     */
    public static List<ExamProcedure> findAllByInstitution(Long idInstitution, String code,
                                                           String name, String mnemonic) {
        if (code == null) {
            code = new String();
        }
        if (name == null) {
            name = new String();
        }
        if (mnemonic == null) {
            mnemonic = new String();
        }
        code = code.trim().replace("%", "");
        name = name.trim().replace("%", "");
        mnemonic = mnemonic.trim().replace("%", "");

        ExpressionList<ExamProcedure> search = finder.where().eq("idInstitution", idInstitution);

        if (!code.isEmpty()) {
            search = search.eq("code", code);
        }
        if (!name.isEmpty()) {
            search = search.ilike("name", name + "%");
        }
        if (!mnemonic.isEmpty()) {
            search = search.eq("mnemonic", mnemonic);
        }
        return search.findList();
    }

    public static List<ExamProcedure> findAllByInstitution(Long idInstitution) {
        return finder.where().eq("idInstitution", idInstitution).or(Expr.eq("deleted", false), Expr.eq("deleted", null)).findList();
    }

    public static List<ExamProcedure> findAllByExamModality(ExamModality examModality) {
        return finder.where().eq("examModality", examModality).or(Expr.eq("deleted", false), Expr.eq("deleted", null)).findList();
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<ExamProcedure> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("examprocedure")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            ExamProcedure obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public ExamProcedure bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null && (this.deleted == null || this.deleted == false) ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null && this.getId() != null && (this.deleted == null || this.deleted == false) ? this.getId().toString() : null;
    }

    @Override
    public void delete() {
        this.deleted = true;
        this.update();
    }

    public void unwrap(ExamProcedureDTO examProcedureDTO) {
        this.id = examProcedureDTO.id;
        this.name = examProcedureDTO.name;
        this.code = examProcedureDTO.code;
        this.examModality = examProcedureDTO.examModality;
        this.mnemonic = examProcedureDTO.mnemonic;
        this.examProcedureEnabled = examProcedureDTO.examProcedureEnabled;
    }

    public ExamProcedureDTO wrap() {
        ExamProcedureDTO examProcedureDTO = new ExamProcedureDTO();
        examProcedureDTO.id = this.id;
        examProcedureDTO.name = this.name;
        examProcedureDTO.code = this.code;
        examProcedureDTO.examModality = this.examModality;
        examProcedureDTO.mnemonic = this.mnemonic;
        examProcedureDTO.examProcedureEnabled = this.examProcedureEnabled;
        examProcedureDTO.preparation = this.preparation;
        return examProcedureDTO;
    }

    public SimpleExamProcedure wrapAsSimple(){
        SimpleExamProcedure dto = new SimpleExamProcedure();
        dto.duration = new Duration(this.duration);
        dto.id = id;
        dto.name = name;
        if(nonNull(examModality)){
            dto.modality = examModality.wrap();
        }
        return dto;
    }
}

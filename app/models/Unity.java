package models;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonBackReference;
import common.annotations.BaseSimpleModelFormatter;
import common.utils.FormattingUtils;
import dto.UnityDTO;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "Unity", schema = "global")
public class Unity extends BaseTenantModel implements PathBindable<Unity>, QueryStringBindable<Unity> {

    private static final long serialVersionUID = -9170679200673143226L;

    private static Model.Finder<Integer, Unity> finder = new Model.Finder<Integer, Unity>(Integer.class, Unity.class);

    @Id
    @Column(name = "IDUnity")
    public Integer id;

    @Column(name = "Name", length = 50, nullable = false)
    @Required
    public String name;

    @Column(name = "UnityCode", length = 30, unique = true)
    @Required
    @NotNull
    public String unityCode;

    @ManyToOne
    @JoinColumn(name = "IDAddress", nullable = false)
    @BaseSimpleModelFormatter(clazz = Address.class, idFieldIsString = false, idFieldName = "id")
    public Address address;

    @Column(name = "AddressNumber", length = 6)
    public Integer addressNumber;

    @Column(name = "Latitude", length = 50)
    public String latitude;

    @Column(name = "Longitude", length = 50)
    public String longitude;

    @Column(name = "Telephone", length = 20)
    public String telephone;

    @Column(name = "OpeningHours", length = 15)
    public String openingHours;

    @Column(name = "CreationDate")
    @Temporal(TemporalType.DATE)
    @CreatedTimestamp
    public Date creationDate;

    @Column(name = "LastUpdate")
    @Temporal(TemporalType.DATE)
    @UpdatedTimestamp
    public Date updateDate;

    @Column(name = "UnityEnabled")
    public Boolean unityEnabled = true;

    @Column(name = "BusinessSocialName", length = 100)
    @Required
    public String businessSocialName;

    @Column(name = "CNPJ", length = 14)
    @Required
    public String cnpj;

    @JsonBackReference
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "unity", fetch = FetchType.LAZY)
    public Pacs pacs;

    public Unity() {

    }

    public Unity(Institution institution, String name, Address address, Integer addressNumber, String unityCode) {
        super(institution);
        this.name = name;
        this.address = address;
        this.addressNumber = addressNumber;
        this.unityCode = unityCode;
    }

    public static Unity findById(Integer id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<Unity> findAllByTenant(Long tenantId) {
        return finder.where().eq("institution.id", tenantId).findList();
    }

    public static Unity findByUnityCode(String unityCode) {
        return finder.where().eq("unityCode", unityCode).findUnique();
    }

    public static boolean isValidUnityCode(String unityCode) {
        return Unity.findByUnityCode(unityCode) == null;
    }

    @Override
    public Option<Unity> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("unity")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Unity obj = findById(new Integer(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Unity bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Integer(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public UnityDTO wrap() {
        return new UnityDTO(this.id, this.name, this.unityCode);
    }

    public boolean validUnityName() {
        return !(finder.where().eq("institution", this.institution).eq("name", this.name).ne("id", this.id).findRowCount() > 0);
    }
}

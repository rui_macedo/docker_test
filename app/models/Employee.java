package models;

import common.utils.FormattingUtils;
import org.joda.time.DateTime;

import dto.EmployeeDTO;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "Employee", schema = "global")
public class Employee extends BaseTenantModel implements PathBindable<Employee>,
        QueryStringBindable<Employee> {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, Employee> finder = new Model.Finder<Long, Employee>(
            Long.class, Employee.class);
    @Id
    @Column(name = "IDEmployee")
    public Long id;
    @Required
    @Column(length = 100, name = "Name")
    public String name;
    @Column(length = 14, name = "CPF")
    public String cpf;
    @Column(length = 10, name = "RegistrationNumber")
    public String registrationNumber;
    @Column(length = 100, name = "email")
    public String email;
    @Column(name = "BirthDate")
    public DateTime birthDate;
    @Column(length = 25, name = "phone")
    public String phone;

    /*Constructor*/
    public Employee(Institution institution, String name) {
        super(institution);
        this.name = name;
    }

    public static Employee findById(Long id) {
        return finder.byId(id);
    }

    public static List<Employee> findAllByInstitution(Long idInstitution) {
        return finder.where().eq("idInstitution", idInstitution).findList();
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<Employee> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Employee obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Employee bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    public EmployeeDTO wrap(){
        return new EmployeeDTO(this.id, this.name);
    }


}
   
   
   
   

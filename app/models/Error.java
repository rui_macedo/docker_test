package models;

public class Error {
    private final String message;
    private String field;

    private Error(String message) {
        this.message = message;
    }

    private Error(String message, String field) {
        this.message = message;
        this.field = field;
    }

    public static Error create(String message) {
        return new Error(message);
    }

    public static Error create(String message, String fild) {
        return new Error(message, fild);
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        //return "Error [message=" + message + "]";
        return "Error [field=" + this.field
                + ", message=" + this.message + "]";
    }

}

package models.scheduling;

import com.pixeon.utils.common.utils.DateUtils;
import common.utils.FormattingUtils;
import dto.ContactPatientDTO;
import models.*;
import org.joda.time.DateTime;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

import static java.util.Objects.nonNull;

@Entity
@Table(name = "ContactPatient", schema = Schemas.SCHEDULING)
public class ContactPatient extends BaseSimpleModel implements PathBindable<ContactPatient>,
        QueryStringBindable<ContactPatient> {
        private static final long serialVersionUID = 1L;

    public static Model.Finder<Long, ContactPatient> finder = new Model.Finder<Long, ContactPatient>(
            Long.class, ContactPatient.class);

    @Id
    @Column(name = "IDContactPatient")
    public Long id;

    @Column(name = "IDInstitution")
    public Long institutionId;

    @Column(name = "IDPatient")
    public Long patientId;

    @Column(name = "IDPatientPreRegister")
    public Long prePatientId;

    @ManyToOne
    @JoinColumn(name = "IDContact", nullable = false)
    public Contact contact;

    @Column(name = "Notes", length = 500)
    public String notes;

    @Column(name = "CreationDate")
    public DateTime creationDate;

    @Column(name = "UpdatedDate")
    public DateTime updatedDate;

    public ContactPatient(Long institutionId, Contact contact) {
        this.institutionId = institutionId;
        this.contact = contact;
        this.creationDate =  DateUtils.getCurrentDateTimeGMTZero();
        this.updatedDate =  DateUtils.getCurrentDateTimeGMTZero();
    }

    public ContactPatient() {
        this.creationDate =  DateUtils.getCurrentDateTimeGMTZero();
        this.updatedDate =  DateUtils.getCurrentDateTimeGMTZero();
    }

    public static ContactPatient findById(Long id) {
        return nonNull(id) ? finder.byId(id) : null;
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<ContactPatient> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("contactpatient")[0];
        if (! FormattingUtils.isNumberCompatible(pathId)) {
            return Option.None();
        }
        ContactPatient obj = findById(new Long(pathId));
        return nonNull(obj) ?  Option.Some(obj) : Option.None();
    }

    @Override
    public ContactPatient bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)): null;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    public static List<ContactPatient> findAllByContact(Long contactId){
        return finder.where().eq("contact.id", contactId).isNotNull("patientId").findList();
    }

    public ContactPatientDTO wrap(){
        ContactPatientDTO dto = new ContactPatientDTO();
        dto.creationDate = this.creationDate;
        dto.updatedDate = this.updatedDate;
        if(nonNull(patientId)){
            Patient patient = Patient.findById(patientId);
            dto.patient = patient.wrap();
        }
        dto.isValidPatient = nonNull(patientId);
        if(nonNull(prePatientId)){
            PrePatient ppr = PrePatient.findById(this.prePatientId);
            dto.patientPreRegister = ppr.wrap();
        }
        dto.id = this.id;
        dto.institutionId = this.institutionId;
        dto.notes = this.notes;
        return dto;
    }

}
 


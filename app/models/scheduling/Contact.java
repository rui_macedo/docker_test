package models.scheduling;

import com.avaje.ebean.ExpressionList;
import com.pixeon.utils.common.utils.DateUtils;
import common.utils.FormattingUtils;
import dto.ContactDTO;
import models.*;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import play.db.ebean.Model;
import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "Contact", schema = Schemas.SCHEDULING)
public class Contact extends BaseSimpleModel implements PathBindable<Contact>, QueryStringBindable<Contact> {

    private static final long serialVersionUID = -7121824162686242216L;

    private static Model.Finder<Long, Contact> finder = new Model.Finder<>(Long.class, Contact.class);

    @Id
    @Column(name = "IDContact")
    public Long id;

    @Column(name = "IDInstitution")
    public Long institutionId;

    @Column(name = "Name", length = 50)
    public String name;

    @Column(name = "Phone", length = 16)
    public String phone;

    @Column(name = "PhoneType")
    @Enumerated(EnumType.STRING)
    public PhoneTypeEnum phoneType;

    @Column(name = "Notes", length = 2000)
    public String notes;

    @Column(name = "CreationDate")
    public DateTime created;

    @Column(name = "UpdatedDate")
    public DateTime updated;

    @OneToMany(mappedBy = "contact")
    public Set<ContactPatient> contactPatients;

    public Contact(Long institutionId, String name) {
        this.institutionId = institutionId;
        this.name = name;
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
    }

    public Contact(Long institutionId, String name, String notes, String phone){
        this.institutionId = institutionId;
        this.name = name;
        this.notes = notes;
        this.phone = phone;
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
    }

    public Contact(String name, String notes, String phone) {
        this.name = name;
        this.notes = notes;
        this.phone = phone;
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
    }

    public Contact(){
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
    }

    public static Contact findById(Long id) {
        return finder.byId(id);
    }

    public static List<Contact> findAllByInstitution(Long institutionId, String phone, String name) {

        ExpressionList<Contact> search = finder.where().eq("institutionId", institutionId);

        if (StringUtils.isNotBlank(phone)) {
            search = search.ilike("phone", "%" + phone + "%");
        }

        if (StringUtils.isNotBlank(name)) {
            search = search.ilike("name", "%" + name + "%");
        }

        return search.findList();
    }

    @Override
    public F.Option<Contact> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("contact")[0];
        return FormattingUtils.isNumberCompatible(pathId) ? F.Option.Some(findById(new Long(pathId))) : F.Option.None();
    }

    @Override
    public Contact bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Number getId() {
        return id;
    }

    public static Contact create(ContactDTO request){
        Contact contact = new Contact();
        contact.unwrap(request);
        return contact;
    }

    public ContactDTO wrap(){
        ContactDTO contact = new ContactDTO();
        contact.creationDate = created;
        contact.updatedDate = updated;
        contact.name = name;
        contact.institutionId = institutionId;
        contact.id = id;
        contact.notes = notes;
        contact.phoneType = phoneType;
        contact.phone = phone;
        return contact;
    }

    public void unwrap(ContactDTO request){
        this.institutionId = request.institutionId;
        this.name = request.name;
        this.notes = request.notes;
        this.phone = request.phone;
        this.phoneType = request.phoneType;
    }

    public boolean hasPatient(Long patientId){
        return finder.where().eq("contactPatients.patientId", patientId).findList().size() > 0;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

package models.scheduling;

import common.utils.DateUtils;

import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.BaseSimpleModel;
import models.Restriction;
import models.Schemas;

import static java.util.Objects.nonNull;

@Entity
@Table(name = "SchedulingReservationRestriction", schema = Schemas.SCHEDULING)
public class SchedulingReservationRestriction  extends BaseSimpleModel {

    private static Finder<Long, SchedulingReservationSlot> finder =
            new Finder<>(Long.class, SchedulingReservationSlot.class);

    @Id
    @Column(name = "IDSchedulingReservationRestriction")
    public Long id;

    @Column(name = "IDScheduling")
    public Long schedulingId;

    @Column(name = "IDRestriction")
    public Long restrictionId;

    @ManyToOne
    @JoinColumn(name = "IDSchedulingReservation", nullable = true)
    public SchedulingReservation schedulingReservation;

    @Column(name = "CreationDate")
    public DateTime creationDate;

    @Column(name = "UpdatedDate")
    public DateTime updatedDate;

    public static SchedulingReservationSlot findById(Long id) {
        return nonNull(id) ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public SchedulingReservationRestriction() {
        this.creationDate = DateUtils.getCurrentDateTimeGMTZero();
        this.updatedDate= DateUtils.getCurrentDateTimeGMTZero();
    }

    public SchedulingReservationRestriction(Long restrictionId, SchedulingReservation schedulingReservation) {
        this.restrictionId = restrictionId;
        this.schedulingReservation = schedulingReservation;
        this.schedulingId = schedulingReservation.scheduling.getId();
        this.creationDate = DateUtils.getCurrentDateTimeGMTZero();
        this.updatedDate= DateUtils.getCurrentDateTimeGMTZero();
    }
}

package models.scheduling;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pixeon.utils.common.utils.DateUtils;
import dto.SchedulingDTO;
import dto.SchedulingOrderedDTO;
import models.BaseSimpleModel;
import models.Schemas;
import org.joda.time.DateTime;
import models.SlotReservation;
import play.db.ebean.Model;
import utils.HashGeneratorUtils;

import javax.persistence.*;
import java.util.List;

import static java.util.Objects.nonNull;

@Entity
@Table(name = "Scheduling", schema = Schemas.SCHEDULING)
public class Scheduling extends BaseSimpleModel {

    private static Model.Finder<Long, Scheduling> finder = new Model.Finder<>(Long.class, Scheduling.class);

    @Id
    @Column(name = "IDScheduling")
    public Long id;

    @Column(name = "IDInstitution")
    public Long institutionId;

    @ManyToOne
    @JoinColumn(name = "IDContact", nullable = false)
    public Contact contact;

    @Column(name = "IDPatient")
    public Long patientId;

    @Column(name = "IDEmployee")
    public Long employeeId;

    @Column(name = "PatientName", length = 60)
    public String patientName;

    @Column(name = "Notes", length = 500)
    public String notes;

    @Column(name = "Status", nullable = false)
    @Enumerated(EnumType.STRING)
    public Status status;

    @Column(name = "CreationDate")
    public DateTime created;

    @Column(name = "UpdatedDate")
    public DateTime updated;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "scheduling")
    public List<SchedulingFilter> schedulingFilters;

    public enum Status {

        //The User is working on this scheduling
        IN_PROGRESS,

        //It has been cancelled by patient
        CANCELED,

        //The user has finished this session
        FINISHED,

        //This session has already been booked successfully
        BOOKED;
    }

    public Scheduling() {
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
        this.status = Status.IN_PROGRESS;
    }


    public static Scheduling findById(Long id) {
        return nonNull(id) ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public SchedulingDTO wrap(){
        SchedulingDTO dto = new SchedulingDTO();
        if(nonNull(contact)){
            dto.contact = contact.wrap();
        }
        dto.creationDate = created;
        dto.updatedDate = updated;
        dto.employeeId = employeeId;
        dto.notes = notes;
        dto.patientId = patientId;
        dto.patientName = patientName;
        dto.institutionId = institutionId;
        dto.status = status;
        dto.id = id;
        for(SchedulingFilter filter : schedulingFilters){
            dto.filters.add(filter.wrap());
        }
        dto.protocol = getProtocol();
        return dto;
    }

    public void finish(){
        updated = DateUtils.getCurrentDateTimeGMTZero();;
        status = Status.FINISHED;
        update();
    }

    public void booked(){
        updated = DateUtils.getCurrentDateTimeGMTZero();
        status = Status.BOOKED;
        update();
    }

    public Boolean isValuable(){
        return nonNull(contact) &&
                (nonNull(patientId) || nonNull(patientName));
    }

    @Override
    public String toString() {
        return "Scheduling{" +
                "id=" + id +
                ", institutionId=" + institutionId +
                ", contact=" + contact.toString() +
                ", patientId=" + patientId +
                ", employeeId=" + employeeId +
                ", patientName='" + patientName + '\'' +
                ", notes='" + notes + '\'' +
                ", status=" + status +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }


    public String getProtocol(){
        return this.created.year().getAsShortText() + String.format("%07d", this.getId());
    }
}


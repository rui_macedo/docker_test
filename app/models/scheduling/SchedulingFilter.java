package models.scheduling;

import com.pixeon.utils.common.utils.DateUtils;
import dto.SchedulingFilterDTO;
import forms.FormSchedulingFilter;
import models.BaseSimpleModel;
import models.Schemas;
import org.joda.time.DateTime;

import javax.persistence.*;

import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Entity
@Table(name = "SchedulingFilter", schema = Schemas.SCHEDULING)
public class SchedulingFilter extends BaseSimpleModel {

    private static Finder<Long, SchedulingFilter> finder = new Finder<>(Long.class, SchedulingFilter.class);

    @Id
    @Column(name = "IDSchedulingFilter")
    public Long id;

    @Column(name = "IDInstitution")
    public Long institutionId;

    @ManyToOne
    @JoinColumn(name = "IDScheduling", nullable = false)
    public Scheduling scheduling;

    @Column(name = "FilterType")
    @Enumerated(EnumType.STRING)
    public Type filterType;

    @Column(name = "FilterValue")
    public String filterValue;

    @Column(name = "CreationDate")
    public DateTime created;

    @Column(name = "UpdatedDate")
    public DateTime updated;

    public static List<SchedulingFilter> findBySchedulingId(Long schedulingId) {
        return finder.where().eq("scheduling.id", schedulingId).findList();
    }

    public enum Type {
        INSURANCE,
        INSURANCE_PLAN,
        EXAM_PROCEDURE,
    }

    public SchedulingFilter() {
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
    }

    public SchedulingFilter(Long institutionId, FormSchedulingFilter form) {
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
        this.institutionId = institutionId;
        this.filterValue = form.filterValue;
        this.filterType = form.filterType;
    }

    public static SchedulingFilter findById(Long id) {
        return nonNull(id) ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public SchedulingFilterDTO wrap(){
        SchedulingFilterDTO dto = new SchedulingFilterDTO();
        dto.creationDate = this.created;
        dto.updatedDate = this.updated;
        dto.filterType = this.filterType;
        dto.id = getId();
        dto.institutionId = this.institutionId;
        dto.filterValue = this.filterValue;
        dto.schedulingId = this.scheduling.getId();
        return dto;
    }

}


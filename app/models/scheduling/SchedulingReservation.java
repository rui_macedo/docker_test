package models.scheduling;

import com.google.common.base.Joiner;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlRow;
import com.google.common.collect.ImmutableMap;
import com.pixeon.utils.common.utils.DateUtils;

import dto.RestrictionCheckDTO;
import models.BaseSimpleModel;
import models.Restriction;
import models.Schemas;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.Minutes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.*;

import static java.util.Objects.nonNull;
import static java.util.Objects.isNull;

@Entity
@Table(name = "SchedulingReservation", schema = Schemas.SCHEDULING)
public class SchedulingReservation extends BaseSimpleModel {

    private static Finder<Long, SchedulingReservation> finder = new Finder<>(Long.class, SchedulingReservation.class);

    @Id
    @Column(name = "IDSchedulingReservation")
    public Long id;

    @Column(name = "IDUnity")
    public Long unityId;

    @Column(name = "IDInstitution")
    public Long institutionId;

    @ManyToOne
    @JoinColumn(name = "IDScheduling", nullable = false)
    public Scheduling scheduling;

    @Column(name = "IDExamProcedure")
    public Long examProcedureId;

    @Column(name = "IDInsurance")
    public Long insuranceId;

    @Column(name = "IDInsurancePlan")
    public Long insurancePlanId;

    @Column(name = "IDPhysician")
    public Long physicianId;

    @OneToMany(mappedBy = "schedulingReservation")
    public List<SchedulingReservationSlot> slots;

    @OneToMany(mappedBy = "schedulingReservation")
    public List<SchedulingReservationRestriction> restrictions;

    @Column(name = "Price")
    public BigDecimal price;

    @Column(name = "CreationDate")
    public DateTime created;

    @Column(name = "UpdatedDate")
    public DateTime updated;


    public SchedulingReservation() {
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
    }

    public SchedulingReservation(Long institutionId, Scheduling scheduling, Long insuranceId,
                                 Long insurancePlanId, Long examProcedureId, BigDecimal price,
                                 Long physicianId, Long unityId) {
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
        this.institutionId = institutionId;
        this.scheduling = scheduling;
        this.examProcedureId = examProcedureId;
        this.insuranceId = insuranceId;
        this.insurancePlanId = insurancePlanId;
        this.physicianId = physicianId;
        this.price = price;
        this.unityId = unityId;
    }

    public static SchedulingReservation findById(Long id) {
        return nonNull(id) ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void addRestrictions(List<Long> ids){

      if(CollectionUtils.isNotEmpty(ids)){
          for(Long restrictionId : ids){

              SchedulingReservationRestriction reservationRestriction  =
                      new SchedulingReservationRestriction(restrictionId, this);
              reservationRestriction.save();
          }
      }
    }


    public void addSlots(List<Long> slotIds){

        for(Long slot : slotIds){

            SchedulingReservationSlot reservationSlot =
                    new SchedulingReservationSlot(slot, this);
            reservationSlot.save();
        }

    }

    public static List<SchedulingReservation> findBySchedulingId(Long schedulingId){
        return finder.where().eq("scheduling.id", schedulingId).findList();
    }

    public Map<String, DateTime> getDates(){

        String query = "select min(StartedAt) as startedAt, max(EndedAt) as endedAt\n" +
                "from \n" +
                "  Scheduling.SchedulingReservationSlot srs\n" +
                "  join Scheduling.Slot s on srs.IDSlot = s.IDSlot" +
                " where " +
                "    s.IDSlot in (" + Joiner.on(",").join(getSlotIds()) + ")";

        SqlRow sqlRow = Ebean.createSqlQuery(query).findUnique();
        if(isNull(sqlRow)){
            return MapUtils.EMPTY_MAP;
        }
        return ImmutableMap.<String, DateTime>builder()
                .put("startedAt", new DateTime(sqlRow.getDate("startedAt")))
                .put("endedAt",new DateTime(sqlRow.getDate("endedAt")))
                .build();

    }

    private List<Long> getSlotIds(){

        List<Long> ids = new ArrayList<>();
        for(SchedulingReservationSlot slot : slots){
            ids.add(slot.slotId);

        }
        return ids;
    }

    public List<RestrictionCheckDTO>  getRestrictionAsCheckDTO(){

        List<RestrictionCheckDTO> dto = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(this.restrictions)){
            for(SchedulingReservationRestriction reservation : this.restrictions){
                Restriction restriction = Restriction.findById(reservation.restrictionId);
                dto.add(new RestrictionCheckDTO(
                        restriction.getId(),
                        restriction.name,
                        true
                ));
            }
        }

        return dto;
    }


}


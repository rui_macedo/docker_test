package models.scheduling;

import com.pixeon.utils.common.utils.DateUtils;
import dto.PrePatientDTO;
import models.BaseSimpleModel;
import models.Schemas;
import org.joda.time.DateTime;
import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

import static java.util.Objects.nonNull;

@Entity
@Table(name = "PatientPreRegister", schema = Schemas.SCHEDULING)
public class PrePatient extends BaseSimpleModel{

    private static final long serialVersionUID = 1L;

    private static Model.Finder<Long, PrePatient> finder = new Model.Finder<>(Long.class, PrePatient.class);

    @Id
    @Column(name = "IDPatientPreRegister")
    public Long id;

    @Column(name = "IDInstitution")
    public Long institutionId;

    @Column(name = "Name", length = 100)
    public String name;

    @Column(name = "CreationDate")
    public DateTime created;

    @Column(name = "UpdatedDate")
    public DateTime updated;

    public PrePatient() {
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
    }

    public static PrePatient findById(Long id) {
        return nonNull(id) ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public static List<PrePatient> findAllByInstitution(Long institutionId){
        return finder.where().eq("institutionId", institutionId).findList();
    }

    public PrePatientDTO wrap(){
        PrePatientDTO dto = new PrePatientDTO();
        dto.creationDate = created;
        dto.updatedDate = updated;
        dto.name = name;
        dto.institutionId = institutionId;
        dto.id = id;
        return dto;
    }

}

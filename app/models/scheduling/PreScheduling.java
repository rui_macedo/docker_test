package models.scheduling;

import com.pixeon.utils.common.utils.DateUtils;

import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import dto.SchedulingDTO;
import models.BaseSimpleModel;
import models.Schemas;
import play.db.ebean.Model;

import static java.util.Objects.nonNull;

@Entity
@Table(name = "PreScheduling", schema = Schemas.SCHEDULING)
public class PreScheduling extends BaseSimpleModel {

    private static Model.Finder<Long, PreScheduling> finder = new Model.Finder<>(Long.class, PreScheduling.class);

    @Id
    @Column(name = "IDPreScheduling")
    public Long id;

    @Column(name = "IDInstitution")
    public Long institutionId;

    @ManyToOne
    @JoinColumn(name = "IDContact", nullable = false)
    public Contact contact;

    @Column(name = "IDPatient")
    public Long patientId;

    @Column(name = "IDEmployee")
    public Long employeeId;

    @Column(name = "PatientName", length = 60)
    public String patientName;

    @Column(name = "Notes", length = 500)
    public String notes;

    @Column(name = "CreationDate")
    public DateTime created;

    @Column(name = "UpdatedDate")
    public DateTime updated;

    @Column(name = "FinishedDate")
    public DateTime finishDate;

    public PreScheduling() {
        this.created = DateUtils.getCurrentDateTimeGMTZero();
        this.updated = DateUtils.getCurrentDateTimeGMTZero();
    }


    public static PreScheduling findById(Long id) {
        return nonNull(id) ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public SchedulingDTO wrap(){
        SchedulingDTO dto = new SchedulingDTO();
        if(nonNull(contact)){
            dto.contact = contact.wrap();
        }
        dto.creationDate = created;
        dto.updatedDate = updated;
        dto.employeeId = employeeId;
        dto.notes = notes;
        dto.patientId = patientId;
        dto.patientName = patientName;
        dto.institutionId = institutionId;
        dto.id = id;
        return dto;
    }

    public void finish(){
        updated = DateUtils.getCurrentDateTimeGMTZero();;
        finishDate = DateUtils.getCurrentDateTimeGMTZero();
    }

    public Boolean isValuable(){
        return nonNull(contact) &&
               (nonNull(patientId) || nonNull(patientName));
    }

}


package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import play.data.validation.Constraints.Required;

import javax.persistence.*;

@Entity
@Table(name = "SchedulingBookPeriodResource", schema = "scheduling")
public class SchedulingBookPeriodResource extends BaseTenantModel {
    /*
     * Attributes
     */
    private static final long serialVersionUID = 289319451664035264L;

    @Id
    @Column(name = "IDSchedulingBookPeriodResource")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDSchedulingBookPeriod", nullable = false)
    @Required
    @JsonBackReference
    public SchedulingBookPeriod schedulingBookPeriod;

    @ManyToOne
    @JoinColumn(name = "IDResource", nullable = false)
    @Required
    public Resource resource;

  /*
   * Constructors
   */

    public SchedulingBookPeriodResource(Institution institution,
                                        SchedulingBookPeriod schedulingBookPeriod, Resource resource) {
        super(institution);
        this.schedulingBookPeriod = schedulingBookPeriod;
        this.resource = resource;
    }

    public SchedulingBookPeriodResource() {
    }

    ;
  /*
   * Abstract methods
   */


    /*
     * Override methods
     */
    @Override
    public Long getId() {
        return id;
    }
  /*
   * Get/setters/modifiers
   */



  /*
   * Repository methods
   */
}

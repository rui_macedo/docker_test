package models;

import common.annotations.BaseTenantModelFormatter;
import play.data.validation.Constraints.Required;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue("EQUIPMENT")
public class EquipmentResource extends Resource {
  /*
   * Attributes
   */

    private static final long serialVersionUID = -9010645306370920478L;

    @OneToOne
    @JoinColumn(name = "IDEquipment", nullable = true)
    @BaseTenantModelFormatter(clazz = Equipment.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public Equipment equipment;
  /*
   * Constructors
   */

    EquipmentResource() {
    }

    ;

    public EquipmentResource(Equipment equip, Institution institution) {
        if (equip.institution.equals(institution)) {
            this.equipment = equip;
            this.institution = institution;
        }
    }

  /*
   * Abstract methods
   */


  /*
   * Override methods
   */

  /*
   * Get/setters/modifiers
   */


  /*
   * Repository methods
   */
}

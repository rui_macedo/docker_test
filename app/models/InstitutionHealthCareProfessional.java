package models;

import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;


@Entity
@Table(name = "InstitutionHealthCareProfessional", schema = "global")
public class InstitutionHealthCareProfessional extends BaseTenantModel implements PathBindable<InstitutionHealthCareProfessional>,
        QueryStringBindable<InstitutionHealthCareProfessional> {
    /*
     * Attributes
     */
    private static final long serialVersionUID = -5013331221054013199L;
    private static Model.Finder<Long, InstitutionHealthCareProfessional> finder = new Model.Finder<Long, InstitutionHealthCareProfessional>(Long.class, InstitutionHealthCareProfessional.class);

    @Id
    @Column(name = "IDInstitutionHealthCareProfessional")
    public Long id;

    @Column(name = "IDEmployee", nullable = false)
    @Required
    public Long employeeReference;
  
  /*
   * Constructors
   */

  /*
   * Abstract methods
   */

    /*
     * Repository methods
     */
    public static InstitutionHealthCareProfessional findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    /*
     * Override methods
     */
    @Override
    public Option<InstitutionHealthCareProfessional> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            InstitutionHealthCareProfessional obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public InstitutionHealthCareProfessional bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

  /*
   * Get/setters/modifiers
   */

    @Override
    public Long getId() {
        return id;
    }
}

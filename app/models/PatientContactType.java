package models;

import play.db.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PatientContactType", schema="patient")
public class PatientContactType extends BaseSimpleModel{

  private static final Model.Finder<Long, PatientContactType> finder = new Model.Finder<Long, PatientContactType>(
      Long.class, PatientContactType.class);

  private static final long serialVersionUID = 639378937834959932L;

  @Id
  @Column(name = "IDPatientContactType", nullable = false)
  public Long id;

  @Column(name = "ShortName", length = 10)
  public String shortName;

  @Column(name = "Name", length = 50, nullable = false)
  public Long name;

  @Override
  public Long getId() {
    return this.id;
  }

  protected Model.Finder<Long, PatientContactType> getFinder() {
    return finder;
  }

}

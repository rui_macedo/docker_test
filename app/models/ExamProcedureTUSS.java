package models;

import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "ExamProcedureTUSS", schema = "examprocedure")
public class ExamProcedureTUSS extends BaseSimpleModel implements PathBindable<ExamProcedureTUSS>, QueryStringBindable<ExamProcedureTUSS> {
    /**
     * Represents the Global reference to ExamProcedure. According with TUSS list of codes and names
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, ExamProcedureTUSS> finder = new Model.Finder<Long, ExamProcedureTUSS>(Long.class, ExamProcedureTUSS.class);
    @Id
    @Column(name = "IDExamProcedureTUSS")
    public Long id;
    @Column(name = "Name", length = 200, nullable = false)
    @Required
    public String name;
    @Column(name = "Code", length = 30, nullable = false)
    @Required
    public String code;


    /* Constructor */
    public ExamProcedureTUSS(String name, String code) {
        super();
        this.name = name;
        this.code = code;
    }

    public static ExamProcedureTUSS findById(Long id) {
        return finder.byId(id);
    }

    public static List<ExamProcedureTUSS> findAll() {
        return finder.all();
    }

    public static List<ExamProcedureTUSS> findAll(String search) {
        search = search.replace("%", "");
        search = search.trim();

        if (search == null || search.isEmpty()) {
            return finder.findList();
        } else {
            return finder.where().ilike("name", "%" + search + "%").findList();
        }

    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<ExamProcedureTUSS> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            ExamProcedureTUSS obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public ExamProcedureTUSS bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

}

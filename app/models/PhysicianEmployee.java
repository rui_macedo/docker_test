package models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import common.annotations.BaseSimpleModelFormatter;
import common.utils.FormattingUtils;
import org.joda.time.DateTime;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "PhysicianEmployee", schema = "global")
public class PhysicianEmployee extends BaseTenantModel implements PathBindable<PhysicianEmployee>,
        QueryStringBindable<PhysicianEmployee> {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, PhysicianEmployee> finder =
            new Model.Finder<Long, PhysicianEmployee>(Long.class, PhysicianEmployee.class);
    @Id
    @Column(name = "IDPhysicianEmployee")
    public Long id;
    @Column(name = "Name", nullable = false, length = 100)
    @Required
    public String name;
    /* Employee in this Institution */
    @OneToOne
    @JoinColumn(name = "IDEmployee", nullable = false)
    @JsonManagedReference(value = "physicianEmployee")
    @Required
    public Employee employee;
    @Column(name = "Signature", nullable = true)
    public String signature;
    /* Physician */
    @ManyToOne
    @JoinColumn(name = "IDPhysician")
    @BaseSimpleModelFormatter(clazz = Physician.class, idFieldIsString = false, idFieldName = "id")
    public Physician physician;
    /* Medical Council */
    @ManyToOne
    @JoinColumn(name = "IDMedicalCouncil", nullable = false)
    @BaseSimpleModelFormatter(clazz = MedicalCouncil.class, idFieldIsString = false,
            idFieldName = "id")
    @Required
    public MedicalCouncil medicalCouncil;
    @Column(name = "MedicalCouncilRegistration", nullable = false, length = 20)
    @Required
    public String medicalCouncilRegistration;
    @ManyToOne
    @JoinColumn(name = "MedicalCouncilState", nullable = false)
    @BaseSimpleModelFormatter(clazz = State.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public State medicalCouncilState;
    @Column(name = "IdentificationRG", length = 20)
    @Required
    public String identificationRG;
    @Column(name = "IdentificationCPF", length = 20)
    public String identificationCPF;
    @Column(name = "DateOfBirth")
    public DateTime dateOfBirth;
    @ManyToOne
    @JoinColumn(name = "IDAddress", nullable = false)
    @BaseSimpleModelFormatter(clazz = Address.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public Address address;
    @Column(name = "AddressNumber", length = 6)
    @Required
    public String addressNumber;
    @Column(name = "AddressComplement", length = 30)
    public String addressComplement;
    @Column(name = "phoneNumber", length = 30)
    public String phoneNumber;
    @Column(name = "faxNumber", length = 30)
    public String faxNumber;
    @Column(name = "email", length = 100)
    public String email;


    /* Constructor using Physician Information */
    public PhysicianEmployee(Institution institution, Physician physician, Employee employee) {
        super(institution);
        this.physician = physician;
        this.employee = employee;
        this.name = physician.name;
        this.medicalCouncil = physician.medicalCouncil;
        this.medicalCouncilRegistration = physician.getMedicalCouncilRegistration();
        this.identificationCPF = physician.identificationCPF;
        this.identificationRG = physician.identificationRG;
        this.address = physician.address;
        this.addressComplement = physician.addressComplement;
        this.addressNumber = physician.addressNumber;
        this.phoneNumber = physician.phoneNumber;
        this.faxNumber = physician.faxNumber;
        this.email = physician.email;
        this.dateOfBirth = physician.dateOfBirth;
        this.medicalCouncilState = physician.medicalCouncilState;
    }

    public static PhysicianEmployee findById(Long id) {
        if (id == null) {
            return null;
        }
        return finder.byId(id);

    }

    public static List<PhysicianEmployee> findAllByName(Long idInstitution, String name) {
        name = name.trim();
        name = name.replace("%", "");
        return finder.where().eq("idInstitution", idInstitution).ilike("name", name + "%").findList();
    }

    public static List<PhysicianEmployee> findAllByMedicalCouncilRegistration(Long idInstitution,
                                                                              String medicalCouncilRegistration) {
        medicalCouncilRegistration = medicalCouncilRegistration.trim();
        medicalCouncilRegistration = medicalCouncilRegistration.replace("%", "");
        return finder.where().eq("idInstitution", idInstitution)
                .eq("medicalCouncilRegistration", medicalCouncilRegistration).findList();
    }

    public static List<PhysicianEmployee> findAllByInstitution(Long idInstitution) {
        return finder.where().eq("idInstitution", idInstitution).findList();
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<PhysicianEmployee> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            PhysicianEmployee obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public PhysicianEmployee bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }


}

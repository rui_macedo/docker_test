package models;

import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.Junction;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import common.annotations.BaseTenantModelFormatter;
import common.constants.CalendarDay;
import common.constants.DayShift;
import common.utils.DateUtils;
import dto.SchedulingBookPeriodDTO;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "SchedulingBookPeriod", schema = "scheduling")
public class SchedulingBookPeriod extends BaseTenantModel {

  /*
   * Attributes
   */

    private static final long serialVersionUID = -7461949918116825775L;
    private static Model.Finder<Long, SchedulingBookPeriod> finder =
            new Model.Finder<Long, SchedulingBookPeriod>(Long.class, SchedulingBookPeriod.class);

    @Id
    @Column(name = "IDSchedulingBookPeriod")
    public Long id;
    @ManyToOne
    @JoinColumn(name = "IDSchedulingBookWeekAgenda")
    @BaseTenantModelFormatter(clazz = SchedulingBookWeekAgenda.class, idFieldIsString = false,
            idFieldName = "id")
    @Required
    @JsonBackReference
    public SchedulingBookWeekAgenda schedulingBookWeekAgenda;
    @OneToMany(mappedBy = "schedulingBookPeriod", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @BaseTenantModelFormatter(clazz = SchedulingBookPeriodResource.class, idFieldIsString = false,
            idFieldName = "id")
    @JsonManagedReference
    public Set<SchedulingBookPeriodResource> schedulingBookPeriodResources;
    @Column(name = "StartAt", nullable = false)
    Integer startAt;
    @Column(name = "EndAt", nullable = false)
    Integer endAt;
    @Transient
    private Interval zeroEpochInterval;

    /*
     * Constructors
     */
    public SchedulingBookPeriod(Institution institution, Integer startAt, Integer endAt,
                                SchedulingBookWeekAgenda schedulingBookWeekAgenda) {
        super(institution);
        this.startAt = startAt;
        this.endAt = endAt;
        this.schedulingBookWeekAgenda = schedulingBookWeekAgenda;
    }

    public SchedulingBookPeriod() {
    }

    ;

  /*
   * Abstract methods
   */

    public static SchedulingBookPeriod findPeriodByBook(SchedulingBook book, int startAt, int endAt,
                                                        CalendarDay calendarDay) {
        return finder.where().eq("startAt", startAt).eq("endAt", endAt)
                .eq("schedulingBookWeekAgenda.schedulingBook", book)
                .eq("schedulingBookWeekAgenda.calendarDay", calendarDay).findUnique();
    }

  /*
   * Get/setters/modifiers
   */

    public static List<SchedulingBookPeriod> findAllPeriodsByCalendarDay(SchedulingBook book, CalendarDay calendarDay) {
        return finder.where().eq("schedulingBookWeekAgenda.schedulingBook", book)
                .eq("schedulingBookWeekAgenda.calendarDay", calendarDay).findList();
    }

    public static List<SchedulingBookPeriod> findSpecificOverlapedPeriodsByCalendarDay(SchedulingBook book,
                                                                                       CalendarDay calendarDay, List<DayShift> namedPeriods) {
        ExpressionList<SchedulingBookPeriod> query =
                finder.where().eq("schedulingBookWeekAgenda.schedulingBook", book)
                        .eq("schedulingBookWeekAgenda.calendarDay", calendarDay);
        if (namedPeriods != null && !namedPeriods.isEmpty()) {
            Junction<SchedulingBookPeriod> disjunction = query.disjunction();
            for (DayShift pr : namedPeriods) {
                disjunction.or(Expr.and(Expr.ge("startAt", pr.getStartAt()), Expr.lt("startAt", pr.getEndAt())), Expr.and(Expr.le("endAt", pr.getEndAt()), Expr.gt("endAt", pr.getStartAt())));
            }
            return disjunction.findList();
        }
        return query.findList();
    }

    public static Set<SchedulingBookPeriod> unwrapByListPeriod(List<SchedulingBookPeriodDTO> schedulingBookPeriods, Long idInstitution) {
        Set<SchedulingBookPeriod> set = new HashSet<>();
        if (schedulingBookPeriods == null) {
            return set;
        }
        for (SchedulingBookPeriodDTO dto : schedulingBookPeriods) {
            SchedulingBookPeriod period = new SchedulingBookPeriod();
            period.startAt = dto.startedAt;
            period.endAt = dto.endedAt;
            period.institution = Institution.byId(idInstitution);
            set.add(period);
        }
        return set;
    }

    /*
     * Override methods
     */
    @Override
    public Long getId() {
        return this.id;
    }

    public List<Resource> getResources() {
        List<Resource> resources = new ArrayList<Resource>();
        for (SchedulingBookPeriodResource relResource : schedulingBookPeriodResources) {
            resources.add(relResource.resource);
        }
        return resources;
    }

    public Interval getZeroEpochInterval() {
        if (this.zeroEpochInterval == null) {
            this.zeroEpochInterval = new Interval(startAt, endAt);
        }
        return zeroEpochInterval;
    }

    public int getSlotSizeMin() {
        return this.schedulingBookWeekAgenda.schedulingBook.slotSizeMin;
    }

/*  public void addResource(Resource resource) {
    if (isValidResource(resource)) {
      SchedulingBookPeriodResource periodResource =
          new SchedulingBookPeriodResource(institution, this, resource);
      periodResource.save();
      this.schedulingBookPeriodResources.add(periodResource);
      this.update();
    }
  }*/

    public int getSlotSizeMillis() {
        return getSlotSizeMin() * 60 * 1000;
    }

  /*
   * Repository methods
   */

    public void settInterval(Interval interval) {
        Long st = interval.getStartMillis();
        Long en = interval.getEndMillis();
        if (st > 86400000 || en > 86400000) {
            throw new IllegalArgumentException(
                    "This interval it's only valid to hours in one day. You only can set at max 24 hours (in millis)");
        }
        this.startAt = (int) interval.getStartMillis();
        this.endAt = (int) interval.getEndMillis();
    }

    public Interval calculateAvaibleInterval(Interval intervalRestriction) {
        Interval realInterval = this.getZeroEpochInterval();
        int slot = getSlotSizeMillis();
        if (intervalRestriction != null) {
            realInterval =
                    DateUtils.subtractIntervalWithSlotRound(realInterval, intervalRestriction, slot);
            if (realInterval == null) {
                return null;
            }
        }
        // FIXME: PRIMEIRA VERSÃO
        if (schedulingBookPeriodResources.size() > 1) {
            throw new IllegalAccessError("This version only can support one resource by period!");
        }
        for (SchedulingBookPeriodResource periodResource : schedulingBookPeriodResources) {
            // retrieve relative resource agenda
            ResourceWeekAgenda resourceAgenda =
                    periodResource.resource
                            .getWeekAgendaByCalendarDay(this.schedulingBookWeekAgenda.calendarDay);
            Interval resInterval = resourceAgenda.getZeroEpochInterval();
            realInterval = DateUtils.overlapIntervalsWithSlotRound(realInterval, resInterval, slot);
        }
        return realInterval;
    }

    public List<Interval> getAvaibleIntervalSlotsInADay(Interval searchDayInterval, List<DayShift> namedPeriods) {
        Interval avaiblePeriodInterval = null;
        if (!searchDayInterval.getStart().toLocalDate()
                .equals(searchDayInterval.getEnd().toLocalDate())) {
            throw new IllegalArgumentException("Interval must be in the same day");
        }
        if (searchDayInterval.getStart().toLocalDate().equals(DateTime.now().toLocalDate())) {
            avaiblePeriodInterval =
                    this.calculateAvaibleInterval(new Interval(0, searchDayInterval.getStart()
                            .getMillisOfDay()));
        } else {
            avaiblePeriodInterval = this.calculateAvaibleInterval(null);
        }
        if (avaiblePeriodInterval != null) {
            List<ScheduledProcedureResource> scheduledToResources =
                    new ArrayList<ScheduledProcedureResource>();
            for (SchedulingBookPeriodResource periodResource : this.schedulingBookPeriodResources) {
                scheduledToResources.addAll(ScheduledProcedureResource
                        .findScheduledProcedureResourceByResource(periodResource.resource, searchDayInterval));
            }

            List<Interval> availableSlots =
                    DateUtils.splitInterval(avaiblePeriodInterval, this.getSlotSizeMillis());
            List<Interval> invalidSlots = new ArrayList<Interval>();
            for (ScheduledProcedureResource scheduledToResource : scheduledToResources) {
                Interval scheduledProcedure = scheduledToResource.getZeroEpochInterval();
                if (avaiblePeriodInterval.overlaps(scheduledProcedure)) {
                    for (Interval slot : availableSlots) {
                        if (slot.equals(scheduledProcedure)) {
                            invalidSlots.add(slot);
                        }

                    }
                }
            }
            if (!namedPeriods.isEmpty()) {
                for (Interval slot : availableSlots) {
                    boolean isOverlapedByInterval = false;
                    for (DayShift period : namedPeriods) {
                        if (period.getInterval().contains(slot)) {
                            isOverlapedByInterval = true;
                            break;
                        }
                    }
                    if (!isOverlapedByInterval) {
                        invalidSlots.add(slot);
                    }
                }
            }


            availableSlots.removeAll(invalidSlots);
            return availableSlots;
        }
        return Collections.emptyList();
    }

    public boolean isValidResource(Resource resource) {
        boolean isValid = true;
        if (this.institution.equals(resource.institution)) {
            for (SchedulingBookPeriodResource schedulingBookPeriodResource : schedulingBookPeriodResources) {
                if (schedulingBookPeriodResource.resource.equals(resource)) {
                    isValid = false;
                    break;
                }
            }
        } else {
            isValid = false;
        }
        return isValid;
    }

    public SchedulingBookPeriodDTO wrap() {
        SchedulingBookPeriodDTO dto = new SchedulingBookPeriodDTO();
        dto.id = this.id;
        dto.startedAt = this.startAt;
        dto.endedAt = this.endAt;
        return dto;
    }

    public Integer getStartAt() {
        return startAt;
    }

    public Integer getEndAt() {
        return endAt;
    }

}

package models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import org.joda.time.DateTime;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;
import utils.JsonFloatSerializer;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "IndexerValue", schema = "global")
public class IndexerValue extends BaseTenantModel implements PathBindable<IndexerValue>,
        QueryStringBindable<IndexerValue> {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, IndexerValue> finder = new Model.Finder<Long, IndexerValue>(
            Long.class, IndexerValue.class);
    @Id
    @Column(name = "IDIndexerValue")
    public Long id;
    /* Indexer owner */
    @ManyToOne(fetch = FetchType.LAZY)
    @Required
    @JoinColumn(name = "IDIndexer", nullable = false)
    @BaseTenantModelFormatter(clazz = Indexer.class, idFieldIsString = false,
            idFieldName = "id")
    public Indexer indexer;
    @Column(name = "ValidFromDate")
    @Required
    public DateTime validFromDate;
    @JsonSerialize(using = JsonFloatSerializer.class)
    @Column(name = "Value")
    @Required
    private Float value;

    /*Constructor*/
    public IndexerValue(Institution institution, Indexer indexer) {
        super(institution);
        this.indexer = indexer;
    }

    public static IndexerValue findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<IndexerValue> findAllByInstitution(Long idInstitution, Indexer indexer) {
        return finder.where()
                .eq("idInstitution", idInstitution)
                .eq("idIndexer", indexer.id)
                .findList();
    }

    public Float getValue() {
        return FormattingUtils.formatFloatNumber(this.value, 4);
    }

    public void setValue(Float value) {
        this.value = FormattingUtils.formatFloatNumber(value, 4);
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<IndexerValue> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("indexervalue")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            IndexerValue obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public IndexerValue bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }


}
 
 
 
 
 
 

package models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import play.Play;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import play.data.validation.Constraints.MinLength;
import utils.email.Mail;


@JsonSerialize
public class EmailMessage {

    @Email
    @Required
    public String from = Play.application().configuration().getString("mailservice.sender"); // default
    // value

    @Email
    @Required
    public String to;

    public String cc;
    public String bcc;

    @Required
    @MinLength(value = 5)
    public String subject;

    public String htmlBody;
    public String textBody;

    @Override
    public String toString() {
        return "EmailMessage [from=" + from + ", to=" + to + ", cc=" + cc + ", bcc=" + bcc
                + ", subject=" + subject + ", htmlBody=" + htmlBody + ", textBody=" + textBody + "]";
    }

    public void send() {
        Mail.sendMail(this);
    }
}
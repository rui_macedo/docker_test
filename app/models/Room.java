package models;

import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "Room", schema = "global")
public class Room extends BaseTenantModel implements PathBindable<Room>, QueryStringBindable<Room> {

    private static final long serialVersionUID = -7121824162686242216L;

    private static Model.Finder<Long, Room> finder = new Model.Finder<Long, Room>(Long.class,
            Room.class);

    @Id
    @Column(name = "IDRoom")
    public Long id;

    @Column(name = "Name", nullable = false, unique = true, length = 30)
    @Required
    public String name;

    @ManyToOne
    @JoinColumn(name = "IDSection", nullable = true)
    @BaseTenantModelFormatter(clazz = Section.class, idFieldIsString = false, idFieldName = "id")
    public Section section;

    public Room(Institution institution, String name) {
        super(institution);
        this.name = name;
    }

    public Room() {

    }

    public static Room findById(Long id) {
        return finder.byId(id);
    }

    public static List<Room> findAllByInstitutionId(Long institutionId) {
        return finder.where().eq("institution.id", institutionId).findList();
    }

    @Override
    public Option<Room> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("room")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return Option.None();
        }
        Room obj = findById(new Long(pathId));
        return obj != null ? Option.Some(obj) : Option.None();
    }

    @Override
    public Room bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Number getId() {
        return id;
    }
}

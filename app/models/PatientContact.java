package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="PatientContact", schema="patient")
public class PatientContact extends BaseSimpleModel {
  private static final Model.Finder<Long, PatientContact> finder = new Model.Finder<Long, PatientContact>(
      Long.class, PatientContact.class);

  private static final long serialVersionUID = 5826936622362667473L;

  @Id
  @Column(name = "IDPatientContact", nullable = false)
  public Long id;

  @JsonBackReference
  @ManyToOne
  @JoinColumn(name = "IDPatient")
  public Patient patient;

  @JsonBackReference
  @ManyToOne
  @JoinColumn(name = "IDPatientContactType")
  public PatientContactType type;

  @Column(name = "Value", length=50, nullable = false)
  public String value;

  @Override
  public
  Long getId() {
    return this.id;
  }

  protected Model.Finder<Long, PatientContact> getFinder() {
    return finder;
  }

  public static List<PatientContact> findEmailsByPatientId(Number patientId) {
    return finder.where().eq("patient.id", patientId).eq("type.shortName", "email").findList();
  }


}

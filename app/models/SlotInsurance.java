package models;

import common.utils.FormattingUtils;
import play.db.ebean.Model;
import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "SlotInsurance", schema = "scheduling")
public class SlotInsurance extends BaseSimpleModel implements PathBindable<SlotInsurance>, QueryStringBindable<SlotInsurance> {

    private static final long serialVersionUID = 6602902515211074435L;

    public static Model.Finder<Long, SlotInsurance> finder = new Model.Finder<>(Long.class, SlotInsurance.class);

    @Id
    @Column(name = "IDSlot")
    public Long id;

    @ManyToOne
    @JoinColumn(name = "IDSlot", nullable = true)
    public Slot slot;

    @ManyToOne
    @JoinColumn(name = "IDInsurance", nullable = true)
    public Insurance insurance;

    public SlotInsurance() {
    }

    public SlotInsurance(Slot slot, Insurance insurance) {
        super();
        this.slot = slot;
        this.insurance = insurance;
    }

    public static SlotInsurance findById(Long id) {
        return id != null ? finder.byId(id) : null;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public F.Option<SlotInsurance> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("slotInsurance")[0];
        if (!FormattingUtils.isNumberCompatible(pathId)) {
            return F.Option.None();
        }
        SlotInsurance obj = findById(new Long(pathId));
        return obj != null ? F.Option.Some(obj) : F.Option.None();
    }

    @Override
    public SlotInsurance bind(String arg0, String arg1) {
        return FormattingUtils.isNumberCompatible(arg1) ? findById(new Long(arg1)) : null;
    }

}

package models.searchalg.finders;

import common.constants.DayShift;
import models.ExamProcedure;
import models.Institution;
import models.Unity;
import models.searchalg.DynamicSlotGroup;
import org.joda.time.DateTime;
import play.libs.F.Promise;
import services.searchers.SchedulingSearchProvider;

import java.util.ArrayList;
import java.util.List;

public class FreeSlotsByExamProcedureFinderStrategy implements DynamicSlotGroupFinderStrategy {

    private ExamProcedure examProcedure;
    private List<Unity> unities;
    private Institution institution;

    public FreeSlotsByExamProcedureFinderStrategy(ExamProcedure examProcedure, List<Unity> unities,
                                                  Institution institution) {
        this.examProcedure = examProcedure;
        this.unities = unities;
        this.institution = institution;
    }

    @Override
    public List<Promise<DynamicSlotGroup>> getPromiseListOfDynamicSlotGroup(DateTime dayToSearch, List<DayShift> periods) {
        List<Promise<DynamicSlotGroup>> slotsByExamProcedure = new ArrayList<Promise<DynamicSlotGroup>>();
        Promise<DynamicSlotGroup> slotsInDay = SchedulingSearchProvider.findFreeSlotsInDayByExamProcedure(dayToSearch, periods, examProcedure, unities, institution);
        if (slotsInDay != null) {
            slotsByExamProcedure.add(slotsInDay);
        }
        return slotsByExamProcedure;
    }

}

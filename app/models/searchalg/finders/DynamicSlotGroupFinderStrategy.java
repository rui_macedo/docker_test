package models.searchalg.finders;

import common.constants.DayShift;
import models.searchalg.DynamicSlotGroup;
import org.joda.time.DateTime;
import play.libs.F.Promise;

import java.util.List;

public interface DynamicSlotGroupFinderStrategy {

    public List<Promise<DynamicSlotGroup>> getPromiseListOfDynamicSlotGroup(DateTime dayToSearch, List<DayShift> periods);

}

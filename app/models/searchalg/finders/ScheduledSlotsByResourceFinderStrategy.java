package models.searchalg.finders;

import common.constants.DayShift;
import models.Resource;
import models.ScheduledProcedureResource;
import models.searchalg.DynamicSlot;
import models.searchalg.DynamicSlotGroup;
import org.joda.time.DateTime;
import play.libs.F.Function0;
import play.libs.F.Promise;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ScheduledSlotsByResourceFinderStrategy implements DynamicSlotGroupFinderStrategy {

    private Resource resource;

    public ScheduledSlotsByResourceFinderStrategy(Resource resource) {
        this.resource = resource;

    }

    /* (non-Javadoc)
     * @see DynamicSlotGroupFinderStrategy#getPromiseListOfDynamicSlotGroup(org.joda.time.DateTime, java.util.List)
     * This method isn't using periods. Nowdays, is not defined this attribute in search of scheduled slots
     */
    @Override
    public List<Promise<DynamicSlotGroup>> getPromiseListOfDynamicSlotGroup(DateTime dayToSearch,
                                                                            List<DayShift> periods) {
        List<Promise<DynamicSlotGroup>> slotsByExamProcedure =
                new ArrayList<Promise<DynamicSlotGroup>>();
        Promise<DynamicSlotGroup> dailySlots = Promise.promise(new Function0<DynamicSlotGroup>() {
            public DynamicSlotGroup apply() {
                Collection<ScheduledProcedureResource> schs =
                        ScheduledProcedureResource.findDaylyScheduledProcedureResourceByResource(resource,
                                dayToSearch);
                List<DynamicSlot> schedules = new ArrayList<DynamicSlot>();
                for (ScheduledProcedureResource spr : schs) {
                    schedules.add(new DynamicSlot(spr));
                }
                return new DynamicSlotGroup(schedules);
            }
        });
        slotsByExamProcedure.add(dailySlots);
        return slotsByExamProcedure;
    }
}

package models.searchalg.analyzers;

import models.searchalg.DynamicSlotGroup;
import models.searchalg.validators.DynamicSlotGroupValidator;

import java.util.ArrayList;
import java.util.List;

public class ListAllAnalyzer implements DynamicSlotGroupAnalyzer {

    private List<DynamicSlotGroup> returnAll;

    @Override
    public void setStudySet(List<DynamicSlotGroup> dynamicSlotGroups) {
        this.returnAll = dynamicSlotGroups;
    }

    @Override
    public List<DynamicSlotGroup> analyze(DynamicSlotGroupValidator validator) {
        List<DynamicSlotGroup> validOnes = new ArrayList<DynamicSlotGroup>();
        for (DynamicSlotGroup dsg : returnAll) {
            if (validator.validate(dsg)) {
                validOnes.add(dsg);
            }
        }
        return validOnes;
    }
}

package models.searchalg.analyzers;

import models.searchalg.DynamicSlot;
import models.searchalg.DynamicSlotGroup;
import models.searchalg.validators.DynamicSlotGroupValidator;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SetOfExamsAnalyzer implements DynamicSlotGroupAnalyzer {

    public DynamicSlotGroup reference;

    public List<DynamicSlotGroup> comparativeList;

    public Duration maxTolerance;

    @Override
    public void setStudySet(List<DynamicSlotGroup> dynamicSlotGroups) {
        //reset comparativeList
        comparativeList = new ArrayList<DynamicSlotGroup>();
        //for better analyzes, sets the bigger dynamicslotgroup as reference
        int size = -1;
        for (DynamicSlotGroup d : dynamicSlotGroups) {
            if (d.getSize() > size) {
                size = d.getSize();
                reference = d;
            }
        }
        Iterator<DynamicSlotGroup> itGroups = dynamicSlotGroups.iterator();
        while (itGroups.hasNext()) {
            DynamicSlotGroup g = itGroups.next();
            if (!g.equals(reference)) {
                comparativeList.add(g);
            }
        }
    }

    @Override
    public List<DynamicSlotGroup> analyze(DynamicSlotGroupValidator validator) {
        List<DynamicSlotGroup> resultList = new ArrayList<DynamicSlotGroup>();
        for (DynamicSlot slot : reference.getExams()) {
            DynamicSlotGroup groupOfExams = new DynamicSlotGroup();
            groupOfExams.addDynamicSlot(slot);
            for (DynamicSlotGroup group : comparativeList) {
                DynamicSlot bestSlot = group.getMostClosestSlot(slot);
                if (!groupOfExams.addDynamicSlot(bestSlot)) {
                    List<DynamicSlot> ignored = new ArrayList<DynamicSlot>();
                    ignored.add(bestSlot);
                    int attempts = 0;
                    while (attempts < 4) {
                        bestSlot = group.getMostClosestSlot(slot, ignored.toArray(new DynamicSlot[ignored.size()]));
                        if (!groupOfExams.addDynamicSlot(bestSlot)) {
                            attempts++;
                        } else {
                            break;
                        }
                    }
                }
            }
            if (groupOfExams.getExams().size() == (comparativeList.size() + 1) && validator.validate(groupOfExams)) {
                resultList.add(groupOfExams);
            }
        }
        return resultList;
    }
}

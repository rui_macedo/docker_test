package models.searchalg.analyzers;

import models.searchalg.DynamicSlotGroup;
import models.searchalg.validators.DynamicSlotGroupValidator;

import java.util.List;


public interface DynamicSlotGroupAnalyzer {

    public void setStudySet(List<DynamicSlotGroup> dynamicSlotGroups);

    public List<DynamicSlotGroup> analyze(DynamicSlotGroupValidator validator);

}

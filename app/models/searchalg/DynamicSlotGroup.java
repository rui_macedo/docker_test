package models.searchalg;

import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * We no longer have a dynamic slot
 */

@Deprecated
public class DynamicSlotGroup implements Comparable<DynamicSlotGroup> {

    private List<DynamicSlot> exams;

    private Integer startAt;

    private Integer endAt;

    public DynamicSlotGroup(List<DynamicSlot> exams) {
        this.exams = exams;
        if (!exams.isEmpty()) {
            Collections.sort(exams);
            this.startAt = exams.get(0).startAt;
            this.endAt = exams.get(exams.size() - 1).endAt;
        }
    }

    public DynamicSlotGroup() {
        exams = new ArrayList<DynamicSlot>();
    }

    public DynamicSlot getMostClosestSlot(DynamicSlot pSlot, DynamicSlot... ignore) {
        DynamicSlot mostClosest = null;
        if (pSlot != null) {
            Interval pSlotInterval = pSlot.getInterval();
            Long duration = 86400000l;//24h biggest time in a day

            List<DynamicSlot> ignoredList = Arrays.asList(ignore);
            for (DynamicSlot slot : exams) {
                if (!ignoredList.contains(slot)) {
                    Interval slotInterval = slot.getInterval();
                    if (!slotInterval.overlaps(pSlotInterval)) {
                        Interval gapInterval = pSlotInterval.gap(slotInterval);
                        if (gapInterval == null) {//slots are neighbors
                            mostClosest = slot;
                            break;//don't need to find others
                        }
                        Long gapBetween = gapInterval.toDurationMillis();
                        if (gapBetween < duration) {
                            mostClosest = slot;
                            duration = gapBetween;
                        }
                    }
                }
            }
        }
        return mostClosest;
    }

    public boolean addDynamicSlot(DynamicSlot bestSlot) {
        boolean overlaps = false;
        if (bestSlot != null) {
            for (DynamicSlot slot : exams) {
                if (slot.getInterval().overlaps(bestSlot.getInterval())) {
                    overlaps = true;
                    break;
                }
            }
            if (!overlaps) {
                exams.add(bestSlot);
                if (startAt == null || startAt > bestSlot.startAt) {
                    startAt = bestSlot.startAt;
                }
                if (endAt == null || endAt < bestSlot.endAt) {
                    endAt = bestSlot.endAt;
                }
            }
            return !overlaps;
        }
        return false;
    }

    public List<DynamicSlot> getExams() {
        Collections.sort(exams);
        return Collections.unmodifiableList(exams);
    }

    public Integer getStartAt() {
        return this.startAt;
    }

    public Integer getEndAt() {
        return this.endAt;
    }

    public Integer getSize() {
        return exams.size();
    }

    @Override
    public int compareTo(DynamicSlotGroup o) {
        int diff = this.startAt - o.startAt;
        if (diff == 0) {
            diff = this.endAt - o.endAt;
        }
        return diff;
    }
}

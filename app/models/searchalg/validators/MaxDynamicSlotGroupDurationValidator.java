package models.searchalg.validators;

import models.searchalg.DynamicSlotGroup;
import org.joda.time.Duration;

public class MaxDynamicSlotGroupDurationValidator implements DynamicSlotGroupValidator {

    private Duration duration;

    public MaxDynamicSlotGroupDurationValidator(Duration maxDuration) {
        this.duration = maxDuration;
    }

    @Override
    public boolean validate(DynamicSlotGroup dynamicSlotGroup) {
        if (!dynamicSlotGroup.getExams().isEmpty()) {
            Duration dur = new Duration(dynamicSlotGroup.getStartAt(), dynamicSlotGroup.getEndAt());
            return dur.isShorterThan(duration);
        } else {
            return false;
        }
    }
}

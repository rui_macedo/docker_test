package models.searchalg.validators;

import models.searchalg.DynamicSlotGroup;

public class AlwaysTrueValidator implements DynamicSlotGroupValidator {

    @Override
    public boolean validate(DynamicSlotGroup dynamicSlotGroup) {
        return true;
    }
}

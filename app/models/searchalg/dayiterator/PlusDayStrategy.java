package models.searchalg.dayiterator;

import org.joda.time.DateTime;

import javax.annotation.concurrent.NotThreadSafe;

//FIXME
@NotThreadSafe
public class PlusDayStrategy implements DayIteratorStrategy {

    private DateTime reference;
    private int iteratorTo;

    public PlusDayStrategy(DateTime startDate, int iteratorTo) {
        this.reference = startDate;
        this.iteratorTo = iteratorTo;
    }

    @Override
    public DateTime next() {
        iteratorTo--;
        reference = getCurrentIteratonDay().withTimeAtStartOfDay().plusDays(1);
        return reference;
    }

    @Override
    public boolean hasNext() {
        return iteratorTo > 0;
    }

    @Override
    public DateTime getCurrentIteratonDay() {
        return reference;
    }


}

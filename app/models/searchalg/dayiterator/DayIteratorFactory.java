package models.searchalg.dayiterator;

import common.constants.CalendarDay;
import common.utils.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.List;

public class DayIteratorFactory {

    public static DayIteratorStrategy createDayIteratorStrategy(DateTime start, DateTime end,
                                                                List<CalendarDay> weekDays) {
        if (start == null) {
            start = DateUtils.getCurrentDateTime();
        }
        int days;
        if (end != null && start.isBefore(end)) {
            days = Days.daysBetween(start, end).getDays();
            days = days > 31 ? 31 : days;
        } else {
            days = 15;
        }
        if (weekDays != null && !weekDays.isEmpty()) {
            return new PlusSpecificWeekDays(weekDays, start, days);
        } else {
            return new PlusDayStrategy(start, days);
        }
    }
}

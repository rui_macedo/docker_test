package models.searchalg.dayiterator;

import common.constants.CalendarDay;
import common.utils.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;

import javax.annotation.concurrent.NotThreadSafe;
import java.util.Collections;
import java.util.List;

@NotThreadSafe
public class PlusSpecificWeekDays implements DayIteratorStrategy {

    private final DateTime endDate;
    private List<CalendarDay> weekDays;
    private DateTime referenceDate;
    private int currentListIdx = 0;

    public PlusSpecificWeekDays(List<CalendarDay> weekDays, DateTime start, int daysToGoFoward) {
        Collections.sort(weekDays);
        this.weekDays = weekDays;
        if (start != null) {
            int dayOfWeek = start.dayOfWeek().get();
            currentListIdx = weekDays.indexOf(CalendarDay.getByDayIdx(dayOfWeek));
            if (currentListIdx >= 0) {
                referenceDate = start;
            } else {
                for (int i = 0; i < weekDays.size(); i++) {
                    CalendarDay cl = weekDays.get(i);
                    if (dayOfWeek < cl.getCalendarDayIdx()) {
                        currentListIdx = i;
                        break;
                    }
                }
                if (currentListIdx >= 0) {
                    referenceDate =
                            DateUtils.getNextWeekDay(start, weekDays.get(currentListIdx).getCalendarDayIdx());
                } else {
                    referenceDate = DateUtils.getNextWeekDay(start, weekDays.get(0).getCalendarDayIdx());
                }
            }
        } else {
            referenceDate = DateUtils.getNextWeekDay(weekDays.get(0).getCalendarDayIdx());
        }
        this.endDate = referenceDate.plusDays(daysToGoFoward);
    }

    @Override
    public DateTime next() {
        currentListIdx++;
        if (currentListIdx >= weekDays.size()) {
            currentListIdx = 0;
        }
        this.referenceDate =
                DateUtils.getNextWeekDay(referenceDate, weekDays.get(currentListIdx).getCalendarDayIdx());
        return this.referenceDate;
    }

    @Override
    public boolean hasNext() {
        int idx = currentListIdx + 1;
        if (idx >= weekDays.size()) {
            idx = 0;
        }
        DateTime next = DateUtils.getNextWeekDay(referenceDate, weekDays.get(idx).getCalendarDayIdx());
        return Days.daysBetween(next, endDate).getDays() >= 0;
    }

    @Override
    public DateTime getCurrentIteratonDay() {
        return referenceDate;
    }
}

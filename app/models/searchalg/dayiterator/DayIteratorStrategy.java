package models.searchalg.dayiterator;

import org.joda.time.DateTime;

public interface DayIteratorStrategy {

    public DateTime next();

    public boolean hasNext();

    public DateTime getCurrentIteratonDay();

}

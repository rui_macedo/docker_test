package models.searchalg;

import common.constants.CalendarDay;
import models.ExamProcedure;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class ScheduleDailyCard {
    public List<DynamicSlotGroup> slotsOfDay = new ArrayList<DynamicSlotGroup>();
    private String priorityInfoValue;
    private String priorityInfoLabel;
    private CalendarDay dayOfCalendar;
    private Date referenceDate;
    private List<ExamProcedure> examProcedures = new ArrayList<ExamProcedure>();
    private Integer size = 0;

    public ScheduleDailyCard(String priorityInfoValue, String priorityInfoLabel, DateTime referenceDate, ExamProcedure examProcedure, DynamicSlotGroup dynamicSlotGroup) {
        this.priorityInfoLabel = priorityInfoLabel;
        this.priorityInfoValue = priorityInfoValue;
        this.referenceDate = referenceDate.toDate();
        this.dayOfCalendar = CalendarDay.getByDayIdx(referenceDate.getDayOfWeek());
        this.addExamProcedure(examProcedure);
        this.addDynamicSlotGroup(dynamicSlotGroup);
        this.size = dynamicSlotGroup.getSize();
    }

    public ScheduleDailyCard(String priorityInfoValue, String priorityInfoLabel, DateTime referenceDate, List<ExamProcedure> examProcedures, List<DynamicSlotGroup> dynamicSlotGroups) {
        this.priorityInfoLabel = priorityInfoLabel;
        this.priorityInfoValue = priorityInfoValue;
        this.referenceDate = referenceDate.toDate();
        this.dayOfCalendar = CalendarDay.getByDayIdx(referenceDate.getDayOfWeek());
        this.examProcedures = examProcedures;
        this.slotsOfDay = dynamicSlotGroups;
        for (DynamicSlotGroup d : dynamicSlotGroups) {
            size += d.getSize();
        }
    }

    public String getPriorityInfoValue() {
        return priorityInfoValue;
    }

    public String getPriorityInfoLabel() {
        return priorityInfoLabel;
    }

    public CalendarDay getDayOfCalendar() {
        return dayOfCalendar;
    }

    public Date getReferenceDate() {
        return referenceDate;
    }

    public List<ExamProcedure> getExamProcedures() {
        return Collections.unmodifiableList(examProcedures);
    }

    public Integer getSize() {
        return size;
    }

    public List<DynamicSlotGroup> getSlotsOfDay() {
        Collections.sort(slotsOfDay);
        return Collections.unmodifiableList(slotsOfDay);
    }

    public void addExamProcedure(ExamProcedure examProcedure) {
        examProcedures.add(examProcedure);
    }

    public void addDynamicSlotGroup(DynamicSlotGroup dynamicSlotGroup) {
        slotsOfDay.add(dynamicSlotGroup);
        this.size += dynamicSlotGroup.getSize();
    }

}

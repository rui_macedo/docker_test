package models.searchalg;

import com.fasterxml.jackson.annotation.JsonIgnore;
import models.*;
import org.fest.util.Collections;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.List;

/**
 * Putting this class as deprecated case we will not have a Dynamic slot anymore.
 * <p>
 * Our slot will be generated and stored in Slot table in DB
 */
@Deprecated
public class DynamicSlot implements Comparable<DynamicSlot> {
    public int startAt;
    public int duration;
    public int endAt;
    public SchedulingBook book;
    public List<Resource> resources;
    public ExamProcedure examProcedure;
    @JsonIgnore
    public Interval interval;
    public ScheduledProcedure scheduledProcedure;
    private Boolean freeSlot;

    public DynamicSlot(SchedulingBookPeriod period, Interval slot, ExamProcedure examProcedure) {
        this.interval = slot;
        this.setBounds(interval);
        this.resources = period.getResources();
        book = period.schedulingBookWeekAgenda.schedulingBook;
        this.examProcedure = examProcedure;
        freeSlot = true;
    }

    public DynamicSlot(ScheduledProcedureSchedulingBook scheduledProcedureSchedulingBook) {
        freeSlot = false;
        scheduledProcedure = scheduledProcedureSchedulingBook.scheduledProcedure;
        examProcedure = scheduledProcedure.examProcedure;
        interval = scheduledProcedureSchedulingBook.getZeroEpochInterval();
        this.setBounds(interval);
        book = scheduledProcedureSchedulingBook.schedulingBook;
        resources = Resource.findByScheduledProcedure(scheduledProcedure);
    }

    public DynamicSlot(ScheduledProcedureResource scheduledProcedureResource) {
        freeSlot = false;
        scheduledProcedure = scheduledProcedureResource.scheduledProcedure;
        examProcedure = scheduledProcedure.examProcedure;
        interval = scheduledProcedureResource.getZeroEpochInterval();
        this.setBounds(interval);
        book = scheduledProcedureResource.schedulingBook;
        resources = Collections.list(scheduledProcedureResource.resource);
    }

    public static List<DynamicSlot> convertList(List<Interval> availablePeriodSlots,
                                                SchedulingBookPeriod period, ExamProcedure examProcedure) {
        List<DynamicSlot> converted = new ArrayList<DynamicSlot>();
        for (Interval slot : availablePeriodSlots) {
            converted.add(new DynamicSlot(period, slot, examProcedure));
        }
        return converted;
    }

    private void setBounds(Interval interval) {
        this.startAt = (int) interval.getStartMillis();
        this.endAt = (int) interval.getEndMillis();
        this.duration = (int) interval.toDurationMillis();
    }

    public Interval getInterval() {
        return interval;
    }

    @Override
    public int compareTo(DynamicSlot o) {
        int diff = this.startAt - o.startAt;
        if (diff == 0) {
            diff = this.endAt - o.endAt;
        }
        return diff;
    }

    public Boolean isFreeSlot() {
        return freeSlot;
    }
}

package models.searchalg;

import models.searchalg.analyzers.DynamicSlotGroupAnalyzer;
import models.searchalg.dayiterator.DayIteratorStrategy;
import models.searchalg.finders.DynamicSlotGroupFinderStrategy;
import models.searchalg.validators.DynamicSlotGroupValidator;

public class AlgorithmConfiguration {


    private DynamicSlotGroupAnalyzer dynamicSlotGroupAnalyzer;
    private DynamicSlotGroupValidator dynamicSlotGroupValidator;
    private DynamicSlotGroupFinderStrategy finderStrategy;
    private DayIteratorStrategy dayIteratorStrategy;

    private CardInfoType cardInfoType;


    public AlgorithmConfiguration(DynamicSlotGroupAnalyzer dynamicSlotGroupAnalyzer,
                                  DynamicSlotGroupValidator dynamicSlotGroupValidator, DynamicSlotGroupFinderStrategy finderStrategy, CardInfoType cardInfoType
            , DayIteratorStrategy dayIteratorStrategy) {
        this.dynamicSlotGroupAnalyzer = dynamicSlotGroupAnalyzer;
        this.dynamicSlotGroupValidator = dynamicSlotGroupValidator;
        this.finderStrategy = finderStrategy;
        this.cardInfoType = cardInfoType;
        this.dayIteratorStrategy = dayIteratorStrategy;
    }

    public DynamicSlotGroupAnalyzer getDynamicSlotGroupAnalyzer() {
        return dynamicSlotGroupAnalyzer;
    }

    public DynamicSlotGroupValidator getDynamicSlotGroupValidator() {
        return dynamicSlotGroupValidator;
    }

    public CardInfoType getCardInfoType() {
        return cardInfoType;
    }

    public DynamicSlotGroupFinderStrategy getFinderStrategy() {
        return finderStrategy;
    }

    public DayIteratorStrategy getDayIteratorStrategy() {
        return dayIteratorStrategy;
    }

    public enum CardInfoType {
        LIST_OF_DAY("ProcedureByDay", "1"),
        SCHEDULED_SLOTS("ScheduledSlots", "1"),
        SET_OF_DATE("SlotSetByDay", "1");

        private String priorityInfoLabel;
        private String priorityInfoValue;

        private CardInfoType(String priorityInfoLabel, String priorityInfoValue) {
            this.priorityInfoLabel = priorityInfoLabel;
            this.priorityInfoValue = priorityInfoValue;
        }

        public String getPriorityInfoLabel() {
            return this.priorityInfoLabel;
        }

        public String getPriorityInfoValue() {
            return this.priorityInfoValue;
        }
    }

}

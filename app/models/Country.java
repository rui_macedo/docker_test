package models;

import common.utils.FormattingUtils;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;

@Entity
@Table(name = "Country", schema = "global")
public class Country extends BaseSimpleModel implements PathBindable<Country>,
        QueryStringBindable<Country> {

    /*
     * Attributes
     */
    private static final long serialVersionUID = 2360601169161204157L;

    private static Model.Finder<Integer, Country> finder = new Model.Finder<Integer, Country>(Integer.class,
            Country.class);

    @Id
    @Column(name = "IDCountry")
    public Integer id;

    @Column(name = "OriginalId", nullable = true)
    public Long originalId;

    @Column(name = "Name", nullable = true, length = 50)
    public String name;

    @Column(name = "ShortName", nullable = true, length = 30)
    public String shortName;

    /*
     * Constructors
     */
    public Country(Long originalId, String name, String shortName) {
        this.originalId = originalId;
        this.name = name;
        this.shortName = shortName;
    }

    Country() {
        super();
    }

  /*
   * Abstract methods
   */


  /*
   * Override methods
   */

    /*
     * Repository methods
     */
    public static Country findById(Integer id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    @Override
    public Option<Country> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("country")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Country obj = findById(new Integer(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Country bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Integer(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }
  /*
   * Get/setters/modifiers
   */

    @Override
    public Integer getId() {
        return id;
    }
}

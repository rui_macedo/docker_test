package models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import common.annotations.BaseTenantModelFormatter;
import common.utils.FormattingUtils;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;
import utils.JsonFloatSerializer;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "ProcedureTableExamProcedure", schema = "examprocedure")
public class ProcedureTableExamProcedure extends BaseTenantModel implements
        PathBindable<ProcedureTableExamProcedure>, QueryStringBindable<ProcedureTableExamProcedure> {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, ProcedureTableExamProcedure> finder =
            new Model.Finder<Long, ProcedureTableExamProcedure>(Long.class,
                    ProcedureTableExamProcedure.class);
    @Id
    @Column(name = "IDProcedureTableExamProcedure")
    public Long id;
    /* Procedure table owner */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDProcedureTable", nullable = true)
    @BaseTenantModelFormatter(clazz = ProcedureTable.class, idFieldIsString = false,
            idFieldName = "id")
    public ProcedureTable procedureTable;
    /* Reference to the Exam Procedure */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDExamProcedure", nullable = true)
    @BaseTenantModelFormatter(clazz = ExamProcedure.class, idFieldIsString = false,
            idFieldName = "id")
    public ExamProcedure examProcedure;
    @Column(name = "Code", length = 30, nullable = false)
    public String code;
    @Column(name = "Mnemonic", length = 10)
    public String mnemonic;
    /* Reference to the Indexer for "Honorários Médicos" */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDIndexerCH", nullable = true)
    @BaseTenantModelFormatter(clazz = Indexer.class, idFieldIsString = false,
            idFieldName = "id")
    public Indexer indexerCH;
    /* Reference to the Indexer for "Custo Operacional" */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDIndexerCO", nullable = true)
    @BaseTenantModelFormatter(clazz = Indexer.class, idFieldIsString = false,
            idFieldName = "id")
    public Indexer indexerCO;
    /* Reference to the Indexer for "Filme" */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDIndexerFIL", nullable = true)
    @BaseTenantModelFormatter(clazz = Indexer.class, idFieldIsString = false,
            idFieldName = "id")
    public Indexer indexerFIL;
    /* Reference to the Indexer for "Materiais" */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDIndexerMAT", nullable = true)
    @BaseTenantModelFormatter(clazz = Indexer.class, idFieldIsString = false,
            idFieldName = "id")
    public Indexer indexerMAT;
    /* Reference to the Indexer for "Outros" */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDIndexerOTR", nullable = true)
    @BaseTenantModelFormatter(clazz = Indexer.class, idFieldIsString = false,
            idFieldName = "id")
    public Indexer indexerOTR;
    /* Reference to the Indexer for "Anestesista" (Porte anestesico) */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDIndexerANT", nullable = true)
    @BaseTenantModelFormatter(clazz = Indexer.class, idFieldIsString = false,
            idFieldName = "id")
    public Indexer indexerANT;
    @JsonSerialize(using = JsonFloatSerializer.class)
    @Column(name = "QtdeCH", columnDefinition = "float default 0")
    private Float qtdeCH = 0F;
    @JsonSerialize(using = JsonFloatSerializer.class)
    @Column(name = "QtdeCO", columnDefinition = "float default 0")
    private Float qtdeCO = 0F;
    @JsonSerialize(using = JsonFloatSerializer.class)
    @Column(name = "QtdeFIL", columnDefinition = "float default 0")
    private Float qtdeFIL = 0F;
    @JsonSerialize(using = JsonFloatSerializer.class)
    @Column(name = "QtdeMAT", columnDefinition = "float default 0")
    private Float qtdeMAT = 0F;
    @JsonSerialize(using = JsonFloatSerializer.class)
    @Column(name = "QtdeOTR", columnDefinition = "float default 0")
    private Float qtdeOTR = 0F;
    @Column(name = "QtdeAUX", columnDefinition = "smallint default 0")
    private Byte qtdeAUX = 0;
    @JsonSerialize(using = JsonFloatSerializer.class)
    @Column(name = "QtdeANT", columnDefinition = "float default 0")
    private Float qtdeANT = 0F;

    /* Constructor */
    public ProcedureTableExamProcedure(Institution institution, ProcedureTable procedureTable,
                                       ExamProcedure examProcedure) {
        super(institution);
        this.procedureTable = procedureTable;
        this.examProcedure = examProcedure;
    }

    public static ProcedureTableExamProcedure findById(Long id) {
        return finder.byId(id);
    }

    public static List<ProcedureTableExamProcedure> findAllByInstitution(Long idInstitution, ProcedureTable procedureTable) {
        return finder.where()
                .eq("idInstitution", idInstitution)
                .eq("idProcedureTable", procedureTable.id)
                .findList();
    }

    public Float getQtdeCH() {
        return FormattingUtils.formatFloatNumber(qtdeCH, 4);
    }

    public void setQtdeCH(Float qtdeCH) {
        this.qtdeCH = FormattingUtils.formatFloatNumber(qtdeCH, 4);
    }

    public Float getQtdeCO() {
        return FormattingUtils.formatFloatNumber(qtdeCO, 4);
    }

    public void setQtdeCO(Float qtdeCO) {
        this.qtdeCO = FormattingUtils.formatFloatNumber(qtdeCO, 4);
    }

    public Float getQtdeFIL() {
        return FormattingUtils.formatFloatNumber(qtdeFIL, 4);
    }

    public void setQtdeFIL(Float qtdeFIL) {
        this.qtdeFIL = FormattingUtils.formatFloatNumber(qtdeFIL, 4);
    }

    public Float getQtdeMAT() {
        return FormattingUtils.formatFloatNumber(qtdeMAT, 4);
    }

    public void setQtdeMAT(Float qtdeMAT) {
        this.qtdeMAT = FormattingUtils.formatFloatNumber(qtdeMAT, 4);
    }

    public Float getQtdeOTR() {
        return FormattingUtils.formatFloatNumber(qtdeOTR, 4);
    }

    public void setQtdeOTR(Float qtdeOTR) {
        this.qtdeOTR = FormattingUtils.formatFloatNumber(qtdeOTR, 4);
    }

    public Byte getQtdeAUX() {
        if (this.qtdeAUX == null) {
            this.qtdeAUX = 0;
        }
        return qtdeAUX;
    }

    public void setQtdeAUX(Byte qtdeAUX) {
        if (qtdeAUX == null) {
            qtdeAUX = 0;
        }
        this.qtdeAUX = qtdeAUX;
    }

    public Float getQtdeANT() {
        return FormattingUtils.formatFloatNumber(qtdeANT, 4);
    }

    public void setQtdeANT(Float qtdeANT) {
        this.qtdeANT = FormattingUtils.formatFloatNumber(qtdeANT, 4);
    }

    @Override
    public Number getId() {
        return this.id;
    }

    @Override
    public Option<ProcedureTableExamProcedure> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("proceduretableexamprocedure")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            ProcedureTableExamProcedure obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public ProcedureTableExamProcedure bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }


}

package models;

import common.annotations.BaseSimpleModelFormatter;
import common.utils.FormattingUtils;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "City", schema = "global")
public class City extends BaseSimpleModel implements PathBindable<City>, QueryStringBindable<City> {

    /*
     * Attributes
     */
    private static final long serialVersionUID = 2438128608381850707L;

    private static Model.Finder<Long, City> finder = new Model.Finder<Long, City>(Long.class,
            City.class);

    @Id
    @Column(name = "IDCity")
    public Long id;

    @Column(name = "OriginalId", nullable = true)
    public Long originalId;

    @Column(name = "Name", nullable = true, length = 60)
    public String name;

    @Column(name = "ShortName", nullable = true, length = 30)
    public String shortName;

    @ManyToOne
    @JoinColumn(name = "IDState", nullable = false)
    @BaseSimpleModelFormatter(clazz = State.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public State state;

    /*
     * Constructors
     */
    public City(Long originalId, String name, State state) {
        this.originalId = originalId;
        this.name = name;
        this.state = state;
    }

    City() {
        super();
    }

  /*
   * Abstract methods
   */


  /*
   * Override methods
   */

    /*
     * Repository methods
     */
    public static City findById(Long id) {
        if (id != null) {
            return finder.byId(id);
        } else {
            return null;
        }
    }

    public static List<City> findAllByState(State state) {
        return finder.where().eq("state", state).findList();
    }

    public static List<City> findByStateAndNameLike(State state, String cityName) {
        return finder.where().eq("state", state).ilike("name", "%" + cityName + "%").findList();
    }

    @Override
    public Option<City> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("city")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            City obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public City bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

  /*
   * Get/setters/modifiers
   */

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public Long getId() {
        return id;
    }
}

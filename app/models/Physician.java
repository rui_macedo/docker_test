package models;

import com.avaje.ebean.Expr;
import com.fasterxml.jackson.annotation.JsonIgnore;
import common.annotations.BaseSimpleModelFormatter;
import common.utils.FormattingUtils;
import dto.PhysicianDTO;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import play.data.validation.Constraints.MaxLength;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.libs.F.Option;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;
import utils.CRMNormalizer;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "Physician", schema = "global")
public class Physician extends BaseSimpleModel implements PathBindable<Physician>,
        QueryStringBindable<Physician> {
    /**
     * Global Physician information. All physicians here can be a Solicitor
     */
    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long, Physician> finder = new Model.Finder<Long, Physician>(
            Long.class, Physician.class);
    @Id
    @Column(name = "IDPhysician")
    public Long id;
    @Column(name = "Name", nullable = false, length = 100)
    @Required
    public String name;
    /* Medical Council */
    @ManyToOne
    @JoinColumn(name = "IDMedicalCouncil")
    @BaseSimpleModelFormatter(clazz = MedicalCouncil.class, idFieldIsString = false,
            idFieldName = "id")
    @Required
    public MedicalCouncil medicalCouncil;
    @ManyToOne
    @JoinColumn(name = "MedicalCouncilState")
    @BaseSimpleModelFormatter(clazz = State.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public State medicalCouncilState;
    @Column(name = "IdentificationRG", length = 20)
    @Required
    public String identificationRG;
    @Column(name = "IdentificationCPF", length = 20)
    public String identificationCPF;
    @Column(name = "DateOfBirth")
    public DateTime dateOfBirth;
    @ManyToOne
    @JoinColumn(name = "IDAddress")
    @BaseSimpleModelFormatter(clazz = Address.class, idFieldIsString = false, idFieldName = "id")
    @Required
    public Address address;
    @Column(name = "AddressNumber", length = 6)
    @Required
    public String addressNumber;
    @Column(name = "AddressComplement", length = 30)
    public String addressComplement;
    @Column(name = "phoneNumber", length = 30)
    public String phoneNumber;
    @Column(name = "faxNumber", length = 30)
    public String faxNumber;
    @Column(name = "email", length = 100)
    public String email;
    @Column(name = "UF")
    @MaxLength(value = 5)
    public String uf;
    @OneToMany(mappedBy = "physician", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<PhysicianEmployee> physicianEmployees = new HashSet<PhysicianEmployee>();
    @Column(name = "MedicalCouncilRegistration", nullable = false, length = 20)
    @Required
    private String medicalCouncilRegistration;


    /* Constructor */
    public Physician(String name, MedicalCouncil medicalCouncil, String medicalCouncilRegistration,
                     State medicalCouncilState) {
        super();
        this.name = name;
        this.medicalCouncil = medicalCouncil;
        setMedicalCouncilRegistration(medicalCouncilRegistration);
        this.medicalCouncilState = medicalCouncilState;
    }

    public static Physician findById(Long id) {
        if (id == null) {
            return null;
        }
        return finder.byId(id);
    }

    public static List<Physician> findAllByName(String search) {
        if (search == null) {
            search = new String();
        }
        search = search.trim().replace("%", "");

        if (search == null || search.isEmpty()) {
            return finder.findList();
        } else {
            return finder.where().ilike("name", "%" + search + "%").findList();
        }
    }

    public static List<Physician> findAllByMedicalCouncilRegistration(MedicalCouncil medicalCouncil,
                                                                      String medicalCouncilRegistration) {
        return finder.where().eq("IDMedicalCouncil", medicalCouncil.getId().toString())
                .eq("medicalCouncilRegistration", normalize(medicalCouncilRegistration)).findList();
    }

    public static Physician findByMedicalCouncilRegistrationAndUF(String councilNumber,
                                                                  String councilState) {
        if (StringUtils.isBlank(councilNumber)) {
            return null;
        }
        return finder.where()
                .and(Expr.eq("uf", councilState), Expr.eq("medicalCouncilRegistration", normalize(councilNumber)))
                .findUnique();
    }

    public static List<Physician> all() {
        return finder.all();
    }

    private static String normalize(String councilNumber) {
        return CRMNormalizer.normalize(councilNumber);
    }

    @Override
    public Number getId() {
        return this.id;
    }

    public String getMedicalCouncilRegistration() {
        return medicalCouncilRegistration;
    }

    public void setMedicalCouncilRegistration(String medicalCouncilRegistration) {
        this.medicalCouncilRegistration = normalize(medicalCouncilRegistration);
    }

    @Override
    public Option<Physician> bind(String arg0, Map<String, String[]> data) {
        String pathId = data.get("id")[0];
        if (FormattingUtils.isNumberCompatible(pathId)) {
            Physician obj = findById(new Long(pathId));
            if (obj != null) {
                return Option.Some(obj);
            } else {
                return Option.None();
            }
        } else {
            return Option.None();
        }
    }

    @Override
    public Physician bind(String arg0, String arg1) {
        if (FormattingUtils.isNumberCompatible(arg1)) {
            return findById(new Long(arg1));
        } else {
            return null;
        }
    }

    @Override
    public String javascriptUnbind() {
        return this.getId() != null ? this.getId().toString() : null;
    }

    @Override
    public String unbind(String arg0) {
        return this.getId() != null ? this.getId().toString() : null;
    }

    public PhysicianDTO wrap() {
        return new PhysicianDTO(this.id, this.name);
    }


}

resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

resolvers += "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases"

resolvers += Resolver.url("julienba.github.com", url("http://julienba.github.com/repo/"))(Resolver.ivyStylePatterns)

//addSbtPlugin("org.jba" % "play2-plugins-mustache" % "1.1.3") // play 2.2
// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.3.7")


// Add Jacoco plugin for code coverage
addSbtPlugin("de.johoop" % "jacoco4sbt" % "2.1.6")

import sbt._
import Keys._
import com.typesafe.config._

object ApplicationBuild {
  val conf = ConfigFactory.parseFile(new File("conf/application.conf")).resolve()
  val settings: Seq[Setting[_]] = Seq (
	organization := conf.getString("app.organization"),
	version := conf.getString("app.version"),
  name := conf.getString("app.name")
  )
}
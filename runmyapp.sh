#!/bin/bash

# TODO set as env var
CLOUD_APP_CONFIG_FILE="/system/cloud-app.conf"
CLOUD_APP_DIR="/system/cloud-app"
CLOUD_APP_BIN_DIR="/system/cloud-app/bin"
CLOUD_APP_ARGS="-mem 1200 -Dconfig.file=$CLOUD_APP_CONFIG_FILE -Dlogger.file=/system/logger.xml"
NEW_RELIC_JAR="/system/newrelic.jar"

function exitWithError {
 echo $1 1>&2
 exit 1
}

if [ -f $CLOUD_APP_DIR/RUNNING_PID ] && ! ps -p `cat $CLOUD_APP_DIR/RUNNING_PID`&> /dev/null; then
    echo "Removing invalid running_pid file"
    rm $CLOUD_APP_DIR/RUNNING_PID;
fi


if [ -f "$NEW_RELIC_JAR" ]
then
	CLOUD_APP_ARGS="$CLOUD_APP_ARGS -J-javaagent:$NEW_RELIC_JAR"
fi

if [ ! -d "$CLOUD_APP_BIN_DIR" ]
then
    exitWithError "The \$CLOUD_APP_BIN_DIR ($CLOUD_APP_BIN_DIR) is not a directory. Exiting."
fi

for f in $CLOUD_APP_BIN_DIR/*
do
	if [ ! -d "$f" ] && [ -x "$f" ] # !dir && executable
	then
	    if [ -z ${CLOUD_APP_EXEC_FILE+x} ] # EXEC_FILE not defined
	    then
	        CLOUD_APP_EXEC_FILE=$f
	    else
		 exitWithError "Multiple executable files have been found in $CLOUD_APP_BIN_DIR . Exiting."
            fi
	fi
done

if [ -z ${CLOUD_APP_EXEC_FILE+x} ]
then
    exitWithError "No executable files have been found in $CLOUD_APP_BIN_DIR. Exiting."
else
     exec $CLOUD_APP_EXEC_FILE $CLOUD_APP_ARGS
fi
